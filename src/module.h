
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core
 * \{
 */

#ifndef MIP6D_NG_SRC_MODULE_H
#define MIP6D_NG_SRC_MODULE_H

#include <config.h>
#include <confuse.h>

int module_init(const char * const path);
#ifndef HAVE_LTDL
int module_static_init();
#endif

int module_construct_cfg_opt(cfg_opt_t ** main_opts, unsigned char * opts_len);
void module_free_cfg_opt(cfg_opt_t * main_opts);

int module_call_init(int argc, char * argv[], cfg_t * cfg);

#endif  /* MIP6D_NG_SRC_MODULE_H */

/*! \} */

