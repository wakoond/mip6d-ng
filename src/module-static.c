
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup internal-core
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <mip6d-ng/module.h>
#include <module.h>

#ifdef MODULE_REGISTER_LIST_DECL
MODULE_REGISTER_LIST_DECL
#endif

/*!
 * \brief Loading modules
 *
 * This function is used in the non-dynamic module loading case.
 * It calls the module init function, specified by the MODULE_REGISTER_LIST
 * macro, which is defined by the Makefile
 * Finally it calls, the module_static_init() function
 * \param path Path of loadable modules (unused)
 * \return Zero if OK, otherwise negative
 */
int module_init(const char * const path)
{
	MODULE_REGISTER_LIST

	return module_static_init();
}

/*! \} */
