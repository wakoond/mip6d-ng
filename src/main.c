
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! 
 * \addtogroup internal-core
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <ltdl.h>

#include <fcntl.h>
#include <signal.h>
#include <dirent.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/tasks.h>
#include <mip6d-ng/cfg.h>
#include <mip6d-ng/threading.h>

#include "module.h"
#include "cfg.h"
#include "main.h"

/*!
 * \brief Options of core
 *
 * This structure stores the main options, such as logging options
 */
static struct main_options options = {
	.config  = "etc/mip6d-ng.conf",
	.modules = "etc/mip6d-ng/modules",
	.loglevel = LERR,
	.logfile = NULL,
	.logsize = 1024*1024*1024,
	.exit_hook_id = 0,
	.started_hook_id = 0
};

/*!
 * \brief Print out usage information
 */
static void _usage(char * argv[])
{
	printf("Usage: %s -c <config file> -m <modules dir>\n", argv[0]);
}

/*!
 * \brief Getting core's command line arguments
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argv_p Remaining (unused) command line arguments
 * \return Number of remaining command line arguments if OK, otherwise negative
 */
static int _get_arguments(int argc, char * argv[], char *** r_argv_p)
{
	static const char * const short_opts = "c:m:h";

	static const struct option long_opts[] = {
		{ "config",		required_argument,	NULL,	'c' },
		{ "modules",	required_argument,	NULL,	'm' },
		{ "help",		no_argument,		NULL,	'h' },
		{ NULL,			no_argument,		NULL,	0	}
	};
	int opt;
	int r_argc = 0;
	int ret;

	ret = prepare_get_my_opts(argc, argv, &r_argc, r_argv_p);
	if (ret)
		return ret;

	for (opt = get_my_opts(argc, argv, short_opts, long_opts, NULL, &r_argc, r_argv_p); opt != -1; 
			opt = get_my_opts(argc, argv, short_opts, long_opts, NULL, &r_argc, r_argv_p)) {

		switch (opt) {
		case 'c':
			options.config = strdup(optarg);
			break;
		case 'm':
			options.modules = strdup(optarg);
			break;
		case 'h':
			_usage(argv);
			exit(EXIT_FAILURE);
		};
	}

	return r_argc;
}

/*!
 * \brief Exit (exit()) handler
 */
static void exit_handler()
{
	DEBUG("Exit. Run exit hook");

	hooks_run(options.exit_hook_id, NULL);

	fflush(stderr);
	fflush(stdout);
}

/*!
 * \brief Signal handler for exit handling
 * \param sig Signal
 */
static void signal_handler(int sig)
{
	exit(EXIT_SUCCESS);
}

/*!
 * \brief Main
 *
 * Entry point of mip6d-ng
 * \param argc argc
 * \param argv argv
 */
int main(int argc, char * argv[])
{
	int ret;
	char ** r_argv = NULL;
	int r_argc;
	cfg_t * cfg;

	logger_init(NULL, LERR, 0);
	ret = threading_init();
	if (ret) {
		ERROR("Unable to initialize threading support");
		exit(EXIT_FAILURE);
	}

	ret = _get_arguments(argc, argv, &r_argv);
	if (ret < 0) {
		ERROR("Unable to get command line arguments");
		exit(EXIT_FAILURE);
	}
	r_argc = ret;
	
	DEBUG("Config file:  %s", options.config);
	DEBUG("Modules dir: %s", options.modules);

	ret = module_init(options.modules);
	if (ret) {
		ERROR("Unable to load modules");
		exit(EXIT_FAILURE);
	}

	cfg = config_load(options.config, main_opts, main_opts_size);
	if (cfg == NULL) {
		ERROR("Unable to load configuration file");
		exit(EXIT_FAILURE);
	}
	main_config_load(cfg, &options);

	logger_reinit(options.logfile, options.loglevel, options.logsize);

	ret = imsg_init();
	if (ret) {
		ERROR("Unable to start imessaging");
		exit(EXIT_FAILURE);
	}

	ret = hooks_init();
	if (ret) {
		ERROR("Unable to start hooks");
		exit(EXIT_FAILURE);
	}

	ret = hooks_register("core-exit");
	if (ret < 0) {
		ERROR("Unable to register hook: core-exit");
		exit(EXIT_FAILURE);
	}
	options.exit_hook_id = (unsigned int)ret;

	ret = hooks_register("core-all-inited");
	if (ret < 0) {
		ERROR("Unable to register hook: core-all-inited");
		exit(EXIT_FAILURE);
	}
	options.inited_hook_id = (unsigned int)ret;

	ret = hooks_register("core-all-started");
	if (ret < 0) {
		ERROR("Unable to register hook: core-all-started");
		exit(EXIT_FAILURE);
	}
	options.started_hook_id = (unsigned int)ret;

	(void)signal(SIGINT, signal_handler);
	(void)signal(SIGQUIT, signal_handler);
	(void)signal(SIGABRT, signal_handler);
	(void)atexit(exit_handler);

	ret = tasks_init();
	if (ret) {
		ERROR("Unable to start tasks");
		exit(EXIT_FAILURE);
	}	

	ret = module_call_init(r_argc, r_argv, cfg);
	if (ret) {
		ERROR("Unable to init modules");
		exit(EXIT_FAILURE);
	}

	config_release(cfg);

	ret = hooks_run(options.inited_hook_id, NULL);
	if (ret) {
		ERROR("Unable to run post-init (start) hooks");
		exit(EXIT_FAILURE);
	}

	ret = hooks_run(options.started_hook_id, NULL);
	if (ret) {
		ERROR("Unable to run post-start hooks");
		exit(EXIT_FAILURE);
	}

	for (;;)
		sleep(10);
}

/*! \} */

