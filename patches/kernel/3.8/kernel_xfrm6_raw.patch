diff --git a/include/net/xfrm.h b/include/net/xfrm.h
index 63445ed..aebf716 100644
--- a/include/net/xfrm.h
+++ b/include/net/xfrm.h
@@ -35,6 +35,7 @@
 #define XFRM_PROTO_IPV6		41
 #define XFRM_PROTO_ROUTING	IPPROTO_ROUTING
 #define XFRM_PROTO_DSTOPTS	IPPROTO_DSTOPTS
+#define XFRM_PROTO_RAW		255
 
 #define XFRM_ALIGN4(len)	(((len) + 3) & ~3)
 #define XFRM_ALIGN8(len)	(((len) + 7) & ~7)
@@ -1497,6 +1498,7 @@ extern int xfrm6_input_addr(struct sk_buff *skb, xfrm_address_t *daddr,
 extern int xfrm6_tunnel_register(struct xfrm6_tunnel *handler, unsigned short family);
 extern int xfrm6_tunnel_deregister(struct xfrm6_tunnel *handler, unsigned short family);
 extern __be32 xfrm6_tunnel_alloc_spi(struct net *net, xfrm_address_t *saddr);
+extern void xfrm6_tunnel_free_spi(struct net *net, xfrm_address_t *saddr);
 extern __be32 xfrm6_tunnel_spi_lookup(struct net *net, const xfrm_address_t *saddr);
 extern int xfrm6_extract_output(struct xfrm_state *x, struct sk_buff *skb);
 extern int xfrm6_prepare_output(struct xfrm_state *x, struct sk_buff *skb);
diff --git a/net/ipv6/Kconfig b/net/ipv6/Kconfig
index 4f7fe72..4cf1a85 100644
--- a/net/ipv6/Kconfig
+++ b/net/ipv6/Kconfig
@@ -113,6 +113,17 @@ config IPV6_MIP6
 
 	  If unsure, say N.
 
+config INET6_XFRM_RAW
+	tristate "IPv6: XFRM raw IP6 tunnel"
+	depends on EXPERIMENTAL
+	select INET6_XFRM_TUNNEL
+	select INET6_TUNNEL
+	default n
+	---help---
+	  Support for an XFRM based IP6 tunnel.
+
+	  If unsure, say N.
+
 config INET6_XFRM_TUNNEL
 	tristate
 	select INET6_TUNNEL
diff --git a/net/ipv6/Makefile b/net/ipv6/Makefile
index 4ea2448..60972ed 100644
--- a/net/ipv6/Makefile
+++ b/net/ipv6/Makefile
@@ -27,6 +27,7 @@ ipv6-objs += $(ipv6-y)
 obj-$(CONFIG_INET6_AH) += ah6.o
 obj-$(CONFIG_INET6_ESP) += esp6.o
 obj-$(CONFIG_INET6_IPCOMP) += ipcomp6.o
+obj-$(CONFIG_INET6_XFRM_RAW) += xfrm6_raw.o
 obj-$(CONFIG_INET6_XFRM_TUNNEL) += xfrm6_tunnel.o
 obj-$(CONFIG_INET6_TUNNEL) += tunnel6.o
 obj-$(CONFIG_INET6_XFRM_MODE_TRANSPORT) += xfrm6_mode_transport.o
diff --git a/net/ipv6/xfrm6_raw.c b/net/ipv6/xfrm6_raw.c
new file mode 100644
index 0000000..20babff
--- /dev/null
+++ b/net/ipv6/xfrm6_raw.c
@@ -0,0 +1,115 @@
+/*
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program; if not, write to the Free Software
+ * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
+ */
+
+/* Kernel module for raw xfrm tunneling */
+
+
+#include <linux/module.h>
+#include <linux/in.h>
+#include <net/ip.h>
+#include <net/xfrm.h>
+#include <net/icmp.h>
+#include <net/ipv6.h>
+#include <net/protocol.h>
+#include <linux/ipv6.h>
+#include <linux/icmpv6.h>
+
+
+static int xfrm6_raw_init_state(struct xfrm_state *x)
+{
+	int err = 0;
+	struct net *net = xs_net(x);
+
+	if (x->props.mode != XFRM_MODE_TUNNEL)
+		goto error;
+
+	if (x->encap)
+		goto error;
+
+	x->id.spi = xfrm6_tunnel_alloc_spi(net, (xfrm_address_t *)&x->props.saddr);
+	if (!x->id.spi)
+		goto error;
+
+	x->id.proto = IPPROTO_IPV6;
+	x->props.header_len = sizeof(struct ipv6hdr);
+
+out:
+	return err;
+
+error:
+	err = -EINVAL;
+	goto out;
+}
+
+static void xfrm6_raw_destroy(struct xfrm_state *x)
+{
+	struct net *net = xs_net(x);
+
+	xfrm6_tunnel_free_spi(net, (xfrm_address_t *)&x->props.saddr);
+}
+
+static int xfrm6_raw_input(struct xfrm_state *x, struct sk_buff *skb)
+{
+	return skb_network_header(skb)[IP6CB(skb)->nhoff];
+}
+
+static int xfrm6_raw_output(struct xfrm_state *x, struct sk_buff *skb)
+{
+	*skb_mac_header(skb) = IPPROTO_IPV6;
+	skb_push(skb, -skb_network_offset(skb));
+
+	return 0;
+}
+
+static int xfrm6_raw_1st_header(struct xfrm_state *x, struct sk_buff *skb, u8 **nexthdr)
+{
+	return 0;
+}
+
+static const struct xfrm_type xfrm6_raw_type =
+{
+	.description	= "xfrm6_raw_module",
+	.owner		= THIS_MODULE,
+	.proto		= IPPROTO_RAW,
+	.init_state	= xfrm6_raw_init_state,
+	.destructor	= xfrm6_raw_destroy,
+	.input		= xfrm6_raw_input,
+	.output		= xfrm6_raw_output,
+	.hdr_offset	= xfrm6_raw_1st_header,
+};
+
+static int __init xfrm6_raw_init(void)
+{
+	if (xfrm_register_type(&xfrm6_raw_type, AF_INET6) < 0) {
+		printk(KERN_INFO "xfrm6_raw_init: can't add xfrm type\n");
+		return -EAGAIN;
+	}
+
+	return 0;
+}
+
+static void __exit xfrm6_raw_fini(void)
+{
+	if (xfrm_unregister_type(&xfrm6_raw_type, AF_INET6) < 0)
+		printk(KERN_INFO "xfrm6_raw_fini: can't remove xfrm type\n");
+}
+
+module_init(xfrm6_raw_init);
+module_exit(xfrm6_raw_fini);
+MODULE_LICENSE("GPL");
+MODULE_DESCRIPTION("XFRM raw tunneling");
+MODULE_AUTHOR("Emmanuel THIERRY <emmanuel.thierry@telecom-bretagne.eu>");
+MODULE_ALIAS_XFRM_TYPE(AF_INET6, XFRM_PROTO_RAW);
diff --git a/net/ipv6/xfrm6_tunnel.c b/net/ipv6/xfrm6_tunnel.c
index ee5a706..b197e5b 100644
--- a/net/ipv6/xfrm6_tunnel.c
+++ b/net/ipv6/xfrm6_tunnel.c
@@ -199,7 +199,7 @@ static void x6spi_destroy_rcu(struct rcu_head *head)
 			container_of(head, struct xfrm6_tunnel_spi, rcu_head));
 }
 
-static void xfrm6_tunnel_free_spi(struct net *net, xfrm_address_t *saddr)
+void xfrm6_tunnel_free_spi(struct net *net, xfrm_address_t *saddr)
 {
 	struct xfrm6_tunnel_net *xfrm6_tn = xfrm6_tunnel_pernet(net);
 	struct xfrm6_tunnel_spi *x6spi;
@@ -222,6 +222,7 @@ static void xfrm6_tunnel_free_spi(struct net *net, xfrm_address_t *saddr)
 	}
 	spin_unlock_bh(&xfrm6_tunnel_spi_lock);
 }
+EXPORT_SYMBOL(xfrm6_tunnel_free_spi);
 
 static int xfrm6_tunnel_output(struct xfrm_state *x, struct sk_buff *skb)
 {
diff --git a/net/xfrm/xfrm_user.c b/net/xfrm/xfrm_user.c
index eb872b2..d502611 100644
--- a/net/xfrm/xfrm_user.c
+++ b/net/xfrm/xfrm_user.c
@@ -227,6 +227,17 @@ static int verify_newsa_info(struct xfrm_usersa_info *p,
 		    !attrs[XFRMA_COADDR])
 			goto out;
 		break;
+	case IPPROTO_IPV6:
+		if (attrs[XFRMA_ALG_COMP]	||
+		    attrs[XFRMA_ALG_AUTH]	||
+		    attrs[XFRMA_ALG_AUTH_TRUNC]	||
+		    attrs[XFRMA_ALG_AEAD]	||
+		    attrs[XFRMA_ALG_CRYPT]	||
+		    attrs[XFRMA_ENCAP]		||
+		    attrs[XFRMA_SEC_CTX]	||
+		    attrs[XFRMA_COADDR])
+			goto out;
+		break;
 #endif
 
 	default:
@@ -552,6 +563,9 @@ static struct xfrm_state *xfrm_state_construct(struct net *net,
 			goto error;
 	}
 
+	if (x->id.proto == IPPROTO_IPV6)
+		x->id.proto = IPPROTO_RAW;
+
 	xfrm_mark_get(attrs, &x->mark);
 
 	err = __xfrm_init_state(x, false);
