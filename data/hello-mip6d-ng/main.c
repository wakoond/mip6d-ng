

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include "hello.h"

static struct imsg_opts imsg_opts = {
	.event_fp = NULL,
	.message_fp = NULL,
};

static int hello_exit_handler(void * arg)
{
	DEBUG("Hello mip6d-ng module exit....");

	return 0;
}

int hello_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;
	int msg_id_me;

	DEBUG("Initializing module: Hello mip6d-ng");

	ret = imsg_register(MSG_HELLO, &imsg_opts);
    if (ret < 0)
		return ret;
	msg_id_me = ret;
	DEBUG("My messaging ID is: %d", msg_id_me);

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_HELLO, hello_exit_handler);
	}

	return 0;
}

MIP6_MODULE_INIT(MSG_HELLO, SEQ_HELLO, hello_module_init);

