
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-mcoa
 * \{
 */

#ifndef MIP6D_NG_CCOM_MHO_BID_H_
#define MIP6D_NG_CCOM_MHO_BID_H_

/*!
 * \brief Binding Identifier Mobility Option without Care-of Address
 */
struct ip6_mh_opt_bid {
	/*! Type */
	uint8_t		ip6mob_type;
	/*! Length */
	uint8_t		ip6mob_len;
	/*! Binding Identifier */
	uint16_t    ip6mob_bid;
	/*! Status */
	uint8_t     ip6mob_status;
	/*! Flags */
	uint8_t     ip6mob_flags;
} __attribute__ ((packed));

/*!
 * \brief Binding Identifier Mobility Option with IPv6 Care-of Address
 */
struct ip6_mh_opt_bid_coa {
	/*! Type */
	uint8_t		ip6mob_type;
	/*! Length */
	uint8_t		ip6mob_len;
	/*! Binding Identifier */
	uint16_t    ip6mob_bid;
	/*! Status */
	uint8_t     ip6mob_status;
	/*! Flags */
	uint8_t     ip6mob_flags;
	/*! Care-of Address */
	struct in6_addr	ip6mob_coa;
} __attribute__ ((packed));

/*!
 *  \brief Binding Identifier MHO type
 */
#define IP6_MHOPT_BID		0x23

#endif /* MIP6D_NG_CCOM_MHO_BID_H_ */

 /*! \} */
