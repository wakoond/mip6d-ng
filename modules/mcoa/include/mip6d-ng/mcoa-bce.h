
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-mcoa
 * \{
 */

#ifndef MIP6D_NG_MCOA_BCE_H
#define MIP6D_NG_MCOA_BCE_H

#include <inttypes.h>
#include <mip6d-ng/bce.h>

/*!
 * \brief BCE extension
 */
struct bce_ext_bid {
	/*! Extension header */
	struct bce_ext ext;
	/*! BID value */
	uint16_t bid;
	/*! Preference value */
	uint8_t preference;
};

/*! 
 * \brief Argument of the bid_parse hook
 *
 * It could to be used by the flowb module to parse the bid-priority value
 */
struct hook_bid_parse_arg {
	/*! BID option */
	struct ip6_mh_opt * opt;
	/*! BCE */
	struct bce * bce;
	/*! BCE extension: BID */
	struct bce_ext_bid * ext_bid;
};

#endif /* MIP6D_NG_MCOA_BCE_H */

/*! \} */
