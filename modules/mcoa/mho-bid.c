
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mho-bid.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/bce.h>

#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>

#include "mho-bid.h"
#include "main.h"

/*!
 * \brief Create Binding Identifier Mobility Header Option
 *
 * It adds BID option to the MH with the given address. If coa is not NULL, 
 * it will be aded to the BID option. 
 * \param iov Message iov
 * \param bid Binding Identifier
 * \param status Status
 * \param flags Flags
 * \param coa Care-of Address (it could to be NULLto skip)
 * \return Zero if OK
 */
int mho_bid_create(struct iovec * iov, uint16_t bid, uint8_t status, uint8_t flags, struct in6_addr * coa)
{
	struct ip6_mh_opt_bid * opt;
	struct ip6_mh_opt_bid_coa * optc;
	size_t optlen;
	
	if (coa == NULL)
		optlen = sizeof(struct ip6_mh_opt_bid);
	else
		optlen = sizeof(struct ip6_mh_opt_bid_coa);

	iov->iov_base = malloc(optlen);
	iov->iov_len = optlen;

	if (iov->iov_base == NULL)
		return -1;

	if (coa == NULL) {
		opt = (struct ip6_mh_opt_bid *)iov->iov_base;

		opt->ip6mob_type = IP6_MHOPT_BID;
		opt->ip6mob_len = 4;
		opt->ip6mob_bid = htons(bid);
		opt->ip6mob_status = status;
		opt->ip6mob_flags = flags;
	} else {
		optc = (struct ip6_mh_opt_bid_coa *)iov->iov_base;

		optc->ip6mob_type = IP6_MHOPT_BID;
		optc->ip6mob_len = 20;
		optc->ip6mob_bid = htons(bid);
		optc->ip6mob_status = status;
		optc->ip6mob_flags = flags;

		memcpy(&optc->ip6mob_coa, coa, sizeof(optc->ip6mob_coa));
	}

	hooks_run(mcoa_opts.bid_modify_hook_id, iov);

	return 0;
}

/*!
 * \brief Parsing Binding Identifier Mobility Header Option
 *
 * It could be parse Binding Update or Binding Acknowledge option. The private argument
 * should to be a Binding Cache entry or a Binding Update List entry. 
 * Currently the option length selects the mobility message. Length 4 means the BA message, and
 * length 20 means the BU message. Currently it does not have any effect in case of receiving BA,
 * now the priv argument could to be NULL in this case. 
 * When receiving a BU message, it updates the BC entrie's coa filed with the given one, and 
 * adds the BID extension to the BCE.
 * It executes the bid_parse hook to let the other modules (flowb) to parse the BID option.
 * 
 * \param mh_type MH type, it should to be BU
 * \param opt MH option, it should to be struct ip6_mh_opt_altcoa
 * \param optslen MH option len
 * \param priv Extra argument, it should to be struct bce
 * \return Zero if OK
 */
static int mho_bid_recv(uint8_t mh_type, struct ip6_mh_opt * opt, size_t optslen, void * priv)
{
	struct ip6_mh_opt_bid * bidopt;
	struct ip6_mh_opt_bid_coa * bidoptc;
	struct bce * bce = (struct bce *)priv;
	struct bce_ext_bid * ext_bid = NULL;
	struct hook_bid_parse_arg hooka;

	if (opt == NULL || bce == NULL || optslen < sizeof(struct ip6_mh_opt_bid) || (opt->ip6mhopt_len != 4 && opt->ip6mhopt_len != 20))
		return -1;

	if (opt->ip6mhopt_len == 4) {
		bidopt = (struct ip6_mh_opt_bid *)opt;
		
		INFO("Found BID option: %u. Status %u", ntohs(bidopt->ip6mob_bid), bidopt->ip6mob_status);
	} else if (opt->ip6mhopt_len == 20) {
		bidoptc = (struct ip6_mh_opt_bid_coa *)opt;

		INFO("Found BID option: %u@" IP6ADDR_FMT, ntohs(bidoptc->ip6mob_bid), IP6ADDR_TO_STR(&bidoptc->ip6mob_coa));
		memcpy(&bce->coa, &bidoptc->ip6mob_coa, sizeof(bce->coa));

		ext_bid = malloc(sizeof(struct bce_ext_bid));
		if (ext_bid == NULL) {
			ERROR("Out of memory: Unable to alloc new BCE ext BID");
			return -1;
		}

		strncpy(ext_bid->ext.name, "bid", sizeof(ext_bid->ext.name));
		ext_bid->bid = ntohs(bidoptc->ip6mob_bid);
		ext_bid->preference = 255 - ext_bid->bid;

		list_add_tail(&ext_bid->ext.list, &bce->ext);

		bce->mark = ext_bid->bid;
	}

	hooka.opt = opt;
	hooka.bce = bce;
	hooka.ext_bid = ext_bid;
	hooks_run(mcoa_opts.bid_parse_hook_id, &hooka);

	return 0;
}

/*!
 * \brief register Binding Identifier MH Option
 * \return Zero if OK
 */
int mho_bid_init()
{
	struct msg_ccom_reg_mh_opt opt;

	opt.type = IP6_MHOPT_BID;
	opt.align_n = 8;
	opt.align = 2;		/* 8n+2 */
	opt.fn = mho_bid_recv;

	return IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ccom, MSG_CCOM_REG_MH_OPT, &opt, sizeof(opt), NULL, 0);
}

/*! \} */
