
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#ifndef _BID_H_
#define _BID_H_

#include <mip6d-ng/core-comm.h>

/*!
 * \brief Interfaces with their properties in the MCoA module
 */
struct mcoa_interface {
	/*! Linked list management */
	struct list_head list;
	/*! Interface index */
	int ifindex;
	/*! BID value */
	uint16_t bid;
	/*! Preference value */
	uint8_t preference;
	/*! In non-zero, the interface is alive (up and running) */
	unsigned char alive;
};

int mcoa_set_preference(int ifindex, uint8_t preference);
int mcoa_get_interface_num();
uint8_t mcoa_get_preference(uint16_t bid);

int mcoa_create_bule_rr(struct bule * bule);
int mcoa_clean_bule_rr(struct bule * bule, unsigned char use_prev);

int mcoa_init_mn();
int mcoa_init_ha();
int mcoa_iterate_interfaces(int (* fn)(struct mcoa_interface *, void *), void * arg);
int mcoa_interface_alive(uint16_t bid, unsigned char alive);
struct mcoa_interface * mcoa_interface_get_best();

int mcoa_init_bule(struct bule * bule);
int mcoa_extend_bu(void * param);
int mcoa_extend_ba(void * param);
int mcoa_bce_match(void * param);

int mcoa_bule_api_get(unsigned char * reply_msg, unsigned int mlen);
int mcoa_bce_api_get(unsigned char * reply_msg, unsigned int mlen);

#endif /* _BID_H_ */

/*! \} */
