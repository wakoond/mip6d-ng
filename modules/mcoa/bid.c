
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/md.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>

#include <libnetlink.h>

#include "bid.h"
#include "main.h"
#include "default.h"
#include "mho-bid.h"

/*!
 * \brief List of interfaces 
 */
static LIST_HEAD(mcoa_interfaces);

/*!
 * \brief Lock of interface list
 */
static pthread_mutex_t mcoa_interfaces_lock = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Set the pereference value of a given interface
 *
 * Interface is identified by its interface index
 * \param ifindex Interface index
 * \param preference Preference value
 * \return Zero if OK
 */
int mcoa_set_preference(int ifindex, uint8_t preference)
{
	int retval = -1;
	struct list_head * pos;
	struct mcoa_interface * mif;

	pthread_mutex_lock(&mcoa_interfaces_lock);

	list_for_each(pos, &mcoa_interfaces) {
		mif = list_entry(pos, struct mcoa_interface, list);

		if (mif->ifindex == ifindex) {
			mif->preference = preference;
			DEBUG("Set MCoA %d interface preference to %u", ifindex, preference);
			retval = 0;
			break;
		}
	}

	pthread_mutex_unlock(&mcoa_interfaces_lock);

	return retval;
}

/*!
 * \brief Get the preference value of a given interface
 *
 * Interface is identified by its Binding Identifier
 * \param bid Binding Identifier
 * \return Preference value or negative
 */
uint8_t mcoa_get_preference(uint16_t bid)
{
	int retval = -1;
	struct list_head * pos;
	struct mcoa_interface * mif;

	pthread_mutex_lock(&mcoa_interfaces_lock);

	list_for_each(pos, &mcoa_interfaces) {
		mif = list_entry(pos, struct mcoa_interface, list);

		if (mif->bid == bid) {
			retval = mif->preference;
			break;
		}
	}

	pthread_mutex_unlock(&mcoa_interfaces_lock);

	return retval;
}

/*!
 * \brief Get the number of interfaces
 * \return Number of interfaces
 */
int mcoa_get_interface_num()
{
	int num = 0;
	struct list_head * pos;

	pthread_mutex_lock(&mcoa_interfaces_lock);

	list_for_each(pos, &mcoa_interfaces) {
		num++;
	}

	pthread_mutex_unlock(&mcoa_interfaces_lock);

	return num;
}

/*!
 * \brief Initialize Mobile Node
 *
 * It gets the list of interfaces from the MD module, and calculate their BID and preference values.
 * The first interface will get BID value 1, the second BID value 2, etc.
 * The first interface will get the highest preference value, 254, the second 253, etc.
 *
 * \return Zero if OK
 */
int mcoa_init_mn() 
{
	int ret;
	int interfaces[255];
	int ilen = sizeof(interfaces) / sizeof(int);
	int i;
	struct mcoa_interface * mif;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.md, MSG_MD_GET_INTERFACES,
			&ilen, sizeof(ilen), interfaces, sizeof(interfaces));
	if (ret <= 0) {
		ERROR("Unable to get the list of interfaces from MD module");
		return -1;
	}

	for (i = 0; i < ret; i++) {
		mif = malloc(sizeof(struct mcoa_interface));
		if (mif == NULL) {
			ERROR("Out of memory: Unable to allocate default interface entry");
			return -1;
		}
		memset(mif, 0, sizeof(struct mcoa_interface));

		mif->ifindex = interfaces[i];
		mif->preference = 255 - (i+1);
		mif->bid = i+1;

		pthread_mutex_lock(&mcoa_interfaces_lock);
		list_add_tail(&mif->list, &mcoa_interfaces);
		pthread_mutex_unlock(&mcoa_interfaces_lock);

		DEBUG("MCoA interface %d: bid %u preference %u", mif->ifindex, mif->bid, mif->preference);
	}
	
	return 0;
}

/*!
 * \brief Initialize Home Agent
 *
 * Currently it does nothing
 *
 * \return Zero
 */
int mcoa_init_ha() 
{

	return 0;
}

/*!
 * \brief Get BID of the given interface
 *
 * \param ifindex Interface index
 * \return BID value or zero
 */
static uint16_t get_bid(int ifindex) 
{
	struct list_head * pos;
	struct mcoa_interface * mif;
	uint16_t v = 0;

	pthread_mutex_lock(&mcoa_interfaces_lock);

	list_for_each(pos, &mcoa_interfaces) {
		mif = list_entry(pos, struct mcoa_interface, list);

		if (mif->ifindex == ifindex) {
			v = mif->bid;
			break;
		}
	}

	if (v)
		goto out;

out:

	pthread_mutex_unlock(&mcoa_interfaces_lock);

	return v;
}

/*!
 * \brief Iterate over the interfaces
 *
 * It calls the fn callback for all of the interfaces. Non-zero return value of fn
 * breaks the loop.
 *
 * \param fn Callback
 * \param arg Extra argument for fn
 * \return Return value of the last fn
 */
int mcoa_iterate_interfaces(int (* fn)(struct mcoa_interface *, void *), void * arg)
{
	struct list_head * pos;
	struct mcoa_interface * mif;
	int ret = 0;

	if (fn == NULL)
		return 0;

	pthread_mutex_lock(&mcoa_interfaces_lock);

	list_for_each(pos, &mcoa_interfaces) {
		mif = list_entry(pos, struct mcoa_interface, list);

		ret = fn(mif, arg);
		if (ret)
			break;
	}

	pthread_mutex_unlock(&mcoa_interfaces_lock);

	return ret;
}

/*!
 * \brief Set the alive flags of an interface
 *
 * \param bid Binding Identifier
 * \param alive If non-zero interface is up & running
 * \return Zero if OK
 */
int mcoa_interface_alive(uint16_t bid, unsigned char alive)
{
	struct list_head * pos;
	struct mcoa_interface * mif;

	pthread_mutex_lock(&mcoa_interfaces_lock);

	list_for_each(pos, &mcoa_interfaces) {
		mif = list_entry(pos, struct mcoa_interface, list);

		if (mif->bid == bid) {
			mif->alive = alive;
			break;
		}
	}

	pthread_mutex_unlock(&mcoa_interfaces_lock);

	return 0;
}

/*!
 * \brief Find the best interface
 *
 * The lowest preference value means the highest priority.
 * The interface with the lowest preference value, which is alive
 * will be selected.
 *
 * \return Interface or NULL
 */
struct mcoa_interface * mcoa_interface_get_best()
{
	struct list_head * pos;
	struct mcoa_interface * mif, *best = NULL;
	unsigned char preference = 255;

	pthread_mutex_lock(&mcoa_interfaces_lock);

	list_for_each(pos, &mcoa_interfaces) {
		mif = list_entry(pos, struct mcoa_interface, list);
		if (! mif->alive)
			continue;

		if (mif->preference < preference) {
			preference = mif->preference;
			best = mif;
		}
	}

	pthread_mutex_unlock(&mcoa_interfaces_lock);

	return best;
}

/*!
 * \brief Create source routing stuff for the given BULE
 *
 * It creates a rule which: src Coa, dst HA, lookup in table (100 + bid).
 * And creates a default route in table (100 + bid) which: dst HA, gw defaultroute, dev ifindex.
 * It signs the successful creation in the BID extension of the BULE with BULE_BID_F_RR_CREATED.
 * \param bule BULE
 * \return Zero if OK.
 */
int mcoa_create_bule_rr(struct bule * bule)
{
	int ret;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct msg_env_rule rule;
	struct msg_env_route route;

	if (bule == NULL)
		return -1;

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		return -1;
	}

	memset(&rule, 0, sizeof(rule));
	memcpy(&rule.dst, &bule->dest, sizeof(struct in6_addr));
	rule.dplen = 128;
	memcpy(&rule.src, &bule->coa, sizeof(struct in6_addr));
	rule.splen = 128;
	rule.entries |= MSG_ENV_RULE_SRC;
	rule.table = 100 + bid;
	rule.action = RTN_UNICAST;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_RULE,
			&rule, sizeof(rule), NULL, 0);
	if (ret) {
		ERROR("Unable to create MCoA source routing rule");
		return -1;
	}

	memset(&route, 0, sizeof(route));	
	memcpy(&route.dst, &bule->dest, sizeof(struct in6_addr));
	route.dplen = 128;
	memcpy(&route.gw, &bule->default_route, sizeof(struct in6_addr));
	route.entries |= MSG_ENV_ROUTE_GW;
	route.oiface = bule->ifindex;
	route.table = 100 + bid;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_ROUTE,
			&route, sizeof(route), NULL, 0);
	if (ret) {
		ERROR("Unable to create MCoA source routing route");
		return -1;
	}

	ext_bid->flags |= BULE_BID_F_RR_CREATED;

	return 0;
}

/*!
 * \brief It deletes the source routing stuff, created by mcoa_create_bule_rr
 *
 * \param bule BULE
 * \param use_prev If non-zero uses prev_coa, prev_default_route, etc.. instead of coa, default_route, etc.
 * \return Zero if OK
 */
int mcoa_clean_bule_rr(struct bule * bule, unsigned char use_prev)
{
	int ret;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct msg_env_rule rule;
	struct msg_env_route route;

	if (bule == NULL)
		return -1;

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		return -1;
	}

	if ((ext_bid->flags & BULE_BID_F_RR_CREATED) == 0)
		return 0;

	memset(&rule, 0, sizeof(rule));
	memcpy(&rule.dst, &bule->dest, sizeof(struct in6_addr));
	rule.dplen = 128;
	if (use_prev)
		memcpy(&rule.src, &bule->prev_coa, sizeof(struct in6_addr));
	else
		memcpy(&rule.src, &bule->coa, sizeof(struct in6_addr));
	rule.splen = 128;
	rule.entries |= MSG_ENV_RULE_SRC;
	rule.table = 100 + bid;
	rule.action = RTN_UNICAST;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_RULE_DEL,
			&rule, sizeof(rule), NULL, 0);
	if (ret) {
		ERROR("Unable to delete MCoA source routing rule");
		return -1;
	}

	memset(&route, 0, sizeof(route));	
	memcpy(&route.dst, &bule->dest, sizeof(struct in6_addr));
	route.dplen = 128;
	if (use_prev)
		memcpy(&route.gw, &bule->prev_default_route, sizeof(struct in6_addr));
	else
		memcpy(&route.gw, &bule->default_route, sizeof(struct in6_addr));
	route.entries |= MSG_ENV_ROUTE_GW;
	route.oiface = bule->ifindex;
	route.table = 100 + bid;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.env, MSG_ENV_ROUTE_DEL,
			&route, sizeof(route), NULL, 0);
	if (ret) {
		ERROR("Unable to delete MCoA source routing route");
		return -1;
	}

	ext_bid->flags &= ~(BULE_BID_F_RR_CREATED);

	return 0;
}

/*!
 * \brief BULE cleanup function
 *
 * Deletes the source routing stuff
 *
 * \param arg struct bule
 * \return Zero if OK
 */
static int mcoa_clean_bule(void * arg)
{
	struct bule * bule = (struct bule *)arg;

	if (bule == NULL)
		return 0;

	bule_lock(bule);

	mcoa_clean_bule_rr(bule, 0);

	bule_unlock(bule);
	refcnt_put(&bule->refcnt);

	return 0;
}

/*!
 * \brief Initialize BULE
 *
 * It allocates and add the BID extension to the BULE
 *
 * \param bule BULE
 * \return Zero if OK
 */
int mcoa_init_bule(struct bule * bule)
{
	struct bule_ext_bid * ext_bid;
	uint16_t bid;

	if (bule == NULL)
		return 1;

	bid = get_bid(bule->ifindex);
	if (bid == 0) {
		ERROR("Unable to generate new bule for interface %d", bule->ifindex);
		return -1;
	}
		
	ext_bid = malloc(sizeof(struct bule_ext_bid));
	if (ext_bid == NULL) {
		ERROR("Out of memory: Unable to alloc new BULE ext BID");
		return -1;
	}
	memset(ext_bid, 0, sizeof(struct bule_ext_bid));

	strncpy(ext_bid->ext.name, "bid", sizeof(ext_bid->ext.name));
	ext_bid->bid = bid;

	list_add_tail(&ext_bid->ext.list, &bule->ext);

	bule->mark = bid;

	refcnt_get(&bule->refcnt);
	hooks_add(bule->del_hook_id, 1, default_update_by_bule_delete);
	refcnt_get(&bule->refcnt);
	hooks_add(bule->del_hook_id, 2, mcoa_clean_bule);

	return 0;
}

/*!
 * \brief Extend the Binding Update message with BID option
 *
 * It gets the corresponding BID and adds to the BU message
 *
 * \param param struct send_hook_bu_param 
 * \return Zero if OK
 */
int mcoa_extend_bu(void * param)
{
	int ret = -1;
	struct send_hook_bu_param * hookp = (struct send_hook_bu_param *)param;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;

	if (hookp == NULL || hookp->iov == NULL || hookp->iov_count+1 >= hookp->iov_len || hookp->bule == NULL)
		return -1;

	refcnt_get(&hookp->bule->refcnt);
	bule_lock(hookp->bule);

	list_for_each(pos, &hookp->bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		goto out;
	}

	DEBUG("Extend BU with BID option: %u....", bid);

	ret = mho_bid_create(&(hookp->iov[hookp->iov_count++]), bid, 0, 0, &hookp->bule->coa);
	if (ret) {
		ERROR("Unable to add BID option");
		goto out;
	}

out:
	
	bule_unlock(hookp->bule);
	refcnt_put(&hookp->bule->refcnt);

	return ret;
}

/*!
 * \brief Extend the Binding Acknowledge message with BID option
 *
 * It gets the corresponding BID and adds to the BA message
 *
 * \param param struct send_hook_ba_param
 * \return Zero if OK
 */
int mcoa_extend_ba(void * param)
{
	int ret;
	struct send_hook_ba_param * hookp = (struct send_hook_ba_param *)param;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;

	if (hookp == NULL || hookp->iov == NULL || hookp->iov_count+1 >= hookp->iov_len || hookp->bce == NULL) 
		return -1;

	list_for_each(pos, &hookp->bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BCE");
		return -1;
	}

	DEBUG("Extend BA with BID option: %u....", bid);

	ret = mho_bid_create(&(hookp->iov[hookp->iov_count++]), bid, 0, 0, NULL);
	if (ret) {
		ERROR("Unable to add BID option");
		return -1;
	}

	return ret;
}

/*!
 * \brief BCE matching
 *
 * It selects the matching BCE from the list.
 * It selects which BID value is fit to the given one
 *
 * \param param struct bce_ext_match
 * \return Zero if match
 */
int mcoa_bce_match(void * param)
{
	struct bce_ext_match * em = (struct bce_ext_match *)param;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	uint16_t new_bid = 0;

	if (em == NULL || em->bce == NULL || em->new_bce == NULL)
		return -1;

	list_for_each(pos, &em->bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BCE");
		return -1;
	}

	list_for_each(pos, &em->new_bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			new_bid = ext_bid->bid;
			break;
		}
	}

	if (new_bid == 0) {
		ERROR("Unable to find BID extension in BCE");
		return -1;
	}

	DEBUG("BCE ext match: BID %u new BID %u", bid, new_bid);

	return (new_bid == bid) ? 0 : 1;
}

/*!
 * \brief Argument of __mcoa_bule_api_get
 */
struct mcoa_bule_api_get_arg {
	/*! API BULE array */
	struct mcoa_api_get_bul * bul;
	/*! Length of API BULE array */
	unsigned int bnum;
};

/*!
 * \brief Iterate BUL to produce reply to API
 *
 * It is a callback function for the MSG_DATA_ITERATE_BUL command.
 * It gets the BULE and the BID extension, and add all of them to the 
 * array in the argument.
 *
 * \param bule BULE
 * \param arg struct mcoa_bule_api_get_arg
 * \return Zero if OK
 */
int __mcoa_bule_api_get(struct bule * bule, void * arg)
{
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct mcoa_bule_api_get_arg * apia = (struct mcoa_bule_api_get_arg *)arg;

	if (bule == NULL || apia == NULL)
		return 0;

	apia->bul = realloc(apia->bul, (apia->bnum + 1) * sizeof(struct mcoa_api_get_bul));
	if (apia->bul == NULL) {
		ERROR("Out of memory: Unable to alloc get_bul command answer");
		return -1;
	}

	memcpy(&apia->bul[apia->bnum].dest, &bule->dest, sizeof(struct in6_addr));
	memcpy(&apia->bul[apia->bnum].hoa, &bule->hoa, sizeof(struct in6_addr));
	memcpy(&apia->bul[apia->bnum].coa, &bule->coa, sizeof(struct in6_addr));
	apia->bul[apia->bnum].lifetime = htons(bule->lifetime);
	apia->bul[apia->bnum].lifetime_rem = htons(bule->lifetime_rem);
	apia->bul[apia->bnum].last_bu = htonl((int32_t)bule->last_bu);
	apia->bul[apia->bnum].flags = htonl(bule->flags);
	apia->bul[apia->bnum].ifindex = htonl((uint32_t)bule->ifindex);

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}
	apia->bul[apia->bnum].bid = htons(bid);
	apia->bul[apia->bnum].prio = mcoa_get_preference(bid);

	apia->bnum++;

	return 0;
}

/*!
 * \brief API function to get Binding Update List entries
 * \param reply_msg Reply message to construct
 * \param mlen Maximum length of reply_msg
 * \return Length of reply_message if OK, otherwise negative
 */
int mcoa_bule_api_get(unsigned char * reply_msg, unsigned int mlen)
{
	int ret;
	struct msg_data_iterate_bul buli;
	struct mcoa_bule_api_get_arg apia;

	memset(&apia, 0, sizeof(apia));
	memset(&buli, 0, sizeof(buli));
	buli.fp = __mcoa_bule_api_get;
	buli.arg = &apia;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.data, MSG_DATA_ITERATE_BUL,
			&buli, sizeof(buli), NULL, 0);
	if (ret < 0) {
		ERROR("Unable to iterate BUL");
		return ret;
	}

	if (apia.bnum == 0)
		return 0;

	if (reply_msg != NULL && mlen > 0) {
		if (mlen > apia.bnum * sizeof(struct mcoa_api_get_bul))
			mlen = apia.bnum * sizeof(struct mcoa_api_get_bul);
		memcpy(reply_msg, (unsigned char *)apia.bul, mlen);

		return apia.bnum * sizeof(struct mcoa_api_get_bul);
	}
	
	return -1;
}

/*!
 * \brief Argument of __mcoa_bce_api_get
 */
struct mcoa_bce_api_get_arg {
	struct mcoa_api_get_bc * bc;
	unsigned int bnum;
};

/*!
 * \brief Iterate BC to produce reply to API
 *
 * It is a callback function for the MSG_DATA_ITERATE_BC command.
 * It gets the BCE and the BID extension, and add all of them to the 
 * array in the argument.
 *
 * \param bce BCE
 * \param arg struct mcoa_bce_api_get_arg
 * \return Zero if OK
 */
int __mcoa_bce_api_get(struct bce * bce, void * arg)
{
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct mcoa_bce_api_get_arg * apia = (struct mcoa_bce_api_get_arg *)arg;

	if (bce == NULL || apia == NULL)
		return 0;

	apia->bc = realloc(apia->bc, (apia->bnum + 1) * sizeof(struct mcoa_api_get_bc));
	if (apia->bc == NULL) {
		ERROR("Out of memory: Unable to alloc get_bc command answer");
		return -1;
	}

	memcpy(&apia->bc[apia->bnum].hoa, &bce->hoa, sizeof(struct in6_addr));
	memcpy(&apia->bc[apia->bnum].coa, &bce->coa, sizeof(struct in6_addr));
	apia->bc[apia->bnum].lifetime = htons(bce->lifetime);
	apia->bc[apia->bnum].flags = htonl(bce->flags);

	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}
	apia->bc[apia->bnum].bid = htons(bid);
	apia->bc[apia->bnum].prio = ext_bid->preference;

	apia->bnum++;

	return 0;
}

/*!
 * \brief API function to get Binding Cache entries
 * \param reply_msg Reply message to construct
 * \param mlen Maximum length of reply_msg
 * \return Length of reply_message if OK, otherwise negative
 */
int mcoa_bce_api_get(unsigned char * reply_msg, unsigned int mlen)
{
	int ret;
	struct msg_data_iterate_bc bci;
	struct mcoa_bce_api_get_arg apia;

	memset(&apia, 0, sizeof(apia));
	memset(&bci, 0, sizeof(bci));
	bci.fp = __mcoa_bce_api_get;
	bci.arg = &apia;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.data, MSG_DATA_ITERATE_BC,
			&bci, sizeof(bci), NULL, 0);
	if (ret < 0) {
		ERROR("Unable to iterate BC");
		return ret;
	}

	if (apia.bnum == 0)
		return 0;

	if (reply_msg != NULL && mlen > 0) {
		if (mlen > apia.bnum * sizeof(struct mcoa_api_get_bc))
			mlen = apia.bnum * sizeof(struct mcoa_api_get_bc);
		memcpy(reply_msg, (unsigned char *)apia.bc, mlen);

		return apia.bnum * sizeof(struct mcoa_api_get_bc);
	}
	
	return -1;
}

/*! \} */
