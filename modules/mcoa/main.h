/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#ifndef MIP6D_NG_MCOA_MAIN_H_
#define MIP6D_NG_MCOA_MAIN_H_

#include <mip6d-ng/mcoa.h>

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
struct mcoa_opts {
	/*! Messaging IDs */
	struct {
		/*! MCoA module's own ID */
		int me;
		/*! Messaging ID of md module */
		int md;
		/*! Messaging ID of control module */
		int ctrl;
		/*! Messaging ID of data module */
		int data;
		/*! Messaging ID of environment module */
		int env;
		/*! Messaging ID of core-comm module */
		int ccom;
		/*! Messaging ID of api module */
		int api;
	} msg_ids;
	/*! BID modify hook */
	int bid_modify_hook_id;
	/*! BID option parsing hook */
	int bid_parse_hook_id;
};

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
extern struct mcoa_opts mcoa_opts;

#endif /* MIP6D_NG_MCOA_H_ */

/*! \} */

