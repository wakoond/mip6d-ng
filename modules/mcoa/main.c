
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-mcoa
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/md.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/environment.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/api.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>

#include "bid.h"
#include "mho-bid.h"
#include "default.h"
#include "main.h"

struct mcoa_opts mcoa_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the mcoa module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int mcoa_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int retval = -1;
	struct msg_mcoa_set_preference * sp;
	uint16_t * bid_p;
	struct imsg_arg_reply_len * reply_len;
	unsigned char * msg;
	size_t mlen;

	switch (message) {
	case MSG_MCOA_SET_PREFERENCE:
		sp = (struct msg_mcoa_set_preference *)arg;
		if (sp == NULL)
			break;
		retval = mcoa_set_preference(sp->ifindex, sp->preference);
		break;
	case MSG_MCOA_GET_NUM:
		retval = mcoa_get_interface_num();
		break;
	case MSG_MCOA_GET_PREFERENCE:
		bid_p = (uint16_t *)arg;
		if (bid_p == NULL)
			break;
		retval = mcoa_get_preference(*bid_p);
	case MSG_MCOA_API_GET_BUL:
		reply_len = (struct imsg_arg_reply_len *)arg;
		if (reply_len == NULL)
			break;
		mlen = reply_len->ralen;
		msg = (unsigned char *)reply_arg;
		if (msg != NULL) {
			retval = mcoa_bule_api_get(msg, mlen);
		}
		break;
	case MSG_MCOA_API_GET_BC:
		reply_len = (struct imsg_arg_reply_len *)arg;
		if (reply_len == NULL)
			break;
		mlen = reply_len->ralen;
		msg = (unsigned char *)reply_arg;
		if (msg != NULL) {
			retval = mcoa_bce_api_get(msg, mlen);
		}
		break;

	}

	return retval;
}

/*!
 * \brief Event processing
 *
 * For details, please read the API documentation of the mcoa module
 * \param sender Sender module ID
 * \param event Event ID
 * \param arg Event argument
 * \param need_to_free Non-zero if arg should to be freed
 */
static void mcoa_proc_event(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free)
{
	struct bule * bule;
	struct bce * bce;
	struct evt_ccom_ba * ba;

	if (sender == mcoa_opts.msg_ids.data) {
		switch (event) {
		case EVT_DATA_INIT_BULE:
			bule = (struct bule *)arg;
			if (bule == NULL)
				break;
			bule_lock(bule);
			mcoa_init_bule(bule);
			bule_unlock(bule);
			refcnt_put(&bule->refcnt);
			break;
		case EVT_DATA_CHANGE_BULE:
			bule = (struct bule *)arg;
			if (bule == NULL)
				break;
			bule_lock(bule);
			default_update_by_bule_change(bule);
			mcoa_clean_bule_rr(bule, 1);
			bule_unlock(bule);
			refcnt_put(&bule->refcnt);
			break;
		case EVT_DATA_NEW_BCE:
			bce = (struct bce *)arg;
			if (bce == NULL)
				break;
			bce_lock(bce);
			default_init_bce(bce);
			bce_unlock(bce);
			refcnt_put(&bce->refcnt);
			break;
		}
	} else if (sender == mcoa_opts.msg_ids.ccom) {
		switch (event) {
		case EVT_CCOM_BA:
			ba = (struct evt_ccom_ba *)arg;
			if (ba == NULL)
				break;
			default_update_by_ba(ba);
			if (ba->bule == NULL)
				break;
			bule_lock(ba->bule);
			mcoa_create_bule_rr(ba->bule);
			bule_unlock(ba->bule);
			refcnt_put(&ba->bule->refcnt);

		}
	}

	if (need_to_free)
		free(arg);
}

static struct imsg_opts imsg_opts = {
	.event_fp = mcoa_proc_event,
	.message_fp = mcoa_proc_message
};

/*!
 * \brief Cleanup
 *
 * It destroys the created rules, routes, etc.
 * 
 * \return Zero
 */
static int mcoa_exit_handler(void * arg)
{
	int ret;
	enum ctrl_node_type node_type;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
	}

	switch (node_type) {
	case CTRL_NODE_TYPE_HA:
		ret = default_clean_ha();
		if (ret)
			return ret;
		break;
	case CTRL_NODE_TYPE_MN:
		ret = default_clean_mn();
		if (ret)
			return ret;
		break;
	case CTRL_NODE_TYPE_CN:
		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}

	return 0;
}

/*!
 * \brief Initializing mcoa module
 *
 * Registers exit function to the 'core-exit' hook.
 * Register internal messaging.
 * It creates two hooks: "mcoa-mho-bid-modify" and "mcoa-mho-bid-parse".
 * It subscribes for the necessary events and hooks.
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int mcoa_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;
	unsigned int create_bu_id;
	unsigned int create_ba_id;
	unsigned int match_bce_id;
	enum ctrl_node_type node_type;
	struct msg_api_register_cmd api_rc;

	DEBUG("Initializing module: mcoa");

	ret = imsg_register(MSG_MCOA, &imsg_opts);
    if (ret < 0)
		return ret;
	mcoa_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_CTRL);
	if (ret < 0) {
		ERROR("Unable to find module: control");
		return -1;
	}
	mcoa_opts.msg_ids.ctrl = ret;
	
	ret = imsg_get_id(MSG_DATA);
	if (ret < 0) {
		ERROR("Unable to find module: data");
		return -1;
	}
	mcoa_opts.msg_ids.data = ret;

	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to find module: environment");
		return -1;
	}
	mcoa_opts.msg_ids.env = ret;

	ret = imsg_get_id(MSG_CCOM);
	if (ret < 0) {
		ERROR("Unable to find module: core-comm");
		return -1;
	}
	mcoa_opts.msg_ids.ccom = ret;	

	ret = imsg_get_id(MSG_API);
	if (ret < 0) {
		DEBUG("Unable to find module: API");
		mcoa_opts.msg_ids.api = -1;
	} else {
		mcoa_opts.msg_ids.api = ret;
	}

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_MCOA, mcoa_exit_handler);
	}

	ret = hooks_register("mcoa-mho-bid-modify");
	if (ret < 0) {
		ERROR("Unable to register hook: mcoa-mho-bid-modify");
		return -1;
	}
	mcoa_opts.bid_modify_hook_id = ret;

	ret = hooks_register("mcoa-mho-bid-parse");
	if (ret < 0) {
		ERROR("Unable to register hook: mcoa-mho-bid-parse");
		return -1;
	}
	mcoa_opts.bid_parse_hook_id = ret;

	ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
		return -1;
	}

	switch (node_type) {
	case CTRL_NODE_TYPE_HA:
		ret = mcoa_init_ha();
		if (ret)
			return ret;

		ret = default_init_ha();
		if (ret)
			return ret;

		ret = hooks_get_id("create-mh-ba");
		if (ret >= 0) {
			create_ba_id = (unsigned int)ret;
			hooks_add(create_ba_id, 1, mcoa_extend_ba);
		} else  {
			ERROR("Unable to register for create-mh-ba hook");
			return -1;
		}
		ret = hooks_get_id("data-bc-ext-match");
		if (ret >= 0) {
			match_bce_id = (unsigned int)ret;
			hooks_add(match_bce_id, 1, mcoa_bce_match);
		} else  {
			ERROR("Unable to register for data-bc-ext-match hook");
			return -1;
		}

		if (mcoa_opts.msg_ids.api >= 0) {
			memset(&api_rc, 0, sizeof(api_rc));
			api_rc.command = API_MCOA_GET_BC;
			snprintf(api_rc.command_name, sizeof(api_rc.command_name), "MCOA_GET_BC");
			api_rc.imsg_cmd = MSG_MCOA_API_GET_BC;
			ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
			if (ret) {
				ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
			}
		}
		break;
	case CTRL_NODE_TYPE_MN:
		ret = imsg_get_id(MSG_MD);
		if (ret < 0) {
			ERROR("Unable to find module: md-simple");
			return -1;
		}
		mcoa_opts.msg_ids.md = ret;

		ret = mcoa_init_mn();
		if (ret)
			return ret;
		
		ret = default_init_mn();
		if (ret)
			return ret;

		ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.md, MSG_MD_DISABLE_PCOA_CHECK, NULL, 0, NULL, 0);
		if (ret) {
			ERROR("Unable to disable pcoa check in md module");
			return -1;
		}
		ret = imsg_event_subscribe(mcoa_opts.msg_ids.data, EVT_DATA_INIT_BULE, mcoa_opts.msg_ids.me);
		ret |= imsg_event_subscribe(mcoa_opts.msg_ids.data, EVT_DATA_CHANGE_BULE, mcoa_opts.msg_ids.me);
		if (ret < 0) {
			ERROR("Unable to subscribe for BULE events");
			return -1;
		}
		ret = hooks_get_id("create-mh-bu");
		if (ret >= 0) {
			create_bu_id = (unsigned int)ret;
			hooks_add(create_bu_id, 1, mcoa_extend_bu);
		} else  {
			ERROR("Unable to register for create-mh-bu hook");
			return -1;
		}

		if (mcoa_opts.msg_ids.api >= 0) {
			memset(&api_rc, 0, sizeof(api_rc));
			api_rc.command = API_MCOA_GET_BUL;
			snprintf(api_rc.command_name, sizeof(api_rc.command_name), "MCOA_GET_BUL");
			api_rc.imsg_cmd = MSG_MCOA_API_GET_BUL;
			ret = IMSG_MSG_ARGS(mcoa_opts.msg_ids.me, mcoa_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
			if (ret) {
				ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
			}
		}
		break;
	case CTRL_NODE_TYPE_CN:
		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}

	mho_bid_init();

	return 0;
}

MIP6_MODULE_INIT(MSG_MCOA, SEQ_MCOA, mcoa_module_init);

/*! \} */

