
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup internal-data
 * \{
 */

#ifndef MIP6D_NG_BULE_H_
#define MIP6D_NG_BULE_H_

int data_proc_bule_add(unsigned int ifindex, struct in6_addr * coa, struct in6_addr * dst, struct in6_addr * drtr);
int data_proc_bule_update(struct bule * bule, struct in6_addr * coa, struct in6_addr * dst, struct in6_addr * drtr);
int data_proc_bule_del(struct bule * bule, unsigned char bul_lock);
struct bule * data_bule_find(unsigned int ifindex, struct in6_addr * coa, struct in6_addr * dest, struct in6_addr * hoa);
struct bule * data_bule_find_d(unsigned int ifindex, struct in6_addr * coa, struct in6_addr * dest, struct in6_addr * hoa);

int bule_clear_all();

int bule_api_get(unsigned char * reply_msg, unsigned int mlen);
int bule_iterate(int (*fp)(struct bule *, void *), void * arg, unsigned char include_to_delete);

#endif /* MIP6D_NG_BULE_H_ */

/*! \} */
