
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup internal-data
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/tasks.h>

#include <mip6d-ng/md.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/core-comm.h>

#include "bule.h"
#include "mov.h"
#include "main.h"

/*! 
 * \brief Processing movement event
 *
 * It tries to find a corresponding BUL entry, if found, it will be updated,
 * otherwise a new entry will be created
 * \param mov Movement event
 * \return Zero if OK
 */
int data_proc_mov(struct evt_md_movement * mov)
{
	int ret;
	struct in6_addr haa;
	struct in6_addr hoa;
	struct bule * bule;
	uint32_t flags;

	if (mov == NULL)
		return -1;

	ret = IMSG_MSG_ARGS(data_opts.msg_ids.me, data_opts.msg_ids.ctrl, MSG_CTRL_GET_HAA, NULL, 0, &haa, sizeof(haa));
	if (ret) {
		ERROR("Unable to get HAA from ctrl");
		return -1;
	}

	ret = IMSG_MSG_ARGS(data_opts.msg_ids.me, data_opts.msg_ids.ctrl, MSG_CTRL_GET_HOA, NULL, 0, &hoa, sizeof(hoa));
	if (ret) {
		ERROR("Unable to get HOA from ctrl");
		return -1;
	}

	bule = data_bule_find_d(mov->ifindex, NULL, &haa, &hoa);


	//INFO("Handle movement. BULE %s; %s CoA: " IP6ADDR_FMT "/%u via " IP6ADDR_FMT " on %u",
	//			(bule == NULL) ? "not found" : "found",
	//			(mov->action == EVT_MD_MOVEMENT_CONN) ? "New" : (mov->action == EVT_MD_MOVEMENT_UPDATE) ? "Update" : "Del", 
	//				IP6ADDR_TO_STR(&mov->coa), mov->plen, IP6ADDR_TO_STR(&mov->drtr), mov->ifindex);

	if (mov->action == EVT_MD_MOVEMENT_CONN || mov->action == EVT_MD_MOVEMENT_UPDATE) {
		if (bule == NULL) 
			data_proc_bule_add(mov->ifindex, &mov->coa, &haa, &mov->drtr);
		else {
			bule_lock(bule);
			flags = bule->flags;
			bule_unlock(bule);
			if ((flags & BULE_F_TO_DELETE) == BULE_F_TO_DELETE) {
				DEBUG("Found pending lifetime=0 BULE. Delete and create new BULE.");
				bule_lock(bule);
				if (bule->lifet0_send_task != 0) {
					tasks_del(bule->lifet0_send_task);
					bule->lifet0_send_task = 0;
					refcnt_put(&bule->refcnt);
				}
				bule_unlock(bule);
				refcnt_put(&bule->refcnt);
				bule = NULL;
				data_proc_bule_add(mov->ifindex, &mov->coa, &haa, &mov->drtr);
			} else {
				data_proc_bule_update(bule, &mov->coa, &haa, &mov->drtr);
			}
		}
	} else
		data_proc_bule_del(bule, 1);

	if (bule != NULL)
		refcnt_put(&bule->refcnt);

	return 0;
}

/*! \} */
