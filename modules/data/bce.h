
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup internal-data
 * \{
 */

#ifndef MIP6D_NG_BCE_H_
#define MIP6D_NG_BCE_H_

#include <mip6d-ng/bce.h>

int bce_init();
int bce_clean();

int data_bce_init_new(struct bce * bce);
int data_proc_bce(struct bce * bce);
int bce_clear_all();

int bce_api_get(unsigned char * reply_msg, unsigned int mlen);
int bce_iterate(int (*fp)(struct bce *, void *), void * arg);

#endif /* MIP6D_NG_BULE_H_ */

/*! \} */
