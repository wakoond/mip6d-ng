
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
 * \addtogroup iapi-data
 * \{
 */

#ifndef MIP6D_NG_DATA_BULE_H_
#define MIP6D_NG_DATA_BULE_H_

#include <config.h>
#include <inttypes.h>
#include <pthread.h>
#include <netinet/in.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/refcnt.h>
#include <mip6d-ng/logger.h>

/*! BULE flag: signaling environment initialized */
#define BULE_F_INITED			0x0001
/*! BULE flag: BA received */
#define BULE_F_BINDING_DONE		0x0002
/*! BULE flag: data environment initialized */
#define BULE_F_DATA_INITED		0x0004
/*! BULE flag: scheduled resend task running */
#define BULE_F_RESEND			0x0008
/*! BULE flag: to send Lifetime=0-BU */
#define BULE_F_SEND_LIFET_0		0x0010
/*! BULE flag: deleting is in progress */
#define BULE_F_TO_DELETE		0x8000

/*!
 * \brief Binding Update List Entry
 */
struct bule {
	/*! BULE reference count */
	struct refcnt refcnt;
	/*! Linked list management */
	struct list_head list;
	/*! BULE locking */
	pthread_mutex_t _lock;
	/*! The IP address of the node to which a Binding Update was sent */
	struct in6_addr dest;
	/*! The home address for which that Binding Update was sent */
	struct in6_addr hoa;
	/*! The care‐of address sent in that Binding Update */
	struct in6_addr coa;
	/*! The initial value of the Lifetime field sent */
	uint16_t lifetime;
	/*! The remaining lifetime of that binding */
	uint16_t lifetime_rem;
	/*! The maximum value of the Sequence Number field sent in previous Binding Updates to this destination */
	uint16_t seq;
	/*! The time at which a Binding Update was last sent to this destination */
	time_t last_bu;
	/*! Additional state flags */
	uint32_t flags;
	/*! The time at which a HoTI or CoTI message was last sent to this destination */
	time_t last_ti;
	/*! Cookie values used in the HoTI and CoTI messages */
	void * cookies;
	/*! Home and care­‐of keygen tokens received from the CN */
	void * tokens;
	/*! Home and care‐of nonce indices received from the CN */
	void * indicies;
	/*! The time at which each of the tokens and nonces were received from the CN */
	time_t last_tokens;

	/*! Interface index */
	unsigned int ifindex;
	/*! Default route via the interface */
	struct in6_addr default_route;
	/*! Previous CoA (used for updating) */
	struct in6_addr prev_coa;
	/*! Previous default route (used for updating) */
	struct in6_addr prev_default_route;
	/*! Interface index used for sending */
	unsigned int sending_ifindex;
	/*! CoA used for sending (usually == coa) */
	struct in6_addr sending_coa;
	/*! Default route used for sending */
	struct in6_addr sending_default_route;
	/*! (Netfilter) mark */
	unsigned int mark;
	/*! Identifier */
	unsigned long id;
	/*! Delete hook ID */
	int del_hook_id;
	/*! Scheduled resend task */
	unsigned long resend_task;
	/*! Resend interval */
	double resend_interval;
	/*! Task for sending Lifetime=0-BU */
	unsigned long lifet0_send_task;

	/*! Linked list for BULE extension */
	struct list_head ext;
};

#ifdef LOCK_UNLOCK_DEBUG
#define bule_lock(bule)		do { \
		DEBUG("Locking BULE %d at %s:%d...", bule->ifindex, __FUNCTION__, __LINE__); \
		pthread_mutex_lock(&bule->_lock); \
	} while(0)
#define bule_unlock(bule)	do { \
		pthread_mutex_unlock(&bule->_lock); \
		DEBUG("Unlocked BULE %d at %s:%d", bule->ifindex, __FUNCTION__, __LINE__); \
	} while(0)
#else /* LOCK_UNLOCK_DEBUG */
#define bule_lock(bule)		do { \
		pthread_mutex_lock(&bule->_lock); \
	} while(0)
#define bule_unlock(bule)	do { \
		pthread_mutex_unlock(&bule->_lock); \
	} while(0)
#endif /* LOCK_UNLOCK_DEBUG */

#define BULE_EXT_NAME_LEN		20

/*!
 * \brief BULE extension header
 *
 * It should be the first field of an extension
 */
struct bule_ext {
	/*! Linked list for BCE extension */
	struct list_head list;
	/*! Name of the extension */
	char name[BULE_EXT_NAME_LEN];
};

#endif /* MIP6D_NG_DATA_BULE_H_ */

/*! \} */

