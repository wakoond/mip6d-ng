
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-data
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>
#include <pthread.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/md.h>

#include <mip6d-ng/control.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/api.h>

#include "bule.h"
#include "mov.h"
#include "bce.h"
#include "main.h"

struct data_opts data_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the data module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int data_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int retval = -1;
	struct msg_data_find_bule * fbule;
	struct bule * bule;
	struct imsg_arg_reply_len * reply_len;
	unsigned char * msg;
	size_t mlen;
	struct bce * bce;
	struct msg_data_iterate_bul * buli;
	struct msg_data_iterate_bc * bci;

	switch (message) {
	case MSG_DATA_FIND_BULE:
		fbule = (struct msg_data_find_bule *)arg;
		if (fbule == NULL)
			break;
		bule = data_bule_find(fbule->ifindex, &fbule->coa, &fbule->dest, &fbule->hoa);
		if (reply_arg != NULL) {
			*((struct bule **)reply_arg) = bule;
		}
		retval = 0;
		break;
	case MSG_DATA_API_GET_BUL:
		reply_len = (struct imsg_arg_reply_len *)arg;
		if (reply_len == NULL)
			break;
		mlen = reply_len->ralen;
		msg = (unsigned char *)reply_arg;
		if (msg != NULL) {
			retval = bule_api_get(msg, mlen);
		}
		break;
	case MSG_DATA_ITERATE_BUL:
		buli = (struct msg_data_iterate_bul *)arg;
		if (buli == NULL || buli->fp == NULL)
			break;
		retval = bule_iterate(buli->fp, buli->arg, buli->include_to_delete);
		break;
	case MSG_DATA_CLEAR_BUL:
		retval = bule_clear_all();
		break;
	case MSG_DATA_INIT_NEW_BCE:
		bce = (struct bce *)arg;
		if (bce == NULL)
			break;
		retval = data_bce_init_new(bce);
		break;
	case MSG_DATA_API_GET_BC:
		reply_len = (struct imsg_arg_reply_len *)arg;
		if (reply_len == NULL)
			break;
		mlen = reply_len->ralen;
		msg = (unsigned char *)reply_arg;
		if (msg != NULL) {
			retval = bce_api_get(msg, mlen);
		}
		break;
	case MSG_DATA_ITERATE_BC:
		bci = (struct msg_data_iterate_bc *)arg;
		if (bci == NULL || bci->fp == NULL)
			break;
		retval = bce_iterate(bci->fp, bci->arg);
		break;
	case MSG_DATA_CLEAR_BC:
		retval = bce_clear_all();
		break;
	}

	return retval;
}

/*!
 * \brief Event processing
 *
 * For details, please read the API documentation of the data module
 * \param sender Sender module ID
 * \param event Event ID
 * \param arg Event argument
 * \param need_to_free Non-zero if arg should to be freed
 */
static void data_proc_event(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free)
{
	if (sender == data_opts.msg_ids.mds && event == EVT_MD_MOVEMENT) {
		data_proc_mov((struct evt_md_movement *)arg);
	} else if (sender == data_opts.msg_ids.ccom && event == EVT_CCOM_BU) {
		data_proc_bce((struct bce *)arg);
		refcnt_put(&((struct bce *)arg)->refcnt);
	}

	if (need_to_free)
		free(arg);
}

static struct imsg_opts imsg_opts = {
	.event_fp = data_proc_event,
	.message_fp = data_proc_message,
	.event_max = EVT_DATA_NONE
};

/*!
 * \brief Cleanup
 *
 * Delete all BC and BUL entries
 * \return Zero
 */
static int data_exit_handler(void * arg)
{
	DEBUG("Clear module: DATA");

	bule_clear_all();
	bce_clear_all();

	return 0;
}

/*!
 * \brief Initializing data module
 *
 * Registers exit function to the 'core-exit' hook
 * Register internal messaging
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int data_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;
	enum ctrl_node_type node_type;
	struct msg_api_register_cmd api_rc;

	DEBUG("Initializing module: data");

	ret = imsg_register(MSG_DATA, &imsg_opts);
	if (ret < 0)
		return ret;
	data_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_CTRL);
	if (ret < 0) {
		ERROR("Unable to find module: control");
		return -1;
	}
	data_opts.msg_ids.ctrl = ret;
	
	ret = imsg_get_id(MSG_CCOM);
	if (ret < 0) {
		ERROR("Unable to find module: core-comm");
		return -1;
	}
	data_opts.msg_ids.ccom = ret;

	ret = imsg_get_id(MSG_API);
	if (ret < 0) {
		DEBUG("Unable to find module: API");
		data_opts.msg_ids.api = -1;
	} else {
		data_opts.msg_ids.api = ret;
	}

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_DATA, data_exit_handler);
	}

	ret = IMSG_MSG_ARGS(data_opts.msg_ids.me, data_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
		return -1;
	}

	switch (node_type) {
	case CTRL_NODE_TYPE_HA:
		bce_init();

		ret = imsg_event_subscribe(data_opts.msg_ids.ccom, EVT_CCOM_BU, data_opts.msg_ids.me);
		if (ret) {
			ERROR("Unable to subscribe for BU events (%d)", ret);
		}

		if (data_opts.msg_ids.api >= 0) {
			memset(&api_rc, 0, sizeof(api_rc));
			api_rc.command = API_DATA_GET_BC;
			snprintf(api_rc.command_name, sizeof(api_rc.command_name), "DATA_GET_BC");
			api_rc.imsg_cmd = MSG_DATA_API_GET_BC;
			ret = IMSG_MSG_ARGS(data_opts.msg_ids.me, data_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
			if (ret) {
				ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
			}
		}
		break;
	case CTRL_NODE_TYPE_MN:
		ret = imsg_get_id(MSG_MD);
		if (ret < 0) {
			ERROR("Unable to find module: md-simple");
			return -1;
		}
		data_opts.msg_ids.mds = ret;

		ret = imsg_event_subscribe(data_opts.msg_ids.mds, EVT_MD_MOVEMENT, data_opts.msg_ids.me);
		if (ret) {
			ERROR("Unable to subscribe for movement events (%d)", ret);
		}
		if (data_opts.msg_ids.api >= 0) {
			memset(&api_rc, 0, sizeof(api_rc));
			api_rc.command = API_DATA_GET_BUL;
			snprintf(api_rc.command_name, sizeof(api_rc.command_name), "DATA_GET_BUL");
			api_rc.imsg_cmd = MSG_DATA_API_GET_BUL;
			ret = IMSG_MSG_ARGS(data_opts.msg_ids.me, data_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
			if (ret) {
				ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
			}
		}
		break;
	case CTRL_NODE_TYPE_CN:
		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}

	return 0;
}

MIP6_MODULE_INIT(MSG_DATA, SEQ_DATA, data_module_init);

/*! \} */

