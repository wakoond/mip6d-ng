
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-md
 * \{
 */

#ifndef MIP6D_NG_MDS_MAIN_H_
#define MIP6D_NG_MDS_MAIN_H_

#include <net/if.h>
#include <mip6d-ng/list.h>
#include <inttypes.h>
#include <pthread.h>

/*!
 * \brief MD interface representation
 */
struct md_interface {
	/*! Linked list management */
	struct list_head list;
	/*! Interface locking */
	pthread_mutex_t lock;
	/*! Interface name */
	char name[IFNAMSIZ + 1];
	/*! Interface index */
	unsigned int ifindex;
	/*! Flags */
	uint8_t flags;
	/*! IPv6 Address (CoA) */
	struct in6_addr addr;
	/*! IPv6 prefix length */
	unsigned int plen;
	/*! Default gateway */
	struct in6_addr defrt;
	/*! Delayed delete task id */
	unsigned long delete_task_id;
	/*! NUD task id */
	unsigned long nud_task_id;
};

/*! struct mds_interface flags: Interface is down */
#define MD_IFACE_DOWN		0x00
/*! struct mds_interface flags: Interface is up */
#define MD_IFACE_UP			0x01
/*! struct mds_interface flags: Interface got CoA */
#define MD_IFACE_COA		0x02
/*! struct mds_interface flags: Interface has default route */
#define MD_IFACE_DEFRT		0x04
/*! struct mds_interface flags: Neighbor Unreachability Detection (NUD) reported working connection */
#define MD_IFACE_NUD_OK		0x10
/*! Inerface is connected */
#define MD_IFACE_CONN		(MD_IFACE_UP | MD_IFACE_COA | MD_IFACE_DEFRT | MD_IFACE_NUD_OK)

/*!
 * \brief Common MD simple options
 */
struct md_opts {
	/*! IMSG IDs */
	struct {
		/*! Own ID */
		int me;
		/*! Environment module's ID */
		int env;
		/*! Control module's ID */
		int ctrl;
		/*! Core-comm module's ID */
		int ccom;
	} msg_ids;
	/*! Delay before deleting interface */
	unsigned int del_delay;
	/*! Router Solicitation sending interval */
	unsigned int rs_interval;
};

/*!
 * \brief Common MD simple options
 */
extern struct md_opts md_opts;

#endif /* MIP6D_NG_MDS_MAIN_H_ */

/*! \} */
