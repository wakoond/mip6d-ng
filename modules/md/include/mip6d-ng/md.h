
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-md
 * \{
 */

#ifndef MIP6D_NG_MD_H
#define MIP6D_NG_MD_H

#include <inttypes.h>
#include <netinet/in.h>

/*! 
 * The name of the internal messaging reference point
 */
#define MSG_MD				"md"

/*!
 * Module load order number of MD simple module
 */
#define SEQ_MD				6

/*!
 * \brief Available Movement Detection API messages
 */
enum md_msgs {
	/*!
	 * \brief Get the list of the interfaces
	 *
	 * Argument: int (size of reply_arg, number of elements)
	 *  
	 * Reply argument: int array (interface indexes)
	 */
	MSG_MD_GET_INTERFACES,
	/*!
	 * \brief Disable checking of existence of previous interfaces. This feature could enable i.e. MCoA support.
	 * 
	 * In this case EVT_MD_MOVEMENT events will be fired for all of the interfaces
	 * 
	 * Arguments: none
	 * 
	 * Reply arguments: none
	 */
	MSG_MD_DISABLE_PCOA_CHECK,
	MSG_MD_NONE
};

/*!
 * \brief Available Movement Detection API events
 */
enum md_events {
	/*!
	 * \brief Movement event
	 * 
	 * Argument: struct evt_md_movement
	 */
	EVT_MD_MOVEMENT,
	/*! None */
	EVT_MD_NONE
};

/*!
 * Movement event argument
 */
struct evt_md_movement {
	/*! Action: EVT_MD_MOVEMENT_* */
	unsigned char action;
	/*! Interface ID */
	unsigned int ifindex;
	/*! CoA */
	struct in6_addr coa;
	/*! Prefix length */
	uint8_t plen;
	/*! Default route */
	struct in6_addr drtr;
};

/*! Movement: connected */
#define EVT_MD_MOVEMENT_CONN		0x01
/*! Movement: disconnected */
#define EVT_MD_MOVEMENT_DISCONN		0x02
/*! Movement: updated CoA */
#define EVT_MD_MOVEMENT_UPDATE		0x03

#endif /* MIP6D_NG_MD_H */

/*! \} */
