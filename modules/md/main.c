
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-md
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>
#include <confuse.h>

#include <netinet/in.h>

#include <linux/rtnetlink.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/tasks.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/threading.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/core-comm.h>

#include <mip6d-ng/md.h>

#include "main.h"

struct md_opts md_opts;

/*!
 * \brief Configuration options
 *
 * See config sample for details!
 */
cfg_opt_t md_cfg_opts[] = {
	CFG_STR_LIST("interfaces", "{}", CFGF_NONE),
	CFG_INT("md-delete-delay", 5, CFGF_NONE),
	CFG_INT("md-rs-interval", 0, CFGF_NONE),
	CFG_END()
};

/*!
 * \brief List of enabled interfaces
 */
static LIST_HEAD(md_interfaces);

/*!
 * Enable (non zero) or disable (zero) checking of previous
 * connected interfaces. If disabled EVT_MD_MOVEMENT events will be fired for
 * all of the interfaces
 */
static unsigned char pcoa_check_enabled = 1;

/*!
 * \brief Locking md_interfaces
 */
static pthread_mutex_t md_interfaces_lock = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Delayed disconnect task
 * 
 * If the interface does not alive again, this dask will fianlly invalidate it
 *
 * \param arg struct md_interface: The Interface
 */
static void md_delayed_disconn(void * arg)
{
	struct md_interface * iface = (struct md_interface *)arg;
	struct evt_md_movement m;

	pthread_mutex_lock(&md_interfaces_lock);

	INFO("Disappeared CoA, not movement, disconnect @ %u", iface->ifindex);

	m.action = EVT_MD_MOVEMENT_DISCONN;
	m.ifindex = iface->ifindex;
	memset(&m.coa, 0, sizeof(m.coa));
	m.plen = 0;
	memset(&m.drtr, 0, sizeof(m.drtr));
	imsg_send_event(md_opts.msg_ids.me, EVT_MD_MOVEMENT, &m, sizeof(m), 1, NULL);

	iface->delete_task_id = 0;

	pthread_mutex_unlock(&md_interfaces_lock);
}

/*!
 * \brief Handle movement
 *
 * Sending movement event to other modules
 *
 * \param iface Interface with primary CoA
 * \param conn_status 1 if connected, 0 if disconnected
 *
 * \return Zero if OK, otherwise negative
 */
static int md_do_move(struct md_interface * iface, int conn_status)
{
	struct list_head * pos;
	struct md_interface * tmp, * new = NULL;
	unsigned char conn;
	int update = 0;
	struct evt_md_movement m;
	struct timespec tm;
	unsigned long id;

	if (iface == NULL)
		return -1;

	DEBUG("DO MOVEMENT: %s %u (current del task id: %lu)", (conn_status) ? "CONN" : "DISCONN", iface->ifindex, iface->delete_task_id);
	
	if (iface->delete_task_id != 0 && conn_status) {
		INFO("MOVEMENT: Cancel delayed disconn. task @ %u", iface->ifindex);
		tasks_del(iface->delete_task_id);
		iface->delete_task_id = 0;
		update = 1;
	}

	/* 
	 * TODO: Need to set update variable better
	 * I.e. in case of CoA address/prefix change or defrtr change,
	 * currently update is zero, and it indicates NEW movement
	 */

	if (pcoa_check_enabled) {
		list_for_each(pos, &md_interfaces) {
			tmp = list_entry(pos, struct md_interface, list);
			if (tmp == iface)
				continue;

			pthread_mutex_lock(&iface->lock);
			if ((tmp->flags & MD_IFACE_CONN) == MD_IFACE_CONN) 
				conn = 1;
			else
				conn = 0;
			pthread_mutex_unlock(&iface->lock);
			if (conn) {
				if (conn_status) {
					INFO("New primary CoA, BUT ALREADY CONNECTED: " IP6ADDR_FMT "/%u via " IP6ADDR_FMT " on %s (%u)",
							IP6ADDR_TO_STR(&iface->addr), iface->plen, IP6ADDR_TO_STR(&iface->defrt), iface->name, iface->ifindex);
					return 0;
				} else {
					new = tmp;
					break;
				}
			}
		}
	}

	if (conn_status) {
		INFO("%s CoA: " IP6ADDR_FMT "/%u via " IP6ADDR_FMT " on %s (%u)",
				(update) ? "Update" : "New", IP6ADDR_TO_STR(&iface->addr), iface->plen, IP6ADDR_TO_STR(&iface->defrt), iface->name, iface->ifindex);
		if (update)
			m.action = EVT_MD_MOVEMENT_UPDATE;
		else
			m.action = EVT_MD_MOVEMENT_CONN;
		m.ifindex = iface->ifindex;
		memcpy(&m.coa, &iface->addr, sizeof(m.coa));
		m.plen = iface->plen;
		memcpy(&m.drtr, &iface->defrt, sizeof(m.drtr));
		imsg_send_event(md_opts.msg_ids.me, EVT_MD_MOVEMENT, &m, sizeof(m), 1, NULL);
	} else {
		if (new) {
			INFO("Existing (new primary) CoA: " IP6ADDR_FMT "/%u via " IP6ADDR_FMT " on %s (%u)",
					IP6ADDR_TO_STR(&new->addr), new->plen, IP6ADDR_TO_STR(&new->defrt), new->name, new->ifindex);
			m.action = EVT_MD_MOVEMENT_UPDATE;
			m.ifindex = new->ifindex;
			memcpy(&m.coa, &new->addr, sizeof(m.coa));
			m.plen = new->plen;
			memcpy(&m.drtr, &iface->defrt, sizeof(m.drtr));
			imsg_send_event(md_opts.msg_ids.me, EVT_MD_MOVEMENT, &m, sizeof(m), 1, NULL);
		} else {
			DEBUG("Disappeared CoA @ %u. Start delayed delete task", iface->ifindex);
			tm.tv_sec = md_opts.del_delay;
			tm.tv_nsec = 0;
			id = tasks_add_rel(md_delayed_disconn, iface, tm);
			if (id == 0) {
				ERROR("Unable to start delayed disconnect task. Do it now.");
				m.action = EVT_MD_MOVEMENT_DISCONN;
				m.ifindex = iface->ifindex;
				memset(&m.coa, 0, sizeof(m.coa));
				m.plen = 0;
				memset(&m.drtr, 0, sizeof(m.drtr));
				imsg_send_event(md_opts.msg_ids.me, EVT_MD_MOVEMENT, &m, sizeof(m), 1, NULL);
			} else
				iface->delete_task_id = id;
		}
	}

	return 0;
}

/*! md_handle_event action: Remove address (CoA) */
#define MD_HANDLE_ACTION_DEL_ADDR		0x01

/*! md_handle_event action: Remove default router (gateway) */
#define MD_HANDLE_ACTION_DEL_DEFRT		0x02

/*!
 * \brief Handle event (update interface status)
 *
 * Use non-zero action parameter only with NULL addr and defrt arguments!
 * It ignores events with home address
 *
 * \param ifindex Interface index
 * \param flags Interface flags (zero to ommit)
 * \param addr Global IPv6 address (NULL to ommit)
 * \param plen IPv6 prefix length
 * \param defrt Default route (gateway) (NULL to ommit)
 * \param action Additional actions, see MD_HANDLE_ACTION_* (zero to ommit)
 */
void md_handle_event(unsigned int ifindex, unsigned int flags, struct in6_addr * addr, unsigned int plen, struct in6_addr * defrt, unsigned char action)
{
	int ret;
	struct list_head * pos;
	struct md_interface * iface;
	int changed = 0;
	unsigned char prev_conn_status;
	struct in6_addr hoa;

	ret = IMSG_MSG_ARGS(md_opts.msg_ids.me, md_opts.msg_ids.ctrl, MSG_CTRL_GET_HOA, NULL, 0, &hoa, sizeof(hoa));
	if (ret) {
		ERROR("Unable to get HOA from ctrl");
		memset(&hoa, 0, sizeof(hoa));
	}

	pthread_mutex_lock(&md_interfaces_lock);

	list_for_each(pos, &md_interfaces) {
		iface = list_entry(pos, struct md_interface, list);
		if (iface->ifindex != ifindex)
			continue;

		pthread_mutex_lock(&iface->lock);
		if ((iface->flags & MD_IFACE_CONN) == MD_IFACE_CONN)
			prev_conn_status = 1;
		else
			prev_conn_status = 0;
		pthread_mutex_unlock(&iface->lock);

		if (flags != 0) {
			if ((flags & IFF_UP) != IFF_UP) {
				pthread_mutex_lock(&iface->lock);
				if (iface->flags != MD_IFACE_DOWN) {
					changed = 1;
					iface->flags = MD_IFACE_DOWN;
					DEBUG("MD: Interface %d -> down", ifindex);
				}
				pthread_mutex_unlock(&iface->lock);
			}

			pthread_mutex_lock(&iface->lock);
			if ((iface->flags & MD_IFACE_UP) != MD_IFACE_UP) {
				changed = 1;
				iface->flags |= MD_IFACE_UP & MD_IFACE_NUD_OK;
				DEBUG("MD: Interface %d -> up", ifindex);
			}
			pthread_mutex_unlock(&iface->lock);
		}

		if (addr != NULL && action == 0) {
			if (memcmp(&hoa, addr, sizeof(hoa)) != 0) {
				pthread_mutex_lock(&iface->lock);
				if (memcmp(&iface->addr, addr, sizeof(iface->addr)) != 0 ||
						iface->plen != plen || ((iface->flags & MD_IFACE_COA) != MD_IFACE_COA)) {
					changed = 1;
					memcpy(&iface->addr, addr, sizeof(iface->addr));
					iface->plen = plen;
					iface->flags |= MD_IFACE_COA;
					DEBUG("MD: Interface %d -> update CoA", ifindex);
				}
				pthread_mutex_unlock(&iface->lock);
			}
		}

		if (defrt != NULL && action == 0) {
			pthread_mutex_lock(&iface->lock);
			if (memcmp(&iface->defrt, defrt, sizeof(iface->defrt)) != 0 ||
					((iface->flags & MD_IFACE_DEFRT) != MD_IFACE_DEFRT)) {
				changed = 1;
				memcpy(&iface->defrt, defrt, sizeof(iface->defrt));
				iface->flags |= MD_IFACE_DEFRT;
				DEBUG("MD: Interface %d -> update default route", ifindex);
			}
			pthread_mutex_unlock(&iface->lock);
		}

		if (action != 0) {
			if (action & MD_HANDLE_ACTION_DEL_ADDR) {
				pthread_mutex_lock(&iface->lock);
				if ((iface->flags & MD_IFACE_COA) == MD_IFACE_COA &&
						memcmp(&iface->addr, addr, sizeof(iface->addr)) == 0) {
					changed = 1;
					memset(&iface->addr, 0, sizeof(iface->addr));
					iface->plen = 0;
					iface->flags &= (~MD_IFACE_COA);
					DEBUG("MD: Interface %d -> delete CoA", ifindex);
				}
				pthread_mutex_unlock(&iface->lock);
			}

			if (action & MD_HANDLE_ACTION_DEL_DEFRT) {
				pthread_mutex_lock(&iface->lock);
				if ((iface->flags & MD_IFACE_DEFRT) == MD_IFACE_DEFRT &&
						memcmp(&iface->defrt, defrt, sizeof(iface->defrt)) == 0) {
					changed = 1;
					memset(&iface->defrt, 0, sizeof(iface->defrt));
					iface->flags &= (~MD_IFACE_DEFRT);
					DEBUG("MD: Interface %d -> delete default route", ifindex);
				}
				pthread_mutex_unlock(&iface->lock);
			}
		}

		if (changed) {
			pthread_mutex_lock(&iface->lock);
			DEBUG("MD: Interface %d -> flags %02X (conn: %02X, prev conn status %u)", ifindex, iface->flags, MD_IFACE_CONN, prev_conn_status);
			if ((iface->flags & MD_IFACE_CONN) == MD_IFACE_CONN) {
				pthread_mutex_unlock(&iface->lock);
				md_do_move(iface, 1);
			} else if (prev_conn_status == 1) {
				pthread_mutex_unlock(&iface->lock);
				md_do_move(iface, 0);
			}
			pthread_mutex_unlock(&iface->lock);
		}

		break;
	}

	pthread_mutex_unlock(&md_interfaces_lock);
}

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the md module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int md_proc_msg(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int retval = -1;
	struct list_head * pos;
	struct md_interface * iface;
	int * ifalen;
	int * ifa;
	int i;

	switch(message) {
	case MSG_MD_GET_INTERFACES:
		if (arg == NULL || reply_arg == NULL)
			break;
		ifalen = (int *)arg;
		ifa = (int *) reply_arg;

		pthread_mutex_lock(&md_interfaces_lock);
		i = 0;
		list_for_each(pos, &md_interfaces) {
			if (i >= *ifalen)
				break;
			iface = list_entry(pos, struct md_interface, list);
			ifa[i++] = iface->ifindex;
		}
		pthread_mutex_unlock(&md_interfaces_lock);
		retval = i;
		break;
	case MSG_MD_DISABLE_PCOA_CHECK:
		pcoa_check_enabled = 0;
		retval = 0;
		break;
	}

	return retval;
}

/*!
 * \brief Event processing
 *
 * For details, please read the API documentation of the md module
 * \param sender Sender module ID
 * \param event Event ID
 * \param arg Event argument
 * \param need_to_free Non-zero if arg should to be freed
 */
static void md_proc_event(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free)
{
	if (sender == md_opts.msg_ids.env) {
		switch(event) {
		case EVT_ENV_NEWLINK:
			do {
				struct evt_env_newlink * link = (struct evt_env_newlink *)arg;
				if (link == NULL)
					return;
				//DEBUG("NEWLINK: %d %s %08X type %u", link->ifindex, link->ifname, link->flags, link->type);
				md_handle_event(link->ifindex, link->flags, NULL, 0, NULL, 0);
			} while (0);
			break;
		case EVT_ENV_DELLINK:
			do {
				struct evt_env_dellink * link = (struct evt_env_dellink *)arg;
				if (link == NULL)
					return;
				//DEBUG("DELLINK: %d", link->ifindex);
				md_handle_event(link->ifindex, (~IFF_UP), NULL, 0, NULL, 0);
			} while (0);
			break;
		case EVT_ENV_ADDR:
			do {
				struct evt_env_addr * addr = (struct evt_env_addr *)arg;
				if (addr == NULL)
					return;
				//DEBUG("%sADDR: %d " IP6ADDR_FMT " scope %02X", (addr->action == EVT_ENV_ADDR_NEW) ? "NEW" : "DEL",
				//		addr->ifindex, IP6ADDR_TO_STR(&addr->addr), addr->scope);
				if (addr->action == EVT_ENV_ADDR_NEW)
					md_handle_event(addr->ifindex, 0, &addr->addr, addr->plen, NULL, 0);
				else
					md_handle_event(addr->ifindex, 0, &addr->addr, 0, NULL, MD_HANDLE_ACTION_DEL_ADDR);
			} while (0);
			break;
		case EVT_ENV_ROUTE:
			do {
				struct evt_env_route * route = (struct evt_env_route *)arg;
				struct in6_addr addr_any;
				if (route == NULL)
					return;
				//DEBUG("%sROUTE: %d: " IP6ADDR_FMT " via " IP6ADDR_FMT " scope %02X type %02X dev %d", (route->action == EVT_ENV_ROUTE_NEW) ? "NEW" : "DEL",
				//		route->table, IP6ADDR_TO_STR(&route->dst), IP6ADDR_TO_STR(&route->gw), route->scope, route->type, route->oif);
				ipv6_addr_set(&addr_any, 0x0, 0x0, 0x0, 0x0);
				if (memcmp(&route->dst, &addr_any, sizeof(struct in6_addr)) == 0 && route->table == ROUTE_TABLE_MAIN) {
					if (route->action == EVT_ENV_ROUTE_NEW)
						md_handle_event(route->oif, 0, NULL, 0, &route->gw, 0);
					else
						md_handle_event(route->oif, 0, NULL, 0, &route->gw, MD_HANDLE_ACTION_DEL_DEFRT);
				}
			} while (0);
			break;
		}
	}

	if (need_to_free)
		free(arg);
}

static struct imsg_opts imsg_opts = {
	.event_fp = md_proc_event,
	.message_fp = md_proc_msg,
	.event_max = EVT_MD_NONE
};

/*!
 * \brief Bootstrapping movement detection
 *
 * \return Zero if OK, otherwise negative
 */
static int md_bootstrapping()
{
	int ret;
	struct list_head * pos, *pos2;
	struct md_interface * iface;
	int i;
	struct in6_addr hoa;

	struct msg_env_get_link_reply glr;
	struct msg_env_addr_get_reply addrs;
	struct msg_env_route_get route;
	struct msg_env_route_get_reply routes;

	ret = IMSG_MSG_ARGS(md_opts.msg_ids.me, md_opts.msg_ids.ctrl, MSG_CTRL_GET_HOA, NULL, 0, &hoa, sizeof(hoa));
	if (ret) {
		ERROR("Unable to get HOA from ctrl");
		memset(&hoa, 0, sizeof(hoa));
	}

	pthread_mutex_lock(&md_interfaces_lock);

	list_for_each_safe(pos, pos2, &md_interfaces) {
		iface = list_entry(pos, struct md_interface, list);
		memset(&glr, 0, sizeof(glr));
		ret = IMSG_MSG_ARGS(md_opts.msg_ids.me, md_opts.msg_ids.env,
					MSG_ENV_GET_LINK, &iface->ifindex, sizeof(iface->ifindex),
					&glr, sizeof(glr));
		if (ret) {
			ERROR("Invalid interface: '%s'", iface->name);
			list_del(pos);
			free(iface);
			continue;
		}

		if ((glr.flags & IFF_UP) != IFF_UP) {
			pthread_mutex_lock(&iface->lock);
			iface->flags = MD_IFACE_DOWN;
			pthread_mutex_unlock(&iface->lock);
			continue;
		}

		pthread_mutex_lock(&iface->lock);
		iface->flags |= (MD_IFACE_UP | MD_IFACE_NUD_OK);
		pthread_mutex_unlock(&iface->lock);

		DEBUG("Interface %s %u is UP", iface->name, iface->ifindex);

		ret = IMSG_MSG_ARGS(md_opts.msg_ids.me, md_opts.msg_ids.env,
					MSG_ENV_ADDR_GET, &iface->ifindex, sizeof(iface->ifindex),
					&addrs, sizeof(addrs));
		if (ret) {
			ERROR("Unable to get addresses of interface: '%s'", iface->name);
			continue;
		}

		for (i = 0; i < MSG_ENV_ADDR_GET_NUM && addrs.last[i] == 0; i++) {
			if (addrs.scopes[i] != RT_SCOPE_UNIVERSE)
				continue;
			if (memcmp(&hoa, &addrs.addrs[i], sizeof(hoa)) == 0)
				continue;
			else {
				pthread_mutex_lock(&iface->lock);
				iface->flags |= MD_IFACE_COA;
				memcpy(&iface->addr, &addrs.addrs[i], sizeof(iface->addr));
				iface->plen = addrs.plens[i];
				pthread_mutex_unlock(&iface->lock);
				DEBUG("Interface %s %u CoA: " IP6ADDR_FMT "/%u", iface->name, iface->ifindex, IP6ADDR_TO_STR(&iface->addr), iface->plen);
				break;
			}
		}

		route.oiface = iface->ifindex;
		ipv6_addr_set(&route.dst, 0x0, 0x0, 0x0, 0x0);
		route.dplen = 0;
		route.table = ROUTE_TABLE_MAIN;
		ret = IMSG_MSG_ARGS(md_opts.msg_ids.me, md_opts.msg_ids.env,
					MSG_ENV_ROUTE_GET, &route, sizeof(route),
					&routes, sizeof(routes));
		if (ret) {
			ERROR("Unable to get routes of interface: '%s'", iface->name);
			continue;
		}

		for (i = 0; i < MSG_ENV_ROUTE_GET_NUM && routes.last[i] == 0; i++) {
			pthread_mutex_lock(&iface->lock);
			iface->flags |= MD_IFACE_DEFRT;
			memcpy(&iface->defrt, &routes.gws[i], sizeof(iface->defrt));
			pthread_mutex_unlock(&iface->lock);
			DEBUG("Interface %s %u has default route via " IP6ADDR_FMT, iface->name, iface->ifindex, IP6ADDR_TO_STR(&iface->defrt));
			break;
		}

		pthread_mutex_lock(&iface->lock);
		if ((iface->flags & MD_IFACE_CONN) == MD_IFACE_CONN) {
			pthread_mutex_unlock(&iface->lock);
			md_do_move(iface, 1);
		}
		pthread_mutex_unlock(&iface->lock);
	}

	pthread_mutex_unlock(&md_interfaces_lock);

	return 0;
}

/*!
 * \brief Sending Router Solicitation if interface is disconnected
 *
 * \param arg Interface
 * \return Unused
 */
static void * md_send_rs(void * arg)
{
	struct md_interface * iface = (struct md_interface *)arg;
	struct msg_ccom_send_rs rs;
	unsigned char conn = 0;

	if (iface == NULL)
		return NULL;
	if (md_opts.rs_interval == 0)
		return NULL;

	memset(&rs, 0, sizeof(rs));
	memcpy(&rs.src, &in6addr_any,sizeof(rs.src));
	memcpy(&rs.dst, &in6addr_allr,sizeof(rs.dst));
	rs.ifindex = iface->ifindex;

	for (;;) {
		pthread_mutex_lock(&iface->lock);
		if ((iface->flags & MD_IFACE_CONN) == MD_IFACE_CONN) 
			conn = 0;
		else
			conn = 1;
		pthread_mutex_unlock(&iface->lock);
		if (conn == 0) {
			//DEBUG("Sending RS on interface %s", iface->name);
			IMSG_MSG_ARGS(md_opts.msg_ids.me, md_opts.msg_ids.ccom, MSG_CCOM_SEND_RS, &rs, sizeof(rs), NULL, 0);
		}
		usleep(md_opts.rs_interval * 1000);
	}

	return NULL;
}

/*!
 * \brief Cleanup
 *
 * Unsubscribe from events to stop movement detection
 *
 * \return Zero
 */
static int md_exit_handler(void * arg)
{
	int ret;

	DEBUG("Clear module: MD simple");

	ret = imsg_event_unsubscribe(md_opts.msg_ids.env, EVT_ENV_NEWLINK, md_opts.msg_ids.me);
	ret |= imsg_event_unsubscribe(md_opts.msg_ids.env, EVT_ENV_DELLINK, md_opts.msg_ids.me);
	ret |= imsg_event_unsubscribe(md_opts.msg_ids.env, EVT_ENV_NEIGH, md_opts.msg_ids.me);
	ret |= imsg_event_unsubscribe(md_opts.msg_ids.env, EVT_ENV_ADDR, md_opts.msg_ids.me);
	ret |= imsg_event_unsubscribe(md_opts.msg_ids.env, EVT_ENV_ROUTE, md_opts.msg_ids.me);
	if (ret) {
		ERROR("Unable to UNsubscribe for environment events (%d)", ret);
		return -1;
	}

	return 0;
}

/*!
 * \brief Start movement detection
 *
 * Called by core-all-started hook
 *
 * \param arg Unused
 *
 * \return Zero
 */
static int md_start(void * arg)
{
	int ret;

	DEBUG("Start module: MD simple");

	ret = imsg_get_id(MSG_CTRL);
	if (ret < 0) {
		ERROR("Unable to find module: control");
		return -1;
	}
	md_opts.msg_ids.ctrl = ret;

	ret = imsg_get_id(MSG_CCOM);
	if (ret < 0) {
		ERROR("Unable to find module: core-comm");
		return -1;
	}
	md_opts.msg_ids.ccom = ret;

	ret = imsg_event_subscribe(md_opts.msg_ids.env, EVT_ENV_NEWLINK, md_opts.msg_ids.me);
	ret |= imsg_event_subscribe(md_opts.msg_ids.env, EVT_ENV_DELLINK, md_opts.msg_ids.me);
	ret |= imsg_event_subscribe(md_opts.msg_ids.env, EVT_ENV_NEIGH, md_opts.msg_ids.me);
	ret |= imsg_event_subscribe(md_opts.msg_ids.env, EVT_ENV_ADDR, md_opts.msg_ids.me);
	ret |= imsg_event_subscribe(md_opts.msg_ids.env, EVT_ENV_ROUTE, md_opts.msg_ids.me);
	if (ret) {
		ERROR("Unable to subscribe for environment events (%d)", ret);
		return -1;
	}

	md_bootstrapping();

	return 0;
}

/*!
 * \brief Initializing md module
 *
 * Registers exit function to the 'core-exit' hook
 * Subscribe for events
 * Register internal messaging
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int md_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;
	unsigned int all_inited_id;
	int i;
	char * iface_p;
	unsigned int ifindex;
	struct md_interface * iface;

	DEBUG("Initializing module: MD");

	ret = imsg_register(MSG_MD, &imsg_opts);
	if (ret < 0)
		return ret;
	md_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to find module: environment");
		return -1;
	}
	md_opts.msg_ids.env = ret;

	md_opts.del_delay = cfg_getint(cfg, "md-delete-delay");
	md_opts.rs_interval = cfg_getint(cfg, "md-rs-interval");
	
	if (cfg == NULL)
		return -1;
	for (i = 0; i < cfg_size(cfg, "interfaces"); i++) {
		iface_p = cfg_getnstr(cfg, "interfaces", i);
		if (iface_p == NULL)
			continue;
		ret = IMSG_MSG_ARGS(md_opts.msg_ids.me, md_opts.msg_ids.env,
					MSG_ENV_GET_IFINDEX, iface_p, strlen(iface_p) + 1,
					&ifindex, sizeof(ifindex));
		if (ret) {
			ERROR("Invalid interface: '%s'", iface_p);
			continue;
		}
		iface = malloc(sizeof(struct md_interface));
		if (iface == NULL) {
			ERROR("Out of memory: Unable to alloc mds iface");
			exit(EXIT_FAILURE);
		}
		memset(iface, 0, sizeof(struct md_interface));
		pthread_mutex_init(&iface->lock, NULL);
		strncpy(iface->name, iface_p, sizeof(iface->name));
		iface->ifindex = ifindex;
		iface->flags = MD_IFACE_DOWN;
		iface->delete_task_id = 0;
		INFO("Interface added: %s %u", iface->name, iface->ifindex);

		pthread_mutex_lock(&md_interfaces_lock);
		list_add_tail(&iface->list, &md_interfaces);
		pthread_mutex_unlock(&md_interfaces_lock);

		if (md_opts.rs_interval > 0) {
			threading_create(md_send_rs, iface);
		}
	}
	if (list_empty(&md_interfaces)) {
		ERROR("Need to specify at least one interface");
		return -1;
	}

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		/* 
		 * Force MDS exit between the firsts:
		 * Do not handle disconn events during cleanup!
		 */
		hooks_add(core_exit_id, 1, md_exit_handler);
	}

	ret = hooks_get_id("core-all-started");
	if (ret >= 0) {
		all_inited_id = (unsigned int)ret;
		hooks_add(all_inited_id, 1, md_start);
	}

	return 0;
}

MIP6_MODULE_INIT(MSG_MD, SEQ_MD, md_module_init, md_cfg_opts);

/*! \} */

