
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/control.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/environment.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/api.h>
#include <mip6d-ng/flowb.h>

#include "flowb.h"
#include "mho-bid.h"
#include "fbe.h"
#include "main.h"
#include "mho-flowi.h"
#include "mho-flows.h"

/*!
 * \brief Configuration options
 *
 * See config sample for details!
 */
cfg_opt_t flowb_bid_pri_opts[] = {
	CFG_INT("priority", 0, CFGF_NONE),
	CFG_END()
};

/*!
 * \brief Configuration options
 *
 * See config sample for details!
 */
cfg_opt_t flowb_main_opts[] = {
	CFG_SEC("bid-pri", flowb_bid_pri_opts, CFGF_TITLE | CFGF_MULTI),
	CFG_END()
};

struct flowb_opts flowb_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the flowb module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int flowb_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int ret;
	int retval = -1;
	int * id_p, id;
	struct flowb_api_register * areg;
	struct flowb_api_register_reply * areg_rep;
	struct flowb_api_update * aup;
	struct flowb_api_del * adel;

	switch (message) {
	case MSG_FLOWB_API_REGISTER:
		if (arg == NULL || reply_arg == NULL)
			break;
		id_p = (int *)arg;
		id = *id_p;
		areg = (struct flowb_api_register *)(id_p + 1);
		areg_rep = (struct flowb_api_register_reply *)reply_arg;
		ret = flowb_register(id, areg, areg_rep);
		if (ret == 0)
			retval = sizeof(struct flowb_api_register_reply);
		break;
	case MSG_FLOWB_API_UPDATE:
		if (arg == NULL)
			break;
		id_p = (int *)arg;
		id = *id_p;
		aup = (struct flowb_api_update *)(id_p + 1);
		retval = flowb_update(id, aup);
		break;
	case MSG_FLOWB_API_DELETE:
		if (arg == NULL)
			break;
		id_p = (int *)arg;
		id = *id_p;
		adel = (struct flowb_api_del *)(id_p + 1);
		retval = flowb_delete(id, adel);
		break;
	};

	return retval;
}

/*!
 * \brief Event processing
 *
 * For details, please read the API documentation of the flowb module
 * \param sender Sender module ID
 * \param event Event ID
 * \param arg Event argument
 * \param need_to_free Non-zero if arg should to be freed
 */
static void flowb_proc_event(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free)
{
	struct bce * bce;
	struct bule * bule;
	struct evt_ccom_ba * ba;

	if (sender == flowb_opts.msg_ids.data) {
		switch (event) {
		case EVT_DATA_NEW_BCE:
			bce = (struct bce *)arg;
			if (bce == NULL)
				break;
			bce_lock(bce);
			flowb_init_bce(bce);
			bce_unlock(bce);
			refcnt_put(&bce->refcnt);
			break;
		case EVT_DATA_INIT_BULE:
			bule = (struct bule *)arg;
			if (bule == NULL)
				break;
			bule_lock(bule);
			flowb_init_bule(bule);
			bule_unlock(bule);
			refcnt_put(&bule->refcnt);
			break;
		}
	} else if (sender == flowb_opts.msg_ids.ccom) {
		switch (event) {
		//case EVT_CCOM_BU:
		//	break;
		case EVT_DATA_NEW_BCE:
			bce = (struct bce *)arg;
			if (bce == NULL)
				break;
			bce_lock(bce);
			flowb_proc_bu(bce);
			bce_unlock(bce);
			refcnt_put(&bce->refcnt);
		case EVT_CCOM_BA:
			ba = (struct evt_ccom_ba *)arg;
			if (ba == NULL || ba->bule == NULL)
				break;
			bule_lock(ba->bule);
			flowb_proc_ba(ba->bule);
			bule_unlock(ba->bule);
			refcnt_put(&ba->bule->refcnt);
		}

	}

	if (need_to_free)
		free(arg);
}

static struct imsg_opts imsg_opts = {
	.event_fp = flowb_proc_event,
	.message_fp = flowb_proc_message
};

/*!
 * \brief Cleanup
 *
 * It flushes the Flow Binding entries
 * 
 * \return Zero
 */
static int flowb_exit_handler(void * arg)
{
	fbe_check_all(0, NULL, NULL, 0, 1);

	return 0;
}

/*!
 * \brief Initializing flowb module
 *
 * Registers exit function to the 'core-exit' hook.
 * Register internal messaging.
 * It subscribes for the necessary events and hooks.
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int flowb_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;
	enum ctrl_node_type node_type;
	struct msg_api_register_cmd api_rc;
	cfg_t * bid_pri;
	int n, i, j;
	const char * iface_p;
	char iface[IF_NAMESIZE + 1];
	unsigned int ifindex;
	unsigned char prio;
	struct msg_mcoa_set_preference sp;
	unsigned int create_bu_id;

	DEBUG("Initializing module: flowb");

	ret = imsg_register(MSG_FLOWB, &imsg_opts);
    if (ret < 0)
		return ret;
	flowb_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_CTRL);
	if (ret < 0) {
		ERROR("Unable to find module: control");
		return -1;
	}
	flowb_opts.msg_ids.ctrl = ret;
	
	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to find module: environment");
		return -1;
	}
	flowb_opts.msg_ids.env = ret;

	ret = imsg_get_id(MSG_CCOM);
	if (ret < 0) {
		ERROR("Unable to find module: core-comm");
		return -1;
	}
	flowb_opts.msg_ids.ccom = ret;

	ret = imsg_get_id(MSG_DATA);
	if (ret < 0) {
		ERROR("Unable to find module: data");
		return -1;
	}
	flowb_opts.msg_ids.data = ret;

	ret = imsg_get_id(MSG_MCOA);
	if (ret < 0) {
		ERROR("Unable to find module: mcoa");
		return -1;
	}
	flowb_opts.msg_ids.mcoa = ret;	

	ret = imsg_get_id(MSG_API);
	if (ret < 0) {
		ERROR("Unable to find module: API");
		return -1;
	}
	flowb_opts.msg_ids.api = ret;

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_FLOWB, flowb_exit_handler);
	}

	ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.ctrl,
			MSG_CTRL_GET_NODE_TYPE, NULL, 0, &node_type, sizeof(node_type));
	if (ret || node_type == CTRL_NODE_TYPE_NONE) {
		ERROR("Unknown node type");
		return -1;
	}

	ret = hooks_get_id("mcoa-mho-bid-modify");
	if (ret < 0) {
		ERROR("Unable to find hook: mcoa-mho-bid-modify");
		return -1;
	}
	ret = hooks_add(ret, 1, mho_bid_modify);
	if (ret < 0) {
		ERROR("Unable to add hook: mcoa-mho-bid-modify");
		return -1;
	}

	ret = hooks_get_id("mcoa-mho-bid-parse");
	if (ret < 0) {
		ERROR("Unable to find hook: mcoa-mho-bid-parse");
		return -1;
	}
	ret = hooks_add(ret, 1, mho_bid_parse);
	if (ret < 0) {
		ERROR("Unable to add hook: mcoa-mho-bid-parse");
		return -1;
	}

	switch (node_type) {
	case CTRL_NODE_TYPE_HA:
		ret = hooks_get_id("create-mh-ba");
		if (ret >= 0) {
			create_bu_id = (unsigned int)ret;
			hooks_add(create_bu_id, 1, fbe_extend_ba);
		} else  {
			ERROR("Unable to register for create-mh-ba hook");
			return -1;
		}

		//ret = imsg_event_subscribe(flowb_opts.msg_ids.ccom, EVT_CCOM_BU, flowb_opts.msg_ids.me);
		ret = imsg_event_subscribe(flowb_opts.msg_ids.data, EVT_DATA_NEW_BCE, flowb_opts.msg_ids.me);
		ret |= imsg_event_subscribe(flowb_opts.msg_ids.data, EVT_DATA_NEW_BCE, flowb_opts.msg_ids.me);
		if (ret) {
			ERROR("Unable to subscribe for events (%d)", ret);
		}

		ret = hooks_get_id("data-bc-ext-update");
		if (ret < 0) {
			ERROR("Unable to find hook: data-bc-ext-update");
			return -1;
		}
		ret = hooks_add(ret, 9, flowb_proc_bce_update);
		if (ret < 0) {
			ERROR("Unable to add hook: data-bc-ext-update");
			return -1;
		}
		break;
	case CTRL_NODE_TYPE_MN:
		n = cfg_size(cfg, "bid-pri");
		for (i = j = 0; i < n; i++) {
			bid_pri = cfg_getnsec(cfg, "bid-pri", i);
			iface_p = cfg_title(bid_pri);
			if (iface_p == NULL || strlen(iface_p) == 0)
				continue;
			strncpy(iface, iface_p, sizeof(iface));
			ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.env,
					MSG_ENV_GET_IFINDEX, iface, strlen(iface) + 1,
					&ifindex, sizeof(ifindex));
			if (ret) 
				continue;
			prio = cfg_getint(bid_pri, "priority");
			if (prio == 0)
				continue;
			memset(&sp, 0, sizeof(sp));
			sp.ifindex = ifindex;
			sp.preference = prio;
			ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.mcoa,
					MSG_MCOA_SET_PREFERENCE, &sp, sizeof(sp), NULL, 0);
			if (ret)
				continue;
			j++;
		}
		ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.mcoa,
				MSG_MCOA_GET_NUM, NULL, 0, NULL, 0);
		if (ret != j) {
			ERROR("bid-prio should to be defined for all of the interfaces");
			return -1;
		}

		ret = hooks_get_id("create-mh-bu");
		if (ret >= 0) {
			create_bu_id = (unsigned int)ret;
			hooks_add(create_bu_id, 1, fbe_extend_bu);
		} else  {
			ERROR("Unable to register for create-mh-bu hook");
			return -1;
		}

		ret = hooks_get_id("api-peer-close-hook");
		if (ret < 0) {
			ERROR("Unable to find hook: api-peer-close-hook");
			return -1;
		}
		ret = hooks_add(ret, 1, flowb_peer_close);
		if (ret < 0) {
			ERROR("Unable to add hook: api-peer-close-hook");
			return -1;
		}

		ret = imsg_event_subscribe(flowb_opts.msg_ids.ccom, EVT_CCOM_BA, flowb_opts.msg_ids.me);
		ret |= imsg_event_subscribe(flowb_opts.msg_ids.data, EVT_DATA_INIT_BULE, flowb_opts.msg_ids.me);
		if (ret) {
			ERROR("Unable to subscribe for events (%d)", ret);
		}

		memset(&api_rc, 0, sizeof(api_rc));
		api_rc.command = API_FLOWB_REGISTER;
		snprintf(api_rc.command_name, sizeof(api_rc.command_name), "FLOWB_REGISTER");
		api_rc.imsg_cmd = MSG_FLOWB_API_REGISTER;
		ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
		if (ret) {
			ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
		}
		memset(&api_rc, 0, sizeof(api_rc));
		api_rc.command = API_FLOWB_UPDATE;
		snprintf(api_rc.command_name, sizeof(api_rc.command_name), "FLOWB_UPDATE");
		api_rc.imsg_cmd = MSG_FLOWB_API_UPDATE;
		ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
		if (ret) {
			ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
		}
		memset(&api_rc, 0, sizeof(api_rc));
		api_rc.command = API_FLOWB_DELETE;
		snprintf(api_rc.command_name, sizeof(api_rc.command_name), "FLOWB_DELETE");
		api_rc.imsg_cmd = MSG_FLOWB_API_DELETE;
		ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
		if (ret) {
			ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
		}
		break;
	case CTRL_NODE_TYPE_CN:
		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}

	mho_flowi_init();
	mho_flows_init();

	return 0;
}

MIP6_MODULE_INIT(MSG_FLOWB, SEQ_FLOWB, flowb_module_init, flowb_main_opts, flowb_bid_pri_opts);

/*! \} */

