
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/mcoa-bule.h>
#include <mip6d-ng/mcoa-bce.h>
#include <mip6d-ng/flowb.h>
#include <mip6d-ng/fbe.h>
#include <mip6d-ng/flowb-bce.h>
#include <mip6d-ng/flowb-bule.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/mcoa.h>

#include "main.h"
#include "flowb.h"
#include "fbe.h"

/*!
 * \brief Send Binding Update now
 *
 * It notifies the control module to send Binding Update now.
 * It is a BULE iterate function, which will be called for all of the BULEs.
 * It uses the extra arg argument, to get the Flow Binding Entry, and
 * it sends BU only if the BID values are matching.
 *
 * \param bule BULE
 * \param arg FBE
 * \return Zero
 */
static int flowb_send_bu_now(struct bule * bule, void * arg)
{
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0, fbid = 0;
	struct fbe * fbe = (struct fbe *)arg;

	if (bule == NULL || fbe == NULL)
		return 0;

	fbe_lock(fbe);
	fbid = fbe->bid;
	fbe_unlock(fbe);

	bule_lock(bule);

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
			break;
		}
	}

	if (bid == 0) {
		ERROR("Unable to find BID extension in BULE");
		return -1;
	}
	
	bule_unlock(bule);

	if (bid == fbid) {
		IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.ctrl, MSG_CTRL_RESEND_BU_NOW,
				bule, sizeof(struct bule), NULL, 0);

		return 1;
	}

	return 0;
}

/*!
 * \brief Register a flow
 *
 * It allocates and initializes a new Flow Binding Entry, and
 * adds it to the list. It request sending BU from the control module
 * on the interface which corresponds to the FBE.
 * \param id Some kind of identifier, it will be used as Flow Identifier
 * \param areg Details from the api
 * \param areg_rep Reply to send via the api
 * \return Zero if OK
 */
int flowb_register(int id, struct flowb_api_register * areg, struct flowb_api_register_reply * areg_rep)
{
	int ret;
	struct fbe * fbe;
	struct in6_addr hoa;
	struct msg_data_iterate_bul buli;

	if (areg == NULL || areg_rep == NULL)
		return -1;

	ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.ctrl, MSG_CTRL_GET_HOA,
			NULL, 0, &hoa, sizeof(hoa));
	if (ret) {
		ERROR("Unable to get HOA from control module");
		return -1;
	}

	areg_rep->id = htonl(id);

	fbe = malloc(sizeof(struct fbe));
	if (fbe == NULL) {
		ERROR("Out of memory: Unable to alloc fbe");
		return -1;
	}

	memset(fbe, 0, sizeof(struct fbe));
	memcpy(&fbe->hoa, &hoa, sizeof(struct in6_addr));

	memcpy(&fbe->ts.src, &areg->src, sizeof(struct in6_addr));
	fbe->ts.splen = areg->splen;
	fbe->ts.sport1 = ntohs(areg->sport);
	memcpy(&fbe->ts.dst, &areg->dst, sizeof(struct in6_addr));
	fbe->ts.dplen = areg->dplen;
	fbe->ts.dport1 = ntohs(areg->dport);
	fbe->ts.proto1 = ntohl(areg->proto);
	
	fbe->bid = ntohs(areg->bid);
	fbe->is_static = areg->is_static;
	fbe->fid = id;
	memcpy(&fbe->key, &areg->key, sizeof(struct flowb_key));

	if (areg->bid > 0) {
		ret = fbe_add(fbe);
		if (ret != 0)
			return ret;
	}

	memset(&buli, 0, sizeof(buli));
	buli.fp = flowb_send_bu_now;
	buli.arg = fbe;

	ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.data, MSG_DATA_ITERATE_BUL,
			&buli, sizeof(buli), NULL, 0);
	if (ret < 0) {
		ERROR("Unable to iterate BUL");
		return ret;
	}	

	return 0;
}

/*!
 * \brief Updates a flow
 *
 * It inactivates the flow, and modifies its BID value.
 * The new status will be new to ensure the sending of
 * Flow Identification MHO.
 *
 * \param fid Flow Identifier to update
 * \param new_bid New BID
 * \return Modified FBE or NULL
 */
static struct fbe * __flowb_update(uint16_t fid, uint16_t new_bid)
{
	struct fbe * fbe;

	DEBUG("Update flow @ %u to BID %u", fid, new_bid);

	fbe = fbe_find_by_fid(fid, NULL);
	if (fbe == NULL)
		return NULL;
	
	fbe_lock(fbe);
	fbe->inactivate(fbe);
	fbe->bid = new_bid;
	fbe->status = FBE_STATUS_NEW;
	fbe_unlock(fbe);
	
	return fbe;
}

/*!
 * \brief Updates a flow
 *
 * It do the update, see the __flowb_update function.
 * Next it sends BU message on the new interface. 
 * (The method is the same, which is used in the flowb_register function)
 *
 * \param id Some kind of identifier
 * \param aup Update details from the api
 * \return Zero if OK
 */
int flowb_update(int id, struct flowb_api_update * aup)
{
	int ret;
	struct fbe * fbe;
	struct msg_data_iterate_bul buli;

	if (aup == NULL)
		return -1;

	fbe = __flowb_update(ntohl(aup->id), ntohs(aup->bid));
	if (fbe == NULL)
		return -1;
	
	memset(&buli, 0, sizeof(buli));
	buli.fp = flowb_send_bu_now;
	buli.arg = fbe;

	ret = IMSG_MSG_ARGS(flowb_opts.msg_ids.me, flowb_opts.msg_ids.data, MSG_DATA_ITERATE_BUL,
			&buli, sizeof(buli), NULL, 0);
	if (ret < 0) {
		ERROR("Unable to iterate BUL");
		return ret;
	}	
	
	return 0;
}

/*!
 * \brief Deletes a flow
 *
 * \param id Some kind of identifier
 * \param adel Delete details from the api
 * \return Zero if OK
 */
int flowb_delete(int id, struct flowb_api_del * adel)
{
	if (adel == NULL)
		return -1;
	
	DEBUG("Delete FBE with FID %u", adel->id);
	fbe_del(0, adel->id, NULL, 1);

	return 0;
}

/*!
 * \brief Handle disconnection of an application
 *
 * It deletes the corresponding Flow Binding entries
 *
 * \param arg Identifier
 * \return Zero if OK
 */
int flowb_peer_close(void * arg)
{
	int * id_p = (int *)arg;

	if (id_p == NULL)
		return -1;

	DEBUG("API peer closed. Delete FBE with FID %u", *id_p);
	fbe_del(0, *id_p, NULL, 0);

	return 0;
}

/*!
 * \brief Processing BU message
 *
 * After parsing all of the options, this will be called.
 * The new and updates and still existing entries
 * are included into the BCE as extensions.
 * It registers new flows, update existing flows, and 
 * deletes flows which are not listed.
 *
 * \param bce BCE
 * \return Zero if OK
 */
int flowb_proc_bu(struct bce * bce)
{
	struct list_head * pos, * pos2;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct bce_ext_fbe * ext_fbe = NULL;
	struct bce_ext_fbe_new * ext_fbe_new = NULL;
	uint16_t * fids = NULL, * fids_new = NULL;
	int fids_num = 0;
	struct bce_ext_fbe_update * ext_fbe_update = NULL;
	struct fbe * fbe;

	if (bce == NULL)
		return -1;

	list_for_each_safe(pos, pos2, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
		} else if (strcmp(ext->name, "fbe") == 0) {
			if (ext_fbe != NULL) {
				if (ext_fbe->fids != NULL)
					free(ext_fbe->fids);
				free(ext_fbe);
			}
			ext_fbe = (struct bce_ext_fbe *)ext;
			list_del(pos);
		} else if (strcmp(ext->name, "fbe_new") == 0) {
			ext_fbe_new = (struct bce_ext_fbe_new *)ext;
			if (ext_fbe_new->fbe != NULL) {
				fbe_add(ext_fbe_new->fbe);
				ext_fbe_new->fbe->status = FBE_STATUS_OK;
				fids_new = realloc(fids, (fids_num + 1) * sizeof(uint16_t));
				if (fids_new == NULL) {
					fids_num = 0;
					free(fids);
				} else {
					fids = fids_new;
					fids[fids_num++] = ext_fbe_new->fbe->fid;
				}
			}
			list_del(pos);
			free(ext_fbe_new);
		} else if (strcmp(ext->name, "fbe_up") == 0) {
			ext_fbe_update = (struct bce_ext_fbe_update *)ext;
			fbe = __flowb_update(ext_fbe_update->fid, ext_fbe_update->bid);
			if (fbe != NULL) {
				fbe->activate(fbe);
			}
			fids_new = realloc(fids, (fids_num + 1) * sizeof(uint16_t));
			if (fids_new == NULL) {
				fids_num = 0;
				free(fids);
			} else {
				fids = fids_new;
				fids[fids_num++] = ext_fbe_update->fid;
			}
			list_del(pos);
			free(ext_fbe_update);
		}
	}

	if (ext_fbe != NULL) {
		if (ext_fbe->fids != NULL) {
			fids_new = realloc(fids, (fids_num + ext_fbe->num) * sizeof(uint16_t));
			if (fids_new == NULL) {
				free(fids);
				fids_num = 0;
			} else {
				fids = fids_new;
				memcpy(&fids[fids_num], ext_fbe->fids, ext_fbe->num * sizeof(uint16_t));
				fids_num += ext_fbe->num;
			}
			free(ext_fbe->fids);
		}
		free(ext_fbe);
	}

	if (bid != 0)
		fbe_check_all(bid, &bce->hoa, fids, fids_num, 1);

	if (fids != NULL)
		free(fids);

	return 0;
}

/*!
 * \brief Processing BU message
 *
 * It is called by the bce_ext_update hook,
 * and it's using the flowb_proc_bu to process the update
 *
 * \param arg struct bce_ext_update
 * \return Zero
 */
int flowb_proc_bce_update(void * arg)
{
	struct bce_ext_update * eu = (struct bce_ext_update *)arg;

	if (eu == NULL)
		return 0;

	flowb_proc_bu(eu->new_bce);

	return 0;
}


/*!
 * \brief Processing BA message
 *
 * After parsing all of the options, this will be called.
 * The existing entries are included into the BULE as extension.
 * 
 * \param bule BULE
 * \return Zero if OK
 */
int flowb_proc_ba(struct bule * bule)
{
	struct list_head * pos, * pos2;
	struct bule_ext * ext;
	struct bule_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct bule_ext_fbe * ext_fbe = NULL;

	if (bule == NULL)
		return -1;

	list_for_each_safe(pos, pos2, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bule_ext_bid *)ext;
			bid = ext_bid->bid;
		} else if (strcmp(ext->name, "fbe") == 0) {
			if (ext_fbe != NULL) {
				if (ext_fbe->fids != NULL)
					free(ext_fbe->fids);
				free(ext_fbe);
			}
			ext_fbe = (struct bule_ext_fbe *)ext;
			list_del(pos);
		}
	}

	if (ext_fbe != NULL && bid != 0) {
		if (ext_fbe->fids != NULL) {
			fbe_check_all(bid, &bule->hoa, ext_fbe->fids, ext_fbe->num, 0);
			free(ext_fbe->fids);
		}
		free(ext_fbe);
	}

	return 0;
}

/*!
 * \brief Inactivate all of the entries
 *
 * It is a callback function called by an iteration.
 * It inactivates all of the entries.
 *
 * \param fbe Flow Binding Entry
 * \param arg Unused
 */
static void flowb_bce_invalidate_all(struct fbe * fbe, void * arg)
{
	if (fbe == NULL)
		return;

	fbe->inactivate(fbe);
}

/*!
 * \brief Cleanup upon deleting BCE
 *
 * Deletes stucked extensions, and invalidates
 * all of the flows which are using this binding.
 *
 * \param arg BCE
 * \return Zero
 */
static int flowb_remove_bce(void * arg)
{
	struct bce * bce = (struct bce *)arg;
	struct list_head * pos, * pos2;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct bce_ext_fbe * ext_fbe = NULL;
	struct bce_ext_fbe_new * ext_fbe_new = NULL;
	struct bce_ext_fbe_update * ext_fbe_update = NULL;

	if (bce == NULL)
		return 0;

	bce_lock(bce);
	list_for_each_safe(pos, pos2, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			ext_bid = 0;
			break;
		} else if (strcmp(ext->name, "fbe") == 0) {
			ext_fbe = (struct bce_ext_fbe *)ext;
			if (ext_fbe->fids != NULL)
				free(ext_fbe->fids);
			list_del(pos);
			free(ext_fbe);
		} else if (strcmp(ext->name, "fbe_new") == 0) {
			ext_fbe_new = (struct bce_ext_fbe_new *)ext;
			if (ext_fbe_new->fbe != NULL)
				free(ext_fbe_new->fbe);
			list_del(pos);
			free(ext_fbe_new);
		} else if (strcmp(ext->name, "fbe_up") == 0) {
			ext_fbe_update = (struct bce_ext_fbe_update *)ext;
			list_del(pos);
			free(ext_fbe_update);
		}
	}
	bce_unlock(bce);

	if (bid != 0) {
		DEBUG("BCE cleanup. Inactivate FBE(s) with BID %u", bid);
		fbe_find_all_by_bid(bid, &bce->hoa, flowb_bce_invalidate_all, bce);
	}

	return 0;
}

/*!
 * \brief Initialize a newly created BCE
 *
 * Registers a cleanup function
 *
 * \param bce BCE
 * \return Zero if OK
 */
int flowb_init_bce(struct bce * bce)
{
	if (bce == NULL)
		return -1;

	hooks_add(bce->del_hook_id, 1, flowb_remove_bce);

	return 0;
}

/*!
 * \brief Inactivate all of the entries
 *
 * It is a callback function called by an iteration.
 * It inactivates all of the entries.
 *
 * \param fbe Flow Binding Entry
 * \param arg Unused
 */
static void flowb_bule_invalidate_all(struct fbe * fbe, void * arg)
{
	if (fbe == NULL)
		return;

	fbe->inactivate(fbe);
}

/*!
 * \brief Cleanup upon deleting BULE
 *
 * Deletes stucked extensions, and invalidates
 * all of the flows which are using this binding.
 *
 * \param arg BULE
 * \return Zero
 */
static int flowb_remove_bule(void * arg)
{
	struct bule * bule = (struct bule *)arg;
	struct list_head * pos, * pos2;
	struct bce_ext * ext;
	struct bce_ext_bid * ext_bid = NULL;
	uint16_t bid = 0;
	struct bce_ext_fbe * ext_fbe = NULL;

	if (bule == NULL)
		return 0;

	bule_lock(bule);
	list_for_each_safe(pos, pos2, &bule->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "bid") == 0) {
			ext_bid = (struct bce_ext_bid *)ext;
			bid = ext_bid->bid;
			ext_bid = 0;
			break;
		} else if (strcmp(ext->name, "fbe") == 0) {
			ext_fbe = (struct bce_ext_fbe *)ext;
			if (ext_fbe->fids != NULL)
				free(ext_fbe->fids);
			list_del(pos);
			free(ext_fbe);
		}
	}
	bule_unlock(bule);

	if (bid != 0) {
		DEBUG("BULE cleanup. Inactivate FBE(s) with BID %u", bid);
		fbe_find_all_by_bid(bid, &bule->hoa, flowb_bule_invalidate_all, bule);
	}

	return 0;
}

/*!
 * \brief Initialize a newly created BULE
 *
 * Registers a cleanup function
 *
 * \param bule BULE
 * \return Zero if OK
 */
int flowb_init_bule(struct bule * bule)
{
	if (bule == NULL)
		return -1;

	hooks_add(bule->del_hook_id, 1, flowb_remove_bule);

	return 0;
}

/*! \} */
