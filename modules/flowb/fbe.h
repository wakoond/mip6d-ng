
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-flowb
 * \{
 */

#ifndef _FBE_H_
#define _FBE_H_

int fbe_add(struct fbe * fbe);
int fbe_del(uint16_t bid, uint16_t fid, struct flowb_key * key, int force);
int fbe_check_all(uint16_t bid, struct in6_addr * hoa, uint16_t * fids, int fids_num, unsigned char do_del);
struct fbe * fbe_find_by_fid(uint16_t fid, struct in6_addr * hoa);
void fbe_find_all_by_bid(uint16_t bid, struct in6_addr * hoa, void (* cb)(struct fbe *, void *), void * arg);

int fbe_extend_bu(void * param);
int fbe_extend_ba(void * param);

#endif /* _FBE_H_ */

/*! \} */
