
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-flowb
 * \{
 */

#ifndef MIP6D_NG_CCOM_MHO_FLOWS_H_
#define MIP6D_NG_CCOM_MHO_FLOWS_H_

/*!
 * \brief Flow Summary Mobility Option
 */
struct ip6_mh_opt_flows {
	/*! Type */
	uint8_t		ip6mos_type;
	/*! Length */
	uint8_t		ip6mos_len;
	/*! Flow Identifier */
	uint16_t    ip6mos_fid;
} __attribute__ ((packed));

/*!
 *  \brief Flow Summary MHO type
 */
#define IP6_MHOPT_FLOWS		0x2C

#endif /* MIP6D_NG_CCOM_MHO_FLOWS_H_ */

/*! \} */
