
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-flowb
 * \{
 */

#ifndef MIP6D_NG_FLOWB_BCE_H
#define MIP6D_NG_FLOWB_BCE_H

#include <mip6d-ng/fbe.h>
#include <mip6d-ng/bce.h>

/*!
 * \brief BCE extension: new flow
 */
struct bce_ext_fbe_new {
	/*! Extension header */
	struct bce_ext ext;
	/*! New flow */
	struct fbe * fbe;
};

/*!
 * \brief BCE extension: update flow
 */
struct bce_ext_fbe_update {
	/*! Extension header */
	struct bce_ext ext;
	/*! Identifier to update */
	uint16_t fid;
	/*! New BID value */
	uint16_t bid;
};

/*!
 * \brief BCE extension: flow identifiers
 */
struct bce_ext_fbe {
	/*! Extension header */
	struct bce_ext ext;
	/*! List of Flow Identifiers, parsed from the BU message */
	uint16_t * fids;
	/*! Number of the Flow Identifiers */
	uint16_t num;
};

#endif /* MIP6D_NG_FLOWB_BCE_H */

/*! \} */
