
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-flowb
 * \{
 */

#ifndef MIP6D_NG_CCOM_MHO_FLOWI_H_
#define MIP6D_NG_CCOM_MHO_FLOWI_H_

/*!
 * \brief Flow Identifier Mobility Option
 */
struct ip6_mh_opt_flowi {
	/*! Type */
	uint8_t		ip6mof_type;
	/*! Length */
	uint8_t		ip6mof_len;
	/*! Flow Identifier */
	uint16_t    ip6mof_fid;
	/*! Flow Priority */
	uint16_t    ip6mof_fpri;
	/*! Reserved */
	uint8_t		ip6mof_reserved;
	/*! Status */
	uint8_t     ip6mof_status;
} __attribute__ ((packed));

/*!
 *  \brief Flow Identifier MHO type
 */
#define IP6_MHOPT_FLOWI		0x2D

/*!
 * \brief Flow Identification Option
 */
struct ip6fli_opt {
	/*! Option Type */
	uint8_t		ip6fliopt_type;
	/*! Option Length */
	uint8_t		ip6fliopt_len;
} __attribute__ ((packed));

/*!
 * \brief Flow Identification Option: BID
 */
struct ip6fli_bid {
	/*! Option Type */
	uint8_t		ip6flib_type;
	/*! Option Length */
	uint8_t		ip6flib_len;
	/*! BID */
	uint16_t	ip6flib_bid;
} __attribute__ ((packed));

/*!
 * \brief Flow Identification BID option type
 */
#define IP6_MHOPT_FLOWI_SUBOPT_BID	0x02

/*!
 * \brief Flow Identification Option: Traffic Selector
 */
struct ip6fli_ts {
	/*! Option Type */
	uint8_t		ip6flits_type;
	/*! Option Length */
	uint8_t		ip6flits_len;
	/*! Traffic Selector Format */
	uint8_t		ip6flits_format;
	/*! Reserved */
	uint8_t		ip6flits_reserved;
} __attribute__ ((packed));

/*!
 * \brief Flow Identification Traffic Selector option type 
 */
#define IP6_MHOPT_FLOWI_SUBOPT_TS	0x03

#endif /* MIP6D_NG_CCOM_MHO_FLOWI_H_ */

/*! \} */
