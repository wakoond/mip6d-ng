
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-flowb
 * \{
 */

#ifndef _MIP6D_NG_FBE_H_
#define _MIP6D_NG_FBE_H_

#include <config.h>
#include <netinet/in.h>
#include <inttypes.h>
#include <pthread.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/flowb.h>

/*!
 * \brief Flow Binding Entry 
 */
struct fbe {
	/*! Linked list management */
	struct list_head list;
	/*! FBE lock */
	pthread_mutex_t _lock;

	/*! Flow Identifier */
	uint16_t fid;
	/*! Flow priority */
	uint16_t prio;

	/*! Traffic Selector */
	struct ts {
		/*! Source address */
		struct in6_addr src;
		/*! Source prefix length */
		uint8_t splen;
		/*! Source port (start or the only one) */
		uint16_t sport1;
		/*! Source port (end) */
		uint16_t sport2;
		/*! Destination address */
		struct in6_addr dst;
		/*! Destination prefix length */
		uint8_t dplen;
		/*! Destination port (start or the only one) */
		uint16_t dport1;
		/*! Destination port (end) */
		uint16_t dport2;
		/*! SPI value (start or the only one) */
		uint32_t spi1;
		/*! SPI value (end) */
		uint32_t spi2;
		/*! Flow label (start or the only one) */
		uint32_t fl1;
		/*! Flow label (end) */
		uint32_t fl2;
		/*! Traffic Class (start or the only one) */
		uint8_t tc1;
		/*! Traffic class (end) */
		uint8_t tc2;
		/*! Protocol identifier (start or the only one) */
		uint8_t proto1;
		/*! Protocol identifier (end) */
		uint8_t proto2;
	} ts;

	/*! Binding Identifier */
	uint16_t bid;

	/*! Flow Binding Entry status */
	uint32_t status;

	/*! If non-zero the FBE is active */
	uint8_t active;
	/*! Callback to activate the entry */
	int (* activate)(struct fbe *);
	/*! Callback to deactivate the entry */
	int (* inactivate)(struct fbe *);

	/*! Home Address */
	struct in6_addr hoa;
	/*! If non-zero this entry don't need to be deleted upon disconnection of the peer */
	unsigned char is_static;
	/*! Authorization key */
	struct flowb_key key;
};

/*!
 * \brief Flow Binding Entry status: new
 *
 * registered but not sent and confirmed
 */
#define FBE_STATUS_NEW			0x0001
/*!
 * \brief Flow Binding Entry status: sent
 *
 * registered and sent, but not confirmed
 */
#define FBE_STATUS_SENT			0x0002
/*!
 * \brief Flow Binding Entry status: ok
 *
 * registered, send and confirmed
 */
#define FBE_STATUS_OK			0x0004

/*! 
 * \brief Flow Binding Entry active status: active
 */
#define FBE_IS_ACTIVE			0x01
 /*! 
 * \brief Flow Binding Entry active status: inactive
 */
#define FBE_IS_INACTIVE			0x02

#ifdef LOCK_UNLOCK_DEBUG
#define fbe_lock(fbe)		do { \
		DEBUG("Locking FBE %u at %s:%d...", fbe->fid, __FUNCTION__, __LINE__); \
		pthread_mutex_lock(&fbe->_lock); \
	} while(0)

#define fbe_unlock(fbe)	do { \
		pthread_mutex_unlock(&fbe->_lock); \
		DEBUG("Unlocked FBE %u at %s:%d", fbe->fid, __FUNCTION__, __LINE__); \
	} while(0)
#else /* LOCK_UNLOCK_DEBUG */
#define fbe_lock(fbe)		do { \
		pthread_mutex_lock(&fbe->_lock); \
	} while(0)

#define fbe_unlock(fbe)	do { \
		pthread_mutex_unlock(&fbe->_lock); \
	} while(0)
#endif /* LOCK_UNLOCK_DEBUG */

#endif /* _MIP6D_NG_FBE_H_ */

/*! \} */
