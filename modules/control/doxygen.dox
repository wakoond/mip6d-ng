
/*!
 *
 * \defgroup iapi-control Module: Control
 * \ingroup iapi
 *
 * \defgroup eapi-control Module: Control
 * \ingroup eapi
 *
 * \defgroup internal-control Module: Control
 * \ingroup internal
 *
 */

/*!
 * \page bule_ctrl Binding Update List Work-flow in the Control module
 *
 * Control module manages the Binding Update List, based on events sent by the
 * data module. More about BUL work-flow in data module: \ref bule .
 * Additionally control module may performs actions regarding to BUL entries, based
 * on events from the core-comm module.
 * 
 * \dot
 * digraph bule_ctrl {
 * 	node [shape=box];
 * 	data_evt_1 [shape="ellipse", style="dashed", label="Event from data module", URL="\ref bule"];
 *	data_evt_1 -> proc_new [style="dashed", label="EVT_DATA_INIT_BULE"];
 *	proc_new [style="dashed", label="Processing new BULE"];
 *  proc_new -> set_hoa;
 * 	set_hoa [label="Set Home Address on the interface", URL="\ref ctrl_mn_bule_env_create"];
 *	set_hoa -> wait;
 *	wait [label="\"Wait\""];
 *	wait -> wait;
 *	env_evt [shape="ellipse", label="Event from environment module"];
 *	env_evt -> wait [label="EVT_ENV_ADDR"];
 *	wait -> hoa_ok;
 *	hoa_ok [label="HoemAddress DAD was successful"];
 *	hoa_ok -> send_bu;
 *	send_bu [label="Send BU"];
 *	send_bu -> has_ba;
 *	has_ba [shape="diamond", label="After a given timeout:\nDid we receive a BA?"];
 *	has_ba -> reset_intval [label="No"];
 * 	reset_intval [label="Increate the re-try interval"];
 *	reset_intval -> send_bu;
 *	has_ba -> resend_bu [label="Yes"];
 *	resend_bu [label="Binding resend if expired"];
 *	resend_bu -> send_bu;
 *	ccom_evt [shape="ellipse", label="Event from core-comm module"];
 *	ccom_evt -> proc_ba [label="EVT_CCOM_BA"];
 *	proc_ba [label="Processing BA"];
 *	proc_ba -> has_ba;
 *	proc_ba -> do_denv;
 *	do_denv [label="Create environmental stuff for data packets"];
 * 	data_evt_2 [shape="ellipse", style="dashed", label="Event from data module", URL="\ref bule"];
 *	data_evt_2 -> proc_up [style="dashed", label="EVT_DATA_CHANGE_BULE"];
 *	proc_up [style="dashed", label="Processing BULE change"];
 *	proc_up -> del_denv
 *	del_denv [label="Clean environmental stuff for data packets"]
 *	del_denv -> do_denv
 *	proc_up -> send_bu
 * 	data_hook [shape="ellipse", style="dashed", label="Run data hook", URL="\ref bule"];
 *	data_hook -> proc_del;
 *	proc_del [style="dashed", label="Processing BULE deletion"];
 *	proc_del -> find_bule_del
 * 	find_bule_del [label="Try to find another BULE,\n which provide a working path to send the Lifetime=0-BU"]
 *	find_bule_del -> del_denv
 * }
 * \enddot
 *
 * The following figure depicts the state machine of Binding Update List entries:
 *
 * \dot
 * digraph bule_states {
 *	none [label="None"]
 *	inited [label="Inited"]
 *	inited_resend [label="Inited and resend"]
 *	inited_binding_resend [label="Inited and binding done and resend"]
 *	none -> inited
 *	inited -> inited_resend [label="Sending 1st BU"]
 *	inited_resend -> inited_binding_resend [label="Receiving BA"]	
 *	inited_binding_resend -> inited_resend [label="Sending BU"]
 *	inited_resend -> none
 * }
 * \enddot
 *
 */

/*!
 * \page bce_ctrl Binding Cache Work-flow in the Control module
 *
 * Control module manages the Binding Cache, based on events sent by the
 * data module. More about BC work-flow in data module: \ref bce .
 * Additionally control module may performs actions regarding to BC entries, based
 * on events from the core-comm module.
 * 
 * \dot
 * digraph bce_ctrl {
 * 	node [shape=box];
 * 	data_evt_1 [shape="ellipse", style="dashed", label="Event from data module", URL="\ref bce"];
 *	data_evt_1 -> proc_new [style="dashed", label="EVT_DATA_NEW_BCE"];
 *	proc_new [style="dashed", label="Processing new BCE"];
 *  proc_new -> do_denv
 *	do_denv [label="Create environmental stuff for data packets\n and enable proxy ND"]
 *	do_denv -> send_ba
 *	send_ba [label="Send Binding Acknowledge"]
 * 	data_evt_2 [shape="ellipse", style="dashed", label="Event from data module", URL="\ref bce"];
 *	data_evt_2 -> proc_up [style="dashed", label="EVT_DATA_CHANGE_BCE"];
 *	proc_up [style="dashed", label="Processing BCE change"];
 *	proc_up -> del_denv
 *	del_denv [label="Clean environmental stuff for data packets"]
 *	proc_up -> do_denv
 * 	data_hook [shape="ellipse", style="dashed", label="Run data hook", URL="\ref bce"];
 *	data_hook -> proc_del;
 *	proc_del [style="dashed", label="Processing BCE deletion"];
 * 	proc_del -> del_denv
 * }
 * \enddot
 *
 */
