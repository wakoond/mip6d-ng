
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-control
 * \{
 */

#ifndef MIP6D_NG_CTRL_MAIN_H_
#define MIP6D_NG_CTRL_MAIN_H_

#include <mip6d-ng/control.h>

/*!
 * \brief Node options 
 */
union ctrl_node_opts {
	/*! Home Agent options */
	struct {
		/*! Home Agent Address */
		struct in6_addr addr;
		/*! Interface index for listening signaling */
		unsigned int ifindex;
	} ha;
	/*! Mobile Node options */
	struct {
		/*! Home Address */
		struct in6_addr hoa;
		/*! Home Agent Address */
		struct in6_addr haa;
		/*! Default Binding Lifetime */
		uint32_t lifetime;
	} mn;
	/*! Correspondent Node options */
	struct {
		/*! TBD... */
		void * opt;
	} cn;
};

/*!
 * \brief Node type 
 */
extern enum ctrl_node_type ctrl_node_type;

/*!
 * \brief Node options 
 */
extern union ctrl_node_opts ctrl_node_opts;

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
struct ctrl_opts {
	/*! Messaging IDs */
	struct {
		/*! Control module's own ID */
		int me;
		/*! Messaging ID of env module */
		int env;
		/*! Messaging ID of ccom module */
		int ccom;
		/*! Messaging ID of data module */
		int data;
		/*! Messaging ID of api module */
		int api;
	} msg_ids;
};

/*!
 * \brief Module-global data, i.e. configuration stuff
 */
extern struct ctrl_opts ctrl_opts;

#endif /* MIP6D_NG_MAIN_H_ */

/*! \} */
