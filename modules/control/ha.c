
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-control
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>
#include <pthread.h>

#include <net/if.h>
#include <arpa/inet.h>
#include <linux/if_addr.h>
#include <netinet/icmp6.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/const.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/tasks.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/missing.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/control.h>

#include <mip6d-ng/mh-bu.h>
#include <mip6d-ng/mh-ba.h>

#include "main.h"
#include "ha.h"

/*!
 * \brief Initialize Mobile Node
 *
 * Read configuration file, and created XFRM rule to
 * receive BU (home address destination option decapsulation).
 * Enable proxy ND on the interface.
 * 
 * \param cfg Confuse cfg
 * \return Zero if OK, otherwise negative
 */
int ctrl_ha_init(cfg_t * cfg)
{
	int ret;
	int len;
	char * addr;
	char * iface;
	struct msg_env_misc_proc_sys ps;
	struct msg_env_hao hao;

	DEBUG("Initializing node: HA");
	ctrl_node_type = CTRL_NODE_TYPE_HA;

	addr = cfg_getstr(cfg, "address");
	len = strlen(addr);
	if (addr == NULL || len == 0) {
		ERROR("Missing address option");
		return -1;
	}
	ret = inet_pton(AF_INET6, addr, &ctrl_node_opts.ha.addr);
	if (ret != 1) {
		ERROR("Invalid address option: '%s'", addr);
		return -1;
	}
	DEBUG("Address: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ctrl_node_opts.ha.addr));

	iface = cfg_getstr(cfg, "interface");
	len = strlen(iface);
	if (iface == NULL || len == 0) {
		ERROR("Missing interface option");
		return -1;
	}
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_GET_IFINDEX, iface, strlen(iface) + 1,
				&ctrl_node_opts.ha.ifindex, sizeof(ctrl_node_opts.ha.ifindex));
	if (ret) {
		ERROR("Invalid interface: '%s'", iface);
		return -1;
	}
	DEBUG("Interface: %s %u", iface, ctrl_node_opts.ha.ifindex);

	memset(&ps, 0, sizeof(ps));
	strncpy(ps.path, PROC_SYS_IP6_PROXY_NDP, sizeof(ps.path));
	ps.ifindex = ctrl_node_opts.ha.ifindex;
	ps.value = 1;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_MISC_PROC_SYS,
			&ps, sizeof(ps), NULL, 0);
	if (ret) {
		ERROR("Unable to enable proxy neighbor discovery on interface");
		return -1;
	}

	memset(&hao, 0, sizeof(hao));
	hao.proto = IPPROTO_MH;
	hao.type = IP6_MH_TYPE_BU;
	hao.dir = XFRM_POLICY_IN;
	hao.prio = 5;
	
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_HAO,
			&hao, sizeof(hao), NULL, 0);
	if (ret) {
		ERROR("Unable to create HAO DST opt XFRM policy: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ctrl_node_opts.ha.addr));
		return -1;
	}

	return 0;
}


/*!
 * \brief Cleanup MN functionalities
 *
 * Disables proxy ND on the interface, and 
 * deletes the XFRM rules created by the ctrl_ha_init
 * function
 * \return Zero if OK
 */
int ctrl_ha_clean()
{
	int ret;
	struct msg_env_misc_proc_sys ps;
	struct msg_env_hao hao;

	memset(&ps, 0, sizeof(ps));
	strncpy(ps.path, PROC_SYS_IP6_PROXY_NDP, sizeof(ps.path));
	ps.ifindex = ctrl_node_opts.ha.ifindex;
	ps.value = 0;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_MISC_PROC_SYS,
			&ps, sizeof(ps), NULL, 0);
	if (ret) {
		ERROR("Unable to disable proxy neighbor discovery on interface");
		return -1;
	}

	memset(&hao, 0, sizeof(hao));
	hao.proto = IPPROTO_MH;
	hao.type = IP6_MH_TYPE_BU;
	hao.dir = XFRM_POLICY_IN;
	hao.prio = 5;
	
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env, MSG_ENV_HAO_DEL,
			&hao, sizeof(hao), NULL, 0);
	if (ret) {
		ERROR("Unable to delete HAO DST opt XFRM policy: " IP6ADDR_FMT, IP6ADDR_TO_STR(&ctrl_node_opts.ha.addr));
		return -1;
	}

	return 0;
}

/*!
 * \brief Creating environmental stuff for data packets
 *
 * It creates the outgoing and incoming tunnel's XFRM representation.
 * If do_local_stuff is non-zero, it enables proxy ND for the given remote 
 * address (home address), and creates NULL XFRM decapsulation rules.
 *
 * \param local Local address (home agent address)
 * \param remote Destination address (home address)
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \param do_local_stuff Creates local-only stuff
 * \return Zero if OK
 */
static int ctrl_ha_data_env_create(struct in6_addr * local, struct in6_addr * remote, struct in6_addr * coa, unsigned int mark, char do_local_stuff)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	struct msg_env_neigh_proxy np;
	struct msg_ccom_send_na na;

	if (do_local_stuff) {
		np.ifindex = ctrl_node_opts.ha.ifindex;
		memcpy(&np.dst, remote, sizeof(struct in6_addr));
		np.flags = 0;
		/* TODO: In NEMO case: Do not forget to add NTF_ROUTER to flags */
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_NEIGH_PROXY, &np, sizeof(np), NULL, 0);
		if (ret) {
			ERROR("Unable to create proxy neighbor entry");
			return -1;
		}

		/* TODO: Handle BCE_F_L (== BU L flag) here */

		memcpy(&na.src, local, sizeof(struct in6_addr));
		memcpy(&na.dst, &in6addr_all_nodes_mc, sizeof(struct in6_addr));
		na.ifindex = ctrl_node_opts.ha.ifindex;
		memcpy(&na.target, remote, sizeof(struct in6_addr));
		na.flags = ND_NA_FLAG_OVERRIDE;
		/* TODO: In NEMO case: Do not forget to add ND_NA_FLAG_ROUTER to flags */
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.ccom,
				MSG_CCOM_SEND_NA, &na, sizeof(na), NULL, 0);
		if (ret) {
			ERROR("Unable to send neighbor advertisement");
			return -1;
		}
	
		/* TODO: Handle BCE_F_L (== BU L flag) here */
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, remote, sizeof(i6t.daddr));
	i6t.dplen = 128;
	if (mark > 0)
		i6t.mark =mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, local, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, coa, sizeof(i6t.saddr));
	i6t.splen = 128;
	memcpy(&i6t.daddr, local, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, coa, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, local, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_IN;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	if (do_local_stuff) {
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = 128;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_IN;
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}

		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = 128;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_FWD;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}
	}

	return 0;
}

/*!
 * \brief Deleting environmental stuff for data packets
 *
 * It deletes the outgoing and incoming tunnel's XFRM representation.
 * If do_local_stuff is non-zero, it disables proxy ND for the given remote 
 * address (home address), and deletes NULL XFRM decapsulation rules.
 *
 * \param local Local address (home agent address)
 * \param remote Destination address (home address)
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \param do_local_stuff Creates local-only stuff
 * \return Zero if OK
 */
static int ctrl_ha_data_env_clean(struct in6_addr * local, struct in6_addr * remote, struct in6_addr * coa, unsigned int mark, char do_local_stuff)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	struct msg_env_neigh_proxy np;
	struct msg_env_neigh n;

	if (do_local_stuff) {
		np.ifindex = ctrl_node_opts.ha.ifindex;
		memcpy(&np.dst, remote, sizeof(struct in6_addr));
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_NEIGH_PROXY_DEL, &np, sizeof(np), NULL, 0);
		if (ret) {
			ERROR("Unable to delete proxy neighbor entry");
			return -1;
		}

		/* TODO: Handle BCE_F_L (== BU L flag) here */

		n.ifindex = ctrl_node_opts.ha.ifindex;
		memcpy(&n.dst, remote, sizeof(struct in6_addr));
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_NEIGH_DEL, &n, sizeof(n), NULL, 0);
		if (ret) {
			ERROR("Unable to delete neighbor entry");
			return -1;
		}
		
		/* TODO: Handle BCE_F_L (== BU L flag) here */
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, remote, sizeof(i6t.daddr));
	i6t.dplen = 128;
	if (mark > 0)
		i6t.mark = mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, local, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, coa, sizeof(i6t.saddr));
	i6t.splen = 128;
	memcpy(&i6t.daddr, local, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, coa, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, local, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_IN;
	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}

	if (do_local_stuff) {
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = 128;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_IN;
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}

		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
		i6t.splen = 128;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_FWD;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.env,
				MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
		if (ret) {
			ERROR("Unable to create IP6IP6 XFRM tunnel");
			return -1;
		}
	}
	
	return 0;
}

/*!
 * \brief Handle Binding Cache deletion
 *
 * Deletes the environmental data stuff
 *
 * \param arg BCE
 * \return Zero if OK
 */
static int ctrl_ha_bce_del(void * arg)
{
	struct bce * bce = (struct bce *)arg;

	if (bce == NULL)
		return 0;

	bce_lock(bce);
	ctrl_ha_data_env_clean(&ctrl_node_opts.ha.addr, &bce->hoa, &bce->coa, bce->mark, (bce->hoa_cnt <= 1) ? 1 : 0);
	bce_unlock(bce);

	return 0;
}

/*!
 * \brief Argument for the ctrl_ha_send_ba_thread function
 */
struct ctrl_ha_send_ba_args {
	/*! BCE */
	struct bce * bce;
	/*! BA status */
	uint8_t status;
};

/*! 
 * \brief Thread to send Binding Acknowledge message
 *
 * \param arg struct ctrl_ha_send_ba_args
 * \return Unused
 */
static void * ctrl_ha_send_ba_thread(void * arg)
{
	int ret;
	struct ctrl_ha_send_ba_args * baa = (struct ctrl_ha_send_ba_args *)arg;
	struct msg_ccom_send_ba sba;

	if (baa == NULL)
		return NULL;
		
	if (baa->bce == NULL) {
		free(baa);
		return NULL;
	}

	bce_lock(baa->bce);

	if (baa->status == 0 && (baa->bce->flags & BCE_F_A) == 0)
		goto out;

	memcpy(&sba.src, &ctrl_node_opts.ha.addr, sizeof(struct in6_addr));
	sba.ifindex = ctrl_node_opts.ha.ifindex;
	sba.status = baa->status;
	sba.flags = 0;
	sba.seq = baa->bce->seq;
	sba.bce = baa->bce;
	
	bce_unlock(baa->bce);

	INFO("Sending BA to " IP6ADDR_FMT " Status: %u", IP6ADDR_TO_STR(&baa->bce->hoa), baa->status);

	ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.ccom, MSG_CCOM_SEND_BA, &sba, sizeof(sba), NULL, 0);
	if (ret) {
		ERROR("Unable to send BA message to the MN");
	}

out:

	refcnt_put(&baa->bce->refcnt);

	free(baa);

	return NULL;
}

/*!
 * \brief Send Binding Acknowledge
 *
 * It starts a thread to send asynchronously the BA message
 *
 * \param bce BCE
 * \param status BA status
 * \return Zero if OK 
 */
static int ctrl_ha_send_ba(struct bce * bce, uint8_t status)
{
	int ret;
	struct ctrl_ha_send_ba_args * baa = NULL;

	if (bce == NULL)
		return -1;

	baa = malloc(sizeof(struct ctrl_ha_send_ba_args));
	if (baa == NULL) {
		ERROR("Out of memory: Unable to allocate send BA worker");
		return -1;
	}
	memset(baa, 0, sizeof(struct ctrl_ha_send_ba_args));

	baa->bce = bce;
	baa->status = status;

	refcnt_get(&bce->refcnt);

	ret = threading_create(ctrl_ha_send_ba_thread, baa);

	return ret;
}

/*!
 * \brief Handle BCE initialization
 *
 * It creates the environmental data stuff, and registers the
 * control module's deletion handler function, and sends BA message
 *
 * \param bce BCE
 * \return Zero if OK
 */
int ctrl_ha_init_bce(struct bce * bce)
{
	int ret;

	DEBUG("HA: Processing BCE init...");

	ret = hooks_add(bce->del_hook_id, 10, ctrl_ha_bce_del);

	ret = ctrl_ha_data_env_create(&ctrl_node_opts.ha.addr, &bce->hoa, &bce->coa, bce->mark, (bce->hoa_cnt == 0) ? 1 : 0);
	if (ret)
		return ret;

	ctrl_ha_send_ba(bce, 0);

	return 0;
}

/*!
 * \brief Handle BCE update
 *
 * It deletes the environmental data stuff, and
 * re-creates it with the new values
 *
 * \param bce BCE
 * \return Zero if OK
 */
int ctrl_ha_bce_update(struct bce * bce)
{
	int ret;
	struct in6_addr dummy;

	memset(&dummy, 0, sizeof(struct in6_addr));

	if (memcmp(&bce->prev_coa, &dummy, sizeof(struct in6_addr)) != 0 && memcmp(&bce->prev_coa, &bce->coa, sizeof(struct in6_addr)) != 0) {
		ret = ctrl_ha_data_env_clean(&ctrl_node_opts.ha.addr, &bce->hoa, &bce->prev_coa, bce->mark, (bce->hoa_cnt == 0) ? 1 : 0);
		if (ret) {
			ERROR("Unable to clean previous DATA environment of this Binding");
			return ret;
		}

		ret = ctrl_ha_data_env_create(&ctrl_node_opts.ha.addr, &bce->hoa, &bce->coa, bce->mark, (bce->hoa_cnt == 0) ? 1 : 0);
		if (ret)
			return ret;
	}
	
	ctrl_ha_send_ba(bce, 0);

	return 0;
}

/*! \} */
