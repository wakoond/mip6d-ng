
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-control
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/md.h>
#include <mip6d-ng/environment.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bce.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/api.h>
#include <mip6d-ng/control.h>

#include "main.h"
#include "mn.h"
#include "ha.h"

/*!
 * \brief HA configuration options
 *
 * See config sample for details!
 */
cfg_opt_t ctrl_ha_opts[] = {
	CFG_STR("address", "", CFGF_NONE),
	CFG_STR("interface", "", CFGF_NONE),
	CFG_END()
};

/*!
 * \brief MN configuration options
 *
 * See config sample for details!
 */
cfg_opt_t ctrl_mn_opts[] = {
	CFG_STR("home-address", "", CFGF_NONE),
	CFG_STR("ha-address", "", CFGF_NONE),
	CFG_INT("lifetime", 60, CFGF_NONE),
	CFG_END()
};

/*!
 * \brief CN configuration options
 *
 * See config sample for details!
 */
cfg_opt_t ctrl_cn_opts[] = {
	CFG_STR("address", "", CFGF_NONE),
	CFG_END()
};

/*!
 * \brief Main control configuration options
 *
 * See config sample for details!
 */
cfg_opt_t ctrl_main_opts[] = {
	CFG_SEC("HA", ctrl_ha_opts, CFGF_NODEFAULT),
	CFG_SEC("MN", ctrl_mn_opts, CFGF_NODEFAULT),
	CFG_SEC("CN", ctrl_cn_opts, CFGF_NODEFAULT),
	CFG_END()
};

enum ctrl_node_type ctrl_node_type = CTRL_NODE_TYPE_NONE;
union ctrl_node_opts ctrl_node_opts;
struct ctrl_opts ctrl_opts;

/*!
 * \brief Message processing
 *
 * For details, please read the API documentation of the control module
 * \param sender Sender module ID
 * \param message Message ID
 * \param arg Message argument
 * \param reply_arg Reply
 * \return Zero if OK, otherwise error code; it depends on message ID
 */
static int ctrl_proc_message(unsigned int sender, unsigned int message, void * arg, void * reply_arg)
{
	int retval = -1;

	enum ctrl_node_type * cnt;
	struct in6_addr * paddr;
	struct bule * bule;

	switch(message) {
	case MSG_CTRL_GET_NODE_TYPE:
		if (reply_arg == NULL)
			break;
		cnt = (enum ctrl_node_type *)reply_arg;
		*cnt = ctrl_node_type;
		retval = 0;
		break;
	case MSG_CTRL_GET_HOA:
	case MSG_CTRL_API_GET_HOA:
		if (reply_arg == NULL)
			break;
		if (ctrl_node_type != CTRL_NODE_TYPE_MN)
			break;
		paddr = (struct in6_addr *)reply_arg;
		memcpy(paddr, &ctrl_node_opts.mn.hoa, sizeof(struct in6_addr));
		if (message == MSG_CTRL_API_GET_HOA)
			retval = sizeof(struct in6_addr);
		else
			retval = 0;
		break;
	case MSG_CTRL_GET_HAA:
	case MSG_CTRL_API_GET_HAA:
		if (reply_arg == NULL)
			break;
		if (ctrl_node_type == CTRL_NODE_TYPE_MN) {
			paddr = (struct in6_addr *)reply_arg;
			memcpy(paddr, &ctrl_node_opts.mn.haa, sizeof(struct in6_addr));
		} else if (ctrl_node_type == CTRL_NODE_TYPE_HA) {
			paddr = (struct in6_addr *)reply_arg;
			memcpy(paddr, &ctrl_node_opts.ha.addr, sizeof(struct in6_addr));
		}
		if (message == MSG_CTRL_API_GET_HAA)
			retval = sizeof(struct in6_addr);
		else
			retval = 0;
		break;
	case MSG_CTRL_RESEND_BU_NOW:
		bule = (struct bule *)arg;
		if (bule == NULL)
			break;
		retval = ctrl_mn_resend_bu_now(bule, 1);
		break;
	}

	return retval;
}

/*!
 * \brief Event processing
 *
 * For details, please read the API documentation of the control module
 * \param sender Sender module ID
 * \param event Event ID
 * \param arg Event argument
 * \param need_to_free Non-zero if arg should to be freed
 */
static void ctrl_proc_event(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free)
{
	struct bule * bule;
	struct evt_env_addr * addr;
	struct evt_ccom_ba * ba;
	struct bce * bce;

	if (sender == ctrl_opts.msg_ids.data) {
		switch (event) {
		case EVT_DATA_INIT_BULE:
			bule = (struct bule *)arg;
			if (bule == NULL)
				break;
			ctrl_mn_init_bule(bule);
			refcnt_put(&bule->refcnt);
			break;
		case EVT_DATA_CHANGE_BULE:
			bule = (struct bule *)arg;
			if (bule == NULL)
				break;
			bule_lock(bule);
			ctrl_mn_update_bule(bule);
			bule_unlock(bule);
			refcnt_put(&bule->refcnt);
			break;
		case EVT_DATA_NEW_BCE:
			bce = (struct bce *)arg;
			if (bce == NULL)
				break;
			bce_lock(bce);
			ctrl_ha_init_bce(bce);	
			bce_unlock(bce);
			refcnt_put(&bce->refcnt);
			break;
		case EVT_DATA_CHANGE_BCE:
			bce = (struct bce *)arg;
			if (bce == NULL)
				break;
			bce_lock(bce);
			ctrl_ha_bce_update(bce);	
			bce_unlock(bce);
			refcnt_put(&bce->refcnt);
			break;
		}
	} else if (sender == ctrl_opts.msg_ids.env) {
		switch (event) {
		case EVT_ENV_ADDR:
			addr = (struct evt_env_addr *)arg;
			if (addr == NULL)
				break;
			if (addr->action == EVT_ENV_ADDR_NEW)
				ctrl_mn_newaddr_event(addr->ifindex, &addr->addr);
			break;
		}
	} else if (sender == ctrl_opts.msg_ids.ccom) {
		switch (event) {
		case EVT_CCOM_BA:
			ba = (struct evt_ccom_ba *)arg;
			if (ba == NULL)
				break;
			ctrl_mn_ba_recv(ba);
			break;
		}
	}

	if (need_to_free)
		free(arg);
}

static struct imsg_opts imsg_opts = {
	.event_fp = ctrl_proc_event,
	.message_fp = ctrl_proc_message,
};

/*!
 * \brief Read CN configuration options
 *
 * Currently unused
 * 
 * \param cfg Confuse cfg
 *
 * \return Zero if OK, otherwise negative
 */
static int ctrl_cn_init(cfg_t * cfg)
{
	return 0;
}

/*!
 * \brief Start control module
 *
 * Connect to data module and subscribe for events
 *
 * \param arg Unused
 * \return Zero if OK
 */
static int ctrl_init2(void * arg)
{
	int ret;

	DEBUG("Start module: Control");

	ret = imsg_get_id(MSG_DATA);
	if (ret < 0) {
		ERROR("Unable to find module: data");
		return -1;
	}
	ctrl_opts.msg_ids.data = ret;

	switch (ctrl_node_type) {
	case CTRL_NODE_TYPE_MN:
		ret = imsg_event_subscribe(ctrl_opts.msg_ids.data, EVT_DATA_INIT_BULE, ctrl_opts.msg_ids.me);
		ret |= imsg_event_subscribe(ctrl_opts.msg_ids.data, EVT_DATA_CHANGE_BULE, ctrl_opts.msg_ids.me);
		ret |= imsg_event_subscribe(ctrl_opts.msg_ids.ccom, EVT_CCOM_BA, ctrl_opts.msg_ids.me);
		if (ret < 0) {
			ERROR("Unable to subscribe for BULE events");
			return -1;
		}

		ret = imsg_event_subscribe(ctrl_opts.msg_ids.env, EVT_ENV_ADDR, ctrl_opts.msg_ids.me);
		if (ret < 0) {
			ERROR("Unable to subscribe for ADDR events");
			return -1;
		}

		break;
	case CTRL_NODE_TYPE_HA:
		ret = imsg_event_subscribe(ctrl_opts.msg_ids.data, EVT_DATA_NEW_BCE, ctrl_opts.msg_ids.me);
		ret |= imsg_event_subscribe(ctrl_opts.msg_ids.data, EVT_DATA_CHANGE_BCE, ctrl_opts.msg_ids.me);
		if (ret < 0) {
			ERROR("Unable to subscribe for BCE events");
			return -1;
		}

		break;
	case CTRL_NODE_TYPE_CN:

		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}

	return 0;
}

/*!
 * \brief Cleanup
 *
 * Clean HA, MN, CN functionalities
 *
 * \param arg Unused
 * \return Zero
 */
static int ctrl_exit_handler(void * arg)
{
	switch (ctrl_node_type) {
	case CTRL_NODE_TYPE_HA:
		ctrl_ha_clean();
		break;
	case CTRL_NODE_TYPE_MN:
		ctrl_mn_clean();
		break;
	case CTRL_NODE_TYPE_CN:
		break;
	case CTRL_NODE_TYPE_NONE:
		//Avoid warning: warning: enumeration value ‘CTRL_NODE_TYPE_NONE’ not handled in switch
		break;
	}
	return 0;
}

/*!
 * \brief Initializing control module
 *
 * Registers exit function to the 'core-exit' hook
 * Register internal messaging.
 * Read and parse configuration.
 * Initialize HA, MN, CN functionalities.
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of non-processed command line arguments
 * \param r_argv Non-processed command line arguments
 * \param cfg Configuration handler
 * \return Zero if OK
 */
int ctrl_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg)
{
	int ret;
	unsigned int core_exit_id;
	unsigned int all_inited_id;
	cfg_t * node;
	struct msg_api_register_cmd api_rc;

	DEBUG("Initializing module: control");

	ret = imsg_register(MSG_CTRL, &imsg_opts);
	if (ret < 0)
		return ret;
	ctrl_opts.msg_ids.me = ret;

	ret = imsg_get_id(MSG_ENV);
	if (ret < 0) {
		ERROR("Unable to find module: environment");
		return -1;
	}
	ctrl_opts.msg_ids.env = ret;

	ret = imsg_get_id(MSG_CCOM);
	if (ret < 0) {
		ERROR("Unable to find module: core-comm");
		return -1;
	}
	ctrl_opts.msg_ids.ccom = ret;

	ret = imsg_get_id(MSG_API);
	if (ret < 0) {
		DEBUG("Unable to find module: API");
		ctrl_opts.msg_ids.api = -1;
	} else {
		ctrl_opts.msg_ids.api = ret;
	}

	ret = hooks_get_id("core-exit");
	if (ret >= 0) {
		core_exit_id = (unsigned int)ret;
		hooks_add(core_exit_id, 100-SEQ_CTRL, ctrl_exit_handler);
	}

	ret = hooks_get_id("core-all-inited");
	if (ret >= 0) {
		all_inited_id = (unsigned int)ret;
		hooks_add(all_inited_id, 2, ctrl_init2);
	}

	if (cfg == NULL)
		return -1;
	node = cfg_getsec(cfg, "ha");
	if (node != NULL) {
		ret = ctrl_ha_init(node);
		
		if (ctrl_opts.msg_ids.api >= 0) {
			memset(&api_rc, 0, sizeof(api_rc));
			api_rc.command = API_CTRL_GET_HAA;
			snprintf(api_rc.command_name, sizeof(api_rc.command_name), "CTRL_GET_HAA");
			api_rc.imsg_cmd = MSG_CTRL_API_GET_HAA;
			ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
			if (ret) {
				ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
			}
		}
	} else {
		node = cfg_getsec(cfg, "mn");
			if (node != NULL) {
				ret = ctrl_mn_init(node);

				if (ctrl_opts.msg_ids.api >= 0) {
					memset(&api_rc, 0, sizeof(api_rc));
					api_rc.command = API_CTRL_GET_HAA;
					snprintf(api_rc.command_name, sizeof(api_rc.command_name), "CTRL_GET_HAA");
					api_rc.imsg_cmd = MSG_CTRL_API_GET_HAA;
					ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
					if (ret) {
						ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
					}

					api_rc.command = API_CTRL_GET_HOA;
					snprintf(api_rc.command_name, sizeof(api_rc.command_name), "CTRL_GET_HOA");
					api_rc.imsg_cmd = MSG_CTRL_API_GET_HOA;
					ret = IMSG_MSG_ARGS(ctrl_opts.msg_ids.me, ctrl_opts.msg_ids.api, MSG_API_REGISTER_CMD, &api_rc, sizeof(api_rc), NULL, 0);
					if (ret) {
						ERROR("Unable to register API command: %u %s", api_rc.command, api_rc.command_name);
					}
				}
			} else {
				node = cfg_getsec(cfg, "cn");
					if (node != NULL) {
						ret = ctrl_cn_init(node);
					} else {
						ERROR("Invalid Node configuration. Not HA/MN/CN.");
						return -1;
					}
			}
	}
	if (ret)
		return ret;

	return 0;
}

MIP6_MODULE_INIT(MSG_CTRL, SEQ_CTRL, ctrl_module_init, ctrl_main_opts, ctrl_ha_opts, ctrl_mn_opts, ctrl_cn_opts);

/*! \} */

