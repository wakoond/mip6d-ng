
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MIP6D_NG_ENVT_XFRM_H
#define MIP6D_NG_ENVT_XFRM_H

int env_test_xfrm_ip6ip6(struct test_options * test_options, int cmd, int flush);
int env_test_xfrm_esp(struct test_options * test_options, int pol_cmd, int sa_cmd, int flush);

#endif /* MIP6D_NG_ENVT_XFRM_H */
