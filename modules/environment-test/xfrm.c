
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "main.h"
#include "xfrm.h"

int env_test_xfrm_ip6ip6(struct test_options * test_options, int cmd, int flush)
{
	int ret;
	unsigned int i;
	struct msg_env_ip6ip6_tunnel i6t;

	if (test_options->flush == 1 && flush) {
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, MSG_ENV_POLICY_FLUSH, 
				NULL, 0, NULL, 0);
		if (ret != 0) {
			ERROR("Unable to flush XFRM policies");
			return -1;
		}
		DEBUG("ENV TEST: XFRM policies flushed");

		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, MSG_ENV_SA_FLUSH, 
				NULL, 0, NULL, 0);
		if (ret != 0) {
			ERROR("Unable to flush XFRM SAs");
			return -1;
		}
		DEBUG("ENV TEST: XFRM SAs flushed");

	}

	if (test_options->node == ENV_TEST_NODE_MR) {
		for (i = 0; i < test_options->num; i++) {
			memset(&i6t, 0, sizeof(i6t));
			memcpy(&i6t.saddr, &test_options->hoa, sizeof(i6t.saddr));
			i6t.splen = 128;
			i6t.prio = 10;
			memcpy(&i6t.tmpl_src, &test_options->coas[i], sizeof(i6t.tmpl_src));
			memcpy(&i6t.tmpl_dst, &test_options->ha, sizeof(i6t.tmpl_dst));
			i6t.dir = XFRM_POLICY_OUT;
			i6t.mark = test_options->bids[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
					&i6t, sizeof(i6t), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do IP6IP6 xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: IP6IP6 xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->bids[i]);
		}
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, &test_options->hoa, sizeof(i6t.saddr));
		i6t.splen = 128;
		i6t.prio = 99;
		memcpy(&i6t.tmpl_src, &test_options->coas[0], sizeof(i6t.tmpl_src));
		memcpy(&i6t.tmpl_dst, &test_options->ha, sizeof(i6t.tmpl_dst));
		i6t.dir = XFRM_POLICY_OUT;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&i6t, sizeof(i6t), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do IP6IP6 xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]), IP6ADDR_TO_STR(&test_options->ha));
			return -1;
		}
		DEBUG("ENV TEST: IP6IP6 xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]), IP6ADDR_TO_STR(&test_options->ha));
		for (i = 0; i < test_options->num; i++) {
			memset(&i6t, 0, sizeof(i6t));
			memcpy(&i6t.saddr, &test_options->ha, sizeof(i6t.saddr));
			i6t.splen = 128;
			memcpy(&i6t.daddr, &test_options->coas[i], sizeof(i6t.daddr));
			i6t.dplen = 128;
			i6t.prio = 10;
			memcpy(&i6t.tmpl_src, &test_options->ha, sizeof(i6t.tmpl_src));
			memcpy(&i6t.tmpl_dst, &test_options->coas[i], sizeof(i6t.tmpl_dst));
			i6t.dir = XFRM_POLICY_IN;
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
					&i6t, sizeof(i6t), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do IP6IP6 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha));
				return -1;
			}
			DEBUG("ENV TEST: IP6IP6 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha));
		}
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.daddr, &test_options->hoa, sizeof(i6t.daddr));
		i6t.dplen = 128;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_IN;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&i6t, sizeof(i6t), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do IP6IP6 xfrm: dst " IP6ADDR_FMT " IN tmpl src :: dst ::" , 
					IP6ADDR_TO_STR(&test_options->hoa));
			return -1;
		}
		DEBUG("ENV TEST: IP6IP6 xfrm: dst " IP6ADDR_FMT " IN tmpl src :: dst ::" , 
					IP6ADDR_TO_STR(&test_options->hoa)); 
	} else {
		for (i = 0; i < test_options->num; i++) {
			memset(&i6t, 0, sizeof(i6t));
			memcpy(&i6t.daddr, &test_options->hoa, sizeof(i6t.daddr));
			i6t.dplen = 128;
			i6t.prio = 10;
			memcpy(&i6t.tmpl_dst, &test_options->coas[i], sizeof(i6t.tmpl_dst));
			memcpy(&i6t.tmpl_src, &test_options->ha, sizeof(i6t.tmpl_src));
			i6t.dir = XFRM_POLICY_OUT;
			i6t.mark = test_options->bids[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
					&i6t, sizeof(i6t), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do IP6IP6 xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: IP6IP6 xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->bids[i]);	
		}
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.daddr, &test_options->hoa, sizeof(i6t.daddr));
		i6t.dplen = 128;
		i6t.prio = 99;
		memcpy(&i6t.tmpl_dst, &test_options->coas[0], sizeof(i6t.tmpl_dst));
		memcpy(&i6t.tmpl_src, &test_options->ha, sizeof(i6t.tmpl_src));
		i6t.dir = XFRM_POLICY_OUT;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&i6t, sizeof(i6t), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do IP6IP6 xfrm: dst " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]), IP6ADDR_TO_STR(&test_options->ha));
			return -1;
		}
		DEBUG("ENV TEST: IP6IP6 xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]), IP6ADDR_TO_STR(&test_options->ha));
		for (i = 0; i < test_options->num; i++) {
			memset(&i6t, 0, sizeof(i6t));
			memcpy(&i6t.daddr, &test_options->ha, sizeof(i6t.daddr));
			i6t.dplen = 128;
			memcpy(&i6t.saddr, &test_options->coas[i], sizeof(i6t.saddr));
			i6t.splen = 128;
			i6t.prio = 10;
			memcpy(&i6t.tmpl_dst, &test_options->ha, sizeof(i6t.tmpl_dst));
			memcpy(&i6t.tmpl_src, &test_options->coas[i], sizeof(i6t.tmpl_src));
			i6t.dir = XFRM_POLICY_IN;
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
					&i6t, sizeof(i6t), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do IP6IP6 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha));
				return -1;
			}
			DEBUG("ENV TEST: IP6IP6 xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha));
		}
		memset(&i6t, 0, sizeof(i6t));
		memcpy(&i6t.saddr, &test_options->hoa, sizeof(i6t.saddr));
		i6t.splen = 128;
		i6t.prio = 10;
		i6t.dir = XFRM_POLICY_IN;
		i6t.skip_state = 1;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&i6t, sizeof(i6t), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do IP6IP6 xfrm: src " IP6ADDR_FMT " IN tmpl src :: dst ::" , 
					IP6ADDR_TO_STR(&test_options->hoa));
			return -1;
		}
		DEBUG("ENV TEST: IP6IP6 xfrm: src " IP6ADDR_FMT " IN tmpl src :: dst ::" , 
					IP6ADDR_TO_STR(&test_options->hoa)); 
	}
	
	return 0;
}

int env_test_xfrm_esp(struct test_options * test_options, int pol_cmd, int sa_cmd, int flush)
{
	int ret;
	unsigned int i;
	struct msg_env_esp_tunnel et;
	struct msg_env_esp_tunnel_sa es;

	if (test_options->flush == 1 && flush) {
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, MSG_ENV_POLICY_FLUSH, 
				NULL, 0, NULL, 0);
		if (ret != 0) {
			ERROR("Unable to flush XFRM policies");
			return -1;
		}
		DEBUG("ENV TEST: XFRM policies flushed");

		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, MSG_ENV_SA_FLUSH, 
				NULL, 0, NULL, 0);
		if (ret != 0) {
			ERROR("Unable to flush XFRM SAs");
			return -1;
		}
		DEBUG("ENV TEST: XFRM SAs flushed");

	}

	if (test_options->node == ENV_TEST_NODE_MR) {
		for (i = 0; i < test_options->num; i++) {
			memset(&et, 0, sizeof(et));
			memcpy(&et.saddr, &test_options->hoa, sizeof(et.saddr));
			et.splen = 128;
			et.prio = 10;
			memcpy(&et.tmpl_src, &test_options->coas[i], sizeof(et.tmpl_src));
			memcpy(&et.tmpl_dst, &test_options->ha, sizeof(et.tmpl_dst));
			et.dir = XFRM_POLICY_OUT;
			et.mark = test_options->bids[i];
			et.tmpl_spi = test_options->spi + i;
			et.tmpl_reqid = test_options->reqid;
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
					&et, sizeof(et), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->bids[i]);

			memset(&es, 0, sizeof(es));
			memcpy(&es.saddr, &test_options->coas[i], sizeof(es.saddr));
			memcpy(&es.daddr, &test_options->ha, sizeof(es.daddr));
			es.spi = test_options->spi + i;
			es.reqid = test_options->reqid;
			strncpy(es.auth_name, test_options->auth_name, sizeof(es.auth_name));
			strncpy(es.auth_key, test_options->auth_key, sizeof(es.auth_key));
			strncpy(es.enc_name, test_options->enc_name, sizeof(es.enc_name));
			strncpy(es.enc_key, test_options->enc_key, sizeof(es.enc_key));

			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
					&es, sizeof(es), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->spi + i, test_options->reqid);
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->spi + i, test_options->reqid);
		}
		memset(&et, 0, sizeof(et));
		memcpy(&et.saddr, &test_options->hoa, sizeof(et.saddr));
		et.splen = 128;
		et.prio = 99;
		memcpy(&et.tmpl_src, &test_options->coas[0], sizeof(et.tmpl_src));
		memcpy(&et.tmpl_dst, &test_options->ha, sizeof(et.tmpl_dst));
		et.dir = XFRM_POLICY_OUT;
		et.tmpl_spi = test_options->spi + 0;
		et.tmpl_reqid = test_options->reqid;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&et, sizeof(et), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]), IP6ADDR_TO_STR(&test_options->ha));
			return -1;
		}
		DEBUG("ENV TEST: ESP xfrm: src " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->coas[0]), IP6ADDR_TO_STR(&test_options->ha));
		for (i = 0; i < test_options->num; i++) {
			memset(&et, 0, sizeof(et));
			memcpy(&et.saddr, &test_options->ha, sizeof(et.saddr));
			et.splen = 128;
			memcpy(&et.daddr, &test_options->coas[i], sizeof(et.daddr));
			et.dplen = 128;
			et.prio = 10;
			memcpy(&et.tmpl_src, &test_options->ha, sizeof(et.tmpl_src));
			memcpy(&et.tmpl_dst, &test_options->coas[i], sizeof(et.tmpl_dst));
			et.dir = XFRM_POLICY_IN;
			et.tmpl_spi = test_options->spi + test_options->num + i;
			et.tmpl_reqid = test_options->reqid;
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
					&et, sizeof(et), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]));
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]));

			memset(&es, 0, sizeof(es));
			memcpy(&es.saddr, &test_options->ha, sizeof(es.saddr));
			memcpy(&es.daddr, &test_options->coas[i], sizeof(es.daddr));
			es.spi = test_options->spi + test_options->num + i;
			es.reqid = test_options->reqid;
			strncpy(es.auth_name, test_options->auth_name, sizeof(es.auth_name));
			strncpy(es.auth_key, test_options->auth_key, sizeof(es.auth_key));
			strncpy(es.enc_name, test_options->enc_name, sizeof(es.enc_name));
			strncpy(es.enc_key, test_options->enc_key, sizeof(es.enc_key));

			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
					&es, sizeof(es), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]),
						test_options->spi + test_options->num + i, test_options->reqid);
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]),
						test_options->spi + test_options->num + i, test_options->reqid);
		}
		memset(&et, 0, sizeof(et));
		memcpy(&et.daddr, &test_options->hoa, sizeof(et.daddr));
		et.dplen = 128;
		et.prio = 10;
		et.dir = XFRM_POLICY_IN;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&et, sizeof(et), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP xfrm: dst " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa));
			return -1;
		}
		DEBUG("ENV TEST: ESP xfrm: dst " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa));
	} else {
		for (i = 0; i < test_options->num; i++) {
			memset(&et, 0, sizeof(et));
			memcpy(&et.daddr, &test_options->hoa, sizeof(et.daddr));
			et.dplen = 128;
			et.prio = 10;
			memcpy(&et.tmpl_dst, &test_options->coas[i], sizeof(et.tmpl_dst));
			memcpy(&et.tmpl_src, &test_options->ha, sizeof(et.tmpl_src));
			et.dir = XFRM_POLICY_OUT;
			et.mark = test_options->bids[i];
			et.tmpl_spi = test_options->spi + test_options->num + i;
			et.tmpl_reqid = test_options->reqid;
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
					&et, sizeof(et), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm: dst " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]),
						test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm: dst " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT " mark %u", 
						IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]),
						test_options->bids[i]);

			memset(&es, 0, sizeof(es));
			memcpy(&es.daddr, &test_options->coas[i], sizeof(es.daddr));
			memcpy(&es.saddr, &test_options->ha, sizeof(es.saddr));
			es.spi = test_options->spi + test_options->num + i;
			es.reqid = test_options->reqid;
			strncpy(es.auth_name, test_options->auth_name, sizeof(es.auth_name));
			strncpy(es.auth_key, test_options->auth_key, sizeof(es.auth_key));
			strncpy(es.enc_name, test_options->enc_name, sizeof(es.enc_name));
			strncpy(es.enc_key, test_options->enc_key, sizeof(es.enc_key));

			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
					&es, sizeof(es), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]),
						test_options->spi + test_options->num + i, test_options->reqid);
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]),
						test_options->spi + test_options->num + i, test_options->reqid);
		}
		memset(&et, 0, sizeof(et));
		memcpy(&et.daddr, &test_options->hoa, sizeof(et.daddr));
		et.dplen = 128;
		et.prio = 99;
		memcpy(&et.tmpl_dst, &test_options->coas[0], sizeof(et.tmpl_dst));
		memcpy(&et.tmpl_src, &test_options->ha, sizeof(et.tmpl_src));
		et.dir = XFRM_POLICY_OUT;
		et.tmpl_spi = test_options->spi + test_options->num + 0;
		et.tmpl_reqid = test_options->reqid;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&et, sizeof(et), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP xfrm: dst " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]));
			return -1;
		}
		DEBUG("ENV TEST: ESP xfrm: dst " IP6ADDR_FMT " OUT tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
					IP6ADDR_TO_STR(&test_options->hoa), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]));
		for (i = 0; i < test_options->num; i++) {
			memset(&et, 0, sizeof(et));
			memcpy(&et.saddr, &test_options->coas[i], sizeof(et.saddr));
			et.splen = 128;
			memcpy(&et.daddr, &test_options->ha, sizeof(et.daddr));
			et.dplen = 128;
			et.prio = 10;
			memcpy(&et.tmpl_src, &test_options->coas[i], sizeof(et.tmpl_src));
			memcpy(&et.tmpl_dst, &test_options->ha, sizeof(et.tmpl_dst));
			et.dir = XFRM_POLICY_IN;
			et.tmpl_spi = test_options->spi + i;
			et.tmpl_reqid = test_options->reqid;
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
					&et, sizeof(et), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha));
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " IN tmpl src " IP6ADDR_FMT " dst " IP6ADDR_FMT , 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha), IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha));

			memset(&es, 0, sizeof(es));
			memcpy(&es.saddr, &test_options->coas[i], sizeof(es.saddr));
			memcpy(&es.daddr, &test_options->ha, sizeof(es.daddr));
			es.spi = test_options->spi + i;
			es.reqid = test_options->reqid;
			strncpy(es.auth_name, test_options->auth_name, sizeof(es.auth_name));
			strncpy(es.auth_key, test_options->auth_key, sizeof(es.auth_key));
			strncpy(es.enc_name, test_options->enc_name, sizeof(es.enc_name));
			strncpy(es.enc_key, test_options->enc_key, sizeof(es.enc_key));

			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, sa_cmd, 
					&es, sizeof(es), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->spi + i, test_options->reqid);
				return -1;
			}
			DEBUG("ENV TEST: ESP xfrm SA: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " spi %d reqid %d", 
						IP6ADDR_TO_STR(&test_options->coas[i]), IP6ADDR_TO_STR(&test_options->ha),
						test_options->spi + i, test_options->reqid);
		}
		memset(&et, 0, sizeof(et));
		memcpy(&et.saddr, &test_options->hoa, sizeof(et.saddr));
		et.splen = 128;
		et.prio = 10;
		et.dir = XFRM_POLICY_IN;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, pol_cmd, 
				&et, sizeof(et), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do ESP xfrm: src " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa));
			return -1;
		}
		DEBUG("ENV TEST: ESP xfrm: src " IP6ADDR_FMT " IN", 
					IP6ADDR_TO_STR(&test_options->hoa));
	}

	return 0;
}
