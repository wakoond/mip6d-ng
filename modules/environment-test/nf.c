
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "main.h"
#include "nf.h"

int env_test_nf(struct test_options * test_options, int cmd, int cmd_mark, int flush)
{
	int ret;
	unsigned int i;
	struct msg_env_nf_to_mip6d_ng nf2;
	struct msg_env_nf_mark_and_acc nfm;
	struct msg_env_nf_flush nff;

	if (test_options->flush == 1 && flush) {
		memset(&nff, 0, sizeof(nff));
		snprintf(nff.name, sizeof(nff.name), "OUTPUT");
		nff.name_builtin = 1;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, MSG_ENV_NF_FLUSH, 
				&nff, sizeof(nff), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to flush NF policies: OUTPUT");
			return -1;
		}
		DEBUG("ENV TEST: NF policies flushed: OUTPUT");

		memset(&nff, 0, sizeof(nff));
		snprintf(nff.name, sizeof(nff.name), "PREROUTING");
		nff.name_builtin = 1;
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, MSG_ENV_NF_FLUSH, 
				&nff, sizeof(nff), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to flush NF policies: PREROUTING");
			return -1;
		}
		DEBUG("ENV TEST: NF policies flushed: PREROUTING");

		for (i = 0; i < test_options->num; i++) {
			memset(&nff, 0, sizeof(nff));
			snprintf(nff.name, sizeof(nff.name), "BID%u", test_options->bids[i]);
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, MSG_ENV_NF_FLUSH, 
					&nff, sizeof(nff), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to flush NF policies: %s", nff.name);
				return -1;
			}
			DEBUG("ENV TEST: NF policies flushed: %s", nff.name);
		}
	}

	if (test_options->node == ENV_TEST_NODE_MR) {
		memset(&nf2, 0, sizeof(nf2));
		memcpy(&nf2.src, &test_options->hoa, sizeof(nf2.src));
		snprintf(nf2.chain, sizeof(nf2.chain), "OUTPUT");
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&nf2, sizeof(nf2), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do NF rule to MIP6D_NG: OUTPUT src " IP6ADDR_FMT,
					IP6ADDR_TO_STR(&test_options->hoa));
			return -1;
		}
		DEBUG("ENV TEST: NF rule to MIP6D_NG OUTPUT src " IP6ADDR_FMT, 
				IP6ADDR_TO_STR(&test_options->hoa));

		for (i = 0; i < test_options->num; i++) {
			memset(&nfm, 0, sizeof(nfm));
			snprintf(nfm.name, sizeof(nfm.name), "BID%u", test_options->bids[i]);
			nfm.mark = test_options->bids[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd_mark, 
					&nfm, sizeof(nfm), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do NF rule to MARK and ACCEPT: bid %u", test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: NF rule to MARK and ACCPET bid %u", test_options->bids[i]);
		}
	} else {
		memset(&nf2, 0, sizeof(nf2));
		memcpy(&nf2.dst, &test_options->hoa, sizeof(nf2.dst));
		snprintf(nf2.chain, sizeof(nf2.chain), "OUTPUT");
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&nf2, sizeof(nf2), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do NF rule to MIP6D_NG: OUTPUT dst " IP6ADDR_FMT,
					IP6ADDR_TO_STR(&test_options->hoa));
			return -1;
		}
		DEBUG("ENV TEST: NF rule to MIP6D_NG OUTPUT dst " IP6ADDR_FMT, 
				IP6ADDR_TO_STR(&test_options->hoa));

		memset(&nf2, 0, sizeof(nf2));
		memcpy(&nf2.dst, &test_options->hoa, sizeof(nf2.dst));
		snprintf(nf2.chain, sizeof(nf2.chain), "PREROUTING");
		ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
				&nf2, sizeof(nf2), NULL, 0);
		if (ret != 0) {
			ERROR("Unable to do NF rule to MIP6D_NG: PREROUTING dst " IP6ADDR_FMT,
					IP6ADDR_TO_STR(&test_options->hoa));
			return -1;
		}
		DEBUG("ENV TEST: NF rule to MIP6D_NG PREROUTING dst " IP6ADDR_FMT, 
				IP6ADDR_TO_STR(&test_options->hoa));

		for (i = 0; i < test_options->num; i++) {
			memset(&nfm, 0, sizeof(nfm));
			snprintf(nfm.name, sizeof(nfm.name), "BID%u", test_options->bids[i]);
			nfm.mark = test_options->bids[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd_mark, 
					&nfm, sizeof(nfm), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do NF rule to MARK and ACCEPT: bid %u", test_options->bids[i]);
				return -1;
			}
			DEBUG("ENV TEST: NF rule to MARK and ACCPET bid %u", test_options->bids[i]);
		}
	}

	return 0;
}

