
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "main.h"
#include "route.h"

int env_test_route(struct test_options * test_options, int cmd)
{
	int ret;
	unsigned int i;
	struct msg_env_route r;

	if (test_options->node == ENV_TEST_NODE_MR) {
		for (i = 0; i < test_options->num; i++) {
			memset(&r, 0, sizeof(r));
			memcpy(&r.gw, &test_options->gws[i], sizeof(r.gw));
			r.entries |= MSG_ENV_ROUTE_GW;
			r.oiface = test_options->ifindicies[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
					&r, sizeof(r), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do route: default via " IP6ADDR_FMT " dev %d",
						IP6ADDR_TO_STR(&test_options->gws[i]), test_options->ifindicies[i]);
				return -1;
			}
			DEBUG("ENV TEST: Default route via " IP6ADDR_FMT " dev %d", 
					IP6ADDR_TO_STR(&test_options->gws[i]), test_options->ifindicies[i]);
		}
	} else {
		for (i = 0; i < test_options->num; i++) {
			memset(&r, 0, sizeof(r));
			memcpy(&r.dst, &test_options->hoa, sizeof(r.dst));
			r.dplen = 64;
			memcpy(&r.gw, &test_options->gws[i], sizeof(r.gw));
			r.entries |= MSG_ENV_ROUTE_GW;
			r.oiface = test_options->ifindicies[i];
			ret = IMSG_MSG_ARGS(test_options->imid, test_options->envid, cmd, 
					&r, sizeof(r), NULL, 0);
			if (ret != 0) {
				ERROR("Unable to do route: " IP6ADDR_FMT "/64 via " IP6ADDR_FMT " dev %d",
						IP6ADDR_TO_STR(&test_options->hoa),
						IP6ADDR_TO_STR(&test_options->gws[i]), test_options->ifindicies[i]);
				return -1;
			}
			DEBUG("ENV TEST: Route " IP6ADDR_FMT "/64 via " IP6ADDR_FMT " dev %d", 
						IP6ADDR_TO_STR(&test_options->hoa),
					IP6ADDR_TO_STR(&test_options->gws[i]), test_options->ifindicies[i]);
		}
	}
	
	return 0;
}

