
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-logger-api
 * \{
 */

#ifndef MIP6D_NG_LOGGER_API_H
#define MIP6D_NG_LOGGER_API_H

/*! 
 * \brief The name of the internal messaging reference point
 */
#define MSG_LOGA		"loga"

/*!
 * \brief Module load order number of control module
 */
#define SEQ_LOGA		3

/*!
 * \brief Available data API message commands
 */
enum loga_messages {
	/*!
	 * \brief Send message to mip6d-ng log
	 *
	 * Argument: char *
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_LOGA_LOG,
	MSG_LOGA_NONE
};

/*!
 * \brief Available data API commands by the api module 
 * \ingroup eapi-logger-api
 */
enum loga_api_commands {
	/*! Send message to mip6d-ng log */
	API_LOGA_LOG,
	API_LOGA_NONE
};

#endif /* MIP6D_NG_LOGGER_API_H */

/*! \} */
