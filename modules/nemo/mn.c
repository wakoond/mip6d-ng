
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2013 Commsignia Ltd. <support@commsignia.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-nemo
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pthread.h>

#include <mip6d-ng/module.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/tasks.h>
#include <mip6d-ng/missing.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/mh-bu.h>
#include <mip6d-ng/bule.h>
#include <mip6d-ng/nemo-bule.h>
#include <mip6d-ng/mho-mnp.h>

#include "mho-mnp.h"
#include "main.h"
#include "mn.h"

/*!
 * \brief Initialize Mobile Node
 *
 * Created XFRM rule to forward packets
 * 
 * \return Zero if OK, otherwise negative
 */
int nemo_mn_init()
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	struct msg_env_nf_to_mip6d_ng nf;

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, &nemo_opts.mnp, sizeof(i6t.daddr));
	i6t.dplen = nemo_opts.mnp_len;
	i6t.prio = 10;
	i6t.dir = XFRM_POLICY_FWD;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.ccom, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	if (nemo_opts.msg_ids.mcoa > 0) {
		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "PREROUTING", sizeof(nf.chain));
		memcpy(&nf.src, &nemo_opts.mnp, sizeof(struct in6_addr));
		nf.src_plen = nemo_opts.mnp_len;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to create ip6tables rule from PREROUTING to mip6d-ng");
			return -1;
		}
	}

	return 0;
}

/*!
 * \brief Cleanup MN functionalities
 *
 * Deletes the XFRM rules created by the nemo_mn_init function
 * 
 * \return Zero if OK
 */
int nemo_mn_clean()
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	struct msg_env_nf_to_mip6d_ng nf;

	DEBUG("NEMO cleaning node: MN");

	if (nemo_opts.msg_ids.mcoa > 0) {
		memset(&nf, 0, sizeof(nf));
		strncpy(nf.chain, "PREROUTING", sizeof(nf.chain));
		memcpy(&nf.src, &nemo_opts.mnp, sizeof(struct in6_addr));
		nf.src_plen = nemo_opts.mnp_len;
		ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.env, MSG_ENV_NF_TO_MIP6D_NG_DEL,
				&nf, sizeof(nf), NULL, 0);
		if (ret) {
			ERROR("Unable to delete ip6tables rule from PREROUTING to mip6d-ng");
			return -1;
		}
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.daddr, &nemo_opts.mnp, sizeof(i6t.daddr));
	i6t.dplen = nemo_opts.mnp_len;
	i6t.prio = 10;
	i6t.dir = XFRM_POLICY_FWD;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.ccom, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}

	return 0;
}

/*!
 * \brief Creating environmental stuff for data packets
 *
 * It creates the outgoing, incoming and forwarding tunnel's XFRM representation.
 *
 * \param local Local address (mobile network prefix)
 * \param local_plen Local prefix length
 * \param remote Destination address (typically the home agent address)
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \return Zero if OK
 */
static int nemo_mn_data_env_create(struct in6_addr * local, unsigned int local_plen, struct in6_addr * remote, struct in6_addr * coa, unsigned int mark)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;
	
	DEBUG("NEMO: BULE data create: IN-OUT TUNNELS");

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, local, sizeof(i6t.saddr));
	i6t.splen = local_plen;
	if (mark > 0)
		i6t.mark = mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, coa, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, remote, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.ccom, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
	i6t.splen = 128;
	memcpy(&i6t.daddr, coa, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, remote, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_FWD;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.ccom, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to create IP6IP6 XFRM tunnel");
		return -1;
	}

	return 0;
}

/*!
 * \brief Cleaning environmental stuff for data packets
 *
 * It destroys the outgoing, incoming and forwarding tunnel's XFRM representation.
 *
 * \param local Local address (mobile network prefix)
 * \param local_plen Local prefix length
 * \param remote Destination address (typically the home agent address)
 * \param coa Care-of Address
 * \param mark MARK value if exists (otherwise zero)
 * \return Zero if OK
 */
static int nemo_mn_data_env_clean(struct in6_addr * local, unsigned int local_plen, struct in6_addr * remote, struct in6_addr * coa, unsigned int mark)
{
	int ret;
	struct msg_env_ip6ip6_tunnel i6t;

	DEBUG("NEMO: BULE data clean: IN-OUT TUNNELS");

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, local, sizeof(i6t.saddr));
	i6t.splen = local_plen;
	if (mark > 0)
		i6t.mark = mark;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, coa, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, remote, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_OUT;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.ccom, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}

	memset(&i6t, 0, sizeof(i6t));
	memcpy(&i6t.saddr, remote, sizeof(i6t.saddr));
	i6t.splen = 128;
	memcpy(&i6t.daddr, coa, sizeof(i6t.daddr));
	i6t.dplen = 128;
	i6t.prio = 10;
	memcpy(&i6t.tmpl_src, remote, sizeof(i6t.tmpl_src));
	memcpy(&i6t.tmpl_dst, coa, sizeof(i6t.tmpl_dst));
	i6t.dir = XFRM_POLICY_FWD;
	i6t.skip_state = 1;
	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.ccom, nemo_opts.msg_ids.env,
			MSG_ENV_IP6IP6_TUNNEL_DEL, &i6t, sizeof(i6t), NULL, 0);
	if (ret) {
		ERROR("Unable to delete IP6IP6 XFRM tunnel");
		return -1;
	}

	return 0;
}

/*!
 * \brief Clean and release a BULE
 *
 * \param arg BULE
 * \return Zero if OK
 */
static int nemo_mn_bule_del(void * arg)
{
	int ret;
	struct bule * bule = (struct bule *)arg;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_mnp * ext_mnp = NULL;

	bule_lock(bule);

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bule_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BULE");
		goto out;
	}

	if (ext_mnp->flags & BULE_MNP_F_DATA_INITED) {
		ret = nemo_mn_data_env_clean(&ext_mnp->mnp, ext_mnp->plen, &bule->dest, &bule->prev_coa, bule->mark);
		if (ret) {
			ERROR("Unable to clean previous DATA environment of this Binding");
		}
		ext_mnp->flags &= ~(BULE_MNP_F_DATA_INITED);
	}

out:
	bule_unlock(bule);
	refcnt_put(&bule->refcnt);

	return 0;
}

/*!
 * \brief Initialize a new BULE
 *
 * It adds the MNP extension to the BULE
 *
 * \param bule BULE
 * \return Zero if OK
 */
int nemo_mn_init_bule(struct bule * bule)
{
	struct bule_ext_mnp * ext_mnp;

	if (bule == NULL)
		return 1;

	bule_lock(bule);

	DEBUG("NEMO: Processing BULE init @ %d...", bule->ifindex);

	/* Get 1 refct for the delete hook */
	refcnt_get(&bule->refcnt);
	hooks_add(bule->del_hook_id, 2, nemo_mn_bule_del);

	ext_mnp = malloc(sizeof(struct bule_ext_mnp));
	if (ext_mnp == NULL) {
		ERROR("Out of memory: Unable to alloc new BULE ext MNP");
		return -1;
	}
	memset(ext_mnp, 0, sizeof(struct bule_ext_mnp));

	strncpy(ext_mnp->ext.name, "mnp", sizeof(ext_mnp->ext.name));
	memcpy(&ext_mnp->mnp, &nemo_opts.mnp, sizeof(ext_mnp->mnp));
	ext_mnp->plen = nemo_opts.mnp_len;

	list_add_tail(&ext_mnp->ext.list, &bule->ext);

	bule_unlock(bule);

	return 0;
}

/*!
 * \brief Update BULE
 *
 * If the BULE has changed, it updates the corresponding data stuff.
 * If it was inited cleans, and configures the new one.
 *
 * \param bule BULE
 * \return Zero if OK
 */
int nemo_mn_update_bule(struct bule * bule)
{
	int ret;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_mnp * ext_mnp = NULL;

	bule_lock(bule);

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bule_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BULE");
		goto out;
	}

	if (ext_mnp->flags & BULE_MNP_F_DATA_INITED) {
		ret = nemo_mn_data_env_clean(&ext_mnp->mnp, ext_mnp->plen, &bule->dest, &bule->prev_coa, bule->mark);
		if (ret) {
			ERROR("Unable to clean previous DATA environment of this Binding");
		}
		ext_mnp->flags &= ~(BULE_MNP_F_DATA_INITED);
	}

	bule_unlock(bule);

	DEBUG("NEMO: UPDATE MN");

out:

	return 0;
}

/*!
 * \brief Extend the Binding Update message with MNP option
 *
 *
 * \param param struct send_hook_bu_param 
 * \return Zero if OK
 */
int nemo_mn_extend_bu(void * param)
{
	int ret = -1;
	struct send_hook_bu_param * hookp = (struct send_hook_bu_param *)param;
	struct ip6_mh_bu * bu;
	uint16_t flags;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_mnp * ext_mnp = NULL;

	if (hookp == NULL || hookp->iov == NULL || hookp->iov_count+1 >= hookp->iov_len || hookp->bule == NULL)
		return -1;

	refcnt_get(&hookp->bule->refcnt);
	bule_lock(hookp->bule);

	list_for_each(pos, &hookp->bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bule_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BULE");
		goto out;
	}

	DEBUG("Extend BU with MNP option: " IP6ADDR_FMT "/%u....", IP6ADDR_TO_STR(&ext_mnp->mnp), ext_mnp->plen);

	bu = (struct ip6_mh_bu *)((hookp->iov[0]).iov_base);
	flags = ntohs(bu->ip6mhbu_flags);
	flags |= MH_BU_F_R;
	bu->ip6mhbu_flags = htons(flags);

	ret = mho_mnp_create(&(hookp->iov[hookp->iov_count++]), &ext_mnp->mnp, ext_mnp->plen);
	if (ret) {
		ERROR("Unable to add MNP option");
		goto out;
	}

out:
	
	bule_unlock(hookp->bule);
	refcnt_put(&hookp->bule->refcnt);

	return ret;
}

/*!
 * \brief Receiving Binding Acknowledge message
 *
 * It checks the BA message and creates the data environmental stuff.
 *
 * \param ba struct evt_ccom_ba
 * \return Zero if OK
 */
int nemo_mn_ba_recv(struct evt_ccom_ba * ba)
{
	int ret;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_mnp * ext_mnp = NULL;

	if (ba == NULL || ba->bule == NULL)
		return -1;

	bule_lock(ba->bule);

	list_for_each(pos, &ba->bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bule_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BULE");
		goto err;
	}

	if ((ba->ba.ip6mhba_flags & MH_BA_F_R) != MH_BA_F_R) {
		ERROR("Home Agent does not support NEMO. No R flag in BA.");
		goto err;
	}

	if ((ext_mnp->flags & BULE_MNP_F_DATA_INITED) == 0) {
		ret = nemo_mn_data_env_create(&ext_mnp->mnp, ext_mnp->plen, &ba->bule->dest, &ba->bule->coa, ba->bule->mark);
		if (ret) {
			ERROR("Unable to set up DATA environment of this Binding");
			goto err;
		}

		ext_mnp->flags |= BULE_MNP_F_DATA_INITED;
	}

	bule_unlock(ba->bule);
	refcnt_put(&ba->bule->refcnt);

	return 0;

err:
	bule_unlock(ba->bule);
	refcnt_put(&ba->bule->refcnt);

	return -1;
}

/*!
 * \brief Argument of __nemo_mn_bule_api_get
 */
struct nemo_mn_bule_api_get_arg {
	/*! API BULE array */
	struct nemo_api_get_bul * bul;
	/*! Length of API BULE array */
	unsigned int bnum;
};

/*!
 * \brief Iterate BUL to produce reply to API
 *
 * It is a callback function for the MSG_DATA_ITERATE_BUL command.
 * It gets the BULE and the BID extension, and add all of them to the 
 * array in the argument.
 *
 * \param bule BULE
 * \param arg struct nemo_mn_bule_api_get_arg
 * \return Zero if OK
 */
int __nemo_mn_bule_api_get(struct bule * bule, void * arg)
{
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_mnp * ext_mnp = NULL;
	struct nemo_mn_bule_api_get_arg * apia = (struct nemo_mn_bule_api_get_arg *)arg;

	if (bule == NULL || apia == NULL)
		return 0;

	apia->bul = realloc(apia->bul, (apia->bnum + 1) * sizeof(struct nemo_api_get_bul));
	if (apia->bul == NULL) {
		ERROR("Out of memory: Unable to alloc get_bul command answer");
		return -1;
	}

	memcpy(&apia->bul[apia->bnum].dest, &bule->dest, sizeof(struct in6_addr));
	memcpy(&apia->bul[apia->bnum].hoa, &bule->hoa, sizeof(struct in6_addr));
	memcpy(&apia->bul[apia->bnum].coa, &bule->coa, sizeof(struct in6_addr));
	apia->bul[apia->bnum].lifetime = htons(bule->lifetime);
	apia->bul[apia->bnum].lifetime_rem = htons(bule->lifetime_rem);
	apia->bul[apia->bnum].last_bu = htonl((int32_t)bule->last_bu);
	apia->bul[apia->bnum].flags = htonl(bule->flags);
	apia->bul[apia->bnum].ifindex = htonl((uint32_t)bule->ifindex);

	list_for_each(pos, &bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bule_ext_mnp *)ext;
			memcpy(&apia->bul[apia->bnum].mnp, &ext_mnp->mnp, sizeof(struct in6_addr));
			apia->bul[apia->bnum].mnp_len = ext_mnp->plen;
			apia->bnum++;
			break;
		}
	}

		return 0;
}

/*!
 * \brief API function to get Binding Update List entries
 * \param reply_msg Reply message to construct
 * \param mlen Maximum length of reply_msg
 * \return Length of reply_message if OK, otherwise negative
 */
int nemo_mn_bule_api_get(unsigned char * reply_msg, unsigned int mlen)
{
	int ret;
	struct msg_data_iterate_bul buli;
	struct nemo_mn_bule_api_get_arg apia;

	memset(&apia, 0, sizeof(apia));
	memset(&buli, 0, sizeof(buli));
	buli.fp = __nemo_mn_bule_api_get;
	buli.arg = &apia;

	ret = IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.data, MSG_DATA_ITERATE_BUL,
			&buli, sizeof(buli), NULL, 0);
	if (ret < 0) {
		ERROR("Unable to iterate BUL");
		return ret;
	}

	if (apia.bnum == 0)
		return 0;

	if (reply_msg != NULL && mlen > 0) {
		if (mlen > apia.bnum * sizeof(struct nemo_api_get_bul))
			mlen = apia.bnum * sizeof(struct nemo_api_get_bul);
		memcpy(reply_msg, (unsigned char *)apia.bul, mlen);

		free(apia.bul);
		
		return apia.bnum * sizeof(struct nemo_api_get_bul);
	}

	if (apia.bul != NULL)
		free(apia.bul);
	
	return -1;
}

/*! \} */
