
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2013 Commsignia Ltd. <support@commsignia.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-nemo
 * \{
 */

#ifndef MIP6D_NG_NEMO_BULE_H
#define MIP6D_NG_NEMO_BULE_H

#include <inttypes.h>
#include <netinet/in.h>
#include <mip6d-ng/bule.h>

/*!
 * \brief BULE extension
 */
struct bule_ext_mnp {
	/*! Extension header */
	struct bule_ext ext;
	/*! Mobile Network Prefix */
	struct in6_addr mnp;
	/*! Mobile Network Prefix length */
	uint8_t plen;
	/*! Flags */
	uint8_t flags;
};

/*!
 * \brief NEMO data environment inited
 */
#define BULE_MNP_F_DATA_INITED		0x01

/*!
 * \brief BA R flag OK
 */
#define BULE_MNP_F_R				0x04

#endif /* MIP6D_NG_NEMO_BULE_H */

/*! \} */
