
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2013 Commsignia Ltd. <support@commsignia.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-nemo
 * \{
 */

#ifndef MIP6D_NG_NEMO_MHO_MNP_H_
#define MIP6D_NG_NEMO_MHO_MNP_H_

/*!
 * \brief Mobile Prefix Mobility Option
 */
struct ip6_mh_opt_mnp {
	/*! Type */
	uint8_t		ip6mom_type;
	/*! Length */
	uint8_t		ip6mom_len;
	/*! Reserved */
	uint16_t    ip6mom_reserved;
	/*! Prefix length */
	uint8_t     ip6mom_plen;
	/*! Mobile Network Prefix */
	struct in6_addr ip6mom_mnp;
} __attribute__ ((packed));

/*!
 *  \brief Binding Identifier MHO type
 */
#define IP6_MHOPT_MNP		0x06

/*! 
 * Mobile Router flag: Binding Update
 */
#define MH_BU_F_R			0x0040

/*! 
 * Mobile Router flag: Binding Acknowledge
 */
#define MH_BA_F_R			0x40

#endif /* MIP6D_NG_NEMO_MHO_MNP_H_ */

 /*! \} */
