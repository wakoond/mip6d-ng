
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2013 Commsignia Ltd. <support@commsignia.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-nemo
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mho-mnp.h>
#include <mip6d-ng/core-comm.h>
#include <mip6d-ng/bce.h>

#include <mip6d-ng/nemo.h>
#include <mip6d-ng/nemo-bule.h>
#include <mip6d-ng/nemo-bce.h>

#include "mho-mnp.h"
#include "main.h"

/*!
 * \brief Create Mobile Network Prefix Mobility Header Option
 *
 * It adds MNP option to the MH with the given prefix.
 * \param iov Message iov
 * \param mnp Mobile Network Prefix
 * \param plen Mobile Network Prefix length
 * \return Zero if OK
 */
int mho_mnp_create(struct iovec * iov, struct in6_addr * mnp, uint8_t plen)
{
	struct ip6_mh_opt_mnp * opt;
	size_t optlen = sizeof(struct ip6_mh_opt_mnp);
	
	iov->iov_base = malloc(optlen);
	iov->iov_len = optlen;

	if (iov->iov_base == NULL)
		return -1;
	
	opt = (struct ip6_mh_opt_mnp *)iov->iov_base;
	memset(opt, 0, optlen);
	
	opt->ip6mom_type = IP6_MHOPT_MNP;
	opt->ip6mom_len = 18;
	opt->ip6mom_plen = plen;
	memcpy(&opt->ip6mom_mnp, mnp, sizeof(opt->ip6mom_mnp));
	
	return 0;
}

/*!
 * \brief Parsing Mobile Network Prefix Mobility Header Option
 *
 * It could parse Binding Update option. The private argument should to be a Binding Cache entry. 
 *  
 * \param mh_type MH type, it should to be BU
 * \param opt MH option, it should to be struct ip6_mh_opt_mnp
 * \param optslen MH option len
 * \param priv Extra argument, it should to be struct bce
 * \return Zero if OK
 */
static int mho_mnp_recv(uint8_t mh_type, struct ip6_mh_opt * opt, size_t optslen, void * priv)
{
	struct ip6_mh_opt_mnp * mnpopt;
	struct bce * bce = (struct bce *)priv;
	struct list_head * pos;
	struct bce_ext * ext;
	struct bce_ext_mnp * ext_mnp = NULL;
	
	if (opt == NULL || bce == NULL || optslen < sizeof(struct ip6_mh_opt_mnp) || opt->ip6mhopt_len != 18)
		return -1;

	mnpopt = (struct ip6_mh_opt_mnp *)opt;

	INFO("Found MNP option: " IP6ADDR_FMT "/%u", IP6ADDR_TO_STR(&mnpopt->ip6mom_mnp), mnpopt->ip6mom_plen);

	list_for_each(pos, &bce->ext) {
		ext = list_entry(pos, struct bce_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bce_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BCE");
		return -1;
	}

	memcpy(&ext_mnp->mnp, &mnpopt->ip6mom_mnp, sizeof(ext_mnp->mnp));
	ext_mnp->plen = mnpopt->ip6mom_plen;

	return 0;
}

/*!
 * Parsing Binding Update flags
 *
 * \param arg struct parse_flags_bu_param
 * \return Zero
 */
 static int mh_bu_flags_parse(void * arg)
{
	struct parse_flags_bu_param * fbu = (struct parse_flags_bu_param *)arg;
	struct bce_ext_mnp * ext_mnp = NULL;

	if ((fbu->flags & MH_BU_F_R) == MH_BU_F_R) {
		ext_mnp = malloc(sizeof(struct bce_ext_mnp));
		if (ext_mnp == NULL) {
			ERROR("Out of memory: Unable to alloc new BCE ext MNP");
			return -1;
		}
		memset(ext_mnp, 0, sizeof(struct bule_ext_mnp));
		strncpy(ext_mnp->ext.name, "mnp", sizeof(ext_mnp->ext.name));

		ext_mnp->flags |= BCE_MNP_F_R;

		list_add_tail(&ext_mnp->ext.list, &fbu->bce->ext);
	}

	return 0;
}

/*!
 * Parsing Binding Acknowledge flags
 *
 * \param arg struct parse_flags_ba_param
 * \return Zero
 */
 static int mh_ba_flags_parse(void * arg)
{
	struct parse_flags_ba_param * fba = (struct parse_flags_ba_param *)arg;
	struct list_head * pos;
	struct bule_ext * ext;
	struct bule_ext_mnp * ext_mnp = NULL;

	list_for_each(pos, &fba->bule->ext) {
		ext = list_entry(pos, struct bule_ext, list);
		if (strcmp(ext->name, "mnp") == 0) {
			ext_mnp = (struct bule_ext_mnp *)ext;
			break;
		}
	}

	if (ext_mnp == NULL) {
		ERROR("Unable to find MNP extension in BULE");
		return 0;
	}

	if ((fba->flags & MH_BA_F_R) == MH_BA_F_R) 
		ext_mnp->flags |= BULE_MNP_F_R;
	else
		ext_mnp->flags &= ~(BULE_MNP_F_R);

	return 0;
}

/*!
 * \brief register Binding Identifier MH Option
 * \return Zero if OK
 */
int mho_mnp_init()
{
	int ret;
	struct msg_ccom_reg_mh_opt opt;

	opt.type = IP6_MHOPT_MNP;
	opt.align_n = 8;
	opt.align = 4;		/* 8n+4 */
	opt.fn = mho_mnp_recv;

	ret = hooks_get_id("parse-mh-bu-flags");
	if (ret > 0)
		hooks_add(ret, 1, mh_bu_flags_parse);
	ret = hooks_get_id("parse-mh-ba-flags");
	if (ret > 0)
		hooks_add(ret, 1, mh_ba_flags_parse);

	return IMSG_MSG_ARGS(nemo_opts.msg_ids.me, nemo_opts.msg_ids.ccom, MSG_CCOM_REG_MH_OPT, &opt, sizeof(opt), NULL, 0);
}

/*! \} */
