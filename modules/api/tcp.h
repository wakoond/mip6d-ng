
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-api
 * \{
 */

#ifndef _TCP_H
#define _TCP_H

#include <netinet/in.h>
#include <pthread.h>
#include <mip6d-ng/list.h>

/*!
 * \brief TCP handler
 */
struct tcp_handle {
	/*! If non-zero it is an IPv4 compatible TCP connection */
	unsigned char is_i4;
	/*! IPv4 specific stuff */
	struct {
		/*! Local address */
		struct in_addr local;
		/*! Local port */
		in_port_t localp;
		/*! Socket's file descriptor */
		int fd;
	} i4;
	/*! If non-zero it is an IPv6 compatible TCP connection */
	unsigned char is_i6;
	/*! IPv6 specific stuff */
	struct {
		/*! Local address */
		struct in6_addr local;
		/*! Local port */
		in_port_t localp;
		/*! Socket's file descriptor */
		int fd;
	} i6;
	/*! Handle lock */
	pthread_mutex_t lock;
	/*! List of connected peers */
	struct list_head peers;
};

/*!
 * \brief TCP peer information
 *
 * It represents one connected peer
 */
struct tcp_peer {
	/*! Linked list management */
	struct list_head list;
	/*! Socket's file descriptor */
	int fd;
	/*! Protocol familiy: AF_INET or AF_INET6 */
	unsigned char family;
	/*! IPv4 or IPv6 stuff */
	union {
		/*! IPv4 specific stuff */
		struct {
			/*! Remote address */
			struct in_addr remote;
			/*! Remote port */
			in_port_t remotep;
		} i4;
		/*! IPv6 specific stuff */
		struct {
			/*! Remote address */
			struct in6_addr remote;
			/*! Remote port */
			in_port_t remotep;
		} i6;
	};
	/*! TCP peer information lock */
	pthread_mutex_t lock;
	/*! ID of the receiving thread of this peer */
	pthread_t thread_id;
	/*! Callback receiving / parsing function */
	void (* fn)(struct tcp_peer * peer, unsigned char *, unsigned int, void *);
	/*! Priv argument of the callback receiving / parsing function */
	void * priv;
	/*! Parent TCP handler */
	struct tcp_handle * handle;
};

int tcp_init(struct tcp_handle * handle, void (* fn)(struct tcp_peer * peer, unsigned char *, unsigned int, void *), void * priv);
int tcp_close(struct tcp_handle * handle);
int tcp_send(struct tcp_peer * peer, unsigned char * buf, unsigned int blen);
int tcp_send_all(struct tcp_handle * handle, unsigned char * buf, unsigned int blen);

#endif

/*! \} */
