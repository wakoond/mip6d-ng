
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-api
 * \{
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <mip6d-ng/list.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>

#include <mip6d-ng/api.h>

#include "commands.h"

/*! 
 * \brief List of commands
 */
static LIST_HEAD(commands);

/*!
 * \brief Command's lock
 */
static pthread_mutex_t commands_lock = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Register a command
 *
 * \param module Module messaging ID
 * \param rc Register command message from imsg
 * \return Zero if OK
 */
int commands_register(unsigned int module, struct msg_api_register_cmd * rc)
{
	struct command * command;

	if (rc == NULL)
		return -1;

	command = malloc(sizeof(struct command));
	if (command == NULL) {
		ERROR("Out of memopry: Unable to register new command");
		return -1;
	}

	command->module = module;
	command->command = rc->command;
	command->imsg_cmd = rc->imsg_cmd;
	strncpy(command->name, rc->command_name, sizeof(command->name));

	pthread_mutex_lock(&commands_lock);
	list_add(&command->list, &commands);
	pthread_mutex_unlock(&commands_lock);

	DEBUG("New API command registered: %s [%u %u]", command->name, command->module, command->command);

	return 0;
}

/*!
 * \brief Get the internal imsg ID of a given command
 *
 * It converts from module name and command ID to 
 * module messaging ID and internal message ID
 *
 * \param module Module name
 * \param cmd Command ID
 * \param module_id Module messaging ID (output)
 * \return Message ID or negative
 */
int commands_get(char * module, uint16_t cmd, unsigned int * module_id)
{
	int ret;
	struct list_head * pos;
	struct command * command;

	ret = imsg_get_id(module);
	if (ret < 0) {
		ERROR("Unknown module in command. Drop.");
		return -100;
	}
	*module_id = ret;

	list_for_each(pos, &commands) {
		command = list_entry(pos, struct command, list);

		if (command->module == *module_id && command->command == cmd) {
			DEBUG("Command: %s %u - %s %u => %u", module, *module_id,
					command->name, cmd, command->imsg_cmd);
			return command->imsg_cmd;
		}
	}

	return -200;
}

/*! \} */
