
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-api
 * \{
 */

#ifndef MIP6D_NG_API_H
#define MIP6D_NG_API_H

#include <inttypes.h>

/*! 
 * \brief The name of the internal messaging reference point
 */
#define MSG_API			" api"

/*!
 * \brief Module load order number of control module
 */
#define SEQ_API			1

/*!
 * \brief Available data API message commands
 */
enum api_messages {
	/*!
	 * \brief Register a command 
	 *
	 * Argument: struct msg_api_register_cmd
	 *
	 * Reply argument: none (it could be NULL)
	 */
	MSG_API_REGISTER_CMD,
	MSG_API_NONE
};

/*!
 * \brief Maximum length of command name
 */
#define API_COMMAND_NAME_MAX	20

/*!
 * \brief Register a command 
 */
struct msg_api_register_cmd {
	/*! Command name */
	char command_name[API_COMMAND_NAME_MAX];
	/*! Command ID, unique within a given module */
	uint16_t command;
	/*! Internal message ID in the module */
	unsigned int imsg_cmd;
};

#endif /* MIP6D_NG_API_H */

/*! \} */
