
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup eapi-api
 * \{
 */

#ifndef _API_CMD_FMT_H_
#define _API_CMD_FMT_H_

#include <inttypes.h>

/*!
 * \brief Request command header 
 */
struct mip6dng_hdr {
	/*! Name of the module */
	char module[20];
	/*! Command ID */
	uint16_t cmd;
	/*! Sequence number */
	uint8_t seq;
} __attribute__((packed));

/*! 
 * \brief Command reply header 
 */
struct mip6dng_reply_hdr {
	/*! Request command */
	struct mip6dng_hdr req;
	/*! Status value */
	uint8_t status;
} __attribute__((packed));

/*!
 * \brief Command status codes 
 */
enum mip6dng_reply_status {
	/*! OK */
	MIP6DNG_REPLY_STATUS_OK = 0,
	/*! Invalid format */
	MIP6DNG_REPLY_STATUS_FORMAT,
	/*! Unknown module */
	MIP6DNG_REPLY_STATUS_UNKNOWN_MOD,
	/*! Unknown command */
	MIP6DNG_REPLY_STATUS_UNKNOWN_CMD,
	/*! Invalid request command */
	MIP6DNG_REPLY_STATUS_INVALID,
	/*! No answer from the module */
	MIP6DNG_REPLY_STATUS_NOANSW,
	/*! Empty response */
	MIP6DNG_REPLY_STATUS_EMPTY,
	MIP6DNG_REPLY_STATUS_NONE,
};

#endif /* _API-CMD-FMT_H_ */

/*! \} */
