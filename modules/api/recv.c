/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-api
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/imessaging.h>

#include "tcp.h"
#include "commands.h"
#include "main.h"

#include <mip6d-ng/api-cmd-fmt.h>

/*!
 * \brief Sending reply for a command
 *
 * It sends answer message to the peer
 *
 * \param peer TCP peer information
 * \param hdr Message header, it contains the details of the original request command
 * \param status Status value
 * \param buf Reply payload
 * \param blen Payload length
 * \return Number of sent bytes or negative
 */
static int recv_send_reply(struct tcp_peer * peer, struct mip6dng_hdr * hdr, uint8_t status, unsigned char * buf, unsigned int blen)
{
	unsigned char message[blen + sizeof(struct mip6dng_reply_hdr)];
	unsigned int mlen;
	struct mip6dng_reply_hdr * rhdr;
	unsigned char * body;

	rhdr = (struct mip6dng_reply_hdr *)message;
	if (hdr == NULL)
		memset(&rhdr->req, 0, sizeof(struct mip6dng_hdr));
	else
		memcpy(&rhdr->req, hdr, sizeof(struct mip6dng_hdr));
	rhdr->status = status;
	body = (unsigned char *)(rhdr + 1);
	if (buf != NULL)
		memcpy(body, buf, blen);

	mlen = sizeof(message);

	return tcp_send(peer, message, mlen);;
}

/*! 
 * \brief Processing received command (message)
 *
 * It gets the peer information and the message as parameter. It validates 
 * the command header, and gets from the name of the module and the command ID, 
 * the module messaging ID and the internal message ID. It executes the internal 
 * message and replies the answer.
 *
 * \param peer TCP peer information
 * \param buf Message
 * \param blen Message length
 * \param priv Unused
 */
void recv_fn(struct tcp_peer * peer, unsigned char * buf, unsigned int blen, void * priv)
{
	int ret;
	struct mip6dng_hdr * hdr;
	unsigned int module;
	int cmd;
	unsigned char request[sizeof(int) + blen - sizeof(struct mip6dng_hdr)];
	unsigned char reply[2*1024];

	if (buf == NULL && blen == 0) {
		DEBUG("Connection closed");
		hooks_run(api_opts.peer_close_hook_id, &peer->fd);
		return;
	}

	if (buf == NULL || blen < sizeof(*hdr)) {
		ERROR("Too short or missing TCP command");
		recv_send_reply(peer, NULL, MIP6DNG_REPLY_STATUS_FORMAT, NULL, 0);
		return;
	}

	hdr = (struct mip6dng_hdr *)buf;

	ret = commands_get(hdr->module, ntohs(hdr->cmd), &module);
	if (ret < 0) {
		ERROR("Invalid module name or command code");
		switch (ret) {
		case -100:
			recv_send_reply(peer, hdr, MIP6DNG_REPLY_STATUS_UNKNOWN_MOD, NULL, 0);
			break;
		case -200:
			recv_send_reply(peer, hdr, MIP6DNG_REPLY_STATUS_UNKNOWN_CMD, NULL, 0);
			break;
		default:
			recv_send_reply(peer, hdr, MIP6DNG_REPLY_STATUS_INVALID, NULL, 0);
			break;
		}
		return;
	}
	cmd = ret;

	if (blen > sizeof(struct mip6dng_hdr)) {
		memcpy(request, &peer->fd, sizeof(int));
		memcpy(request + sizeof(int), (unsigned char *)(hdr + 1), blen - sizeof(struct mip6dng_hdr));
		ret = IMSG_MSG_ARGS(api_opts.msg_ids.me, module, cmd, request, sizeof(int) + blen - sizeof(struct mip6dng_hdr), reply, sizeof(reply));
		if (ret < 0) {
			ERROR("Unable to get answer for command");
			recv_send_reply(peer, hdr, MIP6DNG_REPLY_STATUS_NOANSW, NULL, 0);
			return;
		}
	} else {
		ret = IMSG_MSG_REPLY(api_opts.msg_ids.me, module, cmd, reply, sizeof(reply));
		if (ret < 0) {
			ERROR("Unable to get answer for command");
			recv_send_reply(peer, hdr, MIP6DNG_REPLY_STATUS_NOANSW, NULL, 0);
			return;
		}
	}
	if (ret == 0 || reply == NULL) {
		//ERROR("Empty answer for command");
		recv_send_reply(peer, hdr, MIP6DNG_REPLY_STATUS_EMPTY, NULL, 0);
		return;
	}

	recv_send_reply(peer, hdr, MIP6DNG_REPLY_STATUS_OK, reply, ret);
}

/*! \} */
