
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-environment
 * \{
 */

#ifndef MIP6D_NG_ENVIRONMENT_H
#define MIP6D_NG_ENVIRONMENT_H

#include <netinet/ip6.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <linux/xfrm.h>

/*! 
 * The name of the internal messaging reference point
 */
#define MSG_ENV			" env"

/*!
 * Module load order number of environment module
 */
#define SEQ_ENV			1

/*!
 * \brief Available environment API message commands
 */
enum environment_messages {
		/*! \brief Home Address Destination Option: create or update XFRM policy and state for it
		 * 
		 * Argument: struct msg_env_hao
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_HAO,
		/*! \brief Type 2 Routing Header: create or update XFRM policy and state for it
 		 * 
 		 * Argument: struct msg_env_rt2
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_RT2,
		/*! \brief Create IP6IP6 tunnel (without IPsec): create or update XFRM policy and state for it
 		 *
 		 * Argument: struct msg_env_ip6ip6_tunnel
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_IP6IP6_TUNNEL,
		/*! \brief Create or update ESP TUNNEL policy
 		 *
 		 * Argument: struct msg_env_esp_tunnel
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TUNNEL,
		/*! \brief Create or update ESP TUNNEL state (Security Association)
 		 *
 		 * Argument: struct msg_env_esp_tunnel_sa
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TUNNEL_SA,
		/*! \brief Create or update ESP TRANSPORT policy
 		 *
 		 * Argument: struct msg_env_esp_transport
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TRANSPORT,
		/*! \brief Create or update ESP TRANSPORT state (Security Association)
 		 *
 		 * Argument: struct msg_env_esp_transport_sa
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TRANSPORT_SA,
		/*! \brief Home Address Destination Option: delete XFRM policy and state for it
 		 *
 		 * Argument: struct msg_env_hao
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_HAO_DEL,
		/*! \brief Type 2 Routing Header: delete XFRM policy and state for it
 		 * 
 		 * Argument: struct msg_env_rt2
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_RT2_DEL,
		/*! \brief Delete IP6IP6 tunnel (without IPsec): delete XFRM policy and state for it
 		 *
 		 * Argument: struct msg_env_ip6ip6_tunnel
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_IP6IP6_TUNNEL_DEL,
		/*! \brief Delete ESP TUNNEL policy
 		 *
 		 * Argument: struct msg_env_esp_tunnel
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TUNNEL_DEL,
		/*! \brief Delete ESP TUNNEL state (Security Association)
 		 *
 		 * Argument: struct msg_env_esp_tunnel_sa
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TUNNEL_SA_DEL,
		/*! \brief Delete ESP TRANSPORT policy
 		 *
 		 * Argument: struct msg_env_esp_transport
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TRANSPORT_DEL,
		/*! \brief Delete ESP TRANSPORT state (Security Association)
 		 *
 		 * Argument: struct msg_env_esp_transport_sa
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ESP_TRANSPORT_SA_DEL,
		/*! \brief Flush all (main) XFRM policies
 		 *
 		 * Argument: none (it could be NULL)
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_POLICY_FLUSH,
		/*! \brief Flush all (sub) XFRM policies
 		 *
 		 * Argument: none (it could be NULL)
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_POLICY_FLUSH_SUB,
		/*! \brief Flush all XFRM states
 		 *
 		 * Argument: none (it could be NULL)
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_SA_FLUSH,
		/*! \brief Adding IPv6 address to an interface
 		 *
 		 * Argument: struct msg_env_addr
		 * 
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ADDR,
		/*! \brief Removing IPv6 address from an interface
 		 *
 		 * Argument: struct msg_env_addr
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ADDR_DEL,
		/*! \brief Getting IPv6 address information of a given interface
 		 *
 		 * Argument: unsigned int (interface index)
 		 *
 		 * Reply argument: struct msg_env_addr_get_reply
		 */
		MSG_ENV_ADDR_GET,
		/*! \brief Flush (delete all) IPv6 addresses with HOMEADDR flag
 		 *
 		 * Argument: none (it could be NULL)
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ADDR_FLUSH_HOME_ADDR,
		/*! \brief Adding entry to the IPv6 routing table
 		 *
 		 * Argument: struct msg_env_route
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ROUTE,
		/*! \brief Append entry to an existing one in the IPv6 routing table
 		 *
 		 * Argument: struct msg_env_route
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ROUTE_APPEND,
		/*! \brief Removing entry from the IPv6 routing table
 		 *
 		 * Argument: struct msg_env_route
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_ROUTE_DEL,
		/*! \brief Getting IPv6 route information via the given interface to the given destination
 		 *
 		 * Argument: struct msg_env_route_get
 		 *
 		 * Reply argument: struct msg_env_route_get_reply
		 */
		MSG_ENV_ROUTE_GET,
		/*! \brief Creating IPv6 rule for source routing
 		 *
 		 * Argument: struct msg_env_rule
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_RULE,
		/*! \brief Deleting IPv6 rule for source routing
 		 *
 		 * Argument: struct msg_env_rule
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_RULE_DEL,
		/*! \brief Adding new IPv6 neighbor cache entry
		 *
		 * Argument: struct msg_env_neigh
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NEIGH,
		/*! \brief Deleting IPv6 neighbor cache entry
		 *
		 * Argument: struct msg_env_neigh
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NEIGH_DEL,
		/*! \brief Adding new IPv6 neighbor cache entry for proxy ND
		 *
		 * Argument: struct msg_env_neigh_proxy
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NEIGH_PROXY,
		/*! \brief Deleting proxy BD IPv6 neighbor cache entry
		 *
		 * Argument: struct msg_env_neigh_proxy
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NEIGH_PROXY_DEL,
		/*! \brief Getting interface index from interface name
 		 *
 		 * Argument: character array
 		 * 
 		 * Reply argument: unsigned int pointer (interface index)
		 */
		MSG_ENV_GET_IFINDEX,
		/*! \brief Getting interface name from interface index
 		 *
 		 * Argument: unsigned int
 		 * 
 		 * Reply argument: character array, at least IF_NAMESIZE length
		 */
		MSG_ENV_GET_IFNAME,
		/*! \brief 
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_GET_L2ADDR,
		/*! \brief Getting link status of the given interface
 		 *
 		 * Argument: unsigned int (interface index)
 		 *
 		 * Reply argument: struct msg_env_get_link_reply
		 */
		MSG_ENV_GET_LINK,
		/*! \brief Create a netfilter rule, which jumps to the MIP6D_NG chain
 		 *
 		 * Argument: struct msg_env_nf_to_mip6d_ng
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_TO_MIP6D_NG,
		/*! \brief Delete a netfilter rule, which jumps to the MIP6D_NG chain
 		 * 
 		 * Argument: struct msg_env_nf_to_mip6d_ng
 		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_TO_MIP6D_NG_DEL,
		/*! \brief Create new netfilter jump rule
		 *
		 * Argument struct msg_env_nf_jump
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_JUMP,
		/*! \brief Delete netfilter jump rule
		 *
		 * Argument: struct msg_env_nf_jump
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_JUMP_DEL,
		/*! \brief Create new netfilter chain
		 *
		 * Argument: struct msg_env_nf_chain
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_CHAIN,
		/*! \brief Delete netfilter chain
		 *
		 * Arguments: struct msg_env_nf_chain
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_CHAIN_DEL,
		/*! \brief Create a netfilter rule in the MIP6D_NG chain, which marks and accepts the packets
 		 *
 		 * Argument: struct msg_env_nf_mark_and_acc
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_MARK_AND_ACC,
		/*! \brief  Delete a netfilter rule in the MIP6D_NG chain, which marks and accepts the packets
 		 *
 		 * Argument: struct msg_env_nf_mark_and_acc
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_MARK_AND_ACC_DEL,
		/*! \brief Flush all of the netfilter rules from the mangle table from the given chain
 		 *
 		 * Argument: struct msg_env_nf_flush
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_FLUSH,
		/*! \brief Flush all of the netfilter rules from the mangle table
 		 *
 		 * Argument: none (it could be NULL)
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_NF_FLUSH_ALL,
		/*! \brief Manipulate /proc/sys entry
		 *
		 * Argument: struct msg_env_misc_proc_sys
		 *  
		 * Reply argument: none (it could be NULL)
		 */
		MSG_ENV_MISC_PROC_SYS,
		/*! \brief Get /proc/sys entry
		 *
		 * Argument: struct msg_env_misc_proc_sys
		 *  
		 * Reply argument: none (it could be NULL)
		 *
		 * The return value will be the value
		 */
		MSG_ENV_MISC_PROC_SYS_GET,
		MSG_ENV_NONE
};

/*!
 * \brief Options for creating, updating or deleting Home Address Destination Option XFRM policies and states
 *
 * <b>For creating or updating</b>
 *
 * The src, splen, dst. dplen, proto and type fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The template of the policy will be contain the src and dst fields.\n
 * The XFRM policy will be matching the previously defined selector, and will use the previously defined template.\n
 *   The priority, and the direction of the policy will be specified by the prio and dir members.\n
 *   The action of the created policy will be ALLOW.\n
 * \n
 * The src, and dst fields will be the selectors (id) of the created XFRM state.\n
 * The coa will be defined by the coa field.\n
 * \n
 * The value of the update field is valid for the policy and the state as well.\n
 * \n
 * <b>For deleting</b>
 *
 * The src, splen, dst. dplen, proto and type fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM policy will be matching the previously defined selector.\n
 *   The direction of the policy will be specified by the dir member.\n
 * \n
 * The src, and dst fields will be the selectors (id) of the created XFRM state.\n
 *
 * \see xfrm_set_selector
 * \see xfrm_set_tmpl_dstopt
 * \see xfrm_mip_policy_add 
 * \see xfrm_state_add_ro
 * \see xfrm_mip_policy_del
 * \see xfrm_state_del 
 */
struct msg_env_hao {
	/*! Source Address */
	struct in6_addr src;
	/*! Prefix length of source address */
	uint8_t splen;
	/*! Destination Address */
	struct in6_addr dst;
	/*! Prefix length of destination address */
	uint8_t dplen;
	/*! Protocol identifier. One of the IPPROTO_* values */
	int proto;
	/*! Protocol specific type identifier */
	int type;
	/*! Interface ID selector */
	int ifindex;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! 1: Update; 0: Create */
	int update;
	/*! Direction: XFRM_POLICY_IN or XFRM_POLICY_OUT  */
	int dir;
	/*! Priority */
	int prio;
};

/*!
 * \brief Options for creating, updating or deleting Type 2 Routing Header XFRM policies and states
 *
 * <b>For creating or updating</b>
 *
 * The src, splen, dst. dplen, proto and type fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM policy will be matching the previously defined selector, and will use the an empty (::/0 src and dst) template.\n
 *   The priority, and the direction of the policy will be specified by the prio and dir members.\n
 *   The action of the created policy will be ALLOW.\n
 * \n
 * The src, and dst fields will be the selectors (id) of the created XFRM state.\n
 * The coa will be defined by the coa field.\n
 * \n
 * The value of the update field is valid for the policy and the state as well.\n
 * \n
 * <b>For deleting</b>
 *
 * The src, splen, dst. dplen, proto and type fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM policy will be matching the previously defined selector.\n
 *   The direction of the policy will be specified by the dir member.\n
 * \n
 * The src, and dst fields will be the selectors (id) of the created XFRM state.\n
 *
 * \see xfrm_set_selector
 * \see xfrm_set_tmpl_rh
 * \see xfrm_mip_policy_add 
 * \see xfrm_state_add_ro
 * \see xfrm_mip_policy_del
 * \see xfrm_state_del 
 */
struct msg_env_rt2 {
	/*! Source Address */
	struct in6_addr src;
	/*! Prefix length of source address */
	uint8_t splen;
	/*! Destination Address */
	struct in6_addr dst;
	/*! Prefix length of destination address */
	uint8_t dplen;
	/*! Protocol identifier. One of the IPPROTO_* values */
	int proto;
	/*! Protocol specific type identifier */
	int type;
	/*! Interface ID selector */
	int ifindex;
	/*! Care-of Address */
	struct in6_addr coa;
	/*! 1: Update; 0: Create */
	int update;
	/*! Direction: XFRM_POLICY_IN or XFRM_POLICY_OUT  */
	int dir;
	/*! Priority */
	int prio;
};

/*!
 * \brief Options for creating, updating or deleting IP6-IP6 tunnel policies and states
 *
 * <b>For creating or updating</b>
 *
 * The src, splen, dst. dplen, proto, type, code and ifindex fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The template of the policy will be contain the tmpl_src and tmpl_dst fields.\n
 * The XFRM policy will be matching the previously defined selector, and will use the previously defined template.\n
 *   Additionally, if the mark value greater than zero, it will be added as an extra selector condition (mask 0xffffffff).\n
 *   The priority, and the direction of the policy will be specified by the prio and dir members.\n
 *   The action of the created policy will be ALLOW.\n
 * \n
 * If skip_satate is not zero: The tmpl_src, and tmpl_dst fields will be the selectors (id) of the created XFRM state.\n
 * \n
 * The value of the update field is valid for the policy and the state as well.\n
 * \n
 * <b>For deleting</b>
 *
 * The src, splen, dst. dplen, proto, type, code and ifindex fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM policy will be matching the previously defined selector.\n
 *   Additionally, if the mark value greater than zero, it will be added as an extra selector condition (mask 0xffffffff).\n
 *   The direction of the policy will be specified by the dir member.\n
 * \n
 * If skip_state is nont zero: The tmpl_src, and tmpl_dst fields will be the selectors (id) of the created XFRM state.\n
 *
 * \see xfrm_set_selector
 * \see xfrm_set_tmpl_ipsec
 * \see xfrm_set_mark
 * \see xfrm_ipsec_policy_add
 * \see xfrm_state_add_tunnel
 * \see xfrm_ipsec_policy_del
 * \see xfrm_state_del 
 */
struct msg_env_ip6ip6_tunnel {
	/*! Destination Address */
	struct in6_addr daddr;
	/*! Prefix length of source address */
	uint8_t dplen;
	/*! Source Address */
	struct in6_addr saddr;
	/*! Prefix length of source address */
	uint8_t splen;
	/*! Protocol identifier. One of the IPPROTO_* values */
	int proto;
	/*! Protocol specific type identifier */
	int type;
	/*! Protocol specific code identifier */
	int code;
	/*! Interface index */
	int ifindex;
	/*! 1: Update; 0: Create */
	int update;
	/*! Direction: XFRM_POLICY_IN or XFRM_POLICY_OUT  */
	int dir;
	/*! Priority */
	int prio;

	/*! Destination address in the template */
	struct in6_addr tmpl_dst;
	/*! Source address in the template */
	struct in6_addr tmpl_src;

	/*! MARK value (additional selector condition */
	unsigned int mark;

	/*! 1: Create state; 0: Do not create state */
	unsigned char skip_state;
};

/*!
 * \brief Options for creating, updating or deleting ESP tunnel policies
 *
 * <b>For creating or updating</b>
 *
 * The src, splen, dst. dplen, proto, type, code and ifindex fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The template of the policy will be contain the tmpl_src, tmpl_dst and tmpl_spi, tmpl_reqid fields.\n
 * The XFRM policy will be matching the previously defined selector, and will use the previously defined template.\n
 *   Additionally, if the mark value greater than zero, it will be added as an extra selector condition (mask 0xffffffff).\n
 *   The priority, and the direction of the policy will be specified by the prio and dir members.\n
 *   The action of the created policy will be ALLOW.\n
 *
 * <b>For deleting</b>
 *
 * The src, splen, dst. dplen, proto, type, code and ifindex fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM policy will be matching the previously defined selector.\n
 *   Additionally, if the mark value greater than zero, it will be added as an extra selector condition (mask 0xffffffff).\n
 *   The direction of the policy will be specified by the dir member.\n
 * \n
 *
 * \see xfrm_set_selector
 * \see xfrm_set_tmpl_ipsec
 * \see xfrm_set_mark
 * \see xfrm_ipsec_policy_add
 * \see xfrm_ipsec_policy_del
 */
struct msg_env_esp_tunnel {
        /*! Destination Address */
        struct in6_addr daddr;
        /*! Prefix length of source address */
        uint8_t dplen;
        /*! Source Address */
        struct in6_addr saddr;
        /*! Prefix length of source address */
        uint8_t splen;
        /*! Protocol identifier. One of the IPPROTO_* values */
        int proto;
        /*! Protocol specific type identifier */
        int type;
        /*! Protocol specific code identifier */
        int code;
        /*! Interface index */
        int ifindex;
        /*! 1: Update; 0: Create */
        int update;
        /*! Direction: XFRM_POLICY_IN ot XFRM_POLICY_OUT  */
        int dir;
        /*! Priority */
        int prio;

        /*! Destination address in the template */
        struct in6_addr tmpl_dst;
        /*! Source address in the template */
        struct in6_addr tmpl_src;
        /*! IPsec SPI value */
        uint32_t tmpl_spi;
        /*! IPsec REQid value */
        uint32_t tmpl_reqid;

        /*! MARK value (additional selector condition */
        unsigned int mark;
};

/*!
 * \brief Options for creating, updating or deleting ESP tunnel states
 *
 * <b>For creating or updating</b>
 *
 * The saddr, daddr, spi and reqid fields:\n
 *   Used for XFRM state selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM state will be matching the previously defined selector.\n
 *   The SA will use the provided authorization and encryption values.
 *
 * <b>For deleting</b>
 *
 * The saddr, daddr and spi fields:\n
 *  Used for XFRM identifying the state.\n
 * \n
 *
 * \see xfrm_state_add_tunnel
 * \see xfrm_state_del
 */
struct msg_env_esp_tunnel_sa {
	/*! Destination Address */
	struct in6_addr daddr;
	/*! Source Address */
	struct in6_addr saddr;
	/*! IPsec SPI value */
	uint32_t spi;
	/*! IPsec REQid value */
	uint32_t reqid;

	/*! 1: Update; 0: Create */
	int update;

	/*! Name of the authorization algorithm */
	char auth_name[20];
	/*! Authorization key */
	char auth_key[200];
	/*! Name of the encryption algorithm */
	char enc_name[20];
	/*! Encryption key */
	char enc_key[200];
};

/*!
 * \brief Options for creating, updating or deleting ESP transport policies
 *
 * <b>For creating or updating</b>
 *
 * The src, splen, dst. dplen, proto, type, code and ifindex fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The template of the policy will be contain the tmpl_src, tmpl_dst and tmpl_spi, tmpl_reqid fields.\n
 * The XFRM policy will be matching the previously defined selector, and will use the previously defined template.\n
 *   Additionally, if the mark value greater than zero, it will be added as an extra selector condition (mask 0xffffffff).\n
 *   The priority, and the direction of the policy will be specified by the prio and dir members.\n
 *   The action of the created policy will be ALLOW.\n
 *
 * <b>For deleting</b>
 *
 * The src, splen, dst. dplen, proto, type, code and ifindex fields:\n
 *  Used for XFRM policy selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM policy will be matching the previously defined selector.\n
 *   Additionally, if the mark value greater than zero, it will be added as an extra selector condition (mask 0xffffffff).\n
 *   The direction of the policy will be specified by the dir member.\n
 * \n
 *
 * \see xfrm_set_selector
 * \see xfrm_set_tmpl_ipsec
 * \see xfrm_set_mark
 * \see xfrm_ipsec_policy_add
 * \see xfrm_ipsec_policy_del
 */
struct msg_env_esp_transport {
		/*! Destination Address */
		struct in6_addr daddr;
		/*! Prefix length of source address */
		uint8_t dplen;
		/*! Source Address */
		struct in6_addr saddr;
		/*! Prefix length of source address */
		uint8_t splen;
		/*! Protocol identifier. One of the IPPROTO_* values */
		int proto;
		/*! Protocol specific type identifier */
		int type;
		/*! Protocol specific code identifier */
		int code;
		/*! Interface index */
		int ifindex;
		/*! 1: Update; 0: Create */
		int update;
		/*! Direction: XFRM_POLICY_IN or XFRM_POLICY_OUT  */
		int dir;
		/*! Priority */
		int prio;

		/*! Destination address in the template */
		struct in6_addr tmpl_dst;
		/*! Source address in the template */
		struct in6_addr tmpl_src;
		/*! IPsec SPI value */
		uint32_t tmpl_spi;
		/*! IPsec REQid value */
		uint32_t tmpl_reqid;

		/*! MARK value (additional selector condition */
		unsigned int mark;
};

/*!
 * \brief Options for creating, updating or deleting ESP transport states
 *
 * <b>For creating or updating</b>
 *
 * The saddr, daddr, spi and reqid fields:\n
 *   Used for XFRM state selector. To ignore selection condition, just leave them empty (zeros)\n
 * The XFRM state will be matching the previously defined selector.\n
 *   The SA will use the provided authorization and encryption values.
 *
 * <b>For deleting</b>
 *
 * The saddr, daddr and spi fields:\n
 *  Used for XFRM identifying the state.\n
 * \n
 *
 * \see xfrm_state_add_transport
 * \see xfrm_state_del
 */
struct msg_env_esp_transport_sa {
	/*! Destination Address */
	struct in6_addr daddr;
	/*! Source Address */
	struct in6_addr saddr;
	/*! IPsec SPI value */
	uint32_t spi;
	/*! IPsec REQid value */
	uint32_t reqid;

	/*! 1: Update; 0: Create */
	int update;

	/*! Name of the authorization algorithm */
	char auth_name[20];
	/*! Authorization key */
	char auth_key[200];
	/*! Name of the encryption algorithm */
	char enc_name[20];
	/*! Encryption key */
	char enc_key[200];
};

/*!
 * \brief Options for adding or removing IPv6 addresses
 *
 * To skip any of the following fields, simply leave them empty (zero):
 *   scope, flags
 *
 * For deletion only the addr, plen and ifindex fields will be used
 *
 * \see rtnl_addr_isused
 * \see rtnl_addr_add
 * \see rtn_addr_del
 */
struct msg_env_addr {
	/*! IPv6 address */
	struct in6_addr addr;
	/*! Prefix length of the given address */
	uint8_t plen;
	/*! Interface identifier */
	unsigned int ifindex;
	/*! Address scope, default: RT_SCOPE_UNIVERSE */
	uint8_t scope;
	/*! Additional flags (for adding) */
	uint8_t flags;
};

#define MSG_ENV_ADDR_GET_NUM		5

/*!
 * \brief Reply options for IPv6 addresses query
 *
 * The maximum number of addresses per interface is MSG_ENV_ADDR_GET_NUM
 *
 * \see rtn_addr_get
 */
struct msg_env_addr_get_reply {
	/*! IPv6 addresses */
	struct in6_addr addrs[MSG_ENV_ADDR_GET_NUM];
	/*! Prefix length of the given addresses */
	uint8_t plens[MSG_ENV_ADDR_GET_NUM];
	/*! Address scopes */
	uint8_t scopes[MSG_ENV_ADDR_GET_NUM];
	/*! This slot the last, and it is unused */
	uint8_t last[MSG_ENV_ADDR_GET_NUM];
};

/*!
 * \brief Options for adding or removing IPv6 routes to / from the kernel routing table
 *
 * The following fields could be activated by using the entries bit field:
 *    src (and splen)			add MSG_ENV_ROUTE_SRC to entries
 *    gw						add MSG_ENV_ROUTE_GW to entries
 *    metric					add MSG_ENV_ROUTE_METRIC to entries
 *
 * The other fields are required.
 *
 * \see rtnl_route_isused
 * \see rtnl_route_add
 * \see rtnl_route_append
 * \see rtnl_route_del
 */
struct msg_env_route {
	/*! Interface identifier of the outgoing interface */
	unsigned int oiface;
	/*! Route table number */
	uint8_t table;
	/*! Preference value (metric) */
	uint32_t metric;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Prefix length of the destination address */
	uint8_t dplen;
	/*! Source IPv6 address */
	struct in6_addr src;
	/*! Prefix length of the source address */
	uint8_t splen;
	/* Protocol selector */
	uint8_t proto;
	/*! IPv6 address of the gateway */
	struct in6_addr gw;
	/*! Additional flags */
	unsigned int flags;
	/*! Bit field to sign the content of the route entry: src, gw, metric */
	uint8_t entries;
};

/*! Use if source address is requested in the route entry. Add to the entries bit field. */
#define MSG_ENV_ROUTE_SRC		0x01
/*! Use if gateway address is requested in the route entry. Add to the entries bit field. */
#define MSG_ENV_ROUTE_GW		0x02
/*! Use if metric value is requested in the route entry. Add to the entries bit field. */
#define MSG_ENV_ROUTE_METRIC	0x04

/*!
 * \brief Options for query IPv6 routes via the given interface and given destination
 *
 * \see rtnl_route_get
 */
struct msg_env_route_get {
	/*! Interface identifier of the outgoing interface */
	unsigned int oiface;
	/*! Route table number */
	uint8_t table;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Prefix length of the destination address */
	uint8_t dplen;
};

#define MSG_ENV_ROUTE_GET_NUM		5

/*!
 * \brief Reply options for IPv6 routes query via the given interface and destination
 *
 * The maximum number of routes is MSG_ENV_ROUTE_GET_NUM.
 *
 * \see rtnl_route_get
 */
struct msg_env_route_get_reply {
	/*! Preference values (metric) */
	uint32_t metrics[MSG_ENV_ROUTE_GET_NUM];
	/*! Source IPv6 addresses */
	struct in6_addr srcs[MSG_ENV_ROUTE_GET_NUM];
	/*! Prefix length of the source addresses */
	uint8_t splens[MSG_ENV_ROUTE_GET_NUM];
	/*! IPv6 address of the gateways */
	struct in6_addr gws[MSG_ENV_ROUTE_GET_NUM];
	/*! This slot the last, and it is unused */
	uint8_t last[MSG_ENV_ROUTE_GET_NUM];
};

/*!
 * \brief Options for adding or removing IPv6 rule entries to / from the kernel
 *
 * The following fields could be activated by using the entries bit field:
 *    src (and splen)			add MSG_ENV_RULE_SRC to entries
 *    prio						add MSG_ENV_RULE_PRIO to entries
 *    fwmark					add MSG_ENV_RULE_FWMARK to entries
 *    iface						add MSG_ENV_RULE_IF to entries
 *
 * The other fields are required.
 *
 * \see rtnl_rule_isused
 * \see rtnl_rule_add
 * \see rtnl_rule_del
 */
struct msg_env_rule {
	/*! Interface identifier */
	unsigned int iface;
	/*! (Destination) route table number */
	uint8_t table;
	/* Priority */
	uint32_t priority;
	/*! fw mark value */
	uint32_t fwmark;
	/*! Action, i.e.: RTN_UNICAST */
	uint8_t action;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Prefix length of the destination address */
	uint8_t dplen;
	/*! Source IPv6 address */
	struct in6_addr src;
	/*! Prefix length of the source address */
	uint8_t splen;
	/* Additional flags */
	unsigned int flags;
	/*! Bit field to sign the content of the rule entry: src, prio, fwmark, if */
	uint8_t entries;
};

/*! Use if source address is requested in the rule entry. Add to the entries bit field. */
#define MSG_ENV_RULE_SRC		0x01
/*! Use if priority value is requested in the rule entry. Add to the entries bit field. */
#define MSG_ENV_RULE_PRIO		0x02
/*! Use if FW mark value is requested in the rule entry. Add to the entries bit field. */
#define MSG_ENV_RULE_FWMARK		0x04
/*! Use if interface is requested in the rule entry. Add to the entries bit field. */
#define MSG_ENV_RULE_IF			0x08

/*!
 * \brief Options for creating or deleting IPv6 neighbor entries
 * 
 * \see rtnl_neigh_add
 * \see rtnl_neigh_del
 */
struct msg_env_neigh {
	/*! Interface index */
	int ifindex;
	/*! Neighbor state */
	uint16_t state;
	/*! Neighbor flags */
	uint8_t flags;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! L2 hardware address */
	uint8_t hwa[ETH_ALEN];
	/*! Override existing entries? */
	int override;
};

/*!
 * \brief Options for creating or deleting IPv6 neighbor entries for proxy neighbor discovery
 * 
 * \see rtnl_pneigh_add
 * \see rtnl_pneigh_del
 */
struct msg_env_neigh_proxy {
	/*! Interface index */
	int ifindex;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Neighbor flags */
	uint8_t flags;
};

/*!
 * \brief Reply options for getting link status of the given interface
 *
 * \see rtnl_link_get
 */
struct msg_env_get_link_reply {
	/*! Interface type (See ARPHRD_*) */
	unsigned short type;
	/*! Interface flags (See IFF_*) */
	unsigned int flags;
};

/*!
 * \brief Options for creating or removing Netfilter (ip6tables) rules
 *
 * Used table: mangle
 *
 * The initialization process creates a chain, called MIP6D_NG. This command could specify the traffic,
 * which will be jump to this chain.
 *
 * \see ip6t_jump_rule
 */
struct msg_env_nf_to_mip6d_ng {
	/*! Chain name, ie. OUTPUT, PREROUTING, ... */
	char chain[35];
	/*! Source IPv6 address */
	struct in6_addr src;
	/*! Source prefix length */
	uint8_t src_plen;
	/*! IPv6 protocol */
	uint16_t proto;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Destination prefix length */
	uint8_t dst_plen;
};

/*!
 * \brief Options for creating or removing Netfilter (ip6tables) rules
 *
 * Used table: mangle
 *
 * Cretaes ip6tables jump rule. If the chain value is empty (strlen() ==0), it will be replaced
 * by the default chain: MIP6D_NG
 *
 * \see ip6t_jump_rule
 */
struct msg_env_nf_jump {
	/*! Chain name, ie. OUTPUT, PREROUTING, ... */
	char chain[35];
	/*! Source IPv6 address */
	struct in6_addr src;
	/*! Source prefix length */
	uint8_t src_plen;
	/*! IPv6 protocol */
	uint16_t proto;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Destination prefix length */
	uint8_t dst_plen;
	/*! Target chain name */
	char target[35];
};

/*!
 * \brief Options for creating or removing Netfilter chain
 *
 * Used table: mangle
 *
 * \see ip6t_jump_rule
 */
struct msg_env_nf_chain {
	/*! Chain name */
	char name[35];
};

/*!
 * \brief Options for creating or removing Netfilter (ip6tables) rules
 *
 * Used table: mangle
 *
 * A new chain will be created. Its name will be MIP6D_NG_<name field>.
 * It will contains two rules. The first will mark the packet with the specified mark value.
 * The second will be ACCEPT it.
 *
 * \see ip6t_mark_rule
 * \see ip6t_jump_rule
 */
struct msg_env_nf_mark_and_acc {
	/*! Chain name suffix. Prefix: 'MIP6D_NG_' */
	char name[35];
	/*! Mark value */
	uint8_t mark;
};

/*!
 * \brief  Options for flushing Netfilter (ip6tables) rules
 *
 * The specified chain (name) will be flushed. If name_builtin is zero,
 * the name will be extended with the 'MIP6D_NG_' prefix.
 *
 * \see ip6t_flush_chain
 */
struct msg_env_nf_flush {
	/*! Chain name or chain name suffix */
	char name[35];
	/*! Non-zero if built-in chain */
	unsigned char name_builtin;
};

/*!
 * \brief Options for manupulating or getting /proc/sys entries
 */
struct msg_env_misc_proc_sys {
	/*! /proc/sys entry path, place of the interface should to be substituted with %s */
	char path[100];
	/*! Interface index */
	int ifindex;
	/*! New value for manipulation */
	int value;
};

#define PROC_SYS_IP6_AUTOCONF "/proc/sys/net/ipv6/conf/%s/autoconf"
#define PROC_SYS_IP6_ACCEPT_RA "/proc/sys/net/ipv6/conf/%s/accept_ra"
#define PROC_SYS_IP6_ACCEPT_RA_DEFRTR "/proc/sys/net/ipv6/conf/%s/accept_ra_defrtr"
#define PROC_SYS_IP6_ACCEPT_RA_PINFO "/proc/sys/net/ipv6/conf/%s/accept_ra_pinfo"
#define PROC_SYS_IP6_RTR_SOLICITS "/proc/sys/net/ipv6/conf/%s/router_solicitations"
#define PROC_SYS_IP6_RTR_SOLICIT_INTERVAL "/proc/sys/net/ipv6/conf/%s/router_solicitation_interval"
#define PROC_SYS_IP6_LINKMTU "/proc/sys/net/ipv6/conf/%s/mtu"
#define PROC_SYS_IP6_CURHLIM "/proc/sys/net/ipv6/conf/%s/hop_limit"
#define PROC_SYS_IP6_APP_SOLICIT "/proc/sys/net/ipv6/neigh/%s/app_solicit"
#define PROC_SYS_IP6_BASEREACHTIME_MS "/proc/sys/net/ipv6/neigh/%s/base_reachable_time_ms"
#define PROC_SYS_IP6_RETRANSTIMER_MS "/proc/sys/net/ipv6/neigh/%s/retrans_time_ms"
#define PROC_SYS_IP6_FORWARDING "/proc/sys/net/ipv6/conf/%s/forwarding"
#define PROC_SYS_IP6_PROXY_NDP "/proc/sys/net/ipv6/conf/%s/proxy_ndp"

/*!
 * \brief Available environment API events
 */
enum environment_events {
		/*! \brief EVT_ENV_UNKNOWN_HAO
		 *
		 * Argument: evt_env_unknown_hao
		 */
		EVT_ENV_UNKNOWN_HAO,
		/*! \brief Interface status modification (i.e. ifup)
 		 *
 		 * Argument: struct evt_env_newlink
		 */
		EVT_ENV_NEWLINK,
		/*! \brief Interface destroy
 		 *
 		 * Argument: struct evt_env_dellink
		 */
		EVT_ENV_DELLINK,
		/*! \brief Neighbor status change
 		 *
 		 * Argument: struct evt_env_neigh
		 */
		EVT_ENV_NEIGH,
		/*! \brief IPv6 address change
 		 *
 		 * Argument: struct evt_env_addr
		 */
		EVT_ENV_ADDR,
		/*! \brief IPv6 route modification
 		 *
 		 * Argument: struct evt_env_route
		 */
		EVT_ENV_ROUTE,
		EVT_ENV_NONE
};

struct evt_env_unknown_hao {
	unsigned int flow_proto;
	struct in6_addr hoa;
	struct in6_addr coa;
	struct in6_addr cn;
};

/*!
 * \brief Interface change (i.e. ifup)
 *
 * If interface name is unavailable, the length of the ifname string will be zero.
 * If L2 address is unavailable, it will be filled with zeros.
 */
struct evt_env_newlink {
	/*! Interface index */
	int ifindex;
	/*! Interface name */
	char ifname[IFNAMSIZ + 1];
	/*! Interface flags */
	unsigned long flags;
	/*! Interface type ID */
	unsigned short type;
	/*! L2 address */
	char lladdr[ETH_ALEN];
};

/*!
 * \brief Destroy interface
 */
struct evt_env_dellink {
	/*! Interface index */
	int ifindex;
};

/*!
 * \brief Neighbor change
 */
struct evt_env_neigh {
	/*! Interface index */
	int ifindex;
	/*! New state */
	unsigned short state;
	/*! Destination address */
	struct in6_addr addr;
};

/*!
 * \brief IPv6 address change
 */
struct evt_env_addr {
	/*! EVT_ENV_ADDR_NEW or EVT_ENV_ADDR_DEL */
	char action;
	/*! Interface index */
	int ifindex;
	/*! IPv6 address */
	struct in6_addr addr;
	/*! IPv6 prefix length */
	uint8_t plen;
	/*! Address scope id */
	unsigned short scope;
};

/*!
 * New IPv6 address action sign
 *
 * \see struct evt_env_addr
 */
#define EVT_ENV_ADDR_NEW	0x01
/*!
 * IPv6 address deleted sign
 *
 * \see struct evt_env_addr
 */
#define EVT_ENV_ADDR_DEL	0x02

/*!
 * \brief IPv6 route change
 */
struct evt_env_route {
	/*! EVT_ENV_ROUTE_NEW or EVT_ENV_ROUTE_DEL */
	char action;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Gateway */
	struct in6_addr gw;
	/*! Route scopr ID */
	unsigned short scope;
	/*! Route type */
	unsigned short type;
	/*! Routing table number */
	int table;
	/*! Output interface index */
	int oif;
};

/*!
 * New IPv6 rule action sign
 *
 * \see struct evt_env_route
 */
#define EVT_ENV_ROUTE_NEW	0x01
/*!
 * IPv6 route deleted sign
 *
 * \see struct evt_env_route
 */
#define EVT_ENV_ROUTE_DEL	0x02

#endif /* MIP6D_NG_ENVIRONMENT_H */

/*! \} */
/*! \} */

