
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-environment
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <inttypes.h>
#include <pthread.h>
#include <libnetlink.h>

#include <netinet/icmp6.h>
#include <netinet/ip6.h>
#include <netinet/in.h>
#include <linux/xfrm.h>
#include <libnfnetlink/libnfnetlink.h> 

#include <mip6d-ng/logger.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "xfrm.h"
#include "rtnl.h"

#define XFRM_ALGO_KEY_BUF_SIZE 512

/*!
 * \brief For xfrm policy storage, to proper cleanup
 *
 * It contains everything for deleting a policy
 */
struct env_xfrm_policies {
	/*! Linked list handling */ struct list_head list;
	/*! Type */					uint8_t type;
	/*! Selector */				struct xfrm_selector sel;
	/*! MARK */					struct xfrm_mark mark;
	/*! 1 if MARK is present */ unsigned char m;
	/*! Direction */			int dir;
};

/*!
 * \brief List of created xfrm policies
 */
static LIST_HEAD(env_xfrm_policies);

/*!
 * \brief Locking env_xfrm_policies
 */
static pthread_mutex_t env_xfrm_policies_mutex = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief For xfrm state (SA) storage, to proper cleanup
 *
 * It contains everything for deleting a state
 */
struct env_xfrm_states {
	/*! Linked list handling */		struct list_head list;
	/*! Protocol (IPSEC protocol) */int proto;
	/*! SPI */						uint32_t spi;
	/*! Destination address */		struct in6_addr dst;
	/*! Source address */			struct in6_addr src;
};

/*!
 * \brief List of created xfrm states (SAs)
 */
static LIST_HEAD(env_xfrm_states);

/*!
 * übrief Locking of env_xfrm_sates
 */
static pthread_mutex_t env_xfrm_states_mutex = PTHREAD_MUTEX_INITIALIZER;

#ifdef ENVIRONMENT_DEBUG
/*!
 * \brief Dump xfrm selector
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param sel Selector
 */
static void xfrm_sel_dump(const struct xfrm_selector * const sel)
{
	DEBUG("XFRM \n"
		 "\t\tsel.daddr " IP6ADDR_FMT "/%d\n"
	     "\t\tsel.dport %x sel.dport_mask %x\n"
	     "\t\tsel.saddr " IP6ADDR_FMT "/%d\n"
	     "\t\tsel.sport %x sel.sport_mask %x\n"
	     "\t\tsel.proto %d sel.ifindex %d",
	     IP6ADDR_TO_STR((struct in6_addr *)&sel->daddr), sel->prefixlen_d,
	     ntohs(sel->dport), ntohs(sel->dport_mask),
	     IP6ADDR_TO_STR((struct in6_addr *)&sel->saddr), sel->prefixlen_s,
	     ntohs(sel->sport), ntohs(sel->sport_mask),
	     sel->proto, sel->ifindex);
}

/*!
 * \brief Dump Netlink message
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param nlmsg_flags Flags
 * \param nlmsg_type Type
 */
static void xfrm_nlmsg_dump(int nlmsg_flags, int nlmsg_type)
{
	char type[10];

	switch (nlmsg_type) {
	case XFRM_MSG_UPDPOLICY:
		snprintf(type, sizeof(type), "POL UPD");
		break;
	case XFRM_MSG_NEWPOLICY:
		snprintf(type, sizeof(type), "POL NEW");
		break;
	case XFRM_MSG_DELPOLICY:
		snprintf(type, sizeof(type), "POL DEL");
		break;
	case XFRM_MSG_UPDSA:
		snprintf(type, sizeof(type), "SA UPD");
		break;
	case XFRM_MSG_NEWSA:
		snprintf(type, sizeof(type), "SA NEW");
		break;
	case XFRM_MSG_DELSA:
		snprintf(type, sizeof(type), "SA DEL");
		break;
	default:
		snprintf(type, sizeof(type), "%d", nlmsg_type);
		break;
	}

	DEBUG("XFRM\n"
		 "\t\tnlmsg_flags %x nlmsg_type %s",
	     nlmsg_flags, type);
}

/*!
 * \brief Dump xfrm template
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param tmpl Template
 */
static void xfrm_tmpl_dump(const struct xfrm_user_tmpl * const tmpl)
{
	DEBUG("XFRM \n"
		 "\t\ttmpl.id.daddr " IP6ADDR_FMT "\n"
	     "\t\ttmpl.id.spi %d tmpl.id.proto %d\n"
	     "\t\ttmpl.saddr " IP6ADDR_FMT "\n"
	     "\t\ttmpl.reqid %d tmpl.mode %d\n"
	     "\t\ttmpl.aalgos %x tmpl.ealgos %x tmpl.calgos %x",
	     IP6ADDR_TO_STR((struct in6_addr *)&tmpl->id.daddr),
	     ntohl(tmpl->id.spi), tmpl->id.proto,
	     IP6ADDR_TO_STR((struct in6_addr *)&tmpl->saddr),
	     tmpl->reqid, tmpl->mode,
	     tmpl->aalgos, tmpl->ealgos, tmpl->calgos);
}

/*!
 * \brief Dump xfrm MARK
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param mark MARK
 */
static void xfrm_mark_dump(const struct xfrm_mark * const mark)
{
	DEBUG("XFRM \n"
	     "\t\tmark.value %u mark.mask %08x",
		 mark->v, mark->m);
}
#endif /* ENVIRONMENT_DEBUG */

/*!
 * \brief Dump xfrm policy
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param nlmsg_flags Netlink message flags
 * \param nlmsg_type Netlink message type
 * \param sp Policy info
 * \param ptype Policy type
 * \param tmpls Templates
 * \param num_tmpl Number of templates
 * \param mark MARK
 */
static void xfrm_policy_dump(int nlmsg_flags, int nlmsg_type,
			     const struct xfrm_userpolicy_info * const sp,
			     const struct xfrm_userpolicy_type * const ptype,
			     const struct xfrm_user_tmpl * const tmpls, int num_tmpl,
				 const struct xfrm_mark * const mark)
{
#ifdef ENVIRONMENT_DEBUG
	int i;

	if (sp == NULL || ptype == NULL)
		return;

	xfrm_nlmsg_dump(nlmsg_flags, nlmsg_type);
	xfrm_sel_dump(&sp->sel);
	DEBUG("XFRM\n"
		 "\t\tpolicy.priority %d policy.dir %d\n"
		 "\t\tpolicy.action %d policy.type %u",
	    sp->priority, sp->dir, sp->action, ptype->type);
	if (tmpls != NULL) {
		for (i = 0; i < num_tmpl; i++)
			xfrm_tmpl_dump(&tmpls[i]);
	}
	if (mark != NULL)
		xfrm_mark_dump(mark);
#endif /* ENVIRONMENT_DEBUG */
}

/*!
 * \brief Dump xfrm policy id (used for deleting)
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param nlmsg_flags Netlink message flags
 * \param nlmsg_type Netlink message type
 * \param sp_id Policy info id
 * \param ptype Policy type
 * \param mark MARK
 */
static void xfrm_policy_id_dump(int nlmsg_flags, int nlmsg_type,
				const struct xfrm_userpolicy_id * const sp_id,
				const struct xfrm_userpolicy_type * const ptype,
				const struct xfrm_mark * const mark)
{
#ifdef ENVIRONMENT_DEBUG	
	
	if (sp_id == NULL || ptype == NULL)
		return;

	xfrm_nlmsg_dump(nlmsg_flags, nlmsg_type);
	xfrm_sel_dump(&sp_id->sel);
	DEBUG("XFRM\n"
			"\t\tpolicy_id.dir %d policy_id.type %u",
			sp_id->dir, ptype);
	if (mark != NULL)
		xfrm_mark_dump(mark);
#endif /* ENVIRONMENT_DEBUG */
}

/*!
 * \brief Dump xfrm state
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param nlmsg_flags Netlink message flags
 * \param nlmsg_type Netlink message type
 * \param sa State info
 * \param coa Care-of Address
 * \param auth_name Authorization algorithm name
 * \param auth_key Authorization key
 * \param enc_name Encryption algorithm name
 * \param enc_key Encryption key
 */
static void xfrm_state_dump(int nlmsg_flags, int nlmsg_type,
			    const struct xfrm_usersa_info * const sa,
			    const struct in6_addr * const coa,
				const char * const auth_name, const char * const auth_key,
				const char * const enc_name,  const char * const enc_key)
{
#ifdef ENVIRONMENT_DEBUG
	
	if (sa == NULL)
		return;

	xfrm_nlmsg_dump(nlmsg_flags, nlmsg_type);
	xfrm_sel_dump(&sa->sel);
	if (coa != NULL)
		DEBUG("XFRM\n"
			"\t\tstate.mode %u\n"
			"\t\tstate.id.daddr " IP6ADDR_FMT "\n"
			"\t\tstate.id.spi %d state.id.proto %d\n"
			"\t\tstate.saddr " IP6ADDR_FMT "\n"
			"\t\tstate.reqid %d state.mode %d state.flags %x\n"
			"\t\tcoa " IP6ADDR_FMT "\n"
			"\t\tauth %s %s\n"
			"\t\tenc %s %s",
			sa->mode,
			IP6ADDR_TO_STR((struct in6_addr *)&sa->id.daddr),
			ntohl(sa->id.spi), sa->id.proto,
			IP6ADDR_TO_STR((struct in6_addr *)&sa->saddr),
			sa->reqid, sa->mode, sa->flags,
			IP6ADDR_TO_STR(coa),
			(auth_name == NULL) ? "" : auth_name, (auth_key == NULL) ? "" : auth_key, 
			(enc_name == NULL)  ? "" : enc_name,  (enc_key == NULL)  ? "" : enc_key);
	else
		DEBUG("XFRM\n"
			"\t\tstate.mode %u\n"
			"\t\tstate.id.daddr " IP6ADDR_FMT "\n"
			"\t\tstate.id.spi %d state.id.proto %d\n"
			"\t\tstate.saddr " IP6ADDR_FMT "\n"
			"\t\tstate.reqid %d state.mode %d state.flags %x\n"
			"\t\tauth %s %s\n"
			"\t\tenc %s %s",
			sa->mode,
			IP6ADDR_TO_STR((struct in6_addr *)&sa->id.daddr),
			sa->id.spi, sa->id.proto,
			IP6ADDR_TO_STR((struct in6_addr *)&sa->saddr),
			sa->reqid, sa->mode, sa->flags,
			(auth_name == NULL) ? "" : auth_name, (auth_key == NULL) ? "" : auth_key, 
			(enc_name == NULL)  ? "" : enc_name,  (enc_key == NULL)  ? "" : enc_key);
#endif /* ENVIRONMENT_DEBUG */
}

/*!
 * \brief Dump xfrm state id (used for deleting)
 *
 * If ENVIRONMENT_DEBUG isn't defined, it is empty
 * \param nlmsg_flags Netlink message flags
 * \param nlmsg_type Netlink message type
 * \param sa_id State info id
 * \param saddr Address
 */
static void xfrm_state_id_dump(int nlmsg_flags, int nlmsg_type,
				const struct xfrm_usersa_id * const sa_id,
				const struct in6_addr * const saddr)
{
#ifdef ENVIRONMENT_DEBUG

	if (sa_id == NULL || saddr == NULL)
		return;

	xfrm_nlmsg_dump(nlmsg_flags, nlmsg_type);
	DEBUG("XFRM\n"
			"\t\tstate_id.daddr " IP6ADDR_FMT "\n"
			"\t\tstate_id.spi %d state_id.proto %d\n"
			"\t\tsaddr " IP6ADDR_FMT,
			IP6ADDR_TO_STR((struct in6_addr *)&sa_id->daddr),
			ntohl(sa_id->spi), sa_id->proto,
			IP6ADDR_TO_STR(saddr));
#endif /* ENVIRONMENT_DEBUG */
}

/*!
 * \brief Helper function to set lifetime values to infinite
 * \param lft Lifetime struct
 */
static inline void xfrm_lft(struct xfrm_lifetime_cfg *lft)
{
	lft->soft_byte_limit = XFRM_INF;
	lft->soft_packet_limit = XFRM_INF;
	lft->hard_byte_limit = XFRM_INF;
	lft->hard_packet_limit = XFRM_INF;
}

/*!
 * \brief Adding an XFRM policy
 *
 * If MARK is NULL, it will be skipped
 * It adds the main info of the policy to the list of the created policies
 * The policy will be deleted on exit
 * \param type Type: main or sub policy
 * \param sel Selector
 * \param update If 1 update, if 0 new policy
 * \param dir Direction
 * \param action Action, typically XFRM_POLICY_ALLOW
 * \param priority Priority
 * \param tmpls Templates
 * \param num_tmpl Number of templates
 * \param mark MARK
 * \return Zero if OK
 */
int xfrm_policy_add(uint8_t type, const struct xfrm_selector * const sel,
			   int update, int dir, int action, int priority,
			   struct xfrm_user_tmpl * const tmpls, int num_tmpl, struct xfrm_mark * const mark)
{
	uint8_t buf[NLMSG_SPACE(sizeof(struct xfrm_userpolicy_info)) +
		    RTA_SPACE(sizeof(struct xfrm_userpolicy_type)) +
		    RTA_SPACE(sizeof(struct xfrm_user_tmpl) * num_tmpl) +
			RTA_SPACE(sizeof(struct xfrm_mark) * ((mark == NULL) ? 0 : 1))];
	struct nlmsghdr *n;
	struct xfrm_userpolicy_info *pol;
	struct xfrm_userpolicy_type ptype;
	int err;
	struct env_xfrm_policies * exp;

	if (sel == NULL)
		return -1;

	memset(buf, 0, sizeof(buf));
	
	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct xfrm_userpolicy_info));
	if (update) {
		n->nlmsg_flags = (__u16)(NLM_F_REQUEST | NLM_F_REPLACE);
		n->nlmsg_type = XFRM_MSG_UPDPOLICY;
	} else {
		n->nlmsg_flags = (__u16)(NLM_F_REQUEST | NLM_F_CREATE);
		n->nlmsg_type = XFRM_MSG_NEWPOLICY;
	}
	
	pol = NLMSG_DATA(n);
	memcpy(&pol->sel, sel, sizeof(struct xfrm_selector));
	xfrm_lft(&pol->lft);
	pol->priority = (__u32)priority;
	pol->dir = (__u8)dir;
	pol->action = (__u8)action;
	pol->share = XFRM_SHARE_ANY;

	memset(&ptype, 0, sizeof(ptype));
	ptype.type = type;
	addattr_l(n, sizeof(buf), XFRMA_POLICY_TYPE, &ptype, sizeof(ptype));

	if(tmpls != NULL && num_tmpl > 0)
		addattr_l(n, sizeof(buf), XFRMA_TMPL, 
			  tmpls, sizeof(struct xfrm_user_tmpl) * num_tmpl);

	if (mark != NULL)
		addattr_l(n, sizeof(buf), XFRMA_MARK,
			  mark, sizeof(*mark));

	xfrm_policy_dump((int)n->nlmsg_flags, (int)n->nlmsg_type, pol, &ptype, tmpls, num_tmpl, mark);

	err = rtnl_xfrm_do(n, NULL);
	if (err < 0) {
		ERROR("Unable to add XFRM policy!");
		return err;
	}

	exp = malloc(sizeof(struct env_xfrm_policies));
	if (exp != NULL) {
		exp->type = type;
		memcpy(&exp->sel, sel, sizeof(exp->sel));
		if (mark != NULL) {
			memcpy(&exp->mark, mark, sizeof(exp->mark));
			exp->m = 1;
		} else
			exp->m = 0;
		exp->dir = dir;

		pthread_mutex_lock(&env_xfrm_policies_mutex);
		list_add_tail(&exp->list, &env_xfrm_policies);
		pthread_mutex_unlock(&env_xfrm_policies_mutex);
	}

	return 0;
}

/*!
 * \brief Deleting an XFRM policy
 *
 * If MARK is NULL, it will be skipped
 * The policy will be deleted from the list of the created policies
 * If dpos is NULL, it finds the matching policy from the list, if it is not null, the entry
 * specified by dpos will be deleted
 * \param type Type: main or sub policy
 * \param sel Selector
 * \param dir Direction
 * \param mark MARK
 * \param dpos Position of the policy in the linked list
 * \return Zero if OK
 */
static int __xfrm_policy_del(uint8_t type, const struct xfrm_selector *sel, int dir, const struct xfrm_mark * mark, struct list_head * dpos)
{
	uint8_t buf[NLMSG_SPACE(sizeof(struct xfrm_userpolicy_id))
		    + RTA_SPACE(sizeof(struct xfrm_userpolicy_type))
			+ RTA_SPACE(sizeof(struct xfrm_mark) * ((mark == NULL) ? 0 : 1))];
	struct nlmsghdr *n;
	struct xfrm_userpolicy_id *pol_id;
	struct xfrm_userpolicy_type ptype;
	int err;
	struct env_xfrm_policies * exp;
	struct list_head * pos, * pos2;

	if (sel == NULL)
		return -1;

	memset(buf, 0, sizeof(buf));
	
	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct xfrm_userpolicy_id));
	n->nlmsg_flags = NLM_F_REQUEST;
	n->nlmsg_type = XFRM_MSG_DELPOLICY;

	pol_id = NLMSG_DATA(n);
	memcpy(&pol_id->sel, sel, sizeof(struct xfrm_selector));
	pol_id->dir = (__u8)dir;

	memset(&ptype, 0, sizeof(ptype));
	ptype.type = type;
	addattr_l(n, sizeof(buf), XFRMA_POLICY_TYPE, &ptype, sizeof(ptype));
	if (mark != NULL)
		addattr_l(n, sizeof(buf), XFRMA_MARK, mark, sizeof(*mark));

	xfrm_policy_id_dump((int)n->nlmsg_flags, (int)n->nlmsg_type, pol_id, &ptype, mark);

	err = rtnl_xfrm_do(n, NULL);
	if (err < 0) {
		ERROR("Unable to delete XFRM policy!");
		return err;
	}

	if (dpos == NULL) {
		pthread_mutex_lock(&env_xfrm_policies_mutex);
		list_for_each_safe(pos, pos2, &env_xfrm_policies) {
			exp = (struct env_xfrm_policies *)list_entry(pos, struct env_xfrm_policies, list);
			if (exp->type == type &&
				memcmp(&exp->sel, sel, sizeof(exp->sel)) == 0 &&
				((exp->m == 1 && memcmp(&exp->mark, mark, sizeof(exp->mark)) == 0) || 1) &&
				exp->dir == dir) {
				list_del(pos);
				free(exp);
				break;
			}
		}
		pthread_mutex_unlock(&env_xfrm_policies_mutex);
	} else {
		exp = (struct env_xfrm_policies *)list_entry(dpos, struct env_xfrm_policies, list);
		list_del(dpos);
		free(exp);
	}

	return 0;
}

/*!
 * \brief Deleting an XFRM policy
 *
 * If MARK is NULL, it will be skipped
 * The policy will be deleted from the list of the created policies
 * \param type Type: main or sub policy
 * \param sel Selector
 * \param dir Direction
 * \param mark MARK
 * \return Zero if OK
 */
int xfrm_policy_del(uint8_t type, const struct xfrm_selector *sel, int dir, const struct xfrm_mark * mark)
{
	return __xfrm_policy_del(type, sel, dir, mark, NULL);
}

/*!
 * \brief Deleting all of the created xfrm policies
 * \return Zero if OK
 */
int xfrm_policy_clear()
{
	int ret = 0;
	struct list_head * pos, * pos2;
	struct env_xfrm_policies * exp;

	pthread_mutex_lock(&env_xfrm_policies_mutex);
	list_for_each_safe(pos, pos2, &env_xfrm_policies) {
		exp = (struct env_xfrm_policies *)list_entry(pos, struct env_xfrm_policies, list);
		ret |= __xfrm_policy_del(exp->type, &exp->sel, exp->dir, (exp->m) ? &exp->mark : NULL, pos);
	}
	pthread_mutex_unlock(&env_xfrm_policies_mutex);

	return ret;
}

/*!
 * \brief Deleting all xfrm policies
 *
 * It flushes all of the policies, not only which were created by mip6d-ng
 * \param type Type: main or sub policy
 * \return Zero if OK
 */
int xfrm_policy_flush(uint8_t type)
{
	uint8_t buf[NLMSG_SPACE(sizeof(struct xfrm_userpolicy_info))
		    + RTA_SPACE(sizeof(struct xfrm_userpolicy_type))];
	struct xfrm_userpolicy_type ptype;
	struct nlmsghdr *n;
	int err;
	struct list_head * pos, * pos2;
	struct env_xfrm_policies * exp;
	
	memset(buf, 0, sizeof(buf));
	
	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = NLMSG_LENGTH(0); /* nlmsg data is nothing */
	n->nlmsg_flags = NLM_F_REQUEST;
	n->nlmsg_type = XFRM_MSG_FLUSHPOLICY;

	memset(&ptype, 0, sizeof(ptype));
	ptype.type = type;
	addattr_l(n, sizeof(buf), XFRMA_POLICY_TYPE, &ptype, sizeof(ptype));

	err = rtnl_xfrm_do(n, NULL);
	if (err < 0) {
		ERROR("Unable to flush XFRM policy!");
		return err;
	}

	pthread_mutex_lock(&env_xfrm_policies_mutex);
	list_for_each_safe(pos, pos2, &env_xfrm_policies) {
		exp = (struct env_xfrm_policies *)list_entry(pos, struct env_xfrm_policies, list);
		list_del(pos);
		free(exp);
	}
	pthread_mutex_unlock(&env_xfrm_policies_mutex);

	return 0;
}

/*!
 * \brief Helper function for character string to U8 number conversion
 *
 * \param val Parsed value
 * \param arg Character string
 * \param base Conversion base (10 or 16)
 * \return Zero if OK
 */
static int get_u8(__u8 *val, const char *arg, int base)                                                                                                                         
{
    unsigned long res;
    char *ptr;

    if (!arg || !*arg)
        return -1;
    res = strtoul(arg, &ptr, base);
    if (!ptr || ptr == arg || *ptr || res > 0xFF)
        return -1;
    *val = res;
    return 0;
}

/*
 * \brief Helper function for parsing xfrm state alorythm
 *
 * It fills an xfrm_algo struct. The key will be inserted in binary format to the buf parameter
 * \param alg Output: xfrm algo
 * \param name Algorithm name
 * \param key Key (character string)
 * \param buf Output: binary key
 * \param max Size of buf
 * \return Zero if OK
 */
static int xfrm_algo_parse(struct xfrm_algo *alg, char *name, char *key, char *buf, int max)
{
	int len;
	int slen = strlen(key);

	strncpy(alg->alg_name, name, sizeof(alg->alg_name));

	if (slen > 2 && strncmp(key, "0x", 2) == 0) {
		/* split two chars "0x" from the top */
		char *p = key + 2;
		int plen = slen - 2;
		int i;
		int j;

		/* 
		 * Converting hexadecimal numbered string into real key;
		 * Convert each two chars into one char(value). If number
		 * of the length is odd, add zero on the top for rounding.
		 */

		/* calculate length of the converted values(real key) */
		len = (plen + 1) / 2;
		if (len > max) {
			ERROR("\"ALGOKEY\" makes buffer overflow: '%s'\n", key);
			return -1;
		}

		for (i = - (plen % 2), j = 0; j < len; i += 2, j++) {
			char vbuf[3];
			__u8 val;

			vbuf[0] = i >= 0 ? p[i] : '0';
			vbuf[1] = p[i + 1];
			vbuf[2] = '\0';

			if (get_u8(&val, vbuf, 16)) {
				ERROR("\"ALGOKEY\" is invalid: '%s'", key);
				return -1;
			}

			buf[j] = val;
		}
	} else {
		len = slen;
		if (len > 0) {
			if (len > max) {
				ERROR("\"ALGOKEY\" makes buffer overflow: '%s'", key);
				return -1;
			}
			strncpy(buf, key, len);
		}
	}

	alg->alg_key_len = len * 8;

	return 0;
}

/*!
 * \brief Adding xfrm state (SA)
 *
 * If coa is NULL, it will be skipped
 * If auth_name or auth_key is NULL, auth will be skipped
 * If enc_name or enc_key is NULL, enc will be skipped
 * It adds the main info of the state to the list of the created states
 * The state will be deleted on exit
 * \param sel Selector
 * \param mode Mode, eg. tunnel
 * \param src Source address
 * \param dst Destination address
 * \param proto XFRM protocol
 * \param spi SPI
 * \param reqid Reqid
 * \param coa Care-of Address
 * \param auth_name Authorization algorithm name
 * \param auth_key Authorization key
 * \param enc_name Encryption algorithm name
 * \param enc_key Encryption key
 * \param update If 1 update, if 0 new policy
 * \param flags SA flags
 * \return Zero if OK
 */
static int xfrm_state_add(const struct xfrm_selector *sel,
			  unsigned int mode, struct in6_addr * src, struct in6_addr * dst, int proto, uint32_t spi, uint32_t reqid, 
			  const struct in6_addr *coa,
			  char * auth_name, char * auth_key, char * enc_name, char * enc_key,
			  int update, uint8_t flags)
{
	uint8_t buf[NLMSG_SPACE(sizeof(struct xfrm_usersa_info))
		    + ((coa != NULL) ? RTA_SPACE(sizeof(struct in6_addr)) : 0)
			+ ((auth_name != NULL && auth_key != NULL) ? (RTA_SPACE(sizeof(struct xfrm_algo) + XFRM_ALGO_KEY_BUF_SIZE)) : 0)
			+ ((enc_name != NULL && enc_key != NULL) ? (RTA_SPACE(sizeof(struct xfrm_algo) + XFRM_ALGO_KEY_BUF_SIZE)) : 0)];
	struct nlmsghdr *n;
	struct xfrm_usersa_info *sa;
	struct xfrm_algo_super {
		struct xfrm_algo algo;
		char buf[XFRM_ALGO_KEY_BUF_SIZE];
	} super;
	int err;
	size_t len;
	char * algo_buf;

	memset(buf, 0, sizeof(buf));

	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct xfrm_usersa_info));
	if (update) {
		n->nlmsg_flags = NLM_F_REQUEST;
		n->nlmsg_type = XFRM_MSG_UPDSA;
	} else {
		n->nlmsg_flags = NLM_F_REQUEST;
		n->nlmsg_type = XFRM_MSG_NEWSA;
	}
	
	sa = NLMSG_DATA(n);
	if (sel != NULL)
		memcpy(&sa->sel, sel, sizeof(struct xfrm_selector));
	memcpy(&sa->id.daddr, dst, sizeof(sa->id.daddr));
	sa->id.proto = (__u8)proto;
	sa->id.spi = htonl(spi);
	memcpy(&sa->saddr, src, sizeof(sa->saddr));
	xfrm_lft(&sa->lft);
	sa->family = AF_INET6;
	sa->mode = mode;
	sa->flags = flags;
	sa->reqid = reqid;

	if (coa != NULL)
		addattr_l(n, sizeof(buf), XFRMA_COADDR, coa, sizeof(struct in6_addr));
	
	if (auth_name != NULL && auth_key != NULL) {
		memset(&super, 0, sizeof(super));
		len = sizeof(super.algo);
		algo_buf = super.algo.alg_key;
		err = xfrm_algo_parse(&super.algo, auth_name, auth_key, algo_buf, sizeof(super.buf));
		if (err)
			return err;
		len += super.algo.alg_key_len; 
		addattr_l(n, sizeof(buf), XFRMA_ALG_AUTH, &super, len);
	}
	if (enc_name != NULL && enc_key != NULL) {
		memset(&super, 0, sizeof(super));
		len = sizeof(super.algo);
		algo_buf = super.algo.alg_key;
		err = xfrm_algo_parse(&super.algo, enc_name, enc_key, algo_buf, sizeof(super.buf));
		if (err)
			return err;
		len += super.algo.alg_key_len; 
		addattr_l(n, sizeof(buf), XFRMA_ALG_CRYPT, &super, len);
	}

	xfrm_state_dump((int)n->nlmsg_flags, (int)n->nlmsg_type, sa, coa,
			auth_name, auth_key, enc_name, enc_key);

	err = rtnl_xfrm_do(n, NULL);
	if (err < 0) {
		ERROR("Unable to add state!");
		return err;
	}

	return 0;
}

/*!
 * \brief Adding xfrm state (SA) for RTHDR2 or HOA DstOpt
 *
 * It adds the main info of the state to the list of the created states
 * The state will be deleted on exit
 * \param sel Selector
 * \param src Source address
 * \param dst Destination address
 * \param proto XFRM protocol
 * \param spi SPI value
 * \param coa Care-of Address
 * \param update If 1 update, if 0 new policy
 * \param flags SA flags
 * \return Zero if OK
 */
int xfrm_state_add_ro(const struct xfrm_selector *sel,
			  struct in6_addr * src, struct in6_addr * dst,
			  int proto, uint32_t spi, const struct in6_addr *coa,
			  int update, uint8_t flags)
{
	int err;
	struct env_xfrm_states * exs;

	err = xfrm_state_add(sel, XFRM_MODE_ROUTEOPTIMIZATION, src, dst, proto,
			spi, 0, coa, NULL, NULL, NULL, NULL, update, flags);
	if (err)
		return err;

	exs = malloc(sizeof(struct env_xfrm_states));
	if (exs != NULL) {
		exs->proto = proto;
		memcpy(&exs->src, src, sizeof(exs->src));
		memcpy(&exs->dst, dst, sizeof(exs->dst));
		exs->spi = spi;

		pthread_mutex_lock(&env_xfrm_states_mutex);
		list_add(&exs->list, &env_xfrm_states);
		pthread_mutex_unlock(&env_xfrm_states_mutex);
	}

	return 0;
}

/*!
 * \brief Adding xfrm state (SA) for tunneling policies
 *
 * If auth_name or auth_key is NULL, auth will be skipped
 * If enc_name or enc_key is NULL, enc will be skipped
 * It adds the main info of the state to the list of the created states
 * The state will be deleted on exit
 * \param sel Selector
 * \param src Source address
 * \param dst Destination address
 * \param proto XFRM protocol
 * \param auth_name Authorization algorithm name
 * \param auth_key Authorization key
 * \param enc_name Encryption algorithm name
 * \param enc_key Encryption key
 * \param spi SPI
 * \param reqid Reqid
 * \param update If 1 update, if 0 new policy
 * \param flags SA flags
 * \return Zero if OK
 */
int xfrm_state_add_tunnel(const struct xfrm_selector *sel,
			  struct in6_addr * src, struct in6_addr * dst,
			  int proto, char * auth_name, char * auth_key, char * enc_name, char * enc_key,
			  uint32_t spi, uint32_t reqid, int update, uint8_t flags)
{
	int err;
	struct env_xfrm_states * exs;

	err = xfrm_state_add(sel, XFRM_MODE_TUNNEL, src, dst, proto,
			spi, reqid, NULL, auth_name, auth_key, enc_name, enc_key, update, flags);
	if (err)
		return err;

	exs = malloc(sizeof(struct env_xfrm_states));
	if (exs != NULL) {
		exs->proto = proto;
		memcpy(&exs->src, src, sizeof(exs->src));
		memcpy(&exs->dst, dst, sizeof(exs->dst));
		exs->spi = spi;

		pthread_mutex_lock(&env_xfrm_states_mutex);
		list_add(&exs->list, &env_xfrm_states);
		pthread_mutex_unlock(&env_xfrm_states_mutex);
	}

	return 0;
}

/*!
 * \brief Adding xfrm state (SA) for transport policies
 *
 * If auth_name or auth_key is NULL, auth will be skipped
 * If enc_name or enc_key is NULL, enc will be skipped
 * It adds the main info of the state to the list of the created states
 * The state will be deleted on exit
 * \param sel Selector
 * \param src Source address
 * \param dst Destination address
 * \param proto XFRM protocol
 * \param auth_name Authorization algorithm name
 * \param auth_key Authorization key
 * \param enc_name Encryption algorithm name
 * \param enc_key Encryption key
 * \param spi SPI
 * \param reqid Reqid
 * \param update If 1 update, if 0 new policy
 * \param flags SA flags
 * \return Zero if OK
 */
int xfrm_state_add_transport(const struct xfrm_selector *sel,
			  struct in6_addr * src, struct in6_addr * dst,
			  int proto, char * auth_name, char * auth_key, char * enc_name, char * enc_key,
			  uint32_t spi, uint32_t reqid, int update, uint8_t flags)
{
	int err;
	struct env_xfrm_states * exs;

	err = xfrm_state_add(sel, XFRM_MODE_TRANSPORT, src, dst, proto,
			spi, reqid, NULL, auth_name, auth_key, enc_name, enc_key, update, flags);
	if (err)
		return err;

	exs = malloc(sizeof(struct env_xfrm_states));
	if (exs != NULL) {
		exs->proto = proto;
		memcpy(&exs->src, src, sizeof(exs->src));
		memcpy(&exs->dst, dst, sizeof(exs->dst));
		exs->spi = spi;

		pthread_mutex_lock(&env_xfrm_states_mutex);
		list_add(&exs->list, &env_xfrm_states);
		pthread_mutex_unlock(&env_xfrm_states_mutex);
	}

	return 0;
}

/*!
 * \brief Deleting an XFRM state
 *
 * If src is NULL, it will be skipped
 * The state will be deleted from the list of the created policies
 * If dpos is NULL, it finds the matching state from the list, if it is not null, the entry
 * specified by dpos will be deleted
 * \param src Source address
 * \param dst Destination address
 * \param proto XFRM protocol
 * \param spi SPI
 * \param dpos Position of the policy in the linked list
 * \return Zero if OK
 */
static int __xfrm_state_del(struct in6_addr * src, struct in6_addr * dst, int proto, uint32_t spi, struct list_head * dpos)
{
	uint8_t buf[NLMSG_SPACE(sizeof(struct xfrm_usersa_id)) +
		    RTA_SPACE(sizeof(xfrm_address_t))];
	struct nlmsghdr *n;
	struct xfrm_usersa_id *sa_id;
	int err;
	struct list_head * pos, * pos2;
	struct env_xfrm_states * exs;

	memset(buf, 0, sizeof(buf));

	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct xfrm_usersa_id));
	n->nlmsg_flags = NLM_F_REQUEST;
	n->nlmsg_type = XFRM_MSG_DELSA;

	sa_id = NLMSG_DATA(n);
	memcpy(&sa_id->daddr, dst, sizeof(sa_id->daddr));
	sa_id->family = AF_INET6;
	sa_id->proto = (__u8)proto;
	sa_id->spi = htonl(spi);

	if (src != NULL)
		addattr_l(n, sizeof(buf), XFRMA_SRCADDR, src, sizeof(*src));

	xfrm_state_id_dump((int)n->nlmsg_flags, (int)n->nlmsg_type, sa_id, src);

	err = rtnl_xfrm_do(n, NULL);
	if (err < 0) {
		ERROR("Unable to delete state!");
		return err;
	}

	if (dpos == NULL) {
		pthread_mutex_lock(&env_xfrm_states_mutex);
		list_for_each_safe(pos, pos2, &env_xfrm_states) {
			exs = (struct env_xfrm_states *)list_entry(pos, struct env_xfrm_states, list);
			if (exs->proto == proto && exs->spi == spi &&
				((src != NULL && memcmp(&exs->src, src, sizeof(exs->src)) == 0) || 1) &&
				memcmp(&exs->dst, dst, sizeof(exs->dst)) == 0) {
				list_del(pos);
				free(exs);
				break;
			}
		}
		pthread_mutex_unlock(&env_xfrm_states_mutex);
	} else {
		exs = (struct env_xfrm_states *)list_entry(dpos, struct env_xfrm_states, list);
		list_del(dpos);
		free(exs);
	}

	return 0;
}

/*!
 * \brief Deleting an XFRM state
 *
 * The state will be deleted from the list of the created satates
 * \param src Source address
 * \param dst Destination address
 * \param proto XFRM proto
 * \param spi SPI
 * \return Zero if OK
 */
int xfrm_state_del(struct in6_addr * src, struct in6_addr * dst, int proto, uint32_t spi)
{
	return __xfrm_state_del(src, dst, proto, spi, NULL);
}

/*!
 * \brief Deleting all of the created xfrm states
 * \return Zero if OK
 */
int xfrm_state_clear()
{
	int ret = 0;
	struct list_head * pos, * pos2;
	struct env_xfrm_states * exs;

	pthread_mutex_lock(&env_xfrm_states_mutex);
	list_for_each_safe(pos, pos2, &env_xfrm_states) {
		exs = (struct env_xfrm_states *)list_entry(pos, struct env_xfrm_states, list);
		ret |= __xfrm_state_del(&exs->src, &exs->dst, exs->proto, exs->spi, pos);
	}
	pthread_mutex_unlock(&env_xfrm_states_mutex);

	return ret;
}

/*!
 * \brief Deleting all xfrm states
 *
 * It flushes all of the states, not only which were created by mip6d-ng
 * \return Zero if OK
 */
int xfrm_state_flush()
{
	uint8_t buf[NLMSG_SPACE(sizeof(struct xfrm_usersa_flush))];
	struct nlmsghdr *n;
	struct xfrm_usersa_flush * saf;
	int err;
	struct list_head * pos, * pos2;
	struct env_xfrm_states * exs;
	
	memset(buf, 0, sizeof(buf));
	
	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct xfrm_usersa_flush));
	n->nlmsg_flags = NLM_F_REQUEST;
	n->nlmsg_type = XFRM_MSG_FLUSHSA;

	saf = NLMSG_DATA(n);
	saf->proto = 0;

	err = rtnl_xfrm_do(n, NULL);
	if (err < 0) {
		ERROR("Unable to flush XFRM sa!");
		return err;
	}

	pthread_mutex_lock(&env_xfrm_states_mutex);
	list_for_each_safe(pos, pos2, &env_xfrm_policies) {
		exs = (struct env_xfrm_states *)list_entry(pos, struct env_xfrm_states, list);
		list_del(pos);
		free(exs);
	}
	pthread_mutex_unlock(&env_xfrm_states_mutex);

	return 0;
}

/*!
 * \brief Helper function to configure xfrm slector
 *
 * If saddr is NULL, it will be skipped
 * If daddr is NULL, it willbe skipped
 * If proto is IPPROTO_ICMPV6, type will be used as ICMPv6 type
 * If proto is IPPROTO_MH, type will be used as MH type
 * Otherwise (proto) typw will be used as source port, and code will be used as destination port
 * \param daddr Destination address
 * \param dplen Prefix length of the destination address
 * \param saddr Source address
 * \param splen Prefix length of the source address
 * \param proto Protocol
 * \param type Type (see description)
 * \param code Code (see description)
 * \param ifindex Interface index
 * \param sel Selector
 */
void xfrm_set_selector(const struct in6_addr * const daddr, uint8_t dplen, 
			 const struct in6_addr * const saddr, uint8_t splen, 
			 int proto, int type, int code, int ifindex, struct xfrm_selector * const sel)
{
	memset(sel, 0, sizeof(*sel));

	sel->family = AF_INET6;
	sel->user = getuid();
	sel->ifindex = ifindex;
	sel->proto = (__u8)proto;

	switch (proto) {
	case 0: /* Any */
		break;
	case IPPROTO_ICMPV6:
		sel->sport = htons(type);
		if (type)
			sel->sport_mask = ~((__u16)0);
		sel->dport = 0;
		break;
	case IPPROTO_MH:
		sel->sport = htons(type);
		if (type)
			sel->sport_mask = ~((__u16)0);
		sel->dport = 0;
		break;
	default:
		sel->sport = htons(type);
		if (type)
			sel->sport_mask = ~((__u16)0);
		sel->dport = htons(code);
		if (code)
			sel->dport_mask = ~((__u16)0);
	}

	if (saddr != NULL)
		memcpy(&sel->saddr.a6, saddr, sizeof(*saddr));
	sel->prefixlen_s = splen;
	if (daddr != NULL)
		memcpy(&sel->daddr.a6, daddr, sizeof(*daddr));
	sel->prefixlen_d = dplen;
}

/*!
 * \brief Helper function for creating destination option template
 *
 * If saddr is NULL, it will be skipped
 * If daddr is NULL, it willbe skipped
 * \param dst Destination address
 * \param src Source address
 * \param spi SPI value
 * \param tmpl Template
 */
void xfrm_set_tmpl_dstopt(const struct in6_addr * const dst,
					const struct in6_addr * const src, uint32_t spi,
					struct xfrm_user_tmpl * const tmpl)
{
	memset(tmpl, 0, sizeof(*tmpl));

	tmpl->family = AF_INET6;
    tmpl->id.proto = IPPROTO_DSTOPTS;
	tmpl->mode = XFRM_MODE_ROUTEOPTIMIZATION;
	tmpl->optional = 1;
	tmpl->id.spi = spi;
	tmpl->reqid = 0;
	if (dst != NULL)
		memcpy(&tmpl->id.daddr, dst, sizeof(tmpl->id.daddr));
	if (src != NULL)
		memcpy(&tmpl->saddr, src, sizeof(tmpl->saddr));
}

/*!
 * \brief Helper function for creating rthdr2 template
 *
 * \param spi SPI value
 * \param tmpl Template
 */
void xfrm_set_tmpl_rh(uint32_t spi, struct xfrm_user_tmpl * const tmpl)
{	
	memset(tmpl, 0, sizeof(*tmpl));

	tmpl->family = AF_INET6;
	tmpl->id.proto = IPPROTO_ROUTING;
	tmpl->mode = XFRM_MODE_ROUTEOPTIMIZATION;
	tmpl->optional = 1;
	tmpl->reqid = 0;
	tmpl->id.spi = spi;
}

/*!
 * \brief Helper function for creating template
 *
 * If saddr is NULL, it will be skipped
 * If daddr is NULL, it willbe skipped
 * \param dst Destination address
 * \param src Source address
 * \param proto Protocol
 * \param tunnel (Tunnel or transport)
 * \param spi SPI
 * \param reqid Reqid
 * \param tmpl Template
 */
void xfrm_set_tmpl_ipsec(const struct in6_addr * const dst,
					const struct in6_addr * const src,
					uint8_t proto, int tunnel, uint32_t spi, uint32_t reqid,
					struct xfrm_user_tmpl * const tmpl)
{
	memset(tmpl, 0, sizeof(*tmpl));

	tmpl->family = AF_INET6;
	tmpl->ealgos = ~(uint32_t)0;
	tmpl->aalgos = ~(uint32_t)0;
	tmpl->calgos = ~(uint32_t)0;
	tmpl->id.proto = proto;
	tmpl->optional = 0;
	if (tunnel)
		tmpl->mode = XFRM_MODE_TUNNEL;
	else
		tmpl->mode = XFRM_MODE_TRANSPORT;
	tmpl->reqid = reqid;
	tmpl->id.spi = htonl(spi);
	if (tunnel) {
		if (dst != NULL)
			memcpy(&tmpl->id.daddr, dst, sizeof(struct in6_addr));
		if (src != NULL)
			memcpy(&tmpl->saddr, src, sizeof(struct in6_addr));
	}
}

/*!
 * \brief Helper function to set up MARK parameter
 *
 * \param value MARK value
 * \param mask MARK mask (typicaly 0xffffffff)
 * \param mark MARK
 */
void xfrm_set_mark(unsigned int value, unsigned int mask, struct xfrm_mark * const mark)
{
	memset(mark, 0, sizeof(*mark));

	mark->v = value;
	mark->m = mask;
}

#if 0
#define XFRMRPT_RTA(x)	((struct rtattr*)(((char*)(x)) + NLMSG_ALIGN(sizeof(struct xfrm_user_report))))

/*!
 * \brief XFRM report parser
 * \param msg Netlink message
 * \return Zero if OK
 * \todo This description must be clarified
 */
static int xfrm_parse_report(struct nlmsghdr *msg)
{
	struct xfrm_user_report *rpt;
	struct rtattr *rta_tb[XFRMA_MAX+1];
	//unsigned int event = EVT_ENV_UNKNOWN_HAO;
	struct evt_env_unknown_hao evt_arg;
	xfrm_address_t *hoaaddr = NULL;
	xfrm_address_t *coaaddr = NULL;
	xfrm_address_t *cnaddr = NULL;

	/*! TODO:  Sending UNKNOWN_HAO BE message, if no XFRM rule */

	memset(&evt_arg, 0, sizeof(evt_arg));

	if (msg->nlmsg_len < (__u32)NLMSG_LENGTH(sizeof(*rpt))) {
		INFO("Too short xfrm report nlmsg");
		return -1;
	}
	rpt = NLMSG_DATA(msg);

	if (rpt->proto != IPPROTO_DSTOPTS || rpt->sel.family != AF_INET6)
		return 0;

	memset(rta_tb, 0, sizeof(rta_tb));
	parse_rtattr(rta_tb, XFRMA_MAX, XFRMRPT_RTA(rpt), 
		     (int)(msg->nlmsg_len - NLMSG_LENGTH(sizeof(*rpt))));

	if (!rta_tb[XFRMA_COADDR])
		return -1;

	coaaddr = (xfrm_address_t *) RTA_DATA(rta_tb[XFRMA_COADDR]);

	hoaaddr = &rpt->sel.saddr;
	cnaddr = &rpt->sel.daddr;

	evt_arg.flow_proto = rpt->sel.proto;
	if (hoaaddr != NULL)
		memcpy(&evt_arg.hoa, (struct in6_addr *)hoaaddr, sizeof(evt_arg.hoa));
	if (coaaddr != NULL)
		memcpy(&evt_arg.coa, (struct in6_addr *)coaaddr, sizeof(evt_arg.coa));
	if (cnaddr != NULL)
		memcpy(&evt_arg.cn,  (struct in6_addr *)cnaddr,  sizeof(evt_arg.cn));

	DEBUG("XFRM REPORT: event:UNKNOWN_HAO proto %u HOA " IP6ADDR_FMT " COA " IP6ADDR_FMT " CN " IP6ADDR_FMT,
			evt_arg.flow_proto,
			IP6ADDR_TO_STR(&evt_arg.hoa), IP6ADDR_TO_STR(&evt_arg.coa), IP6ADDR_TO_STR(&evt_arg.cn));

	/* ignore if CoA/HoA of original packet is invalid */
	if (!in6_is_addr_routable_unicast((struct in6_addr *) coaaddr) ||
	    !in6_is_addr_routable_unicast((struct in6_addr *) hoaaddr)) {
		DEBUG("XFRM REPORT: Invalid CoA/HoA appeared in original packet");
		return 0;
	}

	/*! TODO: Sending event */

	return 0;
}

/*!
 * \brief Callback function for receiving XFRM messages
 * \param who Unused
 * \param n Netlink message
 * \param arg Argument (unised)
 * \return Zero if OK
 * \todo This description must be clarified
 */
static int xfrm_rcv(const struct sockaddr_nl * who, struct nlmsghdr *n, void * arg)
{
	if (n->nlmsg_type == XFRM_MSG_REPORT) {
		xfrm_parse_report(n);
	}

	return 0;
}

/*!
 * \brief XFRM message listener
 * \param arg Argument (unused)
 * \return Zero if OK, never
 * \todo This description must be clarified
 */
static void * xfrm_listen(void * arg)
{
	int ret;
	struct rtnl_handle xfrm_rth;
	int x;

	ret = rtnl_xfrm_open(&xfrm_rth, 0);
	if (ret < 0) {
		ERROR("Unable to create XFRM socket");
		exit(EXIT_FAILURE);
	}

	x = XFRMNLGRP_REPORT;
	ret = setsockopt(xfrm_rth.fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &x, sizeof(x));
	if (x < 0) {
		ERROR("Unable to register for XFRM REPORT group");
		exit(EXIT_FAILURE);
	}

	rtnl_listen(&xfrm_rth, xfrm_rcv, NULL);

	return NULL;
}

/*!
 * \brief Start an XFRM message listener thread
 * \return Zero if OK
 */
int xfrm_listen_start()
{
	pthread_t th;

	th = threading_create(xfrm_listen, NULL);

	return (int)(th == (pthread_t)0);
}
#endif

/*! \} */

