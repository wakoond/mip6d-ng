
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-environment
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <inttypes.h>

#include <net/if.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <time.h>
#include <libnetlink.h>
#include <libnfnetlink/libnfnetlink.h>

#include <linux/if_ether.h>
#include <netinet/icmp6.h>
#include <netinet/ip6.h>
#include <netinet/in.h>
#include <sys/uio.h>
#include <sys/socket.h>

#include <mip6d-ng/environment.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>

#include "rtnl.h"
#include "main.h"

/*!
 * \brief RTNetlink listener socket
 */
static struct rtnl_handle rtnl_listen_h;

/*!
 * \brief RTNetlink do function
 *
 * Wrapper for rtnl_talk.
 * It opens an rtnetlink socket, and passes the message to the kernel
 * Finally it closes the socket
 * \param proto Protocol. It could be eg. NETLINK_ROUTE or NETLINK_XFRM, etc.
 * \param sn Message
 * \param rn Reply message, it could be NULL
 * \return Zero if OK
 */
int rtnl_do(int proto, struct nlmsghdr *sn, struct nlmsghdr *rn)
{
	struct rtnl_handle rth;
	int err;
	
	err = rtnl_open_byproto(&rth, 0, proto);
	if (err < 0) {
		ERROR("Unable to open rtnl, proto %d", proto);
		return -1;
	}

	err = rtnl_talk(&rth, sn, 0, 0, rn, NULL, NULL);
	
	rtnl_close(&rth);
	
	return err;
}

/*!
 * \brief RTNetlink address manipulator function
 *
 * It gets the address(es) of a specified interface 
 *   (identified by link-local address, ifindex, and prefix length)
 * It calls the specified callback function with the result of the query.
 * \param addr Link-local address
 * \param plen Prefix length
 * \param ifindex Interface Index
 * \param arg Callback argument
 * \param do_callback Callback function
 * \return Zero if OK
 */
int rtnl_addr_do(const struct in6_addr *addr, int plen, unsigned int ifindex, void *arg,
					int (*do_callback)(struct ifaddrmsg *ifa, struct rtattr *rta_tb[], void *arg))
{
	uint8_t sbuf[256];
	uint8_t rbuf[256];
	struct nlmsghdr *sn, *rn;
	struct ifaddrmsg *ifa;
	int err;
	struct rtattr *rta_tb[IFA_MAX+1];

	memset(sbuf, 0, sizeof(sbuf));
	sn = (struct nlmsghdr *)sbuf;
	sn->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	sn->nlmsg_flags = NLM_F_REQUEST;
	sn->nlmsg_type = RTM_GETADDR;

	ifa = NLMSG_DATA(sn);
	ifa->ifa_family = AF_INET6;
	ifa->ifa_prefixlen = (__u8)plen;
	ifa->ifa_scope = RT_SCOPE_UNIVERSE;
	ifa->ifa_index = ifindex;

	addattr_l(sn, sizeof(sbuf), IFA_LOCAL, addr, sizeof(*addr));

	memset(rbuf, 0, sizeof(rbuf));
	rn = (struct nlmsghdr *)rbuf;
	err = rtnl_route_do(sn, rn);
	if (err < 0) {
		rn = sn;
		ifa = NLMSG_DATA(rn);
	} else {
		ifa = NLMSG_DATA(rn);
		
		if (rn->nlmsg_type != RTM_NEWADDR || 
		    rn->nlmsg_len < (__u32)NLMSG_LENGTH(sizeof(*ifa)) ||
		    ifa->ifa_family != AF_INET6) {
			return -1;
		}
	}
	memset(rta_tb, 0, sizeof(rta_tb));
	parse_rtattr(rta_tb, IFA_MAX, IFA_RTA(ifa), 
		     (int)(rn->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa))));

	if (!rta_tb[IFA_ADDRESS])
		rta_tb[IFA_ADDRESS] = rta_tb[IFA_LOCAL];

	if (!rta_tb[IFA_ADDRESS] ||
	    !IN6_ARE_ADDR_EQUAL((struct in6_addr *)RTA_DATA(rta_tb[IFA_ADDRESS]), addr)) {
		return -1;
	}
	if (do_callback)
		err = do_callback(ifa, rta_tb, arg);

	return err;

}

/*!
 * \brief Modifying IPv6 address configuration of a given interface
 *
 * Please use this function carefully! The cmd parameter should be one of the 
 * available RTM_* defines, which are correct for address modification.
 * \param cmd Command One RTM_* define for address configuration
 * \param nlmsg_flags RTNetlink flags
 * \param addr IPv6 address
 * \param plen Prefix length
 * \param flags IPv6 address flags
 * \param scope IPv6 address scope, i.e. RT_SCOPE_UNIVERSE
 * \param ifindex Interface index
 * \param prefered Preferred lifetime
 * \param valid Valid lifetime
 * \return Zero if OK
 */
static int rtnl_addr_mod(int cmd, uint16_t nlmsg_flags,
		    const struct in6_addr *addr, uint8_t plen, 
		    uint8_t flags, uint8_t scope, unsigned int ifindex, 
		    uint32_t prefered, uint32_t valid)
				      
{
	uint8_t buf[256];
	struct nlmsghdr *n;
	struct ifaddrmsg *ifa;

	memset(buf, 0, sizeof(buf));
	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	n->nlmsg_flags = (__u16)(NLM_F_REQUEST | NLM_F_ACK | nlmsg_flags);
	n->nlmsg_type = (__u16)cmd;

	ifa = NLMSG_DATA(n);
	ifa->ifa_family = AF_INET6;
	ifa->ifa_prefixlen = plen;
	ifa->ifa_flags = flags;
	ifa->ifa_scope = scope;
	ifa->ifa_index = ifindex;

	addattr_l(n, sizeof(buf), IFA_LOCAL, addr, sizeof(*addr));

	if (prefered || valid) {
		struct ifa_cacheinfo ci;
		ci.ifa_prefered = prefered;
		ci.ifa_valid = valid;
		ci.cstamp = 0;
		ci.tstamp = 0;
		addattr_l(n, sizeof(buf), IFA_CACHEINFO, &ci, sizeof(ci));
	}

	return rtnl_route_do(n, NULL);
}

/*!
 * \brief Adding a new IPv6 address to a given interface
 *
 * \param addr IPv6 address
 * \param plen Prefix length
 * \param flags IPv6 address flags
 * \param scope IPv6 address scope, i.e. RT_SCOPE_UNIVERSE
 * \param ifindex Interface index
 * \param prefered Preferred lifetime
 * \param valid Valid lifetime
 * \return Zero if OK
 */
int rtnl_addr_add(const struct in6_addr *addr, uint8_t plen, 
	     uint8_t flags, uint8_t scope, unsigned int ifindex, 
	     uint32_t prefered, uint32_t valid)
{
	return rtnl_addr_mod(RTM_NEWADDR, NLM_F_CREATE|NLM_F_REPLACE,
			addr, plen, flags, scope, ifindex, prefered, valid);
}

/*!
 * \brief Deleting an IPv6 address of a given interface
 *
 * \param addr IPv6 address
 * \param plen Prefix length
 * \param ifindex Interface index
 * \return Zero if OK
 */
int rtnl_addr_del(const struct in6_addr *addr, uint8_t plen, unsigned int ifindex)
{
	return rtnl_addr_mod(RTM_DELADDR, 0, addr, plen, 0, 0, ifindex, 0, 0);
}

/*!
 * \brief Modifying IPv6 route configuration
 *
 * Please use this function carefully! The cmd parameter should be one of the 
 * available RTM_* defines, which are correct for route modification.
 * \param cmd Command One RTM_* define for route configuration
 * \param nlmsg_flags RTNetlink flags
 * \param oif Outgoing interface index
 * \param table Route table id
 * \param proto Protocol id
 * \param flags IPv6 route flags
 * \param priority Priority
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param gateway Gateway address
 * \return Zero if OK
 */
static int rtnl_route_mod(int cmd, uint16_t nlmsg_flags, unsigned int oif, uint8_t table, uint8_t proto,
		     unsigned flags, uint32_t priority,
		     const struct in6_addr *src, int src_plen,
		     const struct in6_addr *dst, int dst_plen,
		     const struct in6_addr *gateway)
{
	int ret;
	uint8_t buf[512];
	struct nlmsghdr *n;
	struct rtmsg *rtm;

	if (cmd == RTM_NEWROUTE && oif == 0)
		return -1;

	memset(buf, 0, sizeof(buf));
	n = (struct nlmsghdr *)buf;
	
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct rtmsg));
	n->nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK;
	n->nlmsg_flags |= nlmsg_flags;
	n->nlmsg_type = (__u16)cmd;

	rtm = NLMSG_DATA(n);
	rtm->rtm_family = AF_INET6;
	rtm->rtm_dst_len = (unsigned char)dst_plen;
	rtm->rtm_src_len = (unsigned char)src_plen;
	rtm->rtm_table = table;
	rtm->rtm_protocol = proto;
	rtm->rtm_scope = RT_SCOPE_UNIVERSE;
	rtm->rtm_type = RTN_UNICAST;
	rtm->rtm_flags = flags;

	addattr_l(n, sizeof(buf), RTA_DST, dst, sizeof(*dst));
	if (src)
		addattr_l(n, sizeof(buf), RTA_SRC, src, sizeof(*src));
	addattr32(n, sizeof(buf), RTA_OIF, oif);
	if (gateway)
		addattr_l(n, sizeof(buf), 
			  RTA_GATEWAY, gateway, sizeof(*gateway));
	if (priority)
		addattr32(n, sizeof(buf), RTA_PRIORITY, priority);

	ret = rtnl_route_do(n, NULL);

	return ret;
}

/*!
 * \brief Adding IPv6 route
 *
 * Note: It won't be able to add multiple entries with the same 
 * source / destination addresses
 * \param oif Outgoing interface index
 * \param table Route table id
 * \param proto Protocol id
 * \param flags IPv6 route flags
 * \param metric Route preference (priority)
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param gateway Gateway address
 * \return Zero if OK
 */
int rtnl_route_add(unsigned int oif, uint8_t table, uint8_t proto,
	      unsigned flags, uint32_t metric, 
	      const struct in6_addr *src, int src_plen,
	      const struct in6_addr *dst, int dst_plen, 
	      const struct in6_addr *gateway)
{
	return rtnl_route_mod(RTM_NEWROUTE, NLM_F_CREATE | NLM_F_EXCL, 			
			 oif, table, proto, flags,
			 metric, src, src_plen, dst, dst_plen, gateway);
}

/*!
 * \brief Adding IPv6 route
 *
 * Note: It is able to add multiple entries with the same 
 * source / destination addresses
 * \param oif Outgoing interface index
 * \param table Route table id
 * \param proto Protocol id
 * \param flags IPv6 route flags
 * \param metric Route preference (priority)
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param gateway Gateway address
 * \return Zero if OK
 */
int rtnl_route_append(unsigned int oif, uint8_t table, uint8_t proto,
	      unsigned flags, uint32_t metric, 
	      const struct in6_addr *src, int src_plen,
	      const struct in6_addr *dst, int dst_plen, 
	      const struct in6_addr *gateway)
{
	return rtnl_route_mod(RTM_NEWROUTE, NLM_F_CREATE | NLM_F_APPEND,
			 oif, table, proto, flags,
			 metric, src, src_plen, dst, dst_plen, gateway);
}

/*!
 * \brief Deleting IPv6 route
 *
 * \param oif Outgoing interface index
 * \param table Route table id
 * \param metric Route preference (priority)
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param gateway Gateway address
 * \return Zero if OK
 */
int rtnl_route_del(unsigned int oif, uint8_t table, uint32_t metric, 
	      const struct in6_addr *src, int src_plen,
	      const struct in6_addr *dst, int dst_plen,
	      const struct in6_addr *gateway)
{
	return rtnl_route_mod(RTM_DELROUTE, 0, oif, table, RTPROT_UNSPEC, 
			 0, metric, src, src_plen, dst, dst_plen, gateway);
}

/*!
 * \brief Modifying IPv6 rule configuration (for source-routing)
 *
 * Please use this function carefully! The cmd parameter should be one of the 
 * available RTM_* defines, which are correct for rule modification.
 * \param iface Interface index
 * \param cmd Command One RTM_* define for rule configuration
 * \param table Routing table id
 * \param priority Priority
 * \param fwmark Flow mark
 * \param action Rule action, i.e. RTN_UNICAST  
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param flags IPv6 rule flags
 * \return Zero if OK
 */
static int rtnl_rule_mod(unsigned int iface, int cmd, uint8_t table,
		    uint32_t priority, uint32_t fwmark, uint8_t action,
		    const struct in6_addr *src, int src_plen,
		    const struct in6_addr *dst, int dst_plen, unsigned int flags)
{
	uint8_t buf[512];
	struct nlmsghdr *n;
	struct rtmsg *rtm;
	char ifname[IF_NAMESIZE];

	memset(buf, 0, sizeof(buf));
	n = (struct nlmsghdr *)buf;
	
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct rtmsg));
	n->nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK;
	if (cmd == RTM_NEWRULE) {
		n->nlmsg_flags |= NLM_F_CREATE;
	}
	n->nlmsg_type = (__u16)cmd;

	rtm = NLMSG_DATA(n);
	rtm->rtm_family = AF_INET6;
	rtm->rtm_dst_len = (unsigned char)dst_plen;
	rtm->rtm_src_len = (unsigned char)src_plen;
	rtm->rtm_table = table;
	rtm->rtm_scope = RT_SCOPE_UNIVERSE;
	rtm->rtm_type = action;
	rtm->rtm_flags = flags;

	addattr_l(n, sizeof(buf), RTA_DST, dst, sizeof(*dst));
	if (src)
		addattr_l(n, sizeof(buf), RTA_SRC, src, sizeof(*src));
	if (priority)
		addattr32(n, sizeof(buf), RTA_PRIORITY, priority);
	if (fwmark)
		addattr32(n, sizeof(buf), RTA_PROTOINFO, fwmark);
	if (iface) {
		if (if_indextoname(iface, ifname) != NULL) {
			addattr_l(n, sizeof(buf), RTA_IIF, ifname, strlen(ifname) + 1);
		}
	}

	return rtnl_route_do(n, NULL);
}

/*!
 * \brief Adding IPv6 rule (for source-routing)
 *
 * \param iface Interface index
 * \param table Routing table id
 * \param priority Priority
 * \param fwmark Flow mark
 * \param action Rule action, i.e. RTN_UNICAST  
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param flags IPv6 rule flags
 * \return Zero if OK
 */
int rtnl_rule_add(unsigned int iface, uint8_t table,
	     uint32_t priority, uint32_t fwmark, uint8_t action,
	     const struct in6_addr *src, int src_plen,
	     const struct in6_addr *dst, int dst_plen, unsigned int flags)
{
	return rtnl_rule_mod(iface, RTM_NEWRULE, table,
			priority, fwmark, action,
			src, src_plen, dst, dst_plen, flags);
}

/*!
 * \brief Deleting IPv6 rule (for source-routing)
 *
 * \param iface Interface index
 * \param table Routing table id
 * \param priority Priority
 * \param fwmark Flow mark
 * \param action Rule action, i.e. RTN_UNICAST  
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param flags IPv6 rule flags
 * \return Zero if OK
 */
int rtnl_rule_del(unsigned int iface, uint8_t table,
	     uint32_t priority, uint32_t fwmark, uint8_t action,
	     const struct in6_addr *src, int src_plen,
	     const struct in6_addr *dst, int dst_plen, unsigned int flags)
{
	return rtnl_rule_mod(iface, RTM_DELRULE, table,
			priority, fwmark, action,
			src, src_plen, dst, dst_plen, flags);
}

/*!
 * \brief Modifying IPv6 neighbor cache
 *
 * Please use this function carefully! The cmd parameter should be one of the 
 * available RTM_* defines, which are correct for rule modification.
 * \param cmd Command One RTM_* define for rule configuration
 * \param nlmsg_flags RTNetlink flags
 * \param ifindex Interface index
 * \param state Neighbor state
 * \param flags IPv6 neighbor flags
 * \param dst Destination address
 * \param hwa L2 hardware address
 * \param hwalen Length of L2 hardware address
 * \return Zero if OK
 */
static int rtnl_neigh_mod(int cmd, uint16_t nlmsg_flags, int ifindex,
			uint16_t state, uint8_t flags, struct in6_addr *dst,
			uint8_t *hwa, int hwalen)
{
	uint8_t buf[256];
	struct nlmsghdr *n;
	struct ndmsg *ndm;

	memset(buf, 0, sizeof(buf));
	n = (struct nlmsghdr *)buf;
	n->nlmsg_len = (__u32)NLMSG_LENGTH(sizeof(struct ndmsg));
	n->nlmsg_flags = (__u16)(NLM_F_REQUEST | nlmsg_flags);
	n->nlmsg_type = (__u16)cmd;

	ndm = NLMSG_DATA(n);
	ndm->ndm_family = AF_INET6;
	ndm->ndm_ifindex = ifindex;
	ndm->ndm_state = state;
	ndm->ndm_flags = flags;
	ndm->ndm_type = RTN_UNICAST;

	addattr_l(n, sizeof(buf), NDA_DST, dst, sizeof(*dst));

	if (hwa) 
		addattr_l(n, sizeof(buf), NDA_LLADDR, hwa, hwalen);

	return rtnl_route_do(n, NULL);
}

/*!
 * \brief Adding new entry to the IPv6 neighbor cache
 *
 * \param ifindex Interface index
 * \param state Neighbor state
 * \param flags IPv6 neighbor flags
 * \param dst Destination address
 * \param hwa L2 hardware address
 * \param hwalen Length of L2 hardware address
 * \param override If non-zero replaces the existing entry
 * \return Zero if OK
 */
int rtnl_neigh_add(int ifindex, uint16_t state, uint8_t flags,
		struct in6_addr *dst, uint8_t *hwa, int hwalen,
		int override)
{
	return rtnl_neigh_mod(RTM_NEWNEIGH, NLM_F_CREATE | (override ? NLM_F_REPLACE : 0),
			ifindex, state, flags,dst, hwa, hwalen);
}

/*!
 * \brief Delete entry from the IPv6 neighbor cache
 *
 * \param ifindex Interface index
 * \param dst Destination address
 * \return Zero if OK
 */
int rtnl_neigh_del(int ifindex, struct in6_addr *dst)
{
	return rtnl_neigh_mod(RTM_DELNEIGH, 0, ifindex, 0, 0, dst, NULL, 0);
}

/*!
 * \brief Adding new entry to the IPv6 neighbor cache for proxy neighbor discovery
 *
 * \param ifindex Interface index
 * \param flags IPv6 neighbor flags
 * \param dst Destination address
 * \return Zero if OK
 */
int rtnl_pneigh_add(int ifindex, uint8_t flags, struct in6_addr *dst)
{
	return rtnl_neigh_mod(RTM_NEWNEIGH, NLM_F_CREATE | NLM_F_REPLACE,
			ifindex, NUD_PERMANENT, flags|NTF_PROXY, dst,
			NULL, 0);
}

/*!
 * \brief Delete proxy neighbor discovery entry from the IPv6 neighbor cache
 *
 * \param ifindex Interface index
 * \param dst Destination address
 * \return Zero if OK
 */
int rtnl_pneigh_del(int ifindex, struct in6_addr *dst)
{
	return rtnl_neigh_mod(RTM_DELNEIGH, 0, ifindex, 0, NTF_PROXY, dst, NULL, 0);
}

/*!
 * \brief Iterates over addresses / routes / rules / etc., 
 * and calls func for each of them
 *
 * It is a wrapper for rtnl_dump_filter function
 * \param proto Protocol. It could be eg. NETLINK_ROUTE or NETLINK_XFRM, etc.
 * \param type RTNetlink command type. It could be eg. RTM_GETADDR or RTM_GETROUTE, etc.
 * \param func Callback function
 * \param extarg Argument for func
 * \return Zero if OK
 */
int rtnl_iterate(int proto, int type, rtnl_filter_t func, void *extarg)
{
	struct rtnl_handle rth;

	if (rtnl_open_byproto(&rth, 0, proto) < 0)
		return -1;

	if (rtnl_wilddump_request(&rth, AF_INET6, type) < 0) {
		rtnl_close(&rth);
		return -1;
	}

	if (rtnl_dump_filter(&rth, func, extarg, NULL, NULL) < 0) {
		rtnl_close(&rth);
		return -1;
	}

	rtnl_close(&rth);

	return 0;
}

/*!
 * \brief Helper structure for getting link info
 */
struct rtnl_link_get_arg {
	/*! Interface index */		unsigned int iface;
	/*! Interface flags */		unsigned int flags;
	/*! Interface type */		unsigned short type;
};

/*!
 * \brief Callback function, used by rtnl_get_link, to check the current link status of the given interface
 *
 * \param sock Unused
 * \param n RTNetlink message
 * \param a Argument, it should be rtnl_link_get_arg type
 * \return Zero if OK
 */
static int __rtnl_link_get(const struct sockaddr_nl * sock, struct nlmsghdr * n, void * a)
{
	struct rtnl_link_get_arg * arg = (struct rtnl_link_get_arg *)a;
	struct ifinfomsg *ifi = NLMSG_DATA(n);

	if (arg == NULL)
		return -1;

	if (n->nlmsg_len < NLMSG_LENGTH(sizeof(*ifi)))
		return -1;

	if (n->nlmsg_type != RTM_NEWLINK)
		return 0;

	if (ifi->ifi_index != arg->iface)
		return 0;

	arg->type = ifi->ifi_type;
	arg->flags = ifi->ifi_flags;

	return 0;
}

/*!
 * \brief Get link status of the given interface
 *
 * \param iface Interface index
 * \param type OUT pointer. Interface type (See ARPHRD_*)
 * \param flags OUT pointer. Interface flags (See IFF_*)
 * \return Zero if OK
 */
int rtnl_link_get(unsigned int iface, unsigned short * type, unsigned int * flags)
{
	int ret;
	struct rtnl_link_get_arg arg;

	arg.iface = iface;
	arg.type = 0;
	arg.flags = 0;

	ret = rtnl_iterate(NETLINK_ROUTE, RTM_GETLINK, __rtnl_link_get, &arg);
	if (ret)
		return ret;

	if (type)
		*type = arg.type;
	if (flags)
		*flags = arg.flags;

	return 0;
}

/*!
 * \brief Helper structure for checking IPv6 addresses
 */
struct rtnl_addr_isused_arg {
	/*! IPv6 address */			struct in6_addr * addr;
	/*! IPv6 prefix length */	uint8_t plen;
	/*! Interface index */		unsigned int iface;
	/*! IPv6 address scope */	uint8_t scope;
	/*! Used or not? */			int used;
};

/*!
 * \brief Callback function, used by rtnl_addr_isused, to check the current IPv6 addresses

 * If the specified address is used on the specified interface, the used flag in the structure
 * argument will be set to 1
 * \param sock Unused
 * \param n RTNetlink message
 * \param a Argument, it should be rtnl_addr_isused_arg type
 * \return Zero if OK
 */
static int __rtnl_addr_isused(const struct sockaddr_nl * sock, struct nlmsghdr * n, void * a)
{
	struct rtnl_addr_isused_arg * arg = (struct rtnl_addr_isused_arg *)a;
	struct ifaddrmsg *ifa = NLMSG_DATA(n);
	struct rtattr * rta_tb[IFA_MAX+1];
	struct in6_addr * addr;

	if (arg == NULL)
		return -1;

	if (n->nlmsg_len < NLMSG_LENGTH(sizeof(*ifa)))
		return -1;

	if (n->nlmsg_type != RTM_NEWADDR)
		return 0;

	if (ifa->ifa_scope != arg->scope)
		return 0;

 	if (ifa->ifa_index != (uint32_t)arg->iface)
		return 0;

	if (ifa->ifa_prefixlen != arg->plen)
		return 0;

	memset(rta_tb, 0, sizeof(rta_tb));
	parse_rtattr(rta_tb, IFA_MAX, IFA_RTA(ifa), n->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa)));

	if (rta_tb[IFA_ADDRESS] == 0)
		return 0;

	addr = RTA_DATA(rta_tb[IFA_ADDRESS]);

	if (memcmp(addr, arg->addr, sizeof(struct in6_addr)) == 0) {
		arg->used = 1;
		return 0;
	}

	return 0;
}

/*!
 * \brief Checking IPv6 address in the system
 *
 * If the specified address is configured it returns true (non zero)
 * \param addr IPv6 address
 * \param plen Prefix length
 * \param scope Address scope
 * \param iface Interface index
 * \return Non-zero if the address is configured
 */
int rtnl_addr_isused(struct in6_addr * addr, uint8_t plen, uint8_t scope, unsigned int iface)
{
	int ret;
	struct rtnl_addr_isused_arg arg;

	arg.addr = addr;
	arg.plen = plen;
	arg.scope = scope;
	arg.iface = iface;
	arg.used = 0;

	ret = rtnl_iterate(NETLINK_ROUTE, RTM_GETADDR, __rtnl_addr_isused, &arg);
	if (ret)
		return ret;

	return arg.used;
}

/*!
 * \brief Callback function, used by rtnl_flush_home_addr, to delete all of the home addresses 
 *
 * \param sock Unused
 * \param n RTNetlink message
 * \param a Argument, Unused
 * \return Zero if OK
 */
static int __rtnl_addr_flush_home_addr(const struct sockaddr_nl * sock, struct nlmsghdr * n, void * a)
{
	struct ifaddrmsg *ifa = NLMSG_DATA(n);
	struct rtattr * rta_tb[IFA_MAX+1];
	struct in6_addr * addr;

	if (n->nlmsg_len < NLMSG_LENGTH(sizeof(*ifa)))
		return -1;

	if (n->nlmsg_type != RTM_NEWADDR)
		return 0;

	if ((ifa->ifa_flags & IFA_F_HOMEADDRESS) != IFA_F_HOMEADDRESS)
		return 0;

	memset(rta_tb, 0, sizeof(rta_tb));
	parse_rtattr(rta_tb, IFA_MAX, IFA_RTA(ifa), n->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa)));

	if (rta_tb[IFA_ADDRESS] == 0)
		return 0;

	addr = RTA_DATA(rta_tb[IFA_ADDRESS]);

	rtnl_addr_del(addr, ifa->ifa_prefixlen, ifa->ifa_index);
	
	return 0;
}

/*!
 * \brief Flush (delete) all IPv6 address in the system with HOMEADDRESS flag
 * \return Zero if OK
 */
int rtnl_addr_flush_home_addr()
{
	return rtnl_iterate(NETLINK_ROUTE, RTM_GETADDR, __rtnl_addr_flush_home_addr, NULL);
}

/*!
 * \brief Helper structure for getting IPv6 addresses
 */
struct rtnl_addr_get_arg {
	/*! Interface index */		unsigned int iface;
	/*! IPv6 address */			struct in6_addr addrs[MSG_ENV_ADDR_GET_NUM];
	/*! IPv6 prefix length */	uint8_t plens[MSG_ENV_ADDR_GET_NUM];
	/*! IPv6 address scope */	uint8_t scopes[MSG_ENV_ADDR_GET_NUM];
	/*! Number of addresses */	int num;
};

/*!
 * \brief Callback function, used by rtnl_addr_get, to get the current IPv6 addresses
 *
 * \param sock Unused
 * \param n RTNetlink message
 * \param a Argument, it should be rtnl_addr_isused_arg type
 * \return Zero if OK
 */
static int __rtnl_addr_get(const struct sockaddr_nl * sock, struct nlmsghdr * n, void * a)
{
	struct rtnl_addr_get_arg * arg = (struct rtnl_addr_get_arg *)a;
	struct ifaddrmsg *ifa = NLMSG_DATA(n);
	struct rtattr * rta_tb[IFA_MAX+1];
	struct in6_addr * addr;

	if (arg == NULL)
		return -1;

	if (n->nlmsg_len < NLMSG_LENGTH(sizeof(*ifa)))
		return -1;

	if (n->nlmsg_type != RTM_NEWADDR)
		return 0;

 	if (ifa->ifa_index != (uint32_t)arg->iface)
		return 0;

	memset(rta_tb, 0, sizeof(rta_tb));
	parse_rtattr(rta_tb, IFA_MAX, IFA_RTA(ifa), n->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa)));

	if (rta_tb[IFA_ADDRESS] == 0)
		return 0;

	addr = RTA_DATA(rta_tb[IFA_ADDRESS]);

	memcpy(&arg->addrs[arg->num], addr, sizeof(struct in6_addr));
	arg->plens[arg->num] = ifa->ifa_prefixlen;
	arg->scopes[arg->num] = ifa->ifa_scope;
	arg->num++;

	return 0;
}

/*!
 * \brief Getting IPv6 address of the given interface
 *
 * \param iface Interface index
 * \param reply Reply struct
 * \return Zero if OK
 */
int rtnl_addr_get(unsigned int iface, struct msg_env_addr_get_reply * reply)
{
	int ret;
	struct rtnl_addr_get_arg arg;
	int i;

	memset(&arg, 0, sizeof(arg));
	arg.iface = iface;

	ret = rtnl_iterate(NETLINK_ROUTE, RTM_GETADDR, __rtnl_addr_get, &arg);
	if (ret)
		return ret;

	if (reply) {
		memset(reply, 0, sizeof(*reply));
		for (i = 0; i < arg.num && i < MSG_ENV_ADDR_GET_NUM; i++) {
			memcpy(&reply->addrs[i], &arg.addrs[i], sizeof(struct in6_addr));
			reply->plens[i] = arg.plens[i];
			reply->scopes[i] = arg.scopes[i];
		}
		if (i < MSG_ENV_ADDR_GET_NUM - 1)
			reply->last[i] = 1;
	}

	return 0;
}

/*!
 * \brief Helper structure for checking IPv6 routes
 */
struct rtnl_route_isused_arg {
	/*! Outgoing interface index */				unsigned int oif;
	/*! Routing table id */						uint8_t table;
	/*! Route preference */						uint32_t metric;
	/*! Source address */						struct in6_addr *src;
	/*! Prefix length of source address */		int src_plen;
	/*! Destination address */					struct in6_addr *dst;
	/*! Prefix length of destination address */ int dst_plen;
	/*! gateway address */						struct in6_addr *gateway;
	/*! Used or not? */							int used;
};

/*!
 * \brief Callback function, used by rtnl_route_isused, to check the current IPv6 routes
 *
 * If the specified route is configured, the used flag in the struct argument will be set to 1
 * \param sock Unused
 * \param n RTNetlink message
 * \param a Argument, it should be rtnl_route_isused_arg type
 * \return Zero if OK
 */
static int __rtnl_route_isused(const struct sockaddr_nl * sock, struct nlmsghdr * n, void * a)
{
	struct rtnl_route_isused_arg * arg = (struct rtnl_route_isused_arg *)a;
	struct rtmsg *rtm = NLMSG_DATA(n);
	struct rtattr * rtm_tb[RTM_MAX+1];
	struct in6_addr * addr;
	struct in6_addr addr_any;
	int * oif;
	uint32_t * prio;

	ipv6_addr_set(&addr_any, 0x0, 0x0, 0x0, 0x0);

	if (arg == NULL) {
		return -1;
	}

	if (n->nlmsg_len < NLMSG_LENGTH(sizeof(*rtm))) {
		return -1;
	}

	if (n->nlmsg_type != RTM_NEWROUTE)
		return 0;

	if (rtm->rtm_scope != RT_SCOPE_UNIVERSE)
		return 0;

	if (rtm->rtm_table != arg->table) {
		if (arg->table == 0 && rtm->rtm_table != RT_TABLE_MAIN)
			return 0;
	}

	if (rtm->rtm_src_len != arg->src_plen)
		return 0;
	
	if (rtm->rtm_dst_len != arg->dst_plen)
		return 0;

	memset(rtm_tb, 0, sizeof(rtm_tb));
	parse_rtattr(rtm_tb, RTM_MAX, RTM_RTA(rtm), n->nlmsg_len - NLMSG_LENGTH(sizeof(*rtm)));

	if (rtm_tb[RTA_DST] != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_DST]);

		if (memcmp(addr, arg->dst, sizeof(struct in6_addr)) != 0) 
			return 0;
	} else {
		if (memcmp(&addr_any, arg->dst, sizeof(struct in6_addr)) != 0)
			return 0;
	}

	if (rtm_tb[RTA_SRC] != NULL && arg->src != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_SRC]);

		if (memcmp(addr, arg->src, sizeof(struct in6_addr)) != 0) 
			return 0;
	}

	if (rtm_tb[RTA_OIF] == NULL) {
		return 0;
	}

	oif = RTA_DATA(rtm_tb[RTA_OIF]);

	if (*oif != arg->oif)
		return 0;

	if (rtm_tb[RTA_GATEWAY] != NULL && arg->gateway != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_GATEWAY]);
		
		if (memcmp(addr, arg->gateway, sizeof(struct in6_addr)) != 0) 
			return 0;
	}

	if (rtm_tb[RTA_PRIORITY] == NULL) {
		return 0;
	}

	prio = RTA_DATA(rtm_tb[RTA_PRIORITY]);
	
	if (*prio != arg->metric && arg->metric != 0) {
		return 0;
	}

	arg->used = 1;

	return 0;
}

/*!
 * \brief Checking IPv6 route in the system
 *
 * If the specified route is configured it returns true (non zero)
 * \param oif Outgoing interface index
 * \param table Routing table id
 * \param metric Route preference
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param gateway Gateway address
 * \return Non-zero if the route is configured
 */
int rtnl_route_isused(unsigned int oif, uint8_t table, uint32_t metric,
	      struct in6_addr *src, int src_plen,
	      struct in6_addr *dst, int dst_plen, 
	      struct in6_addr *gateway)
{
	int ret;
	struct rtnl_route_isused_arg arg;

	arg.oif = oif;
	arg.table = table;
	arg.metric = metric;
	arg.src = src;
	arg.src_plen = src_plen;
	arg.dst = dst;
	arg.dst_plen = dst_plen;
	arg.gateway = gateway;
	arg.used = 0;

	ret = rtnl_iterate(NETLINK_ROUTE, RTM_GETROUTE, __rtnl_route_isused, &arg);
	if (ret)
		return ret;

	return arg.used;
}

/*!
 * \brief Helper structure for getting IPv6 routes
 */
struct rtnl_route_get_arg {
	/*! Outgoing interface index */				unsigned int oif;
	/*! Routing table id */						uint8_t table;
	/*! Destination address */					struct in6_addr * dst;
	/*! Prefix length of destination address */ int dst_plen;
	/*! Route preference */						uint32_t metrics[MSG_ENV_ROUTE_GET_NUM];
	/*! Source address */						struct in6_addr srcs[MSG_ENV_ROUTE_GET_NUM];
	/*! Prefix length of source address */		int src_plens[MSG_ENV_ROUTE_GET_NUM];
	/*! gateway address */						struct in6_addr gateways[MSG_ENV_ROUTE_GET_NUM];
	/*! Number of route */						int num;
};

/*!
 * \brief Callback function, used by rtnl_route_get, to get IPv6 routes
 *
 * \param sock Unused
 * \param n RTNetlink message
 * \param a Argument, it should be rtnl_route_isused_arg type
 * \return Zero if OK
 */
static int __rtnl_route_get(const struct sockaddr_nl * sock, struct nlmsghdr * n, void * a)
{
	struct rtnl_route_get_arg * arg = (struct rtnl_route_get_arg *)a;
	struct rtmsg *rtm = NLMSG_DATA(n);
	struct rtattr * rtm_tb[RTM_MAX+1];
	struct in6_addr * addr;
	struct in6_addr addr_any;
	int * oif;
	uint32_t * prio;

	ipv6_addr_set(&addr_any, 0x0, 0x0, 0x0, 0x0);

	if (arg == NULL) {
		return -1;
	}

	if (n->nlmsg_len < NLMSG_LENGTH(sizeof(*rtm))) {
		return -1;
	}

	if (n->nlmsg_type != RTM_NEWROUTE)
		return 0;

	if (rtm->rtm_scope != RT_SCOPE_UNIVERSE)
		return 0;

	if (rtm->rtm_table != arg->table) {
		if (arg->table == 0 && rtm->rtm_table != RT_TABLE_MAIN)
			return 0;
	}

	if (rtm->rtm_dst_len != arg->dst_plen)
		return 0;

	memset(rtm_tb, 0, sizeof(rtm_tb));
	parse_rtattr(rtm_tb, RTM_MAX, RTM_RTA(rtm), n->nlmsg_len - NLMSG_LENGTH(sizeof(*rtm)));

	if (rtm_tb[RTA_OIF] == NULL) {
		return 0;
	}

	oif = RTA_DATA(rtm_tb[RTA_OIF]);

	if (*oif != arg->oif)
		return 0;

	if (rtm_tb[RTA_DST] != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_DST]);

		if (memcmp(addr, arg->dst, sizeof(struct in6_addr)) != 0)
			return 0;
	} else {
		if (memcmp(&addr_any, arg->dst, sizeof(struct in6_addr)) != 0)
			return 0;
	}

	if (rtm_tb[RTA_SRC] != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_SRC]);
		memcpy(&arg->srcs[arg->num], addr, sizeof(struct in6_addr));
		arg->src_plens[arg->num] = rtm->rtm_src_len;
	} else {
		memcpy(&arg->srcs[arg->num], &addr_any, sizeof(struct in6_addr));
		arg->src_plens[arg->num] = 0;
	}

	if (rtm_tb[RTA_GATEWAY] != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_GATEWAY]);
		memcpy(&arg->gateways[arg->num], addr, sizeof(struct in6_addr));
	} else
		memcpy(&arg->gateways[arg->num], &addr_any, sizeof(struct in6_addr));

	if (rtm_tb[RTA_PRIORITY] == NULL) {
		prio = RTA_DATA(rtm_tb[RTA_PRIORITY]);
		arg->metrics[arg->num] = *prio;
	} else
		arg->metrics[arg->num] = 0;

	arg->num++;

	return 0;
}

/*!
 * \brief Getting IPv6 routes
 *
 * \param oif Outgoing interface index
 * \param table Routing table id
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \param reply Reply struct
 *
 * \return Zero if OK
 */
int rtnl_route_get(unsigned int oif, uint8_t table,
	      struct in6_addr *dst, int dst_plen,
	      struct msg_env_route_get_reply * reply)
{
	int ret;
	struct rtnl_route_get_arg arg;
	int i;

	memset(&arg, 0, sizeof(arg));
	arg.oif = oif;
	arg.table = table;
	arg.dst = dst;
	arg.dst_plen = dst_plen;

	ret = rtnl_iterate(NETLINK_ROUTE, RTM_GETROUTE, __rtnl_route_get, &arg);
	if (ret)
		return ret;

	if (reply) {
		memset(reply, 0, sizeof(*reply));
		for (i = 0; i < arg.num && i < MSG_ENV_ROUTE_GET_NUM; i++) {
			memcpy(&reply->srcs[i], &arg.srcs[i], sizeof(struct in6_addr));
			reply->splens[i] = arg.src_plens[i];
			memcpy(&reply->gws[i], &arg.gateways[i], sizeof(struct in6_addr));
			reply->metrics[i] = arg.metrics[i];
		}
		if (i < MSG_ENV_ROUTE_GET_NUM - 1)
			reply->last[i] = 1;
	}

	return 0;
}

/*!
 * \brief Helper structure for checking IPv6 rules
 */
struct rtnl_rule_isused_arg {
	/*! Interface index */						unsigned int iface;
	/*! Routing table id */						uint8_t table;
	/*! Priority */								uint32_t priority;
	/*! Flow mark */							uint32_t fwmark;
	/*! Action, eg. RTN_UNICAST */				uint8_t action;
	/*! Source address */						struct in6_addr *src;
	/*! Prefix length of source address */		int src_plen;
	/*! Destination address */					struct in6_addr *dst;
	/*! Prefix length of destination address */ int dst_plen;
	/*! Used or not? */							int used;
};

/*!
 * \brief Callback function, used by rtnl_route_isused, to check the current IPv6 rules
 *
 * If the specified rule is configured, the used flag in the struct argument will be set to 1
 * \param sock Unused
 * \param n RTNetlink message
 * \param a Argument, it should be rtnl_rule_isused_arg type
 * \return Zero if OK
 */
static int __rtnl_rule_isused(const struct sockaddr_nl * sock, struct nlmsghdr * n, void * a)
{
	struct rtnl_rule_isused_arg * arg = (struct rtnl_rule_isused_arg *)a;
	struct rtmsg *rtm = NLMSG_DATA(n);
	struct rtattr * rtm_tb[RTM_MAX+1];
	struct in6_addr * addr;
	struct in6_addr addr_any;
	unsigned int * prio;
	uint32_t * fwmark;
	char * iface_str;
	int iface;

	ipv6_addr_set(&addr_any, 0x0, 0x0, 0x0, 0x0);

	if (arg == NULL) {
		return -1;
	}

	if (n->nlmsg_len < NLMSG_LENGTH(sizeof(*rtm))) {
		return -1;
	}

	if (n->nlmsg_type != RTM_NEWRULE)
		return 0;

	if (rtm->rtm_scope != RT_SCOPE_UNIVERSE)
		return 0;

	if (rtm->rtm_table != arg->table) {
		if (arg->table == 0 && rtm->rtm_table != RT_TABLE_MAIN)
			return 0;
	}

	if (rtm->rtm_src_len != arg->src_plen)
		return 0;
	
	if (rtm->rtm_dst_len != arg->dst_plen)
		return 0;

	memset(rtm_tb, 0, sizeof(rtm_tb));
	parse_rtattr(rtm_tb, RTM_MAX, RTM_RTA(rtm), n->nlmsg_len - NLMSG_LENGTH(sizeof(*rtm)));

	if (rtm_tb[RTA_DST] != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_DST]);

		if (memcmp(addr, arg->dst, sizeof(struct in6_addr)) != 0) 
			return 0;
	} else {
		if (memcmp(&addr_any, arg->dst, sizeof(struct in6_addr)) != 0)
			return 0;
	}

	if (rtm_tb[RTA_SRC] != NULL && arg->src != NULL) {
		addr = RTA_DATA(rtm_tb[RTA_SRC]);
		
		if (memcmp(addr, arg->src, sizeof(struct in6_addr)) != 0) 
			return 0;
	}

	if (rtm_tb[RTA_PRIORITY] == NULL) {
		return 0;
	}

	prio = RTA_DATA(rtm_tb[RTA_PRIORITY]);

	if (*prio != arg->priority && arg->priority != 0)
		return 0;

	if (rtm_tb[RTA_PROTOINFO] != NULL) {
		fwmark = RTA_DATA(rtm_tb[RTA_PROTOINFO]);

		if (*fwmark != arg->fwmark && arg->fwmark != 0)
			return 0;
	}

	if (rtm_tb[RTA_IIF] != NULL && arg->iface != 0) {
		iface_str = RTA_DATA(rtm_tb[RTA_IIF]);
		iface = if_nametoindex(iface_str);
		if (iface != arg->iface)
			return 0;
	}

	arg->used = 1;

	return 0;
}

/*!
 * \brief Checking IPv6 rule in the system
 *
 * If the specified route is configured it returns true (non zero)
 * \param iface Interface index
 * \param table Routing table id
 * \param priority Priority
 * \param fwmark Flow mark
 * \param action Action
 * \param src Source address
 * \param src_plen Prefix length of the source address
 * \param dst Destination address
 * \param dst_plen Prefix length of the destination address
 * \return Non-zero if the rule is configured
 */
int rtnl_rule_isused(unsigned int iface, uint8_t table,
	     uint32_t priority, uint32_t fwmark, uint8_t action,
	     struct in6_addr *src, int src_plen,
	     struct in6_addr *dst, int dst_plen)
{
	int ret;
	struct rtnl_rule_isused_arg arg;

	arg.iface = iface;
	arg.table = table;
	arg.priority = priority;
	arg.fwmark = fwmark;
	arg.action = action;
	arg.src = src;
	arg.src_plen = src_plen;
	arg.dst = dst;
	arg.dst_plen = dst_plen;
	arg.used = 0;

	ret = rtnl_iterate(NETLINK_ROUTE, RTM_GETRULE, __rtnl_rule_isused, &arg);
	if (ret)
		return ret;

	return arg.used;
}

/*!
 * \brief Process received (listened) RTNetlink event messages
 *
 * Processing of nlmessage-s and internal messaging forwarding
 *
 * \param who who
 * \param n Netlink message
 * \param arg arg
 *
 * \return Zero if OK
 */
static int rtnl_process_listen(const struct sockaddr_nl * who, struct nlmsghdr * n, void *arg)
{
	size_t sizes[] = {
			[RTM_NEWLINK] = sizeof(struct ifinfomsg),
			[RTM_DELLINK] = sizeof(struct ifinfomsg),
			[RTM_NEWNEIGH] = sizeof(struct ndmsg),
			[RTM_NEWADDR] = sizeof(struct ifaddrmsg),
			[RTM_DELADDR] = sizeof(struct ifaddrmsg),
			[RTM_NEWROUTE] = sizeof(struct rtmsg),
			[RTM_DELROUTE] = sizeof(struct rtmsg),
	};
	int sizes_max = sizeof(sizes) / sizeof(size_t);
	struct ifinfomsg *ifi;
	struct rtattr *rta_link_tb[IFLA_MAX+1];
	struct ndmsg *ndm;
	struct rtattr *rta_nd_tb[NDA_MAX+1];
	struct ifaddrmsg *ifa;
	struct rtattr *rta_addr_tb[IFA_MAX+1];
	struct rtmsg * rtm;
	struct rtattr *rta_rt_tb[RTA_MAX+1];

	if (n->nlmsg_type >= sizes_max)
		return 0;
	if (sizes[n->nlmsg_type] == 0 || n->nlmsg_len < NLMSG_LENGTH(sizes[n->nlmsg_type]))
		return 0;

	switch (n->nlmsg_type) {
	case RTM_NEWLINK:
		ifi = NLMSG_DATA(n);
		if (ifi->ifi_family != AF_UNSPEC && ifi->ifi_family != AF_INET6)
			return 0;

		memset(rta_link_tb, 0, sizeof(rta_link_tb));
		parse_rtattr(rta_link_tb, IFLA_MAX, IFLA_RTA(ifi), n->nlmsg_len - NLMSG_LENGTH(sizeof(*ifi)));

		do {
			struct evt_env_newlink nlevt;
			//int32_t devconf[DEVCONF_MAX];

			nlevt.ifindex = ifi->ifi_index;

			if (rta_link_tb[IFLA_IFNAME])
				strncpy(nlevt.ifname, RTA_DATA(rta_link_tb[IFLA_IFNAME]), sizeof(nlevt.ifname) - 1);
			else
				nlevt.ifname[0] = '\0';
			nlevt.flags = ifi->ifi_flags;
			nlevt.type = ifi->ifi_type;
			if (rta_link_tb[IFLA_ADDRESS])
				memcpy(nlevt.lladdr, RTA_DATA(rta_link_tb[IFLA_ADDRESS]), ETH_ALEN);
			else
				memset(nlevt.lladdr, 0, ETH_ALEN);
			/*
			if (rta_link_tb[IFLA_PROTINFO]) {
				struct rtattr *inet6_tb[IFLA_INET6_MAX+1];

				memset(inet6_tb, 0, sizeof(inet6_tb));
				parse_rtattr(inet6_tb, IFLA_INET6_MAX, RTA_DATA(rta_link_tb[IFLA_PROTINFO]), rta_link_tb[IFLA_PROTINFO]->rta_len);
				if (inet6_tb[IFLA_INET6_CONF])
					memcpy(devconf, RTA_DATA(inet6_tb[IFLA_INET6_CONF]), sizeof(devconf));
			}
			*/

#ifdef ENVIRONMENT_DEBUG
			DEBUG("NEWLINK: %d %s %08X type %u", nlevt.ifindex, nlevt.ifname, nlevt.flags, nlevt.type);
#endif /* ENVIRONMENT_DEBUG */
			imsg_send_event(env_opts.msg_ids.me, EVT_ENV_NEWLINK, &nlevt, sizeof(nlevt), 1, NULL);
		} while(0);
		break;
	case RTM_DELLINK:
		ifi = NLMSG_DATA(n);
		if (ifi->ifi_family != AF_UNSPEC && ifi->ifi_family != AF_INET6)
			return 0;
		do {
			struct evt_env_dellink dlevt;

			dlevt.ifindex = ifi->ifi_index;

#ifdef ENVIRONMENT_DEBUG
			DEBUG("DELLINK: %d", dlevt.ifindex);
#endif /* ENVIRONMENT_DEBUG */
			imsg_send_event(env_opts.msg_ids.me, EVT_ENV_DELLINK, &dlevt, sizeof(dlevt), 1, NULL);
		} while(0);
		break;
	case RTM_NEWNEIGH:
		ndm = NLMSG_DATA(n);
		if (ndm->ndm_family != AF_INET6)
			return 0;
		memset(rta_nd_tb, 0, sizeof(rta_nd_tb));
		parse_rtattr(rta_nd_tb, NDA_MAX, NDA_RTA(ndm), n->nlmsg_len - NLMSG_LENGTH(sizeof(*ndm)));

		do {
			struct evt_env_neigh nevt;

			nevt.ifindex = ndm->ndm_ifindex;
			nevt.state = ndm->ndm_state;

			if (rta_nd_tb[NDA_DST])
				memcpy(&nevt.addr, RTA_DATA(rta_nd_tb[NDA_DST]), sizeof(nevt.addr));
			else
				memset(&nevt.addr, 0, sizeof(nevt.addr));

#ifdef ENVIRONMENT_DEBUG
			DEBUG("NEWNEIGH: %d state %02X dest: " IP6ADDR_FMT, nevt.ifindex, nevt.state, IP6ADDR_TO_STR(&nevt.addr));
#endif /* ENVIRONMENT_DEBUG */
			imsg_send_event(env_opts.msg_ids.me, EVT_ENV_NEIGH, &nevt, sizeof(nevt), 1, NULL);
		} while(0);
		break;
	case RTM_NEWADDR:
	case RTM_DELADDR:
		ifa = NLMSG_DATA(n);
		memset(rta_addr_tb, 0, sizeof(rta_addr_tb));
		parse_rtattr(rta_addr_tb, IFA_MAX, IFA_RTA(ifa), n->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa)));
		if (!rta_addr_tb[IFA_ADDRESS])
			return 0;
		if (ifa->ifa_flags & IFA_F_SECONDARY)
			return 0;
		do {
			struct evt_env_addr aevt;

			aevt.ifindex = ifa->ifa_index;

			if (rta_addr_tb[IFA_ADDRESS])
				memcpy(&aevt.addr, RTA_DATA(rta_addr_tb[IFA_ADDRESS]), sizeof(aevt.addr));
			else
				memset(&aevt.addr, 0, sizeof(aevt.addr));
			aevt.plen = ifa->ifa_prefixlen;
			aevt.scope = ifa->ifa_scope;

			if (n->nlmsg_type == RTM_NEWADDR) {
				aevt.action = EVT_ENV_ADDR_NEW;
#ifdef ENVIRONMENT_DEBUG
				DEBUG("NEWADDR: %d " IP6ADDR_FMT " scope %02X", aevt.ifindex, IP6ADDR_TO_STR(&aevt.addr), aevt.scope);
#endif /* ENVIRONMENT_DEBUG */
			} else {
				aevt.action = EVT_ENV_ADDR_DEL;
#ifdef ENVIRONMENT_DEBUG
				DEBUG("DELADDR: %d " IP6ADDR_FMT " scope %02X", aevt.ifindex, IP6ADDR_TO_STR(&aevt.addr), aevt.scope);
#endif /* ENVIRONMENT_DEBUG */
			}
			imsg_send_event(env_opts.msg_ids.me, EVT_ENV_ADDR, &aevt, sizeof(aevt), 1, NULL);
		} while(0);
		break;
	case RTM_NEWROUTE:
	case RTM_DELROUTE:
		rtm = NLMSG_DATA(n);
		memset(rta_rt_tb, 0, sizeof(rta_rt_tb));
		parse_rtattr(rta_rt_tb, RTA_MAX, RTM_RTA(rtm), n->nlmsg_len - NLMSG_LENGTH(sizeof(*rtm)));
		do {
			struct evt_env_route revt;


			if (rta_rt_tb[RTA_DST])
				memcpy(&revt.dst, RTA_DATA(rta_rt_tb[RTA_DST]), sizeof(revt.dst));
			else
				memset(&revt.dst, 0, sizeof(revt.dst));
			if (rta_rt_tb[RTA_GATEWAY])
				memcpy(&revt.gw, RTA_DATA(rta_rt_tb[RTA_GATEWAY]), sizeof(revt.gw));
			else
				memset(&revt.gw, 0, sizeof(revt.gw));
			revt.scope = rtm->rtm_scope;
			revt.type = rtm->rtm_type;
			revt.table = rtm->rtm_table;
			if (rta_rt_tb[RTA_OIF])
				revt.oif = *((uint32_t *)RTA_DATA(rta_rt_tb[RTA_OIF]));
			else
				revt.oif = -1;

			if (n->nlmsg_type == RTM_NEWROUTE) {
				revt.action = EVT_ENV_ROUTE_NEW;
#ifdef ENVIRONMENT_DEBUG
				DEBUG("NEWROUTE: %d: " IP6ADDR_FMT " via " IP6ADDR_FMT " scope %02X type %02X dev %d",
						revt.table, IP6ADDR_TO_STR(&revt.dst), IP6ADDR_TO_STR(&revt.gw), revt.scope, revt.type, revt.oif);
#endif /* ENVIRONMENT_DEBUG */
			} else {
				revt.action = EVT_ENV_ROUTE_DEL;
#ifdef ENVIRONMENT_DEBUG
				DEBUG("DELROUTE: %d: " IP6ADDR_FMT " via " IP6ADDR_FMT " scope %02X type %02X dev %d",
						revt.table, IP6ADDR_TO_STR(&revt.dst), IP6ADDR_TO_STR(&revt.gw), revt.scope, revt.type, revt.oif);
#endif /* ENVIRONMENT_DEBUG */
			}
			imsg_send_event(env_opts.msg_ids.me, EVT_ENV_ROUTE, &revt, sizeof(revt), 1, NULL);
		} while (0);
		break;
	}

	return 0;
}

/*!
 * \brief Endless loop: listening RTNetlink events (messages)
 *
 * \param arg Unused
 * \return Unused
 */
static void * rtnl_do_listen(void * arg)
{
	rtnl_listen(&rtnl_listen_h, rtnl_process_listen, NULL);

	return NULL;
}


/*!
 * \brief Listening for RTNetlink events
 *
 * The netlink events will be forwarded via imessaging
 *
 * \return Zero if OK
 */
int rtnl_start_listen()
{
    int ret;
    int val;
    pthread_t th;

    ret = rtnl_route_open(&rtnl_listen_h, 0);
    if (ret < 0)
    	return ret;

	val = RTNLGRP_LINK;
	ret = setsockopt(rtnl_listen_h.fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &val, sizeof(val));
	if (ret < 0)
		return -1;

	val = RTNLGRP_NEIGH;
	ret = setsockopt(rtnl_listen_h.fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &val, sizeof(val));
	if (ret < 0)
		return -1;

	val = RTNLGRP_IPV6_IFADDR;
	ret = setsockopt(rtnl_listen_h.fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &val, sizeof(val));
	if (ret < 0)
		return -1;

	val = RTNLGRP_IPV6_IFINFO;
	ret = setsockopt(rtnl_listen_h.fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &val, sizeof(val));
	if (ret < 0)
		return -1;

	val = RTNLGRP_IPV6_ROUTE;
	ret = setsockopt(rtnl_listen_h.fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &val, sizeof(val));
	if (ret < 0)
		return -1;

	val = RTNLGRP_IPV6_MROUTE;
	ret = setsockopt(rtnl_listen_h.fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &val, sizeof(val));
	if (ret < 0)
		return -1;

    th = threading_create(rtnl_do_listen, NULL);

    return (int)(th == (pthread_t)0);
}

/*! \} */
