
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-213 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-environment
 * \{
 */

#ifndef MIP6D_NG_ENV_IPTABLES_H
#define MIP6D_NG_ENV_IPTABLES_H

int ip6t_create_chain(const char * const table, const char * const chain, unsigned char force);
int ip6t_remove_chain(const char * const table, const char * const chain, unsigned char force);
int ip6t_flush_chain(const char * const table, const char * const chain, unsigned char force);
int ip6t_flush_all(const char * const table);
int ip6t_jump_rule(const char * const table, const char * const chain,
		const struct in6_addr * const src, uint8_t src_plen, unsigned int proto, const struct in6_addr * const dst, 
		uint8_t dst_plen, const char * const dst_chain);
int ip6t_jump_rule_del(const char * const table, const char * const chain,
		const struct in6_addr * const src, uint8_t src_plen, unsigned int proto, const struct in6_addr * const dst, 
		uint8_t dst_plen, const char * const dst_chain);
int ip6t_mark_rule(const char * const table, const char * const chain,
		const struct in6_addr * const src, const struct in6_addr * const dst, 
		uint8_t mark);
int ip6t_mark_rule_del(const char * const table, const char * const chain,
		const struct in6_addr * const src, const struct in6_addr * const dst, 
		uint8_t mark);

#endif /* MIP6D_NG_ENV_IPTABLES_H */

/*! \} */

