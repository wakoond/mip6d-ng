
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-environment
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <libiptc/ipt_kernel_headers.h>
#include <libiptc/libip6tc.h>
#include <linux/netfilter/xt_MARK.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/environment.h>

#include "iptables.h"

static const struct in6_addr in6addr_full = {
    .s6_addr = {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
    }
};

/*!
 * \brief Creating an ip6tables JUMP rule
 *
 * It could be activated with ip6t_append
 * \param src Source address
 * \param src_plen Source prefix length
 * \param proto IPv6 protocol (zero to skip)
 * \param dst Destination address
 * \param dst_plen Destination prefix length
 * \param chain Name of target chain
 * \return JUMP rule if OK, otherwise NULL
 */
static struct ip6t_entry * ip6t_alloc_jump(const struct in6_addr * const src, uint8_t src_plen, unsigned int proto,
		const struct in6_addr * const dst, uint8_t dst_plen, const char * const chain)
{
	struct ip6t_entry *e;
	struct ip6t_entry_target *t;
	uint16_t basesz, targetsz;

	if (chain == NULL)
		return NULL;

	basesz = (uint16_t)IP6T_ALIGN(sizeof(struct ip6t_entry));
    targetsz = (uint16_t)IP6T_ALIGN(sizeof(struct ip6t_entry_target)) + 
		IP6T_ALIGN(sizeof(uint8_t));

	e = malloc(basesz + targetsz);
	if (e == NULL)
		return NULL;
	memset(e, 0, basesz + targetsz);
	e->target_offset = basesz;
	e->next_offset = basesz + targetsz;

	/* Addresses and prefixes. */
	if (src != NULL) {
		memcpy(&e->ipv6.src, src, sizeof(*src));
		if (memcmp(src, &in6addr_any, sizeof(in6addr_any)) == 0)
			e->ipv6.smsk = in6addr_any;
		else if (src_plen < 129) {
			unsigned char * p = (void *)&e->ipv6.smsk;
			memset(p, 0xff, src_plen / 8);
			memset(p + (src_plen / 8) + 1, 0, (128 - src_plen) / 8);
			p[src_plen/8] = 0xff << (8 - (src_plen & 7));
		} else
			e->ipv6.smsk = in6addr_full;
	}
	e->ipv6.proto = proto;
	if (dst != NULL) {
		memcpy(&e->ipv6.dst, dst, sizeof(*dst));
		if (memcmp(dst, &in6addr_any, sizeof(in6addr_any)) == 0)
			e->ipv6.dmsk = in6addr_any;
		else if (dst_plen < 129) {
			unsigned char * p = (void *)&e->ipv6.dmsk;
			memset(p, 0xff, dst_plen / 8);
			memset(p + (dst_plen / 8) + 1, 0, (128 - dst_plen) / 8);
			p[dst_plen/8] = 0xff << (8 - (dst_plen & 7));
		} else
			e->ipv6.dmsk = in6addr_full;
	}

	e->nfcache = NFC_UNKNOWN;

	t = (struct ip6t_entry_target *)(e->elems);
	t->u.target_size = targetsz;
	strncpy(t->u.user.name, chain, sizeof(t->u.user.name));
	return e;
}

/*!
 * \brief Creating an ip6tables MARK rule
 *
 * It could be activated with ip6t_append
 * \param src Source address
 * \param dst Destination address
 * \param mark_value Mark value
 * \return MARK rule if OK, otherwise NULL
 */
static struct ip6t_entry * ip6t_alloc_mark(const struct in6_addr * const src,
		const struct in6_addr * const dst, uint8_t mark_value)
{
	struct ip6t_entry *e;
	struct ip6t_entry_target *target;
	struct xt_mark_tginfo2 *mark;
	uint16_t basesz, matchsz, targetsz;

	basesz = (uint16_t)IP6T_ALIGN(sizeof(struct ip6t_entry));
	matchsz = (uint16_t)0;
	targetsz = (uint16_t)IP6T_ALIGN(sizeof(struct ip6t_entry_target)) +
		IP6T_ALIGN(sizeof(struct xt_mark_tginfo2));

	e = malloc(basesz + matchsz + targetsz);
	if (e == NULL)
		return NULL;
	memset(e, 0, basesz + matchsz + targetsz);

	/* Offsets. */
	e->target_offset = basesz + matchsz;
	e->next_offset = basesz + matchsz + targetsz;

	/* Addresses and prefixes. */
	if (src != NULL) {
		memcpy(&e->ipv6.src, src, sizeof(*src));
		if (memcmp(src, &in6addr_any, sizeof(in6addr_any)) == 0)
			e->ipv6.smsk = in6addr_any;
		else
			e->ipv6.smsk = in6addr_full;
	} else
		e->ipv6.smsk = in6addr_any;
	if (dst != NULL) {
		memcpy(&e->ipv6.dst, dst, sizeof(*dst));
		if (memcmp(dst, &in6addr_any, sizeof(in6addr_any)) == 0)
			e->ipv6.dmsk = in6addr_any;
		else
			e->ipv6.dmsk = in6addr_full;
	} else
		e->ipv6.dmsk = in6addr_any;

	e->nfcache = NFC_UNKNOWN;

	/* Target. */
	target = (struct ip6t_entry_target *)(e->elems + matchsz);
	target->u.target_size = targetsz;

	strncpy(target->u.user.name, "MARK", sizeof(target->u.user.name));
	target->u.user.revision = 2;
	mark = (struct xt_mark_tginfo2 *)target->data;
	mark->mark = mark_value;
	mark->mask = 0xFFFFFFFF;
	mark->mask = mark_value | mark->mask;     

	return e;
}

/*!
 * \brief Creating mask for ip6tables rule deleting
 * \param e Entry to delete
 * \param has_match Non-zero if match part is exists
 * \return Delete mask if OK, otherwise NULL
 */
static unsigned char * ip6t_make_delete_mask(struct ip6t_entry *e, int has_match)
{
	struct ip6t_entry_match *match = NULL;
	uint16_t matchsz;
	struct ip6t_entry_target *target = NULL;
	
	if (has_match) {
		match = (struct ip6t_entry_match *)e->elems;
		matchsz = match->u.match_size;
	} else {
		matchsz = 0;
	}
	target = (struct ip6t_entry_target *)(e->elems + matchsz);

    size_t ip6t_size = IP6T_ALIGN(sizeof(struct ip6t_entry));
	size_t match_size = 0;
	if (has_match)
		match_size = IP6T_ALIGN(match->u.match_size);
    size_t target_size = IP6T_ALIGN(target->u.target_size);

    unsigned char * mask;
    unsigned char * x;

    mask = malloc(ip6t_size + match_size + target_size);
	if (mask == NULL)
		return NULL;
    memset(mask, 0, sizeof(*mask));

    /* Fill out base ip6t_entry with 0xff. */
    memset(mask, 0xff, ip6t_size);

    /* Fill out match part with 0xff. */
    x = mask + ip6t_size;
	if (has_match)
	    memset(x, 0xff, match_size);

    /* Fill out target part with 0xff. */
	if (has_match)
	    x += match_size;
    memset(x, 0xff, target_size);

    return mask;
}

/*!
 * \brief Append an ip6tables rule to the specified table-chain
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param e Entry to append
 * \return Zero if OK
 */
static int ip6t_append(const char * const table, const char * const chain, const struct ip6t_entry * e)
{
	int retval = -1;
	int ret;
	struct ip6tc_handle * handle = NULL;
	char * tablename = NULL;
	ip6t_chainlabel chainname;
	struct ip6t_entry_target * target;

	tablename = strdup(table);
	if (tablename == NULL)
		return -1;

	handle = ip6tc_init(tablename);
	if (handle == NULL) {
		ERROR("Unable to init IP6TC handle");
		goto out;
	}

	strncpy((char *)chainname, chain, sizeof(chainname));

	ret = ip6tc_append_entry(chainname, e, handle);
	if (ret != 1) {
		ERROR("Unable to create IP6TC append entry");
		goto out;
	}

	ret = ip6tc_commit(handle);
	if (ret != 1) {
		ERROR("Unable to commit IP6TC append entry: table %s chain %s: %s", table, chain, ip6tc_strerror(errno));
		target = (struct ip6t_entry_target *)(((unsigned char *)e) + e->target_offset);
		if (target != NULL)
			ERROR("\tTarget: %s", target->u.user.name);
		goto out;
	}

	retval = 0;

out:
	if (tablename != NULL)
		free(tablename);
	if (handle != NULL)
		ip6tc_free(handle);

	return retval;
}

/*!
 * \brief Delete an ip6tables rule from the specified table-chain
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param dmask Delete mask
 * \param e Entry to delete
 * \return Zero if OK
 */
static int ip6t_delete(const char * const table, const char * const chain, unsigned char * dmask, const struct ip6t_entry * e)
{
	int retval = -1;
	int ret;
	struct ip6tc_handle * handle = NULL;
	char * tablename = NULL;
	ip6t_chainlabel chainname;

	tablename = strdup(table);
	if (tablename == NULL)
		return -1;

	handle = ip6tc_init(tablename);
	if (handle == NULL) {
		ERROR("Unable to init IP6TC handle");
		goto out;
	}

	strncpy((char *)chainname, chain, sizeof(chainname));

	ret = ip6tc_delete_entry(chainname, e, dmask, handle);
	if (ret != 1) {
		ERROR("Unable to create IP6TC delete entry");
		goto out;
	}

	ret = ip6tc_commit(handle);
	if (ret != 1) {
		ERROR("Unable to commit IP6TC delete entry");
		goto out;
	}

	retval = 0;

out:
	if(tablename != NULL)
		free(tablename);
	if (handle != NULL)
		ip6tc_free(handle);

	return retval;
}

/*!
 * \brief Create a new ip6tables chain in the specified table
 *
 * First it checks the existence of the new chain
 * If it exists and force == 1, it will return -1
 * Otherwise, if it exists, but force == 0, it will return 0
 * \param table ip6tables table
 * \param chain ip6tables chain (to create)
 * \param force Force (see description)
 * \return Zero if OK
 */
static int ip6t_new_chain(const char * const table, const char * const chain, unsigned char force)
{
	int retval = -1;
	int ret;
	struct ip6tc_handle * handle = NULL;
	char * tablename = NULL;
	ip6t_chainlabel chainname;

	tablename = strdup(table);
	if (tablename == NULL)
		return -1;

	handle = ip6tc_init(tablename);
	if (handle == NULL) {
		ERROR("Unable to init IP6TC handle");
		goto out;
	}

	strncpy((char *)chainname, chain, sizeof(chainname));

	ret = ip6tc_is_chain(chainname, handle);
	if (ret == 1 && force != 1) {
		retval = 0;
		goto out;
	}

	ret = ip6tc_create_chain(chainname, handle);
	if (ret != 1) {
		ERROR("Unable to create IP6TC chain entry");
		goto out;
	}

	ret = ip6tc_commit(handle);
	if (ret != 1) {
		ERROR("Unable to commit IP6TC chain entry");
		goto out;
	}

	retval = 0;

out:
	if (tablename != NULL)
		free(tablename);
	if (handle != NULL)
		ip6tc_free(handle);

	return retval;
}

/*!
 * \brief Deleting an ip6tables chain from the specified table
 * \param table ip6tables table
 * \param chain Chain to delete
 * \return Zero if OK
 */
static int ip6t_del_chain(const char * const table, const char * const chain)
{
	int retval = 1;
	int ret;
	struct ip6tc_handle * handle = NULL;
	char * tablename = NULL;
	ip6t_chainlabel chainname;

	tablename = strdup(table);
	if (tablename == NULL)
		return -1;

	handle = ip6tc_init(tablename);
	if (handle == NULL) {
		ERROR("Unable to init IP6TC handle");
		goto out;
	}

	strncpy((char *)chainname, chain, sizeof(chainname));

	ret = ip6tc_delete_chain(chainname, handle);
	if (ret != 1) {
		ERROR("Unable to create IP6TC chain delete entry");
		goto out;
	}

	ret = ip6tc_commit(handle);
	if (ret != 1) {
		ERROR("Unable to commit IP6TC chain delete entry");
		goto out;
	}

	retval = 0;

out:
	if (tablename != NULL)
		free(tablename);
	if (handle != NULL)
		ip6tc_free(handle);

	return retval;
}

/*!
 * \brief Flushing all of the entries from an ip6tables chain
 *
 * First it checks the existence of the new chain
 * If it exists and force == 1, it wiull return -1
 * Otherwise, if it exists, but force == 0, it will return 0
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param force Force (see description)
 * \return Zero if OK
 */
static int ip6t_flush_chain_entries(const char * const table, const char * const chain, unsigned char force)
{
	int retval = -1;
	int ret;
	struct ip6tc_handle * handle = NULL;
	char * tablename = NULL;
	ip6t_chainlabel chainname;

	tablename = strdup(table);
	if (tablename == NULL)
		return -1;

	handle = ip6tc_init(tablename);
	if (handle == NULL) {
		ERROR("Unable to init IP6TC handle");
		goto out;
	}

	strncpy((char *)chainname, chain, sizeof(chainname));

	ret = ip6tc_is_chain(chainname, handle);
	if (ret != 1 && force != 1) {
		retval = 0;
		goto out;
	}

	ret = ip6tc_flush_entries(chainname, handle);
	if (ret != 1) {
		ERROR("Unable to create IP6TC chain flush entry");
		goto out;
	}

	ret = ip6tc_commit(handle);
	if (ret != 1) {
		ERROR("Unable to commit IP6TC chain flush entry");
		goto out;
	}

	retval = 0;

out:
	if (tablename != NULL)
		free(tablename);
	if (handle != NULL)
		ip6tc_free(handle);

	return retval;
}

/*!
 * \brief Create a new ip6tables chain in the specified table
 *
 * First it checks the existence of the new chain
 * If it exists and force == 1, it wiull return -1
 * Otherwise, if it exists, but force == 0, it will return 0
 * \param table ip6tables table
 * \param chain ip6tables chain (to create)
 * \param force Force (see description)
 * \return Zero if OK
 */
int ip6t_create_chain(const char * const table, const char * const chain, unsigned char force)
{
	return ip6t_new_chain(table, chain, force);
}

/*!
 * \brief Create a new ip6tables chain in the specified table
 *
 * If force is true, it flushes the entries first
 * \param table ip6tables table
 * \param chain ip6tables chain (to create)
 * \param force Force (see description)
 * \return Zero if OK
 */
int ip6t_remove_chain(const char * const table, const char * const chain, unsigned char force)
{
	int ret;

	if (force) {
		ret = ip6t_flush_chain_entries(table, chain, 1);
		if (ret)
			return ret;
	}

	return ip6t_del_chain(table, chain);
}

/*!
 * \brief Flushing all of the entries from an ip6tables chain
 *
 * First it checks the existence of the new chain
 * If it exists and force == 1, it will return -1
 * Otherwise, if it exists, but force == 0, it will return 0
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param force Force (see description)
 * \return Zero if OK
 */
int ip6t_flush_chain(const char * const table, const char * const chain, unsigned char force)
{
	return ip6t_flush_chain_entries(table, chain, force);
}

/*!
 * \brief Flushing all of the entries from all of the chains in the given table
 * \param table ip6tables table
 * \return Zero if OK
 */
int ip6t_flush_all(const char * const table)
{
	struct ip6tc_handle * handle = NULL;
	char * tablename = NULL;
    int ret = 0;
    const char * chain;
    char * chains;
    unsigned int i, chaincount = 0;

	tablename = strdup(table);
	if (tablename == NULL)
		return -1;

	handle = ip6tc_init(tablename);
	if (handle == NULL) {
		ERROR("Unable to init IP6TC handle");
		ret = -1;
		goto out;
	}

    chain = ip6tc_first_chain(handle);
    while (chain) {
        chaincount++;
        chain = ip6tc_next_chain(handle);
    }

	if (chaincount == 0)
		goto out;

    chains = malloc(sizeof(ip6t_chainlabel) * chaincount);
    i = 0;
    chain = ip6tc_first_chain(handle);
    while (chain) {
        strcpy(chains + i*sizeof(ip6t_chainlabel), chain);
        i++;
        chain = ip6tc_next_chain(handle);
    }

    for (i = 0; i < chaincount; i++) {
        ret |= ip6t_flush_chain_entries(table, chains + i*sizeof(ip6t_chainlabel), 1);
    }

    free(chains);

out:
	if (tablename != NULL)
		free(tablename);
	if (handle != NULL)
		ip6tc_free(handle);

    return ret;
}

/*!
 * \brief Creating an ip6tables JUMP rule
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param src Source address
 * \param src_plen Source prefix length
 * \param proto Protocol identifier
 * \param dst Destination address
 * \param dst_plen Destination prefix length
 * \param dst_chain Name of target chain
 * \return Zero if OK
 */
int ip6t_jump_rule(const char * const table, const char * const chain,
		const struct in6_addr * const src, uint8_t src_plen, unsigned int proto, const struct in6_addr * const dst, 
		uint8_t dst_plen, const char * const dst_chain)
{
	int ret;
	struct ip6t_entry * entry;

	entry = ip6t_alloc_jump(src, src_plen, proto, dst, dst_plen, dst_chain);
	if (entry == NULL)
		return -1;

	ret = ip6t_append(table, chain, entry);

	free(entry);

	return ret;
}

/*!
 * \brief Deleting an ip6tables JUMP rule
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param src Source address
 * \param src_plen Source prefix length
 * \param proto Protocol identifier
 * \param dst Destination address
 * \param dst_plen Destination prefix length
 * \param dst_chain Name of target chain
 * \return Zero if OK
 */
int ip6t_jump_rule_del(const char * const table, const char * const chain,
		const struct in6_addr * const src, uint8_t src_plen, unsigned int proto, const struct in6_addr * const dst, 
		uint8_t dst_plen, const char * const dst_chain)
{
	int ret;
	struct ip6t_entry * entry;
	unsigned char * mask;

	entry = ip6t_alloc_jump(src, src_plen, proto, dst, dst_plen, dst_chain);
	if (entry == NULL)
		return -1;

	mask = ip6t_make_delete_mask(entry, 0);
	if (mask == NULL)
		return -1;

	ret = ip6t_delete(table, chain, mask, entry);

	free(entry);
	free(mask);

	return ret;
}

/*!
 * \brief Creating an ip6tables MARK rule
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param src Source address
 * \param dst Destination address
 * \param mark Mark value
 * \return Zero if OK
 */
int ip6t_mark_rule(const char * const table, const char * const chain,
		const struct in6_addr * const src, const struct in6_addr * const dst, 
		uint8_t mark)
{
	int ret;
	struct ip6t_entry * entry;

	entry = ip6t_alloc_mark(src, dst, mark);
	if (entry == NULL)
		return -1;

	ret = ip6t_append(table, chain, entry);

	free(entry);

	return ret;
}

/*!
 * \brief Deleting an ip6tables MARK rule
 * \param table ip6tables table
 * \param chain ip6tables chain
 * \param src Source address
 * \param dst Destination address
 * \param mark Mark value
 * \return Zero if OK
 */
int ip6t_mark_rule_del(const char * const table, const char * const chain,
		const struct in6_addr * const src, const struct in6_addr * const dst, 
		uint8_t mark)
{
	int ret = -1;
	struct ip6t_entry * entry = NULL;
	unsigned char * mask = NULL;

	entry = ip6t_alloc_mark(src, dst, mark);
	if (entry == NULL)
		goto out;

	mask = ip6t_make_delete_mask(entry, 0);
	if (mask == NULL)
		goto out;

	ret = ip6t_delete(table, chain, mask, entry);

out:

	if (entry != NULL)
		free(entry);
	if (mask != NULL)
		free(mask);

	return ret;
}
	

/*! \} */

