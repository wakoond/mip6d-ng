
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-213 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-environment
 * \{
 */

#ifndef MIP6D_NG_ENV_XFRM_H
#define MIP6D_NG_ENV_XFRM_H

#include <linux/xfrm.h>
#include <netinet/ip6.h>

int xfrm_policy_add(uint8_t type, const struct xfrm_selector * const sel,
			   int update, int dir, int action, int priority,
			   struct xfrm_user_tmpl * const tmpls, int num_tmpl, struct xfrm_mark * const mark);

/*!
 * \brief Adding a main XFRM policy
 *
 * If MARK is NULL, it will be skipped
 * It adds the main info of the policy to the list of the created policies
 * The policy will be deleted on exit
 * \param sel Selector
 * \param update If 1 update, if 0 new policy
 * \param dir Direction
 * \param action Action, typically XFRM_POLICY_ALLOW
 * \param priority Priority
 * \param tmpls Templates
 * \param num_tmpl Number of templates
 * \param mark MARK
 * \return Zero if OK
 */
static inline int xfrm_ipsec_policy_add(const struct xfrm_selector *sel,
				 int update, int dir, int action, int priority,
			     struct xfrm_user_tmpl * const tmpls, int num_tmpl, struct xfrm_mark * const mark)
{
	return xfrm_policy_add(XFRM_POLICY_TYPE_MAIN, sel, update, dir,
			       action, priority, tmpls, num_tmpl, mark);
}

/*!
 * \brief Adding an XFRM sub policy
 *
 * If MARK is NULL, it will be skipped
 * It adds the main info of the policy to the list of the created policies
 * The policy will be deleted on exit
 * \param sel Selector
 * \param update If 1 update, if 0 new policy
 * \param dir Direction
 * \param action Action, typically XFRM_POLICY_ALLOW
 * \param priority Priority
 * \param tmpls Templates
 * \param num_tmpl Number of templates
 * \return Zero if OK
 */
static inline int xfrm_mip_policy_add(const struct xfrm_selector *sel,
			       int update, int dir, int action, int priority,
			       struct xfrm_user_tmpl * const tmpls, int num_tmpl)
{
	/*
	 * TODO: Fix this
	 *
	 * Without ESP security of signaling messages
	 * the XFRM rules for data and signaling packets contains the same selector
	 * (on HA: data out, rt2 out)
	 * Currently: signaling rules have higher priority and more specific
	 * selector.
	 * (Only one main and one sub policy could be applied. If both are main
	 * the higher priority will be applied)
	 */
	//return xfrm_policy_add(XFRM_POLICY_TYPE_SUB, sel, update, dir,
	//		       action, priority, tmpls, num_tmpl, NULL);
	return xfrm_policy_add(XFRM_POLICY_TYPE_MAIN, sel, update, dir,
			       action, priority, tmpls, num_tmpl, NULL);
}

int xfrm_policy_del(uint8_t type, const struct xfrm_selector *sel, int dir, const struct xfrm_mark * mark);

/*!
 * \brief Deleting a main XFRM policy
 *
 * If MARK is NULL, it will be skipped
 * The policy will be deleted from the list of the created policies
 * \param sel Selector
 * \param dir Direction
 * \param mark MARK
 * \return Zero if OK
 */
static inline int xfrm_ipsec_policy_del(const struct xfrm_selector *sel, int dir, const struct xfrm_mark * mark)
{
	return xfrm_policy_del(XFRM_POLICY_TYPE_MAIN, sel, dir, mark);
}

/*!
 * \brief Deleting an XFRM sub policy
 *
 * If MARK is NULL, it will be skipped
 * The policy will be deleted from the list of the created policies
 * \param sel Selector
 * \param dir Direction
 * \return Zero if OK
 */
static inline int xfrm_mip_policy_del(const struct xfrm_selector *sel, int dir)
{
	/*
	 * TODO: Fix this
	 *
	 * see comment here :xfrm_mip_policy_add
	 */
	//return xfrm_policy_del(XFRM_POLICY_TYPE_SUB, sel, dir, NULL);
	return xfrm_policy_del(XFRM_POLICY_TYPE_MAIN, sel, dir, NULL);
}

int xfrm_policy_clear();

int xfrm_policy_flush(uint8_t type);

int xfrm_state_add_ro(const struct xfrm_selector *sel,
			  struct in6_addr * src, struct in6_addr * dst,
			  int proto, uint32_t spi, const struct in6_addr *coa,
			  int update, uint8_t flags);

int xfrm_state_add_tunnel(const struct xfrm_selector *sel,
			  struct in6_addr * src, struct in6_addr * dst,
			  int proto, char * auth_name, char * auth_key, char * enc_name, char * enc_key,
			  uint32_t spi, uint32_t reqid, int update, uint8_t flags);

int xfrm_state_add_transport(const struct xfrm_selector *sel,
			  struct in6_addr * src, struct in6_addr * dst,
			  int proto, char * auth_name, char * auth_key, char * enc_name, char * enc_key,
			  uint32_t spi, uint32_t reqid, int update, uint8_t flags);

int xfrm_state_del(struct in6_addr * src, struct in6_addr * dst, int proto, uint32_t spi);

int xfrm_state_clear();

int xfrm_state_flush();

void xfrm_set_selector(const struct in6_addr * const daddr, uint8_t dplen, 
			 const struct in6_addr * const saddr, uint8_t splen, 
			 int proto, int type, int code, int ifindex, struct xfrm_selector * const sel);

void xfrm_set_tmpl_dstopt(const struct in6_addr * const dst,
					const struct in6_addr * const src, uint32_t spi,
					struct xfrm_user_tmpl * const tmpl);

void xfrm_set_tmpl_rh(uint32_t spi, struct xfrm_user_tmpl * const tmpl);

void xfrm_set_tmpl_ipsec(const struct in6_addr * const dst,
					const struct in6_addr * const src,
					uint8_t proto, int tunnel, uint32_t spi, uint32_t reqid,
					struct xfrm_user_tmpl * const tmpl);

void xfrm_set_mark(unsigned int value, unsigned int mask, struct xfrm_mark * const mark);


#endif /* MIP6D_NG_ENV_XFRM_H */

/*! \} */

