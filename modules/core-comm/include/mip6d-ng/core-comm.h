
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-core-comm
 * \{
 */

#ifndef MIP6D_NG_CORE_COMM_H
#define MIP6D_NG_CORE_COMM_H

#include <stddef.h>
#include <sys/socket.h>
#include <mip6d-ng/mh-ba.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/data.h>

/*! 
 * The name of the internal messaging reference point
 */
#define MSG_CCOM			"ccom"

/*!
 * Module load order number of Core Comm module
 */
#define SEQ_CCOM			10

/*!
 * \brief Available core-comm API message commands
 */
enum corecomm_messages {
	/*!
	 * \brief Register Mobility Header option
	 * 
	 *  Argument: struct msg_ccom_reg_mh_opt
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_CCOM_REG_MH_OPT,
	/*!
	 * \brief Send Binding Update message
	 *
	 * Argument: struct bule
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_CCOM_SEND_BU,
	/*!
	 * \brief Send Binding Acknowledge message
	 *
	 * Argument: struct msg_ccom_send_ba
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_CCOM_SEND_BA,
	/*!
	 * \brief Send Neighbor Advertisement message
	 *
	 * Argument: struct msg_ccom_send_na
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_CCOM_SEND_NA,
	/*!
	 * \brief Send Router Solicitation message
	 *
	 * Argument: struct msg_ccom_send_rs
	 *
	 * Reply argument: none (it could to be NULL)
	 */
	MSG_CCOM_SEND_RS,
	MSG_CCOM_NONE
};

/*!
 * \brief Register Mobility Header option
 *
 * \see mh_opt_register
 */
struct msg_ccom_reg_mh_opt {
	/*! MH Option type */
	uint8_t type;
	/*! MH Option alignment */
	uint8_t align_n;
	/*! MH Option alignment */
	uint8_t align;
	/*! MH Option parsing callback */
	int (* fn)(uint8_t, struct ip6_mh_opt *, size_t, void *);
};

/*!
 * \brief Send Binding Acknowledge message
 */
struct msg_ccom_send_ba {
	/*! Binding Cache entry */
	struct bce * bce;
	/*! Source address */
	struct in6_addr src;
	/*! Interface index */
	int ifindex;
	/*! BA status */
	uint8_t status;
	/*! BA flags */
	uint8_t flags;
	/*! Sequence number */
	uint16_t seq;
};

/*!
 * \brief Send Neighbor Advertisement message
 */
struct msg_ccom_send_na {
	/*! Source address */
	struct in6_addr src;
	/*! Destination address */
	struct in6_addr dst;
	/*! Interface index */
	int ifindex;
	/*! Target address */
	struct in6_addr target;
	/*! NA flags */
	uint32_t flags;
};

/*!
 * \brief Send Router Solicitation message
 */
struct msg_ccom_send_rs {
	/*! Source address */
	struct in6_addr src;
	/*! Destination address */
	struct in6_addr dst;
	/*! Interface index */
	int ifindex;
};

/*!
 * \brief Available core-comm API events
 */
enum corecomm_events {
	/*!
	 * \brief Binding Acknowledge has been received
	 *
	 * Argument: struct evt_ccom_ba
	 */
	EVT_CCOM_BA,
	/*!
	 * \brief Binding Update has been received
	 *
	 * Argument: struct bce
	 */
	EVT_CCOM_BU,
	EVT_CCOM_NONE
};

/*! 
 * \brief Received Binding Acknowledge 
 */ 
struct evt_ccom_ba {
	/*! Binding Acknowledge */
	struct ip6_mh_ba ba;
	/*! Corresponding BULE */
	struct bule * bule;
	/*! Binding Acknowledge options */
	unsigned int mh_opts_len;
	/*! Binding Acknowledge options' length */
	unsigned char mh_opts[];
};

#define SIZEOF_EVT_CCOM_BA(mh_opts_len) \
    ( offsetof(struct evt_ccom_ba, mh_opts) + (mh_opts_len) * sizeof ((struct evt_ccom_ba *)0)->mh_opts[0] )

/*!
 * \brief MH packet info bundle
 *
 * It contains various addresses of a packet
 */
struct mh_pkt_info_bundle {
	/*! Source IPv6 address */
	struct in6_addr src;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! RT2 address - Local CoA (received in the RT2 headers) */
	struct in6_addr rt2a;
	/*! HOA address - Remote CoA (received in the Home Address Dest. Opt.) */
	struct in6_addr haoa;
	/*! Interface index */
	int ifindex;
	/*! Default route */
	struct in6_addr drtr;
};

/*!
 * \brief MH packet info bundle
 *
 * It contains various addresses of a packet
 */
struct icmp6_pkt_info_bundle {
	/*! Source IPv6 address */
	struct in6_addr src;
	/*! Destination IPv6 address */
	struct in6_addr dst;
	/*! Hoplimit */
	int hoplimit;
	/*! Interface index */
	int ifindex;
};

/*!
 * \brief Argument of send_bu hook
 */
struct send_hook_bu_param {
	/*! Message iov of BU */
	struct iovec * iov;
	/*! Number of pieces in message */
	int iov_count;
	/*! Total length of message, need to be updated by the hook function */
	int iov_len;
	/*! Binding Update List entry */
	struct bule * bule;
};

/*!
 * \brief Argument of send_ba hook
 */
struct send_hook_ba_param {
	/*! Message iov of BA */
	struct iovec * iov;
	/*! Number of pieces in message */
	int iov_count;
	/*! Total length of message, need to be updated by the hook function */
	int iov_len;
	/*! Binding Cache entry */
	struct bce * bce;
};

/*! 
 * \brief Argument of parse_flags_bu hook
 */
struct parse_flags_bu_param {
	/*! Binding Update flags */
	uint32_t flags;
	/*! Binding Cache entry */
	struct bce * bce;
};

/*! 
 * \brief Argument of parse_flags_ba hook
 */
struct parse_flags_ba_param {
	/*! Binding Acknowledge flags */
	uint32_t flags;
	/*! Binding Update List entry */
	struct bule * bule;
};

#endif /* MIP6D_NG_CORE_COMM_H */

/*! \} */
