
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-core-comm
 * \{
 */

#ifndef MIP6D_NG_CCOM_MH_H_
#define MIP6D_NG_CCOM_MH_H_

/*!
 * \brief IPv6 Mobility Header (MH)
 *
 * Followed by type specific messages
 */
struct ip6_mh {
	/*! Next header (NO_NXTHDR by default) */
	uint8_t		ip6mh_proto;
	/*! Header length in unit of 8 octets excluding the first 8 octets */
	uint8_t		ip6mh_hdrlen;
	/*! Type of Mobility Header */
	uint8_t		ip6mh_type;
	/*! Reserved */
	uint8_t		ip6mh_reserved;
	/*! Mobility Header Checksum */
	uint16_t	ip6mh_cksum;
} __attribute__ ((packed));


/*!
 * PAD1
 */
#define IP6_MHOPT_PAD1		0x00

/*!
 * PADN 
 */
#define IP6_MHOPT_PADN		0x01

#endif /* MIP6D_NG_CCOM_MH_H_ */

/*! \} */
