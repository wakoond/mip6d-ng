
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-core-comm
 * \{
 */

#ifndef MIP6D_NG_CCOM_MHO_ALTCOA_H_
#define MIP6D_NG_CCOM_MHO_ALTCOA_H_

/*!
 * \brief Alternate Care-of Address Mobility Option
 */
struct ip6_mh_opt_altcoa {
	/*! Type */
	uint8_t		ip6moa_type;
	/*! Length */
	uint8_t		ip6moa_len;
	/*! Alternate Care-of Address */
	struct in6_addr	ip6moa_addr;
} __attribute__ ((packed));

/*!
 *  Alternate COA
 */
#define IP6_MHOPT_ALTCOA	0x03

#endif /* MIP6D_NG_CCOM_MHO_ALTCOA_H_ */

/*! \} */
