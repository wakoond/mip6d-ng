
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-core-comm
 * \{
 */

#ifndef MIP6D_NG_CCOM_MH_BA_H_
#define MIP6D_NG_CCOM_MH_BA_H_

#include <mip6d-ng/mh.h>

/*!
 * \brief Binding Acknowledgement MH message
 *
 * Followed by optional Mobility Options
 */
struct ip6_mh_ba {
	/*! Mobility header */
	struct ip6_mh	ip6mhba_hdr;
	/*! Status Code */
	uint8_t			ip6mhba_status;
	/*! Flags */
	uint8_t			ip6mhba_flags;
	/*! Sequence Number */
	uint16_t		ip6mhba_seq;
	/*! Lifetime in unit of 4 sec */
	uint16_t		ip6mhba_lifetime;
} __attribute__ ((packed));

/*!
 *  Binding Acknowledge
 */
#define IP6_MH_TYPE_BA		6

/*!
 * BA status: BU accepted 
 */
#define IP6_MH_BA_S_ACCEPTED			0	

/*! 
 * BA status: Accepted, but prefix discovery required 
 */
#define IP6_MH_BA_S_PRFX_DISCOV			1	

/*! 
 * BA status: Reason unspecified 
 */
#define IP6_MH_BA_S_UNSPECIFIED			128	

/*! 
 * BA status: Administratively prohibited 
 */
#define IP6_MH_BA_S_PROHIBIT			129	

/*! 
 * BA status: Insufficient resources 
 */
#define IP6_MH_BA_S_INSUFFICIENT		130	

/*! 
 * BA status: HA registration not supported 
 */
#define IP6_MH_BA_S_HA_NOT_SUPPORTED	131	

/*! 
 * BA status: Not Home subnet 
 */
#define IP6_MH_BA_S_NOT_HOME_SUBNET		132	

/*! 
 * BA status: Not HA for this mobile node 
 */
#define IP6_MH_BA_S_NOT_HA				133	

/*! 
 * BA status: DAD failed 
 */
#define IP6_MH_BA_S_DAD_FAILED			134	

/*! 
 * BA status: Sequence number out of range 
 */
#define IP6_MH_BA_S_SEQNO_BAD			135	

/*! 
 * BA status: Expired Home nonce index 
 */
#define IP6_MH_BA_S_HOME_NI_EXPIRED		136	

/*! 
 * BA status: Expired Care-of nonce index 
 */
#define IP6_MH_BA_S_COA_NI_EXPIRED		137	

/*! 
 * BA status: Expired Nonce Indices 
 */
#define IP6_MH_BA_S_NI_EXPIRED			138	

/*! 
 * BA status: Registration type change  disallowed 
 */
#define IP6_MH_BA_S_REG_NOT_ALLOWED		139	

/*! 
 * BA status: Invalid Care-of Address 
 */
#define IP6_MH_BA_S_INV_COA				074	


#define MH_BA_F_K			0x80

#endif /* MIP6D_NG_CCOM_MH_BU_H_ */

 /*! \} */
