
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/list.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/core-comm.h>

#include "mh-io.h"

/*!
 * \brief Mobility Header Option
 */
struct mh_opt {
	/*! Linked list management */
	struct list_head list;
	/*! MH Option type */
	uint8_t type;
	/*! MH Option alignment */
	uint8_t align_n;
	/*! MH Option alignment */
	uint8_t align;
	/*! MH Option parsing callback */
	int (* fn)(uint8_t, struct ip6_mh_opt *, size_t, void *);
};

static LIST_HEAD(mh_opts);

static const uint8_t mh_pad1[1] = { 0x00 };
static const uint8_t mh_pad2[2] = { 0x01, 0x00 };
static const uint8_t mh_pad3[3] = { 0x01, 0x01, 0x00 };
static const uint8_t mh_pad4[4] = { 0x01, 0x02, 0x00, 0x00 };
static const uint8_t mh_pad5[5] = { 0x01, 0x03, 0x00, 0x00, 0x00 };
static const uint8_t mh_pad6[6] = { 0x01, 0x04, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t mh_pad7[7] = { 0x01, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00 };

/*! 
 * \brief Create and add the required padding to the iov vector
 *
 * \param iov Message iov
 * \param pad Number of padding bytes 
 * \return Zero if OK
 */
static int mho_pad_create(struct iovec * iov, int pad)
{
	if (pad == 2)
		iov->iov_base = (void *)mh_pad2;
	else if (pad == 4)
		iov->iov_base = (void *)mh_pad4;
	else if (pad == 6)
		iov->iov_base = (void *)mh_pad6;
	else if (pad == 1)
		iov->iov_base = (void *)mh_pad1;
	else if (pad == 3)
		iov->iov_base = (void *)mh_pad3;
	else if (pad == 5)
		iov->iov_base = (void *)mh_pad5;
	else if (pad == 7)
		iov->iov_base = (void *)mh_pad7;
	else
		ERROR("Unable to create MH padding: %d", pad);

	iov->iov_len = pad;

	return 0;
}

/*!
 * \brief Get the number of required padding bytes
 *
 * \param xn MH Option padding requirement
 * \param y MH Option padding requirement
 * \param offset Message length to the current position
 * \return Number of required padding bytes
 */
static inline int mh_optpad(int xn, int y, int offset)
{
	return ((y - offset) & (xn - 1));
}

/*!
 * \brief Add the necessary padding bytes between the MH Options
 *
 * It checks the message, and fills the spaces between the options
 * with padding options
 *
 * \param in Message iovec
 * \param out Generated message iovec with padding (output)
 * \param count Number of pieces in message iovec (in)
 * \return Number of piecess in message iovec (out)
 */
int mh_try_pad(const struct iovec * in, struct iovec * out, int count)
{
	size_t len = 0;
	int m, n = 1, pad = 0;
	struct list_head * pos;
	struct mh_opt * o;
	struct ip6_mh_opt * opt;

	out[0].iov_len = in[0].iov_len;
	out[0].iov_base = in[0].iov_base;
	len += in[0].iov_len;

	for (m = 1; m < count; m++) {
		opt = (struct ip6_mh_opt *)in[m].iov_base;
		pad = 0;
		list_for_each(pos, &mh_opts) {
			o = list_entry(pos, struct mh_opt, list);
			if (opt->ip6mhopt_type != o->type)
				continue;

			pad = mh_optpad(o->align_n, o->align, len);
			break;
		}
		if (pad > 0) {
			mho_pad_create(&out[n++], pad);
			len += pad;
		}
		len += in[m].iov_len;
		out[n].iov_len = in[m].iov_len;
		out[n].iov_base = in[m].iov_base;
		n++;
	}
	if (count == 1 || len%8 != 0) {
		pad = mh_optpad(8, 0, len);
		mho_pad_create(&out[n++], pad);
	}

	return n;
}

/*!
 * \brief Register a new Mobility Header Option
 *
 * Used by MH Option implementations to add their stuff to the main 
 * message padding and message parsing core.
 *
 * \param type MH Option type
 * \param align_n MH Option alignment
 * \param align MH Option alignment
 * \param fn MH Option parsing callback
 * \return Zero if OK
 */
int mh_opt_register(uint8_t type, uint8_t align_n, uint8_t align, int (* fn)(uint8_t, struct ip6_mh_opt *, size_t, void *))
{
	struct list_head * pos, * pos2;
	struct mh_opt * opt;

	list_for_each_safe(pos, pos2, &mh_opts) {
		opt = list_entry(pos, struct mh_opt, list);
		if (opt->type == type) {
			INFO("Unable to register Mobility option: %d. Duplicate type.", type);
			return -1;
		}
	}

	opt = malloc(sizeof(struct mh_opt));
	if (opt == NULL) {
		ERROR("Unable to allocate MH opt");
		return -1;
	}

	opt->type = type;
	opt->align_n = align_n;
	opt->align = align;
	opt->fn = fn;

	list_add_tail(&opt->list, &mh_opts);

	return 0;
}

/*!
 * \brief Parsing Mobility Header Options
 *
 * It check the Mobility Options step-by-step, and if any parsing
 * function has been registered for the given type, it calls the registered
 * parsing function
 *
 * \param mh_type Mobility Header type
 * \param opts Mobility Header Options
 * \param optslen Length of Mobility Header Options
 * \param priv Additional parameter, passed to the callback
 * \return Zero if OK
 */
int mh_opt_recv(uint8_t mh_type, unsigned char * opts, size_t optslen, void * priv)
{
	struct ip6_mh_opt * opt = (struct ip6_mh_opt *)opts;
	struct list_head * pos, * pos2;
	struct mh_opt * o;

	for (; opt != NULL && optslen > 0; optslen -= (sizeof(struct ip6_mh_opt) + opt->ip6mhopt_len),
			opt = (struct ip6_mh_opt *)((unsigned char *)opt + sizeof(struct ip6_mh_opt) + opt->ip6mhopt_len)) {
		DEBUG("Checking MH OPT: %u (len %u + 2)", opt->ip6mhopt_type, opt->ip6mhopt_len);
		list_for_each_safe(pos, pos2, &mh_opts) {
			o = list_entry(pos, struct mh_opt, list);
			if (o->type == opt->ip6mhopt_type) {
				if (o->fn != NULL)
					o->fn(mh_type, opt, optslen, priv);
			}
		}
	}

	return 0;
}

/*! \} */

