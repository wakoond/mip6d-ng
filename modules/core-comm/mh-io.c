
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/threading.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/core-comm.h>

#include <mip6d-ng/mho-altcoa.h>
#include <mip6d-ng/mh-ba.h>
#include <mip6d-ng/mh-bu.h>

#include <mip6d-ng/environment.h>

#include "mh-io.h"
#include "mh-opt.h"
#include "mho-altcoa.h"
#include "mh-handlers.h"
#include "main.h"

/*!
 * \brief Maximum length of incoming MH packet 
 */
#define MAX_MH_PKT_LEN 1540

/*!
 * \brief Maximum length of control field in incoming messages
 */
#define CMSG_BUF_LEN 128

/*!
 * \brief Locking MH socket
 */
static pthread_mutex_t mh_socket_mutex = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief MH socket 
 */
static int mh_socket = -1;

/*!
 * \brief Dump message
 *
 * It works if MH_CORE_DEBUG is enabled
 *
 * \param msg Message
 */
static void mh_msg_dump(struct msghdr * msg)
{
#ifdef MH_CORE_DEBUG
	ccom_msg_dump(msg);
#endif
}

/*!
 * \brief Basic receiving function for Mobility Header messages
 *
 * It receives the message from the MH socket, and
 * gets and parses a few basic information regarding to it. It gets basic
 * packet information (source and destination address) and parses the Destination
 * Option and Type 2 Routing headers.
 *
 * \param msg Received message
 * \param msglen Maximum length of the received message
 * \param src_addr Source address
 * \param pkt_info Packet information
 * \param hao_addr Home Address from the Destination Option Header, if non exists it will be filled with zeros
 * \param rt_addr Address form the Type 2 Routing Header, if non exists it will be filled with zeros
 * \return Length of the message or negative
 */
static ssize_t mh_recv(unsigned char * msg, size_t msglen,
		struct sockaddr_in6 * src_addr, struct in6_pktinfo * pkt_info,
		struct in6_addr * hao_addr, struct in6_addr * rt_addr)
{
	int ret;
	struct msghdr mhdr;
	struct cmsghdr * cmsg;
	struct iovec iov;
	static unsigned char chdr[CMSG_BUF_LEN];
	void * databufp = NULL;
	socklen_t hao_len;
	ssize_t len;
	struct in6_addr *seg = NULL;

	iov.iov_len = msglen;
	iov.iov_base = msg;

	mhdr.msg_name = (void *)src_addr;
	mhdr.msg_namelen = sizeof(struct sockaddr_in6);
	mhdr.msg_iov = &iov;
	mhdr.msg_iovlen = 1;
	mhdr.msg_control = (void *)&chdr;
	mhdr.msg_controllen = CMSG_BUF_LEN;

	ret = recvmsg(mh_socket, &mhdr, 0);
	if (ret < 0) {
		ERROR("mh: Receiving error: %d", ret);
		return ret;
	}
	len = ret;

	mh_msg_dump(&mhdr);

	memset(hao_addr, 0, sizeof(*hao_addr));
	memset(rt_addr, 0, sizeof(*rt_addr));

	for (cmsg = CMSG_FIRSTHDR(&mhdr); cmsg != NULL; cmsg = CMSG_NXTHDR(&mhdr, cmsg)) {
		if (cmsg->cmsg_level != IPPROTO_IPV6)
			continue;

		switch (cmsg->cmsg_type) {
		case IPV6_PKTINFO:
			memcpy(pkt_info, CMSG_DATA(cmsg), sizeof(*pkt_info));
			break;
		case IPV6_DSTOPTS:
			ret = inet6_opt_find(CMSG_DATA(cmsg), cmsg->cmsg_len, 0, IP6OPT_HOME_ADDRESS, &hao_len, &databufp);
			if (ret >= 0 && databufp != NULL &&
			    hao_len == sizeof(struct in6_addr)) {
				*hao_addr = *(struct in6_addr *) databufp;
			}
			break;
		case IPV6_RTHDR:
			if (inet6_rth_gettype(CMSG_DATA(cmsg)) == IPV6_RTHDR_TYPE_2) {
				seg = mip6d_ng_inet6_rth_getaddr(CMSG_DATA(cmsg), 0);
				if (seg != NULL)
					*rt_addr = *seg;
			}
			break;
		}
	}

	return len;
}

/*!
 * \brief Listening Mobility Header messages
 *
 * Endless loop for receiving and checking MH messages.
 * It calls the parsing function, if any has been registered 
 * for the given type
 *
 * \param arg Unused
 * \return Unused
 */
static void * mh_listen(void * arg)
{
	int ret;
	uint8_t msg[MAX_MH_PKT_LEN];
	struct in6_pktinfo pktinfo;
	struct in6_addr haoa, rta;
	struct sockaddr_in6 addr;
	struct ip6_mh *mh;
	struct mh_pkt_info_bundle info;
	size_t len;
	int mh_type_known = 0;
	//struct in6_addr * be_src, * be_dst, * be_hoa;

	while (1) {
#ifdef HAVE_PTHREAD_SETCANCELSTATE
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
#endif

		ret = mh_recv(msg, sizeof(msg), &addr, &pktinfo, &haoa, &rta);
		if (ret < 0) {
			ERROR("mh: Listening error. Exit.");
			exit(EXIT_FAILURE);
		}
		len = ret;

		if (len < 0 || len < sizeof(struct ip6_mh))
			continue;

		memset(&info, 0, sizeof(info));

		memcpy(&info.src, &addr.sin6_addr, sizeof(info.src));
		memcpy(&info.dst, &pktinfo.ipi6_addr, sizeof(info.dst));
		if (!IN6_IS_ADDR_UNSPECIFIED(&haoa)) {
			memcpy(&info.haoa, &haoa, sizeof(info.haoa));
		}
		if (!IN6_IS_ADDR_UNSPECIFIED(&rta)) {
			memcpy(&info.rt2a, &rta, sizeof(info.rt2a));
		}

		mh = (struct ip6_mh *) msg;


#ifdef HAVE_PTHREAD_SETCANCELSTATE
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
#endif

		mh_type_known = mh_handlers_call(mh, len,&info);

		if (mh_type_known != 1) {
			/*
			if (!IN6_IS_ADDR_UNSPECIFIED(&info.rt2a))
				be_src = &info.rt2a;
			else
				be_src = &pktinfo.ipi6_addr;
			if (!IN6_IS_ADDR_UNSPECIFIED(&info.haoa)) {
				be_hoa = &info.src;
				be_dst = &info.haoa;
			} else {
				be_hoa = NULL;
				be_dst = &info.src;
			}
			*/
			/*! TODO: MH send BE */
			//mh_send_be(be_dst, be_hoa, be_src, IP6_MH_BES_UNKNOWN_MH, info.ifindex);
		}
	}

	return NULL;
}

/*!
 * \brief Initialize Mobility Header I/O
 *
 * It opens and configures the MH socket, and creates the listening thread
 *
 * \return Zero if OK
 */
int mh_init()
{
	int ret;
	int val;
	pthread_t th;

	ret = socket(AF_INET6, SOCK_RAW, IPPROTO_MH);
	if (ret < 0) {
		ERROR("mh: Unable to open MH socket");
		return ret;
	}
	mh_socket = ret;

	val = 1;

	ret = setsockopt(mh_socket, IPPROTO_IPV6, IPV6_RECVPKTINFO, &val, sizeof(val));
	if (ret < 0) {
		ERROR("mh: Unable to set sockopt: receive packet info");
		return -1;
	}
	ret = setsockopt(mh_socket, IPPROTO_IPV6, IPV6_RECVDSTOPTS, &val, sizeof(val));
	if (ret < 0) {
		ERROR("mh: Unable to set sockopt: receive destination option");
		return -1;
	}
	ret = setsockopt(mh_socket, IPPROTO_IPV6, IPV6_RECVRTHDR, &val, sizeof(val));
	if (ret < 0) {
		ERROR("mh: Unable to set sockopt: receive RT2 header");
		return -1;
	}

	val = 4;
	ret = setsockopt(mh_socket, IPPROTO_RAW, IPV6_CHECKSUM, &val, sizeof(val));
	if (ret < 0) {
		ERROR("mh: Unable to set sockopt: IPv6 checksum offset");
		return  -1;
	}

	th = threading_create(mh_listen, NULL);

	return (int)(th == (pthread_t)0);
}

/*!
 * \brief Get the length of the full message
 *
 * It summarizes the length of iov pieces
 *
 * \param iov Message iov
 * \param count Number of pieces in the message
 * \return Full message length
 */
static size_t mh_length(struct iovec * iov, int count)
{
	size_t len = 0;
	int i;

	for (i = 0; i < count; i++) {
		len += iov[i].iov_len;
	}

	return len;
}

/*!
 * \brief Sending Mobility Header message
 *
 * Before message sending this function initializes the XFRM rules for
 * Destination Option header and Type 2 Routing Header. Additionally it creates 
 * a route to the destination via the given default route on the given interface.
 * It ensures, that the message will be leave the box on the correct interface.
 * After message sending it will be destroyed everything (hao and rt2 XFRM rules, and route).
 *
 * \param info Message info
 * \param iov_orig Message iov, without padding
 * \param iov_count_orig Number of pieces in message iov
 * \return Number of sent bytes or negative
 */
int mh_send(const struct mh_pkt_info_bundle * info, struct iovec * iov_orig, int iov_count_orig)
{
	struct iovec iov[iov_count_orig * 2 + 1];
	int iov_count;
	struct sockaddr_in6 daddr;
	struct in6_pktinfo pinfo;
	struct msghdr msg;
	void * cmsgbuf;
	struct cmsghdr *cmsg;
	int cmsglen;
	int ret = 0;
	int on;
	struct ip6_mh *mh;
	iov_count = mh_try_pad(iov_orig, iov, iov_count_orig);
	int sent = -1;

	//DEBUG("MH sending. Info: src " IP6ADDR_FMT " dst " IP6ADDR_FMT " rt2a " IP6ADDR_FMT " haoa " IP6ADDR_FMT " ifindex %d",
	//		IP6ADDR_TO_STR(&info->src), IP6ADDR_TO_STR(&info->dst),
	//		IP6ADDR_TO_STR(&info->rt2a), IP6ADDR_TO_STR(&info->haoa), info->ifindex);

	mh = (struct ip6_mh *)iov[0].iov_base;
	mh->ip6mh_hdrlen = (mh_length(iov, iov_count) >> 3) - 1;

	memset(&daddr, 0, sizeof(struct sockaddr_in6));
	daddr.sin6_family = AF_INET6;
	memcpy(&daddr.sin6_addr, &info->dst, sizeof(daddr.sin6_addr));
	daddr.sin6_port = htons(IPPROTO_MH);

	memset(&pinfo, 0, sizeof(pinfo));
	memcpy(&pinfo.ipi6_addr, &info->src, sizeof(pinfo.ipi6_addr));
	pinfo.ipi6_ifindex = info->ifindex;

	cmsglen = CMSG_SPACE(sizeof(pinfo));
	cmsgbuf = malloc(cmsglen);
	if (cmsgbuf == NULL) {
		ERROR("mh: Unable to allocate send CMSG");
		return -1;
	}
	memset(cmsgbuf, 0, cmsglen);
	memset(&msg, 0, sizeof(msg));

	msg.msg_control = cmsgbuf;
	msg.msg_controllen = cmsglen;
	msg.msg_iov = iov;
	msg.msg_iovlen = iov_count;
	msg.msg_name = (void *)&daddr;
	msg.msg_namelen = sizeof(daddr);

	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_len = CMSG_LEN(sizeof(pinfo));
	cmsg->cmsg_level = IPPROTO_IPV6;
	cmsg->cmsg_type = IPV6_PKTINFO;
	memcpy(CMSG_DATA(cmsg), &pinfo, sizeof(pinfo));

	mh_msg_dump(&msg);

	pthread_mutex_lock(&mh_socket_mutex);

	if (!IN6_IS_ADDR_UNSPECIFIED(&info->rt2a)) {
		struct msg_env_rt2 rt2;
		
		memset(&rt2, 0, sizeof(rt2));
		memcpy(&rt2.src, &info->src, sizeof(rt2.src));
		rt2.splen = 128;
		memcpy(&rt2.dst, &info->dst, sizeof(rt2.dst));
		rt2.dplen = 128;
		memcpy(&rt2.coa, &info->rt2a, sizeof(rt2.coa));
		rt2.proto = IPPROTO_MH;
		rt2.type = IP6_MH_TYPE_BA;
		rt2.dir = XFRM_POLICY_OUT;
		rt2.prio = 5;
		ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.env, MSG_ENV_RT2,
				&rt2, sizeof(rt2),NULL, 0);
		if (ret) {
			ERROR("Unable to create RT2 opt XFRM policy: " IP6ADDR_FMT " coa " IP6ADDR_FMT,
					IP6ADDR_TO_STR(&info->dst), IP6ADDR_TO_STR(&info->rt2a));
			goto out;
		}
	} else if (!IN6_IS_ADDR_UNSPECIFIED(&info->haoa)) {
		struct msg_env_hao hao;

		memset(&hao, 0, sizeof(hao));
		memcpy(&hao.src, &info->src, sizeof(hao.src));
		hao.splen = 128;
		memcpy(&hao.dst, &info->dst, sizeof(hao.dst));
		hao.dplen = 128;
		memcpy(&hao.coa, &info->haoa, sizeof(hao.coa));
		hao.proto = IPPROTO_MH;
		hao.type = IP6_MH_TYPE_BU;
		hao.dir = XFRM_POLICY_OUT;
		hao.prio = 5;
		ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.env, MSG_ENV_HAO,
				&hao, sizeof(hao), NULL, 0);
		if (ret) {
			ERROR("Unable to create HAO DST opt XFRM policy: " IP6ADDR_FMT " coa " IP6ADDR_FMT,
					IP6ADDR_TO_STR(&info->haoa), IP6ADDR_TO_STR(&info->src));
			goto out;
		}
	}
	
	if (!IN6_IS_ADDR_UNSPECIFIED(&info->drtr)) {
		struct msg_env_route rt;

		memset(&rt, 0, sizeof(rt));
		memcpy(&rt.dst, &info->dst, sizeof(rt.dst));
		rt.dplen = 128;
		rt.proto = IPPROTO_MH;
		memcpy(&rt.gw, &info->drtr, sizeof(rt.gw));
		rt.entries |= MSG_ENV_ROUTE_GW;
		rt.oiface = info->ifindex;

		ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.env, MSG_ENV_ROUTE,
				&rt, sizeof(rt), NULL, 0);
		if (ret) {
			ERROR("Unable to create route to HA " IP6ADDR_FMT " via " IP6ADDR_FMT " dev %d",
					IP6ADDR_TO_STR(&info->dst), IP6ADDR_TO_STR(&info->drtr), info->ifindex);
			goto out;
		}	
	}

	on = 1;
	setsockopt(mh_socket, IPPROTO_IPV6, IPV6_PKTINFO, &on, sizeof(int));
	sent = sendmsg(mh_socket, &msg, 0);
	if (sent < 0)
		ERROR("mh: Send error: %d", sent);

	if (!IN6_IS_ADDR_UNSPECIFIED(&info->rt2a)) {
		struct msg_env_rt2 rt2;
		
		memset(&rt2, 0, sizeof(rt2));
		memcpy(&rt2.src, &info->src, sizeof(rt2.src));
		rt2.splen = 128;
		memcpy(&rt2.dst, &info->dst, sizeof(rt2.dst));
		rt2.dplen = 128;
		memcpy(&rt2.coa, &info->rt2a, sizeof(rt2.coa));
		rt2.proto = IPPROTO_MH;
		rt2.type = IP6_MH_TYPE_BA;
		rt2.dir = XFRM_POLICY_OUT;
		rt2.prio = 5;
		ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.env, MSG_ENV_RT2_DEL,
				&rt2, sizeof(rt2),NULL, 0);
		if (ret) {
			ERROR("Unable to delete RT2 opt XFRM policy: " IP6ADDR_FMT " coa " IP6ADDR_FMT,
					IP6ADDR_TO_STR(&info->dst), IP6ADDR_TO_STR(&info->rt2a));
			goto out;
		}
	} else if (!IN6_IS_ADDR_UNSPECIFIED(&info->haoa)) {
		struct msg_env_hao hao;

		memset(&hao, 0, sizeof(hao));
		memcpy(&hao.src, &info->src, sizeof(hao.src));
		hao.splen = 128;
		memcpy(&hao.dst, &info->dst, sizeof(hao.dst));
		hao.dplen = 128;
		memcpy(&hao.coa, &info->haoa, sizeof(hao.coa));
		hao.proto = IPPROTO_MH;
		hao.type = IP6_MH_TYPE_BU;
		hao.dir = XFRM_POLICY_OUT;
		hao.prio = 5;
		ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.env, MSG_ENV_HAO_DEL,
				&hao, sizeof(hao), NULL, 0);
		if (ret) {
			ERROR("Unable to delete HAO DST opt XFRM policy: " IP6ADDR_FMT " coa " IP6ADDR_FMT,
					IP6ADDR_TO_STR(&info->haoa), IP6ADDR_TO_STR(&info->src));
			goto out;;
		}

	} 
	
	if (!IN6_IS_ADDR_UNSPECIFIED(&info->drtr)) {
		struct msg_env_route rt;

		memset(&rt, 0, sizeof(rt));
		memcpy(&rt.dst, &info->dst, sizeof(rt.dst));
		rt.dplen = 128;
		rt.proto = IPPROTO_MH;
		memcpy(&rt.gw, &info->drtr, sizeof(rt.gw));
		rt.entries |= MSG_ENV_ROUTE_GW;
		rt.oiface = info->ifindex;

		ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.env, MSG_ENV_ROUTE_DEL,
				&rt, sizeof(rt), NULL, 0);
		if (ret) {
			ERROR("Unable to delete route to HA " IP6ADDR_FMT " via " IP6ADDR_FMT " dev %d",
					IP6ADDR_TO_STR(&info->dst), IP6ADDR_TO_STR(&info->drtr), info->ifindex);
			goto out;
		}	
	}

out:
	pthread_mutex_unlock(&mh_socket_mutex);

	free(msg.msg_control);

	return sent;
}

/*! \} */





