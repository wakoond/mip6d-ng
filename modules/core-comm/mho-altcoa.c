
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mho-altcoa.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bce.h>

#include "mho-altcoa.h"

/*!
 * \brief Create Alternate Care-of Address Mobility Header Option
 *
 * It adds AltCoa option to the MH with the given address
 * \param iov Message iov
 * \param addr Alternate Care-of Address
 * \return Zero if OK
 */
int mho_altcoa_create(struct iovec * iov, struct in6_addr * addr)
{
	struct ip6_mh_opt_altcoa * opt;
	size_t optlen = sizeof(struct ip6_mh_opt_altcoa);

	iov->iov_base = malloc(optlen);
	iov->iov_len = optlen;

	if (iov->iov_base == NULL)
		return -1;

	opt = (struct ip6_mh_opt_altcoa *)iov->iov_base;

	opt->ip6moa_type = IP6_MHOPT_ALTCOA;
	opt->ip6moa_len = 16;
	memcpy(&opt->ip6moa_addr, addr, sizeof(opt->ip6moa_addr));

	return 0;
}

/*!
 * \brief Parsing Alternate Care-of Address Mobility Header Option
 *
 * It could be parse Binding Update option. The private argument
 * should to be a Binding Cache entry. This function updates the
 * coa filed of it to the given one.
 * \param mh_type MH type, it should to be BU
 * \param opt MH option, it should to be struct ip6_mh_opt_altcoa
 * \param optslen MH option len
 * \param priv Extra argument, it should to be struct bce
 * \return Zero if OK
 */
static int mho_altcoa_recv(uint8_t mh_type, struct ip6_mh_opt * opt, size_t optslen, void * priv)
{
	struct ip6_mh_opt_altcoa * altcoa = (struct ip6_mh_opt_altcoa *)opt;
	struct bce * bce = (struct bce *)priv;

	if (altcoa == NULL || optslen < sizeof(struct ip6_mh_opt_altcoa) || altcoa->ip6moa_len != 16)
		return -1;

	INFO("Found AltCoa option: " IP6ADDR_FMT, IP6ADDR_TO_STR(&altcoa->ip6moa_addr));
	memcpy(&bce->coa, &altcoa->ip6moa_addr, sizeof(bce->coa));

	return 0;
}

/*!
 * \brief register Alternate-Care-of Address MH Option
 * \return Zero if OK
 */
int mho_altcoa_init()
{
	return mh_opt_register(IP6_MHOPT_ALTCOA, 8, 6, mho_altcoa_recv); /* 8n+6 */
}

/*! \} */
