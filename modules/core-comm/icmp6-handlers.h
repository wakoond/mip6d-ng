
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#ifndef MIP6D_NG_ICMP6_HANDLERS_H_
#define MIP6D_NG_ICMP6_HANDLERS_H_

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>

int icmp6_handler_register(uint8_t type, void (* recv)(const struct icmp6_hdr *, ssize_t, const struct icmp6_pkt_info_bundle *));
void icmp6_handlers_call(const struct icmp6_hdr * ih, ssize_t len, const struct icmp6_pkt_info_bundle * info);

#endif /* MIP6D_NG_ICMP6_HANDLERS_H_ */

/*! \} */
