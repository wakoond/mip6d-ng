
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#ifndef MIP6D_NG_MH_IO_H_
#define MIP6D_NG_MH_IO_H_

#include <mip6d-ng/mh.h>
#include <mip6d-ng/core-comm.h>

int mh_init();
int mh_send(const struct mh_pkt_info_bundle * info, struct iovec * iov, int iov_count);

#endif /* MIP6D_NG_MH_IO_H_ */

/*! \} */
