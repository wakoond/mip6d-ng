
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <sys/socket.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/hooks.h>
#include <mip6d-ng/imessaging.h>

#include <mip6d-ng/mh.h>
#include <mip6d-ng/mh-bu.h>
#include <mip6d-ng/mh-opt.h>
#include <mip6d-ng/mho-altcoa.h>
#include <mip6d-ng/data.h>
#include <mip6d-ng/bce.h>

#include "main.h"
#include "mh.h"
#include "mh-handlers.h"
#include "mh-bu.h"
#include "mho-altcoa.h"

/*!
 * \brief Maximum number of BU message pieces
 */
#define MH_BU_IOVEC_NUM		10

/*!
 * \brief Create Binding Update message
 *
 * It creates the message and adds Alternate Care-of Address MH option to it
 *
 * \param info Packet info
 * \param iov Message iov (output)
 * \param iov_count Number of message pieces (output)
 * \param flags BU flags
 * \param lifetime Binding lifetime
 * \return Number of allocated message pieces, or negative
 */
int mh_bu_create(struct mh_pkt_info_bundle * info, struct iovec ** iov, int * iov_count, uint16_t flags, uint16_t lifetime)
{
	int ret;
	int iov_idx = 0;
	struct ip6_mh_bu * bu;
	static uint16_t bu_seq = 1;

	*iov = malloc(sizeof(struct iovec) * MH_BU_IOVEC_NUM);
	if (*iov == NULL) {
		ERROR("Unable to allocate BU iov.");
		return -1;
	}
	memset(*iov, 0, sizeof(struct iovec) * MH_BU_IOVEC_NUM);
	bu = (struct ip6_mh_bu *)mh_create(&((*iov)[iov_idx++]), IP6_MH_TYPE_BU, sizeof(struct ip6_mh_bu));
	if (bu == NULL) {
		ERROR("Unable to allocate and create BU message");
		goto out;
	}

	bu->ip6mhbu_seqno = htons(bu_seq++);
	bu->ip6mhbu_flags = htons(flags);
	bu->ip6mhbu_lifetime = htons(lifetime);

	ret = mho_altcoa_create(&((*iov)[iov_idx++]), &info->haoa);
	if (ret) {
		ERROR("Unable to add altcoa option.");
		goto out;
	}

	*iov_count = iov_idx;

	return MH_BU_IOVEC_NUM;

out:

	mh_free(*iov, MH_BU_IOVEC_NUM);
	*iov = NULL;

	return -1;
}

/*!
 * \brief Parsing Binding Update
 *
 * It allocates and initializes a new Binding Cache entry, and parse BU stuff into it.
 * It runs the parse_bu_flags hook, to parse more flags, and calls
 * the MH option parsing stuff. Finally it sends EVT_CCOM_BU message, which argument will
 * be the new BCE.
 *
 * \param mh Mobility Header message
 * \param len Size of MH message
 * \param info Packet info
 */
static void mh_bu_recv(const struct ip6_mh * mh, ssize_t len, const struct mh_pkt_info_bundle * info)
{
	int ret;
	int olen;
	struct ip6_mh_bu * bu;
	struct bce * bce = NULL;
	struct parse_flags_bu_param buf;
	uint16_t flags;

	if (mh->ip6mh_hdrlen < 1) {
		ERROR("Invalid BU message");
		return;
	}
	bu = (struct ip6_mh_bu *)mh;
	olen = mh->ip6mh_hdrlen * 8 + 8 - sizeof(struct ip6_mh_bu);

	/* TODO: Make BU check here.... */

	bce = malloc(sizeof(struct bce));
	if (bce == NULL) {
		ERROR("Out of memory: Unable to allocate BCE");
		return;
	}

	ret = IMSG_MSG_ARGS(ccom_opts.msg_ids.me, ccom_opts.msg_ids.data, MSG_DATA_INIT_NEW_BCE, bce, sizeof(struct bce), NULL, 0);
	if (ret) {
		ERROR("Unable to initialize the new BCE");
		return;
	}
	
	memcpy(&bce->hoa, &info->src,  sizeof(struct in6_addr));
	memcpy(&bce->coa, &info->haoa, sizeof(struct in6_addr));
	memcpy(&bce->src_addr, &info->haoa, sizeof(struct in6_addr));
	bce->seq = ntohs(bu->ip6mhbu_seqno);
	bce->lifetime = ntohs(bu->ip6mhbu_lifetime);
	
	bce->flags = 0;
	flags = ntohs(bu->ip6mhbu_flags);
	if (flags & MH_BU_F_A)
		bce->flags |= BCE_F_A;
	if (flags & MH_BU_F_H)
		bce->flags |= BCE_F_H;

	buf.flags = flags;
	buf.bce = bce;
	hooks_run(ccom_opts.parse_bu_flags_hook_id, &buf);
	mh_opt_recv(IP6_MH_TYPE_BU, (unsigned char *)(bu + 1), olen, bce);

	INFO("BU: hoa " IP6ADDR_FMT " coa " IP6ADDR_FMT " SEQ %u FLAGS %04X LIFET %u optslen %d from " IP6ADDR_FMT,
			IP6ADDR_TO_STR(&bce->hoa), IP6ADDR_TO_STR(&bce->coa),
			bce->seq, flags, bce->lifetime, olen,
			IP6ADDR_TO_STR(&bce->src_addr));

	ret = imsg_send_event(ccom_opts.msg_ids.me, EVT_CCOM_BU, bce, sizeof(struct bce), 0, refcnt_copy);
}

/*!
 * \brief Register Binding Update to MH handlers
 */
int mh_bu_register()
{
	return mh_handler_register(IP6_MH_TYPE_BU, mh_bu_recv);
}

/*! \} */

