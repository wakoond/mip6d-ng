
/*
 * This file is part of mip6d-ng distribution, 
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 * 
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-core-comm
 * \{
 */

#include <config.h>
#include <mip6d-ng/missing.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <sys/socket.h>

#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/threading.h>

#include <mip6d-ng/core-comm.h>

#include "icmp6-io.h"
#include "icmp6-handlers.h"
#include "icmp6-ra.h"

/*!
 * \brief Dump ICMP6 RA message
 *
 * Enabled only if ICMP6_CORE_DEBUG is enabled
 *
 * \param ira Router Advertisement message
 */
static void icmp6_ra_dump(struct icmp6_ra * ira)
{
#if ( (defined ICMP6_CORE_DEBUG) || (defined ICMP6_RA_DEBUG) )
	struct list_head * pos;
	struct icmp6_ra_prefix * irap;

	DEBUG("ICMPv6 RA DUMP");
	DEBUG("RA: \n"
		  "\t\t timestamp: " TS_FMT "\n"
		  "\t\t from: " IP6ADDR_FMT " on iface %d\n"
		  "\t\t source link addr: " HWA_FMT " (%d)\n"
		  "\t\t hoplimit %d flags %08X\n"
		  "\t\t lifetime: " TS_FMT " reachable: " TS_FMT " retrans: " TS_FMT " adv ival: " TS_FMT "\n"
		  "\t\t mtu: %d prefixes: %d",
		  TS_TO_STR(&ira->timestamp),
		  IP6ADDR_TO_STR(&ira->src_addr), ira->ifindex,
		  HWA_TO_STR(ira->hwa), ira->hwalen,
		  ira->hoplimit, ira->ra_flags,
		  TS_TO_STR(&ira->rtr_lifetime), TS_TO_STR(&ira->reachable), TS_TO_STR(&ira->retransmit), TS_TO_STR(&ira->adv_ival),
		  ira->mtu, ira->prefix_cnt);
	list_for_each(pos, &ira->prefixes) {
		irap = list_entry(pos, struct icmp6_ra_prefix, list);
		DEBUG("RA PREFIX: \n"
			  "\t\t prefix: " IP6ADDR_FMT "/%u\n"
			  "\t\t lifetime: valid: " TS_FMT " preferred: " TS_FMT,
			  IP6ADDR_TO_STR(&irap->prefix), irap->prefix_len,
			  TS_TO_STR(&irap->valid_lifetime), TS_TO_STR(&irap->preferred_lifetime));
	}
#endif
}

/*!
 * \brief Parse Router Advertisement message
 *
 * Currently this has no effect, only dumps the parsed message
 * \param hdr ICMPv6 message
 * \param len Length of message
 * \param info Packet info
 */
static void icmp6_ra_recv(const struct icmp6_hdr * hdr, ssize_t len, const struct icmp6_pkt_info_bundle * info)
{
	struct icmp6_ra * ira = NULL;
	struct icmp6_ra_prefix * irap = NULL;

	struct nd_router_advert * ra = (struct nd_router_advert * )hdr;
	int optlen = len - sizeof(struct nd_router_advert);
	uint8_t *opt;
	uint16_t olen;
	int err;
	struct nd_opt_prefix_info *pinfo;
	struct nd_opt_mtu *mtu;
	struct nd_opt_adv_interval *r;
	struct list_head * pos, *pos2;

	/* validity checks */
	if (info->hoplimit < 255) {
		INFO("icmp6: RA hoplimit < 255");
		return;
	}
	if (!IN6_IS_ADDR_LINKLOCAL(&info->src)) {
		INFO("icmp6: RA source address is link-local");
		return;
	}
	if (hdr->icmp6_code != 0) {
		INFO("icmp6: RA code is not zero");
		return;
	}
	if (len < sizeof(struct nd_router_advert)) {
		INFO("icmp6: Invalid RA length: %d", len);
		return;
	}

	opt = (uint8_t *)(ra + 1);

	ira = malloc(sizeof(struct icmp6_ra));
	if (ira == NULL) {
		ERROR("icmp6: Unable to allocate RA");
		return;
	}
	memset(ira, 0, sizeof(struct icmp6_ra));

	INIT_LIST_HEAD(&ira->prefixes);
	clock_gettime(CLOCK_REALTIME, &ira->timestamp);
	memcpy(&ira->src_addr, &info->src, sizeof(ira->src_addr));
	ira->ifindex = info->ifindex;
	ira->hoplimit = ra->nd_ra_curhoplimit;
	ira->ra_flags = ra->nd_ra_flags_reserved;
	tssetsec(ira->rtr_lifetime, ntohs(ra->nd_ra_router_lifetime));
	tssetmsec(ira->reachable, ntohl(ra->nd_ra_reachable));
	tssetmsec(ira->retransmit, ntohl(ra->nd_ra_retransmit));

	err = 0;
	while (optlen > 1 && err == 0) {
		olen = opt[1] << 3;
		if (olen > (unsigned int)optlen || olen == 0) {
			INFO("icmp6: Invalid RA opt length.");
			err = 1;
			break;
		}

		switch (opt[0]) {
		case ND_OPT_SOURCE_LINKADDR:
			/*! TODO: This method is valid only for ARPHRD_ETHER, ARPHRD_IEEE802, ARPHRD_IEEE802_TR, ARPHRD_IEEE80211, ARPHRD_FDDI */
			if (olen - 2 != 6) {
				INFO("icmp6: Invalid RA LINKADDR length.");
				err = 1;
				break;
			}
			memcpy(ira->hwa, &opt[2], 6);
			ira->hwalen = 6;
			break;
		case ND_OPT_PREFIX_INFORMATION:
			if (olen < sizeof(struct nd_opt_prefix_info)) {
				INFO("icmp6: Invalid RA PREFIX INFO length.");
				err = 1;
				break;
			}
			pinfo = (struct nd_opt_prefix_info *)opt;

			if (pinfo->nd_opt_pi_prefix_len > 128) {
				INFO("icmp6: Invalid RA prefix length: %d", pinfo->nd_opt_pi_prefix_len);
				err = 1;
				break;
			}

			irap = malloc(sizeof(struct icmp6_ra_prefix));
			if (irap == NULL) {
				ERROR("icmp6: Unable to allocate RA prefix");
				err = 1;
				break;
			}

			tssetmsec(irap->valid_lifetime, ntohl(pinfo->nd_opt_pi_valid_time));
			tssetmsec(irap->preferred_lifetime, ntohl(pinfo->nd_opt_pi_preferred_time));
			memcpy(&irap->prefix, &pinfo->nd_opt_pi_prefix, sizeof(irap->prefix));
			irap->prefix_len = pinfo->nd_opt_pi_prefix_len;

			list_add_tail(&irap->list, &ira->prefixes);
			ira->prefix_cnt++;
			break;
		case ND_OPT_MTU:
			if (olen < sizeof(struct nd_opt_mtu)) {
				INFO("icmp6: Invalid RA MTU length.");
				err = 1;
				break;
			}
			mtu = (struct nd_opt_mtu *)opt;
			ira->mtu = ntohl(mtu->nd_opt_mtu_mtu);
			break;
		case ND_OPT_RTR_ADV_INTERVAL:
			if (olen < sizeof(struct nd_opt_adv_interval)) {
				INFO("icmp6: Invalid RA ADV INTERVAL length.");
				err = 1;
				break;
			}
			r = (struct nd_opt_adv_interval *) opt;
			tssetmsec(ira->adv_ival, ntohl(r->nd_opt_adv_interval_ival));
			break;
		}

		optlen -= olen;
		opt += olen;
	}

	if (err)
		goto out;

	icmp6_ra_dump(ira);

out:
	if (ira) {
		list_for_each_safe(pos, pos2, &ira->prefixes) {
			irap = list_entry(pos, struct icmp6_ra_prefix, list);
			list_del(pos);
			free(irap);
		}
		free(ira);
		ira = NULL;
	}
}

/*!
 * \brief Register Router Advertisement to ICMPv6 handlers
 */
int icmp6_ra_register()
{
	return icmp6_handler_register(ND_ROUTER_ADVERT, icmp6_ra_recv);
}

/*! \} */

