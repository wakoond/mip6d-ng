
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup eapi-libapi
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <libmip6.h>
#include <mip6d-ng/logger-api.h>
#include <mip6d-ng/api-cmd-fmt.h>

#include "recv.h"
#include "handle.h"

/*!
 * \brief Name of the logger-api module of mip6d-ng
 * \ingroup internal-libapi
 */
#define LOGA_NAME	"loga"

/*!
 * \brief Send message to mip6d-ng log
 *
 * \param handle Handle
 * \param msg Log message
 * \return Zero if OK
 */
int mip6dng_log(struct libmip6h * handle, char * msg)
{
	int retval = 0;
	int ret;
	struct timespec to;

	if (handle == NULL)
		return -LIBMIP6_INVALID;

	ret = libmip6_send(handle, LOGA_NAME, API_LOGA_LOG, libmip6_get_seq(handle), (unsigned char *)msg, strlen(msg) + 1);
	if (ret)
		return ret;

	to.tv_sec = 8;
	to.tv_nsec = 0;
	ret = recv_wait(handle, &to);
	if (ret) {
		recv_done(handle);
		return ret;
	}

	retval = handle->recv_status;

	recv_done(handle);

	return retval;
}

/*! \} */

