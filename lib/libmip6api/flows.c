
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup eapi-libapi
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <libmip6.h>
#include <mip6d-ng/flowb.h>
#include <mip6d-ng/api-cmd-fmt.h>

#include "recv.h"
#include "handle.h"

/*!
 * \brief Name of the flowb module of mip6d-ng
 * \ingroup internal-libapi
 */
#define FLOWB_NAME	"flob"

/*!
 * \brief Register a flow in the flowb module of mip6d-ng
 *
 * \param handle Handle
 * \param src Source address
 * \param splen Source prefix length
 * \param sport Source port
 * \param dst Destination address
 * \param dplen Destination prefix length
 * \param dport Destination port
 * \param proto Protocol identifier, i.e. IPPROTO_ICMPV6
 * \param bid Requested interface, identified by its BID value
 * \param is_static If non-zero, the FBE won't be deleted upon disconnection
 * \param key Authentication key, to ignore authentication use NULL
 * \return Flow Identifier or negative error code
 */
int flowb_register(struct libmip6h * handle, struct in6_addr * src, uint8_t splen, uint16_t sport, 
		struct in6_addr * dst, uint8_t dplen, uint16_t dport, unsigned int proto,
		uint16_t bid, unsigned char is_static, struct libmip6_flowb_key * key)
{
	int retval = 0;
	int ret;
	struct timespec to;
	struct flowb_api_register reg;
	struct flowb_api_register_reply * reply;

	if (handle == NULL || src == NULL || dst == NULL)
		return -LIBMIP6_INVALID;

	memset(&reg, 0, sizeof(reg));
	memcpy(&reg.src, src, sizeof(struct in6_addr));
	reg.splen = splen;
	reg.sport = htons(sport);
	memcpy(&reg.dst, dst, sizeof(struct in6_addr));
	reg.dplen = dplen;
	reg.dport = htons(dport);
	reg.proto = htonl(proto);
	reg.bid = htons(bid);
	reg.is_static = is_static;
	if (key != NULL)
		memcpy(&reg.key, key, sizeof(key));

	ret = libmip6_send(handle, FLOWB_NAME, API_FLOWB_REGISTER, libmip6_get_seq(handle), (unsigned char *)&reg, sizeof(reg));
	if (ret)
		return ret;

	to.tv_sec = 5;
	to.tv_nsec = 0;
	ret = recv_wait(handle, &to);
	if (ret) {
		recv_done(handle);
		return ret;
	}

	if (handle->recv_status != 0) {
		retval = handle->recv_status;
		goto out;
	}

	if (handle->recv_mlen > 0) {
		reply = (struct flowb_api_register_reply *)handle->recv_msg;
		retval = (int)ntohl(reply->id);
	} else
		retval = -LIBMIP6_NOANSW;

out:

	recv_done(handle);

	return retval;
}

/*!
 * \brief Update a flow in the flowb module of mip6d-ng
 *
 * \param handle Handle
 * \param bid Requested new interface, identified by its BID value
 * \param id Flow Identifier
 * \return Zero if OK
 */
int flowb_update(struct libmip6h * handle, uint16_t bid, unsigned int id)
{
	int retval = 0;
	int ret;
	struct timespec to;
	struct flowb_api_update up;

	if (handle == NULL)
		return -LIBMIP6_INVALID;

	memset(&up, 0, sizeof(up));
	up.bid = htons(bid);
	up.id = htonl(id);

	ret = libmip6_send(handle, FLOWB_NAME, API_FLOWB_UPDATE, libmip6_get_seq(handle), (unsigned char *)&up, sizeof(up));
	if (ret)
		return ret;

	to.tv_sec = 8;
	to.tv_nsec = 0;
	ret = recv_wait(handle, &to);
	if (ret) {
		recv_done(handle);
		return ret;
	}

	retval = handle->recv_status;

	recv_done(handle);

	return retval;
}

/*!
 * \brief Delete a flow in the flowb module of mip6d-ng
 *
 * \param handle Handle
 * \param id Flow Identifier
 * \return Zero if OK
 */
int flowb_delete(struct libmip6h * handle, unsigned int id)
{
	int retval = 0;
	int ret;
	struct timespec to;
	struct flowb_api_del del;

	if (handle == NULL)
		return -LIBMIP6_INVALID;

	memset(&del, 0, sizeof(del));
	del.id = htonl(id);

	ret = libmip6_send(handle, FLOWB_NAME, API_FLOWB_DELETE, libmip6_get_seq(handle), (unsigned char *)&del, sizeof(del));
	if (ret)
		return ret;

	to.tv_sec = 8;
	to.tv_nsec = 0;
	ret = recv_wait(handle, &to);
	if (ret) {
		recv_done(handle);
		return ret;
	}

	retval = handle->recv_status;

	recv_done(handle);

	return retval;
}

/*! \} */
