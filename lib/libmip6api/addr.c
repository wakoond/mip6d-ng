
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-libapi
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <libmip6.h>
#include <mip6d-ng/control.h>
#include <mip6d-ng/api-cmd-fmt.h>

#include "recv.h"
#include "handle.h"

/*!
 * \brief Name of the control module of mip6d-ng
 */
#define CTRL_NAME	"ctrl"

/*!
 * \brief Get an address from the control module
 *
 * It could to be used to get Home Address or Home Agent Address
 *
 * \param handle Handle
 * \param addr Address (output)
 * \param cmd Command: API_CTRL_GET_HAA or API_CTRL_GET_HOA
 * \return Zero if OK
 */
static int __ctrl_get_addr(struct libmip6h * handle, struct in6_addr * addr, uint16_t cmd)
{
	int retval = 0;
	int ret;
	struct timespec to;
	struct in6_addr * addr_p;

	if (handle == NULL || addr == NULL)
		return -LIBMIP6_INVALID;

	ret = libmip6_send(handle, CTRL_NAME, cmd, libmip6_get_seq(handle), NULL, 0);
	if (ret) {
		recv_done(handle);
		return ret;
	}

	to.tv_sec = 5;
	to.tv_nsec = 0;
	ret = recv_wait(handle, &to);
	if (ret)
		return ret;

	if (handle->recv_status != 0) {
		retval = handle->recv_status;
		goto out;
	}

	if (handle->recv_mlen > 0) {
		addr_p = (struct in6_addr *)handle->recv_msg;
		memcpy(addr, addr_p, sizeof(struct in6_addr));
	} else
		retval = -LIBMIP6_NOANSW;

out:

	recv_done(handle);

	return retval;
}

/*!
 * \}
 * \addtogroup eapi-libapi
 * \{
 */

/*!
 * \brief Get the Home Agent Address from the control module of mip6d-ng
 *
 * \param handle Handle
 * \param addr Address (output)
 * \return Zero if OK
 */
int ctrl_get_haa(struct libmip6h * handle, struct in6_addr * addr)
{
	return __ctrl_get_addr(handle, addr, API_CTRL_GET_HAA);
}

/*!
 * \brief Get the Home Address from the control module of mip6d-ng
 *
 * \param handle Handle
 * \param addr Address (output)
 * \return Zero if OK
 */
int ctrl_get_hoa(struct libmip6h * handle, struct in6_addr * addr)
{
	return __ctrl_get_addr(handle, addr, API_CTRL_GET_HOA);
}

/*! \} */
