
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-libapi
 * \{
 */

#ifndef _LIBMIP6_HANDLE_H
#define _LIBMIP6_HANDLE_H

#include <pthread.h>
#include <inttypes.h>
#include "tcpc.h"

/*!
 * \brief Handle
 */
struct libmip6h {
	/*! TCP connection */
	struct tcpc_handle tcph;
	/*! Lock */
	pthread_mutex_t lock;
	/*! Last applied sequence number */
	uint8_t seq;
	/*! Receive mutex */
	pthread_mutex_t recv_mutex;
	/*! Receive condition variable */
	pthread_cond_t recv_cond;
	/*! Received message */
	unsigned char * recv_msg;
	/*! Length of the received message */
	int recv_mlen;
	/*! receive status */
	int recv_status;
};

#endif

/*! \} */
