
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup eapi-libapi
 * \{
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <libmip6.h>
#include <mip6d-ng/mcoa.h>
#include <mip6d-ng/api-cmd-fmt.h>

#include "recv.h"
#include "handle.h"

/*!
 * \brief Name of the mcoa module of mip6d-ng
 * \ingroup internal-libapi
 */
#define MCOA_NAME	"mcoa"

/*!
 * \brief Get the Binding Update List from the mcoa module of mip6d-ng
 *
 * It contains the BID value as well, while the data_get_bul not.
 * 
 * \param handle Handle
 * \param bul Allocated array of Binding Update List entries, it need to be freed
 * \param entries Number of BUL entries
 * \return Zero if OK
 */
int mcoa_get_bul(struct libmip6h * handle, struct libmip6_bule_mcoa ** bul, unsigned int * entries)
{
	int retval = 0;
	int ret;
	struct timespec to;
	int i;
	struct mcoa_api_get_bul * gb;

	if (handle == NULL || bul == NULL || entries == NULL)
		return -LIBMIP6_INVALID;

	*bul = NULL;
	*entries = 0;

	ret = libmip6_send(handle, MCOA_NAME, API_MCOA_GET_BUL, libmip6_get_seq(handle), NULL, 0);
	if (ret) {
		recv_done(handle);
		return ret;
	}

	to.tv_sec = 5;
	to.tv_nsec = 0;
	ret = recv_wait(handle, &to);
	if (ret)
		return ret;

	if (handle->recv_status != 0) {
		retval = handle->recv_status;
		goto out;
	}

	if (handle->recv_mlen > 0) {
		gb = (struct mcoa_api_get_bul *)handle->recv_msg;
		*entries = handle->recv_mlen / sizeof(struct mcoa_api_get_bul);
		if (*entries > 0) {
			*bul = malloc(*entries * sizeof(struct libmip6_bule_mcoa));
			if (*bul == NULL) {
				retval = LIBMIP6_NOMEM;
				goto out;
			}

			for (i = 0; i < *entries; i++) {
				memcpy(&(*bul)[i].dest, &gb[i].dest, sizeof(struct in6_addr));
				memcpy(&(*bul)[i].hoa, &gb[i].hoa, sizeof(struct in6_addr));
				memcpy(&(*bul)[i].coa, &gb[i].coa, sizeof(struct in6_addr));
				(*bul)[i].lifetime = ntohs(gb[i].lifetime);
				(*bul)[i].lifetime_rem = ntohs(gb[i].lifetime_rem);
				(*bul)[i].last_bu = ntohl(gb[i].last_bu);
				(*bul)[i].flags = ntohl(gb[i].flags);
				(*bul)[i].ifindex = ntohl(gb[i].ifindex);
				(*bul)[i].bid = ntohs(gb[i].bid);
				(*bul)[i].prio = gb[i].prio;

			}
		}
	} else
		retval = -LIBMIP6_NOANSW;

out:

	recv_done(handle);

	return retval;
}

/*!
 * \brief Free the array of Binding Update List entries
 *
 * Free the bul array, allocated by mcoa_get_bul
 *
 * \param handle Handle
 * \param bul Binding Update List entries
 * \return Zero if OK
 */
int mcoa_free_bul(struct libmip6h * handle, struct libmip6_bule_mcoa * bul)
{
	if (handle == NULL)
		return -LIBMIP6_INVALID;

	if (bul != NULL)
		free(bul);

	return 0;
}

/*!
 * \brief Get the Binding Cache from the data module of mip6d-ng
 *
 * 
 * It contains the BID value as well, while the data_get_bc not.
 * 
 * \param handle Handle
 * \param bc Allocated array of Binding Cache entries, it need to be freed
 * \param entries Number of BC entries
 * \return Zero if OK
 */
int mcoa_get_bc(struct libmip6h * handle, struct libmip6_bce_mcoa ** bc, unsigned int * entries)
{
	int retval = 0;
	int ret;
	struct timespec to;
	int i;
	struct mcoa_api_get_bc * gb;

	if (handle == NULL || bc == NULL || entries == NULL)
		return -LIBMIP6_INVALID;

	*bc = NULL;
	*entries = 0;

	ret = libmip6_send(handle, MCOA_NAME, API_MCOA_GET_BC, libmip6_get_seq(handle), NULL, 0);
	if (ret)
		return ret;

	to.tv_sec = 5;
	to.tv_nsec = 0;
	ret = recv_wait(handle, &to);
	if (ret)
		return ret;

	if (handle->recv_status != 0) {
		retval = handle->recv_status;
		goto out;
	}

	if (handle->recv_mlen > 0) {
		gb = (struct mcoa_api_get_bc *)handle->recv_msg;
		*entries = handle->recv_mlen / sizeof(struct mcoa_api_get_bc);
		if (*entries > 0) {
			*bc = malloc(*entries * sizeof(struct libmip6_bce_mcoa));
			if (*bc == NULL) {
				retval = LIBMIP6_NOMEM;
				goto out;
			}

			for (i = 0; i < *entries; i++) {
				memcpy(&(*bc)[i].hoa, &gb[i].hoa, sizeof(struct in6_addr));
				memcpy(&(*bc)[i].coa, &gb[i].coa, sizeof(struct in6_addr));
				(*bc)[i].lifetime = ntohs(gb[i].lifetime);
				(*bc)[i].flags = ntohl(gb[i].flags);
				(*bc)[i].bid = ntohs(gb[i].bid);
				(*bc)[i].prio = gb[i].prio;

			}
		}
	} else
		retval = -LIBMIP6_NOANSW;

out:

	recv_done(handle);

	return retval;
}

/*!
 * \brief Free the array of Binding Cache entries
 *
 * Free the bc array, allocated by mcoa_get_bc
 *
 * \param handle Handle
 * \param bc Binding Cache entries
 * \return Zero if OK
 */
int mcoa_free_bc(struct libmip6h * handle, struct libmip6_bce_mcoa * bc)
{
	if (handle == NULL)
		return -LIBMIP6_INVALID;

	if (bc != NULL)
		free(bc);

	return 0;
}

/*! \} */
