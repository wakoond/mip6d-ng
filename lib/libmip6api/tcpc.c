
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-libapi
 * \{
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/select.h>
#include <fcntl.h>

#include <pthread.h>

#include "tcpc.h"

#ifndef DISABLE_TCPC_DEBUG
#define DEBUG(fmt, ...)     fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...)     fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#else
#define DEBUG(fmt, ...)     
#define ERROR(fmt, ...)     
#endif      

#ifndef TCP_RECV_BUFFER_SIZE
#define TCP_RECV_BUFFER_SIZE	(5*1024)
#endif

/*!
 * \brief Argument of tcp_recv_thread
 */
struct tcpc_recv_arg {
	/*! TCP handle */
	struct tcpc_handle * handle;
	/*! Protocol family: AF_INET or AF_INET6 */
	int family;
	/*! Raceive (parsing) callback function */
	void (* fn)(struct tcpc_handle *, unsigned char *, unsigned int, void *);
	/*! Extra argument of fn */
	void * priv;
};

/*!
 * \brief TCP receive thread
 *
 * It listens and receives messages in an endless loop.
 * Upon successful receiving, it calls the fn callback function
 *
 * \param arg struct tcp_recv_arg
 * \return Unused
 */
static void * tcpc_recv_thread(void * arg)
{
	int ret;
	struct tcpc_recv_arg * ra = (struct tcpc_recv_arg *)arg;
	char addr4[INET_ADDRSTRLEN + 1];
	char addr6[INET6_ADDRSTRLEN + 1];
	int remote_fd;
	fd_set fs;
	struct timeval tv;
	unsigned char buf[TCP_RECV_BUFFER_SIZE];
	int blen = sizeof(buf);

	if (ra == NULL || ra->handle == NULL)
		return NULL;
	if (ra->family != AF_INET && ra->family != AF_INET6)
		return NULL;

	if (ra->family == AF_INET) {
		if (inet_ntop(AF_INET, &ra->handle->i4.remote, addr4, sizeof(addr4)) != NULL)
			DEBUG("IPv4/TCP receiving thread started (from %s:%u)", addr4, ra->handle->i4.remotep);
		remote_fd = ra->handle->i4.fd;
	} else {
		if (inet_ntop(AF_INET6, &ra->handle->i6.remote, addr6, sizeof(addr6)) != NULL) 
			DEBUG("IPv6/TCP receiving thread started (from [%s]:%u)", addr6, ra->handle->i6.remotep);
		remote_fd = ra->handle->i6.fd;
	}

	for (;;) {
		pthread_mutex_lock(&ra->handle->lock);

		if (remote_fd == -1)
			goto out;

		FD_ZERO(&fs);
		FD_SET(remote_fd, &fs);

		tv.tv_sec  = 1;
		tv.tv_usec = 0;
		
		ret = select(remote_fd + 1, &fs, 0, 0, &tv);
		if (ret != 1) 
			goto retry;	
		if (! FD_ISSET(remote_fd, &fs)) 
			goto retry;
	
		blen = sizeof(buf);
		ret = recv(remote_fd, buf, blen, 0);

		if (ret == 0) {
			if (ra->family == AF_INET) 
				DEBUG("IPv4/TCP connection closed from %s:%u", addr4, ra->handle->i4.remotep);
			else
				DEBUG("IPv6/TCP connection closed from [%s]:%u", addr6, ra->handle->i6.remotep);
			goto close_and_out;
		}

		if (ret < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				goto retry;

			if (ra->family == AF_INET) 
				ERROR("IPv4/TCP receive error from %s:%u. %s (%d)", addr4, ra->handle->i4.remotep, strerror(errno), ret);
			else
				ERROR("IPv6/TCP receive error from [%s]:%u. %s (%d)", addr6, ra->handle->i6.remotep, strerror(errno), ret);
			goto close_and_out;
		}
			
		blen = ret;
		if (ra->family == AF_INET) 
			DEBUG("IPv4/TCP received %d bytes from %s:%u", blen, addr4, ra->handle->i4.remotep);
		else
			DEBUG("IPv6/TCP received %d bytes from [%s]:%u", blen, addr6, ra->handle->i6.remotep);
		
		pthread_mutex_unlock(&ra->handle->lock);

		if (ra->fn != NULL) 
			ra->fn(ra->handle, buf, blen, ra->priv);

		sleep(1);
		continue;

retry:
		pthread_mutex_unlock(&ra->handle->lock);

		sleep(1);
	}
	
	return NULL;

close_and_out:

	close(remote_fd);
	if (ra->family == AF_INET) 
		ra->handle->i4.fd = -1;
	else
		ra->handle->i6.fd = -1;

out:
		
	pthread_mutex_unlock(&ra->handle->lock);
			
	return NULL;
}

/*!
 * \brief Initialize TCP connection
 *
 * It connects to the TCP server, and starts the listening and receiving loop.
 * The handle should to be configured: The is_i4 and is_i6 flags should to be configured
 * and the corresponding remote address, port values should to be given.
 *
 * \param handle TCP handle 
 * \param fn Receiving callback function
 * \param priv Extra parameter for fn
 * \return Zero if OK
 */
int tcpc_init(struct tcpc_handle * handle, void (* fn)(struct tcpc_handle * handle, unsigned char *, unsigned int, void *), void * priv)
{
	int ret;
	int v;
	struct sockaddr_in  si4_local;
	struct sockaddr_in6 si6_local;
	struct sockaddr_in  si4_remote;
	struct sockaddr_in6 si6_remote;
	struct tcpc_recv_arg * ra = NULL;
	pthread_attr_t thattr;
	pthread_t th;

	if (handle == NULL) 
		return -1;

	pthread_mutex_init(&handle->lock, NULL);
	handle->i4.fd = -1;
	handle->i6.fd = -1;
		
	pthread_attr_init(&thattr);
	pthread_attr_setdetachstate(&thattr, PTHREAD_CREATE_DETACHED);

	if (handle->is_i4) {
		ret = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (ret < 0) {
			ERROR("Unable to create IPv/TCP socket");
			goto out;
		}
		handle->i4.fd = ret;

		v = 1;
		ret = setsockopt(handle->i4.fd, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v));
		if (ret) {
			ERROR("Unable to do IPv4/TCP SO_REUSEADDR");
			goto out;
		}
		
		memset(&si4_local, 0, sizeof(si4_local));
		si4_local.sin_family = AF_INET;
		si4_local.sin_port = 0;
		si4_local.sin_addr.s_addr = INADDR_ANY;

		ret = bind(handle->i4.fd, (struct sockaddr *)&si4_local, sizeof(si4_local));
		if (ret < 0) {
			ERROR("Unable to do IPv4/TCP bind");
			goto out;
		}

		memset(&si4_remote, 0, sizeof(si4_remote));
		si4_remote.sin_family = AF_INET;
		si4_remote.sin_port = htons(handle->i4.remotep);
		memcpy(&si4_remote.sin_addr, &handle->i4.remote, sizeof(struct in_addr));

		ret = connect(handle->i4.fd, (struct sockaddr *)&si4_remote, sizeof(si4_remote));
		if (ret < 0) {
			ERROR("Unable to do IPv4/TCP connect");
			goto out;
		}

		ret = fcntl(handle->i4.fd, F_SETFL, O_NONBLOCK);
		if (ret) {
			ERROR("Unable to set non-blocking IO on IPv4/TCP socket");
			goto out;
		}

		ra = malloc(sizeof(struct tcpc_recv_arg));
		if (ra == NULL) {
			ERROR("Out of memory: Unable to start IPv4/TCP recv thread");
			goto out;
		}

		ra->handle = handle;
		ra->family = AF_INET;
		ra->fn = fn;
		ra->priv = priv;
		ret = pthread_create(&th, &thattr, tcpc_recv_thread, ra);
		if (ret) {
			ERROR("Unable to create IPv4/TCP recv thread");
			goto out;
		}
		ra = NULL;

	}

	if (handle->is_i6) {
		ret = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
		if (ret < 0) {
			ERROR("Unable to create IPv6/TCP socket");
			goto out;
		}
		handle->i6.fd = ret;

		v = 1;
		ret = setsockopt(handle->i6.fd, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v));
		if (ret) {
			ERROR("Unable to do IPv6/TCP SO_REUSEADDR");
			goto out;
		}
		
		memset(&si6_local, 0, sizeof(si6_local));
		si6_local.sin6_family = AF_INET6;
		si6_local.sin6_port = 0;
		memcpy(&si6_local.sin6_addr, &in6addr_any, sizeof(struct in6_addr));

		ret = bind(handle->i6.fd, (struct sockaddr *)&si6_local, sizeof(si6_local));
		if (ret < 0) {
			ERROR("Unable to do IPv6/TCP bind");
			goto out;
		}

		memset(&si6_remote, 0, sizeof(si6_remote));
		si6_remote.sin6_family = AF_INET6;
		si6_remote.sin6_port = htons(handle->i6.remotep);
		memcpy(&si6_remote.sin6_addr, &handle->i6.remote, sizeof(struct in6_addr));

		ret = connect(handle->i6.fd, (struct sockaddr *)&si6_remote, sizeof(si6_remote));
		if (ret < 0) {
			ERROR("Unable to do IPv6/TCP connect");
			goto out;
		}

		ret = fcntl(handle->i6.fd, F_SETFL, O_NONBLOCK);
		if (ret) {
			ERROR("Unable to set non-blocking IO on IPv6/TCP socket");
			goto out;
		}

		ra = malloc(sizeof(struct tcpc_recv_arg));
		if (ra == NULL) {
			ERROR("Out of memory: Unable to start IPv6/TCP recv thread");
			goto out;
		}

		ra->handle = handle;
		ra->family = AF_INET6;
		ra->fn = fn;
		ra->priv = priv;
		ret = pthread_create(&th, &thattr, tcpc_recv_thread, ra);
		if (ret) {
			ERROR("Unable to create IPv6/TCP recv thread");
			goto out;
		}
		ra = NULL;	
	}
		
	pthread_attr_destroy(&thattr);

	return 0;

out:
	if (handle->i4.fd >= 0) {
		close(handle->i4.fd);
		handle->i4.fd = -1;
	}
	if (handle->i6.fd >= 0) {
		close(handle->i6.fd);
		handle->i6.fd = -1;
	}

	return -1;
}

/*!
 * \brief Close the TCP connection
 *
 * \param handle TCP handle
 * \return Zero if OK
 */
int tcpc_close(struct tcpc_handle * handle)
{
	if (handle == NULL)
		return -1;

	pthread_mutex_lock(&handle->lock);

	if (handle->i4.fd >= 0) {
		close(handle->i4.fd);
		handle->i4.fd = -1;
	}
	if (handle->i6.fd >= 0) {
		close(handle->i6.fd);
		handle->i6.fd = -1;
	}
	
	pthread_mutex_unlock(&handle->lock);

	return 0;
}

/*!
 * \brief TCP sending
 *
 * \param handle TCP handle
 * \param buf Message to send
 * \param blen
 * \return Number of sent bytes or negative
 */
int tcpc_send(struct tcpc_handle * handle, unsigned char * buf, unsigned int blen)
{
	int ret = -1;
	char addr4[INET_ADDRSTRLEN + 1];
	char addr6[INET6_ADDRSTRLEN + 1];
	unsigned char * buf_ptr;
	unsigned int blen_rem = blen;

	if (handle == NULL)
		return -1;
	if (buf == NULL || blen == 0)
		return -1;

	pthread_mutex_lock(&handle->lock);
	
	if (handle->is_i4) {
		inet_ntop(AF_INET, &handle->i4.remote, addr4, sizeof(addr4));

		if (handle->i4.fd < 0)
			goto out_i6; 

		buf_ptr = buf;
		blen_rem = blen;

		for (;;) {
			ret = send(handle->i4.fd, buf_ptr, blen_rem, 0);
			
			if (ret == 0) {
				DEBUG("IPv4/TCP connection closed to %s:%u", addr4, handle->i4.remotep);
				goto close_i4;
			}

			if (ret < 0) {
				if (errno == EAGAIN || errno == EWOULDBLOCK)
					goto out_i6;

				ERROR("IPv4/TCP send error to %s:%u. %s (%d)", addr4, handle->i4.remotep, strerror(errno), ret);
				goto close_i4;
			}

			DEBUG("IPv4/TCP sent %d bytes to %s:%u", ret, addr4, handle->i4.remotep);

			buf_ptr += ret;
			blen_rem -= ret;
			if (blen_rem == 0)
				break;
		}
	}

	/*
	 * Does not work perfectly the paralel sending if IPv4-IPv6
	 * In case of any error it could "crash": One sent, the other not, and the callesr does not know 
	 * what's the problem
	 *
	 * Use i4 OR i6 insted of i4 AND i6.
	 *
	 * TODO: Need to fix this
	 */

out_i6:

	if (handle->is_i6) {
		inet_ntop(AF_INET6, &handle->i6.remote, addr6, sizeof(addr6));
		if (handle->i6.fd < 0)
			goto out;
		
		buf_ptr = buf;
		blen_rem = blen;

		for (;;) {
			ret = send(handle->i6.fd, buf_ptr, blen_rem, 0);
			
			if (ret == 0) {
				DEBUG("IPv6/TCP connection closed to [%s]:%u", addr6, handle->i6.remotep);
				goto close_i6;
			}

			if (ret < 0) {
				if (errno == EAGAIN || errno == EWOULDBLOCK)
					goto out;

				ERROR("IPv6/TCP send error to [%s]:%u. %s (%d)", addr6, handle->i6.remotep, strerror(errno), ret);
				goto close_i6;
			}

			DEBUG("IPv6/TCP sent %d bytes to [%s]:%u", ret, addr6, handle->i6.remotep);

			buf_ptr += ret;
			blen_rem -= ret;
			if (blen_rem == 0)
				break;
		}
	}


out:
	pthread_mutex_unlock(&handle->lock);

	return blen - blen_rem;

close_i4:
		
	close(handle->i4.fd);
	handle->i4.fd = -1;
	
	pthread_mutex_unlock(&handle->lock);

	return -1;

close_i6:
		
	close(handle->i6.fd);
	handle->i6.fd = -1;
	
	pthread_mutex_unlock(&handle->lock);

	return -1;
}

/*! \} */
