
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <time.h>
#include <pthread.h>

#include <mip6d-ng/logger.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>
#include <mip6d-ng/tasks.h>

/*!
 * \brief Task mutex
 *
 * Protects tasks_list
 */
static pthread_mutex_t tasks_mutex = PTHREAD_MUTEX_INITIALIZER;

/*!
 * \brief Tasks signaling
 *
 * Used for signing addition / removal into tasks_list
 */
static pthread_cond_t tasks_cond = PTHREAD_COND_INITIALIZER;

/*!
 * \brief List of tasks
 */
static LIST_HEAD(tasks_list);

/*!
 * \brief Store one registered (and pending) task
 */
struct tasks_list {
	/*! Linked list */						struct list_head list;
	/*! ID for deletion */					unsigned long id;
	/*! Scheduled time */					struct timespec time;
	/*! Task function */					task_t * task_fp;
	/*! Argument of the task function */	void * arg;
};

static void * tasks_runner(void *);

/*!
 * \brief Initialize tasks support
 *
 * Init pseudo-random support, and creates tasks runner thread
 */
int tasks_init()
{
	pthread_t ret;

	srand(time(NULL));

	ret = threading_create(tasks_runner, NULL);
	if (ret == (pthread_t)0)
		return -1;

	return 0;
}

/*!
 * \brief Adding a new thread
 *
 * Adding a new thread, and schedule it
 * (Tasks are stored in order of scheduled time of them)
 * \param task tasks_list entry
 * \return zero if OK
 */
static int __tasks_add(struct tasks_list * task)
{
	struct list_head * pos;
	struct tasks_list * tp;
	unsigned char added = 0, id_mismatch = 0;;

	if (task == NULL)
		return -1;

	pthread_mutex_lock(&tasks_mutex);

	list_for_each(pos, &tasks_list) {
		tp = (struct tasks_list *)list_entry(pos, struct tasks_list, list);
		if (task->id == tp->id) {
			id_mismatch = 1;
			break;
		}
		if (tsbefore(tp->time, task->time) && added == 0) {
			list_add_tail(&task->list, &tp->list);
			added = 1;
		}
	}
	
	if (added == 0 && id_mismatch == 0) {
		list_add_tail(&task->list, &tasks_list);
		added = 1;
	}

	if (added == 1 && id_mismatch == 1) {
		list_del(&task->list);
		added = 0;
	}

	if (added == 1) {
#ifdef CORE_IC_DEBUG
		DEBUG("Added 1 task. [%lu] Time: %u:%lu", task->id, task->time.tv_sec, task->time.tv_nsec);
#endif

		pthread_cond_broadcast(&tasks_cond);
	}

	pthread_mutex_unlock(&tasks_mutex);

	return (added == 1) ? 0 : ((id_mismatch == 0) ? -1 : -2);
}

/*!
 * \brief Adding a new thread
 * \ingroup iapi-corelib
 * Adding a new thread, and schedule it
 * (Tasks are stored in order of scheduled time of them)
 * \param task_fp Task function
 * \param arg Argument of task function
 * \param tm Scheduled time (absolute time)
 * \return ID of the registered task, otherwise (unsigned long)0
 */
unsigned long tasks_add(task_t * task_fp, void * arg, struct timespec tm)
{
    pthread_t thid = pthread_self();
	unsigned long id = rand() + (unsigned long)thid;
	unsigned long retval = (unsigned long)0;
	struct tasks_list * task;
	int ret;

	if (task_fp == NULL)
		return retval;

	task = malloc(sizeof(struct tasks_list));
	if (task == NULL)
		return retval;
	task->id = id;
	tscpy(task->time, tm);
	task->task_fp = task_fp;
	task->arg = arg;

	do {
		ret = __tasks_add(task);
		if (ret == -2)
			task->id += 1;
	} while (ret != 0);

	return id;
}

/*!
 * \brief Deleting a previously added thread
 * \ingroup iapi-corelib
 * \param id ID of task (return value of tasks_add)
 * \return zero if OK, otherwise negative
 */
int tasks_del(unsigned long id)
{
	int retval = -1;
	struct list_head * pos, * pos2;
	struct tasks_list * tp;

	if (id == 0)
		return -1;

	pthread_mutex_lock(&tasks_mutex);

	list_for_each_safe(pos, pos2, &tasks_list) {
		tp = (struct tasks_list *)list_entry(pos, struct tasks_list, list);
		if (id == tp->id) {
			list_del(pos);
			free(tp);
			retval = 0;
			break;
		}
	}
	
#ifdef CORE_IC_DEBUG
	if (retval == 0)
		DEBUG("Deleted 1 task. [%lu]", id);
#endif

	pthread_cond_broadcast(&tasks_cond);
	
	pthread_mutex_unlock(&tasks_mutex);

	return retval;
}

/*!
 * \brief Task runner thread
 *
 * It waits for tasks (cond_wait, while tasks_list is empty)
 * It gets the first task (tasks are sorted by scheduled time)
 * It waits for that time, but cond_timedwait could return, in case of addition / removal.
 *   If it happens, it start it again.
 * Finally it starts the task function, and deletes the task
 * \param arg Unused
 * \return Unused
 */
static void * tasks_runner(void * arg)
{
	struct list_head * pos;
	struct tasks_list * tp;
	struct timespec ts;
	struct timespec now;
	unsigned long id;
	task_t * fp;
	void * fp_arg;
	int ret;

	for(;;) {
		pthread_mutex_lock(&tasks_mutex);

		if (list_empty(&tasks_list)) {
			pthread_cond_wait(&tasks_cond, &tasks_mutex);
		
			if (list_empty(&tasks_list)) 
				goto cont;
		}

		pos = tasks_list.next;
		tp = (struct tasks_list *)list_entry(pos, struct tasks_list, list);
		id = tp->id;

		clock_gettime(CLOCK_REALTIME, &now);
		/* Run past task now */
		if (! tsbefore(now, tp->time)) {
			tscpy(ts, tp->time);

			ret = pthread_cond_timedwait(&tasks_cond, &tasks_mutex, &ts);
			if (ret == 0) 
				goto cont;
			
			if (list_empty(&tasks_list)) 
				goto cont;
		
			pos = tasks_list.next;
			tp = (struct tasks_list *)list_entry(pos, struct tasks_list, list);

			if (id != tp->id) 
				goto cont;
		}
		
		fp = tp->task_fp;
		fp_arg = tp->arg;

		list_del(&tp->list);
		free(tp);

#ifdef CORE_IC_DEBUG
		DEBUG("Running (and deleted) task. [%lu]", id);
#endif

		pthread_mutex_unlock(&tasks_mutex);

		fp(fp_arg);

		continue;

cont:
		pthread_mutex_unlock(&tasks_mutex);
	}

	return NULL;
}

/*! \} */

