
/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/time.h>
#include <pthread.h>

#include <mip6d-ng/imessaging.h>
#include <mip6d-ng/logger.h>
#include <mip6d-ng/threading.h>
#include <mip6d-ng/list.h>
#include <mip6d-ng/misc.h>

/*!
 * \brief Helper structure to pass stuff to the event function 
 */
struct event_arg {
	/*! Event handler */
	event_handler_t * fp;
	/*! Messaging ID of sender module */
	unsigned int sender;
	/*! Event ID */
	unsigned int event;
	/*! Event argument */
	void * arg;
	/*! If non-zero, the argument should to freed */
	unsigned char need_to_free;
};

/*!
 * \brief Helper structure to pass stuff to the message processing function
 */
struct message_arg {
	/*! Message mutex */
	pthread_mutex_t * msg_mutex;
	/*! Conditional variable to sign the completeness of processing */
	pthread_cond_t * msg_cond;
	/* Message handler */
	message_handler_t * fp;
	/*! Messaging ID of sender module */
	unsigned int sender;
	/*! Message ID */
	unsigned int message;
	/*! Message argument */
	void * arg;
	/*! Message reply argument */
	void * reply_arg;
	/*! If non-zero, the argument should to be freed */
	unsigned char need_to_free;
	/*! Return value of message processing */
	int retval;
};

/*!
 * \brief RW lock for messaging list
 *
 * Protects imsg_list
 */
static pthread_rwlock_t imsg_mutex = PTHREAD_RWLOCK_INITIALIZER;

/*!
 * \brief List containing event subscriptions
 */
struct imsg_event_subs {
	/*! Linked list */
	struct list_head list;
	/*! Destination ID */
	unsigned int dest;
};

/*!
 * \brief List containing registered "nodes"
 */
static struct imsg_list {
	/*! Name of "node" */				 char name[20];
	/*! Options (functions) of node */	 const struct imsg_opts * opts;
	/*! Event subscriptions */			 struct list_head * subscriptions;
	/*! If not null, it could re-used */ unsigned char unused;
} * imsg_list = NULL;

/*!
 * \brief Number of registered "nodes"
 */
static unsigned int imsg_num = 0;

/*!
 * \brief Initializing internal messaging
 * \return Zero if OK
 */
int imsg_init ()
{
    pthread_rwlock_wrlock(&imsg_mutex);

   
	pthread_rwlock_unlock(&imsg_mutex);
    
	return 0;
}

/*!
 * \brief Registering a node for internal messaging
 * \ingroup iapi-corelib
 * \param name Name of node (maximum 99 chars)
 * \param opts Messaging options (functions)
 * \return registered messaging ID if OK, otherwise negative
 */
int imsg_register(const char * const name, const struct imsg_opts * opts)
{
	unsigned int i;
	int id = -1;

	if (name == NULL || opts == NULL)
		return -1;

    pthread_rwlock_wrlock(&imsg_mutex);

	for (i = 0; i < imsg_num; i++) {
		if (imsg_list[i].unused == 1) {
			id = (int)i;
			break;
		}
	}

	if (imsg_num == UINT_MAX)
		goto out;

	if (id < 0) {
		imsg_list = realloc(imsg_list, (imsg_num + 1) * sizeof(struct imsg_list));
		if (imsg_list == NULL) {
			ERROR("Out of memory: Reallocating imessaging list");
			exit(EXIT_FAILURE);
		}
		id = (int)(imsg_num++);
	}

	strncpy(imsg_list[id].name, name, sizeof(imsg_list[id].name));
	imsg_list[id].opts = opts;
	imsg_list[id].unused = 0;
	if (opts->event_max > 0) {
		imsg_list[id].subscriptions = malloc((opts->event_max + 1) * sizeof(struct imsg_event_subs));
		if (imsg_list[id].subscriptions == NULL) {
			ERROR("Out of memory: Allocating message subscription array");
			exit(EXIT_FAILURE);
		}
		for (i = 0; i < opts->event_max + 1; i++) {
			INIT_LIST_HEAD(&imsg_list[id].subscriptions[i]);
		}
	}


out:

	pthread_rwlock_unlock(&imsg_mutex);

#ifdef CORE_IC_DEBUG
	DEBUG("Imessaging: new entry registered: '%s' ID %d", name, id);
#endif

	return id;
}

/*!
 * \brief Unregistering a node from internal messaging
 * \ingroup iapi-corelib
 * \param id Messaging ID, returned by imsg_register
 * \return zero if OK, otherwise negative
 */
int imsg_unregister(unsigned int id)
{	
	int retval = 1;

    pthread_rwlock_wrlock(&imsg_mutex);

	if (id >= imsg_num)
		goto out;

	imsg_list[id].name[0] = '\0';
	if (imsg_list[id].subscriptions) {
		free(imsg_list[id].subscriptions);
		imsg_list[id].subscriptions = NULL;
	}
	imsg_list[id].unused = 1;

	retval = 0;

out:

	pthread_rwlock_unlock(&imsg_mutex);

	return retval;
}

/*!
 * \brief Subscribe for an event
 * \ingroup iapi-corelib
 *
 * 'dest' module subscribes for 'event' event of 'id' module.
 *
 * \param id ID of event sender modules
 * \param event Event ID
 * \param dest Destination module ID
 *
 * \return zero if OK, otherwise negative
 */
int imsg_event_subscribe(unsigned int id, unsigned int event, unsigned int dest)
{
	int retval = -1;
	struct imsg_event_subs * sub;

	pthread_rwlock_wrlock(&imsg_mutex);

	if (id >= imsg_num || imsg_list[id].unused == 1)
		goto out;

	if (event > imsg_list[id].opts->event_max)
		goto out;

	sub = malloc(sizeof(struct imsg_event_subs));
	if (sub == NULL) {
		ERROR("Out of memory: Alloc event subscription struct");
		goto out;
	}

	sub->dest = dest;

	list_add_tail(&sub->list, &imsg_list[id].subscriptions[event]);

#ifdef CORE_IC_DEBUG
	DEBUG("Imessaging: new event subscription: '%s' event %d ==> %d", imsg_list[id].name, event, dest);
#endif

	retval = 0;

out:

	pthread_rwlock_unlock(&imsg_mutex);

	return retval;
}

/* \brief Unsubscribe from an event
 * \ingroup iapi-corelib
 *
 * 'dest' module unsubscribes from 'event' event of 'id' module.
 *
 * \param id ID of event sender modules
 * \param event Event ID
 * \param dest Destination module ID
 *
 * \return zero if OK, otherwise negative
 */
int imsg_event_unsubscribe(unsigned int id, unsigned int event, unsigned int dest)
{
	int retval = -1;
	struct list_head * pos, *pos2;
	struct imsg_event_subs * sub;

	pthread_rwlock_wrlock(&imsg_mutex);

	if (id >= imsg_num || imsg_list[id].unused == 1)
		goto out;

	if (event > imsg_list[id].opts->event_max)
		goto out;

	list_for_each_safe(pos, pos2, &imsg_list[id].subscriptions[event]) {
		sub = list_entry(pos, struct imsg_event_subs, list);
		if (sub->dest == dest) {
			list_del(pos);
			free(sub);
			retval = 0;
		}
	}

out:

	pthread_rwlock_unlock(&imsg_mutex);

	return retval;
}

/*!
 * \brief Getting node id from node name
 * \ingroup iapi-corelib
 * \param name Node name
 * \return node id if OK, otherwise negative
 */
int imsg_get_id(const char * const name)
{
	unsigned int i;
	int id = -1;

    pthread_rwlock_rdlock(&imsg_mutex);

	for (i = 0; i < imsg_num; i++) {
		if (strcmp(imsg_list[i].name, name) == 0) {
			id = (int)i;
			break;
		}
	}

	pthread_rwlock_unlock(&imsg_mutex);

	return id;
}

/*!
 * \brief Getting node name form node ID
 * \ingroup iapi-corelib
 * \param id node id
 * \param name Buffer for the name of the node
 * \param nlen Size of buffer
 * \return zero if OK, otherwise negative
 */
int imsg_get_name(unsigned int id, char * const name, size_t nlen)
{
	int retval = -1;

	if (name == NULL || nlen == 0)
		return -1;

    pthread_rwlock_rdlock(&imsg_mutex);

	if (id >= imsg_num)
		goto out;

	strncpy(name, imsg_list[id].name, nlen);

	retval = 0;

out:

	pthread_rwlock_unlock(&imsg_mutex);

	return retval;
}

/*!
 * \brief Staring event handler function in a new thread
 *
 * After execution it frees arg
 * \param arg Should be event_arg
 * \return NULL
 */
static void * __imsg_event_thread(void * arg)
{
	struct event_arg * earg = (struct event_arg *)arg;

	if (earg == NULL)
		return NULL;

	earg->fp(earg->sender, earg->event, earg->arg, earg->need_to_free);

	free(earg);

	return NULL;
}

/*!
 * \brief Delivering event to the destination
 *
 * If requested duplicates arg
 * Start event handler in a new thread
 * \param opts Destination node's opts
 * \param sender ID of sender
 * \param event ID of event
 * \param arg Event arg (coudl be NULL)
 * \param alen Size of arg
 * \param duplicate If not zero, duplicates arg before passing to the handler
 * \param copy Copy function, use NULL to use the default memcpy
 * \return zero if OF, otherwise negative
 */
static int __imsg_send_event(const struct imsg_opts * const opts, unsigned int sender, unsigned char event,
		void * arg, size_t alen, unsigned char duplicate, void * (* copy)(void * dest, const void * src, size_t n))
{
	void * _arg = arg;
	unsigned char need_to_free = 0;
	struct event_arg * earg;

	if (duplicate) {
		_arg = malloc(alen);
		if (_arg == NULL)
			return -1;
		if (copy == NULL)
			memcpy(_arg, arg, alen);
		else
			copy(_arg, arg, alen);
		need_to_free = 1;
	} else {
		if (copy != NULL)
			copy(NULL, arg, alen);
	}

	earg = malloc(sizeof(struct event_arg));
	if (earg == NULL) {
		if (need_to_free)
			free(_arg);
		return -1;
	}

	earg->fp = opts->event_fp;
	earg->sender = sender;
	earg->event = event;
	earg->arg = _arg;
	earg->need_to_free = need_to_free;

	(void)threading_create(__imsg_event_thread, (void *)earg);

	//opts->event_fp kept by earg
	//WARNING: If an outer caller unregisters, and deletes opts
	//   the program here will crash
	return 0;
}

/*!
 * \brief Send event to dest
 * \ingroup iapi-corelib
 * Asynchron delivering. After sending event, this function returns immediately
 * \param sender ID of sender
 * \param event ID of event
 * \param arg Event arg (could be NULL)
 * \param alen Size of arg
 * \param duplicate If non-zero, duplicates arg before delivery
 * \param copy Copy function, use NULL to use the default memcpy
 * \return The number of the called subscribers OK, otherwise negative
 */
int imsg_send_event(unsigned int sender, unsigned int event, void * arg, size_t alen, unsigned char duplicate, 
		void * (* copy)(void * dest, const void * src, size_t n))
{
	int retval = 1;
	int ret;
	int c;
	struct list_head * pos;
	struct imsg_event_subs * sub;

	if (duplicate && (arg == NULL || alen == 0))
		return -1;

    pthread_rwlock_rdlock(&imsg_mutex);
    
	if (sender >= imsg_num)
		goto out;

	if (imsg_list[sender].unused == 1)
		goto out;

	if (event > imsg_list[sender].opts->event_max)
		goto out;

	ret = 0;
	c = 0;
	list_for_each(pos, &imsg_list[sender].subscriptions[event]) {
		sub = list_entry(pos, struct imsg_event_subs, list);

		if (sub->dest >= imsg_num || imsg_list[sub->dest].unused == 1)
			continue;

		if (imsg_list[sub->dest].opts->event_fp == NULL)
			continue;

		ret |= __imsg_send_event(imsg_list[sub->dest].opts, sender, event, arg, alen, duplicate, copy);
		if (ret == 0)
			c++;
	}

	if (ret)
		goto out;

	retval = c;

out:

	pthread_rwlock_unlock(&imsg_mutex);

	return retval;
}

/*!
 * \brief Delivering message, and waiting for reply
 *
 * It calls message handler function
 * It frees arg and its components
 * \param arg Should be message_arg
 * \return NULL
 */
static void * __imsg_message_thread(void * arg)
{
	struct message_arg * marg = (struct message_arg *)arg;
	struct timeval tv;
	struct timespec to;

	if (marg == NULL)
		return NULL;

	pthread_mutex_lock(marg->msg_mutex);
	marg->retval = marg->fp(marg->sender, marg->message, marg->arg, marg->reply_arg);
	pthread_mutex_unlock(marg->msg_mutex);
	pthread_cond_broadcast(marg->msg_cond);

	pthread_mutex_lock(marg->msg_mutex);
	gettimeofday(&tv, NULL);
	to.tv_sec  = tv.tv_sec;
	to.tv_nsec = tv.tv_usec * 1000 + 100 * 1000;
	pthread_cond_timedwait(marg->msg_cond, marg->msg_mutex, &to);

	if (marg->need_to_free && marg->arg != NULL)
		free(marg->arg);
	if (marg->need_to_free && marg->reply_arg != NULL)
		free(marg->reply_arg);
	
	pthread_mutex_unlock(marg->msg_mutex);
	
	pthread_mutex_destroy(marg->msg_mutex);
	pthread_cond_destroy(marg->msg_cond);
	free(marg->msg_mutex);
	free(marg->msg_cond);

	free(marg);
	

	return NULL;
}

/*!
 * \brief Sends a message, and waits for reply
 * \ingroup iapi-corelib
 * Max wait: timeout_usec
 * \param sender ID of sender
 * \param dest ID of destination
 * \param message ID of message
 * \param arg Message arg (could be NULL)
 * \param alen Size of arg
 * \param reply_arg Reply arg (could be NULL)
 * \param ralen Size of reply_arg
 * \param duplicate If non-zero duplicates arg, reply_arg and reply before sending
 * \param copy Copy function, use NULL to use the default memcpy
 * \param timeout_usec Max timeout waiting for reply (in usecs)
 * \return Return value of message handler function if OK, otherwise negative
 */
int imsg_message(unsigned int sender, unsigned int dest, unsigned int message, void * arg, size_t alen, 
		void * reply_arg, size_t ralen, unsigned char duplicate, void * (* copy)(void * dest, const void * src, size_t n),
		unsigned long long timeout_usec)
{
	int retval = -1;
	int ret;
	struct message_arg * marg;
	struct timeval tv;
	struct timespec to;
	struct timespec top;
	struct timespec tot;

	marg = malloc(sizeof(struct message_arg));
	if (marg == NULL)
		return -1;
	marg->msg_mutex = malloc(sizeof(pthread_mutex_t));
	if (marg->msg_mutex == NULL)
		return -1;
	marg->msg_cond = malloc(sizeof(pthread_cond_t));
	if (marg->msg_cond == NULL) {
		free(marg->msg_mutex);
		return -1;
	}
	ret = pthread_mutex_init(marg->msg_mutex, NULL);
	if (ret) {
		free(marg->msg_mutex);
		free(marg->msg_cond);
		return -1;
	}
	ret = pthread_cond_init(marg->msg_cond, NULL);
	if (ret) {
		pthread_mutex_destroy(marg->msg_mutex);
		free(marg->msg_mutex);
		free(marg->msg_cond);
		return -1;
	}

	pthread_rwlock_rdlock(&imsg_mutex);

	if (sender >= imsg_num || dest >= imsg_num)
		goto out;

	if (imsg_list[sender].unused == 1 || imsg_list[dest].unused == 1)
		goto out;

	if (imsg_list[dest].opts->message_fp == NULL)
		goto out;

	marg->fp = imsg_list[dest].opts->message_fp;
	marg->sender = sender;
	marg->message = message;
	if (arg != NULL && alen > 0) {
		if (duplicate) {
			marg->arg = malloc(alen);
			if (marg->arg == NULL)
				goto out;
			if (copy == NULL)
				memcpy(marg->arg, arg, alen);
			else
				copy(marg->arg, arg, alen);
		} else {
			marg->arg = arg;
		}
	} else
		marg->arg = NULL;
	if (reply_arg != NULL && ralen > 0) {
		if (duplicate) {
			marg->reply_arg = malloc(ralen);
			if (marg->reply_arg == NULL)
				goto out;
		} else {
			marg->reply_arg = reply_arg;
		}
	} else
		marg->reply_arg = NULL;
	marg->need_to_free = duplicate;

	pthread_mutex_lock(marg->msg_mutex);

	(void)threading_create(__imsg_message_thread, (void *)marg);

	gettimeofday(&tv, NULL);
	to.tv_sec  = tv.tv_sec;
	to.tv_nsec = tv.tv_usec * 1000;
	top.tv_sec = 0;
	top.tv_nsec = timeout_usec * 1000;
	tsadd(to, top, tot);
	//DEBUG("timeout: %llu usec to {%llu sec %llu nsec}", timeout_usec, tot.tv_sec, tot.tv_nsec);
	ret = pthread_cond_timedwait(marg->msg_cond, marg->msg_mutex, &tot);

	if (ret) {
		marg = NULL;
		DEBUG("!!! IMSG MESSAGE Timeout");
		goto out;
	}

	if (duplicate && reply_arg != NULL && ralen > 0) {
		if (copy == NULL)
			memcpy(reply_arg, marg->reply_arg, ralen);
		else
			copy(reply_arg, marg->reply_arg, ralen);
	}
	retval = marg->retval;

	pthread_cond_broadcast(marg->msg_cond);
	pthread_mutex_unlock(marg->msg_mutex);
	
	marg = NULL;

out:

	if (marg != NULL) {
		if (duplicate && marg->arg != NULL)
			free(marg->arg);
		if (duplicate && marg->reply_arg != NULL)
			free(marg->reply_arg);
		pthread_mutex_destroy(marg->msg_mutex);
		pthread_cond_destroy(marg->msg_cond);
		free(marg->msg_mutex);
		free(marg->msg_cond);
		free(marg);
	}

	pthread_rwlock_unlock(&imsg_mutex);

	return retval;
}

/*! \} */

