

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_MISSING_H
#define MIP6D_NG_MISSING_H


#ifndef MIP6D_NG_MODULE
#include <config.h>
#else
#include <mip6d-ng/config.h>
#endif
#include <unistd.h>
#include <stddef.h>
#include <string.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <netinet/ip6.h>

#ifndef HAVE_SA_FAMILY_T
typedef unsigned short sa_family_t;
#endif

#ifndef HAVE_IPPROTO_MH
#define IPPROTO_MH  135 /* IPv6 Mobility Header */
#endif

#ifndef HAVE_IP6OPT_HOME_ADDRESS
#define IP6OPT_HOME_ADDRESS  0xc9	/* IPv6 Destination Option: Home Address, 11 0 01001 */
#endif

#ifndef HAVE_IPV6_RTHDR_TYPE_2
#define IPV6_RTHDR_TYPE_2  2 		/* IPv6 Routing Header: Type 2 */
#endif

#ifndef HAVE_INET6_RTH_GETTYPE
static inline int inet6_rth_gettype(void *bp)
{
	uint8_t *rthp = (uint8_t *)bp;
	return rthp[2];
}
#endif

#ifndef HAVE_IPV6_RTHDR_TYPE_2
static inline socklen_t mip6d_ng_inet6_rth_space(int type, int segments)
{
	if (type == IPV6_RTHDR_TYPE_0) {
		if (segments > 128)
			return 0;
		return 8 + segments * sizeof(struct in6_addr);
	} else if (type == IPV6_RTHDR_TYPE_2) {
		if (segments != 1)
			return 0;
		return 8 + sizeof(struct in6_addr);
	}
	return 0;
}

static inline void * mip6d_ng_inet6_rth_init(void *bp, socklen_t bplen, int type, int segments)
{
	struct ip6_rthdr *rth = (struct ip6_rthdr *)bp;
	uint8_t type_len = 0;

	if (type == IPV6_RTHDR_TYPE_0) {
		type_len = 8;
		*(uint32_t *)(rth+1) = 0;
	} else if (type == IPV6_RTHDR_TYPE_2) {
		type_len = 8;
		*(uint32_t *)(rth+1) = 0;
	} else
		return NULL;

	if (bplen < type_len + segments * sizeof(struct in6_addr))
		return NULL;

	rth->ip6r_nxt = 0;
	rth->ip6r_len = segments << 1;
	rth->ip6r_type = type;
	rth->ip6r_segleft = 0;

	return bp;
}

static inline int mip6d_ng_inet6_rth_add(void *bp, const struct in6_addr *addr)
{
	struct ip6_rthdr *rth;
	rth = (struct ip6_rthdr *)bp;

	memcpy((uint8_t *)bp + 8 + rth->ip6r_segleft * sizeof(struct in6_addr), addr, sizeof(struct in6_addr));
	rth->ip6r_segleft += 1;

	return 0;
}

static inline struct in6_addr * mip6d_ng_inet6_rth_getaddr(const void *bp, int index)
{
	uint8_t *rthp = (uint8_t *)bp;
	struct in6_addr *addr = NULL;

	if (rthp[1] & 1) return NULL;
	if (index < 0 || index > rthp[3]) return NULL;

	addr = (struct in6_addr *)
		(rthp + 8 + index * sizeof(struct in6_addr));

	return addr;
}
#else
static inline socklen_t mip6d_ng_inet6_rth_space(int type, int segments)
{
	return inet6_rth_space(type, segments);
}

static inline void * mip6d_ng_inet6_rth_init(void *bp, socklen_t bplen, int type, int segments)
{
	return inet6_rth_init(bp, bplen, type, segments);
}
static inline int mip6d_ng_inet6_rth_add(void *bp, const struct in6_addr *addr)
{
	return inet6_rth_add(bp, addr);
}
static inline struct in6_addr * mip6d_ng_inet6_rth_getaddr(const void *bp, int index)
{
	return inet6_rth_getaddr(bp, index);
}
#endif

#ifndef HAVE_INET6_OPT_FIND
#ifndef IP6OPT_PAD1
#define IP6OPT_PAD1 0
#endif

static inline int inet6_opt_find(void *extbuf, socklen_t extlen, int offset, 
		   uint8_t type, socklen_t *lenp,
		   void **databufp)
{
	uint8_t *optp, *tailp;

	optp = (uint8_t *)extbuf;

	if (extlen < 2 || extlen <= offset || extlen < ((optp[1] + 1) << 3))
		return -1;

	tailp = optp + extlen;
	optp += (2 + offset);

	while (optp <= tailp) {
		if (optp[0] == IP6OPT_PAD1) {
			optp++;
			continue;
		}
		if (optp + optp[1] + 2 > tailp)
			return -1;
		if (optp[0] == type) {
			*databufp = optp + 2;
			*lenp = optp[1];
			return *lenp + (uint8_t *)optp - (uint8_t *)extbuf;
		}
		optp += (2 + optp[1]);
	}

	*databufp = NULL;
	return -1;
}
#endif

#ifdef FEATURE_IPTABLES

#include <libiptc/ipt_kernel_headers.h>
#include <libiptc/libip6tc.h>

#ifndef HAVE_IP6T_MIN_ALIGN
#ifndef IP6T_MIN_ALIGN
#define IP6T_MIN_ALIGN (__alignof__(struct ip6t_entry))
#endif
#endif

#ifndef HAVE_IP6T_ALIGN
#define IP6T_ALIGN(s) (((s) + (IP6T_MIN_ALIGN-1)) & ~(IP6T_MIN_ALIGN-1))
#endif

#endif /* FEATURE_IPTABLES */

#endif /* MIP6D_NG_MISSING_H */

/*! \} */

