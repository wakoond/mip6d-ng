

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup internal-corelib
 * \{
 */

#ifndef MIP6D_NG_CFG_H
#define MIP6D_NG_CFG_H

#include <getopt.h>

int prepare_get_my_opts(int argc, char * const argv[], int * const r_argc, char *** r_argv);
int get_my_opts(int argc, char * const argv[], const char *optstring,
		const struct option *longopts, int *longindex, int * const r_argc, char *** r_argv);

void free_r_args(int r_argc, char ** r_argv);
cfg_t * config_load(const char * const path, cfg_opt_t * main_opts, size_t main_opts_size);
void config_release(cfg_t * cfg);

#endif /* MIP6D_NG_CFG_H */

/*! \} */

