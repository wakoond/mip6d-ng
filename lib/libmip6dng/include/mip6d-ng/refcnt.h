

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_REFCNT_H
#define MIP6D_NG_REFCNT_H

#include <mip6d-ng/logger.h>
#include <pthread.h>

/*!
 * \brief Reference counter
 *
 * It should to be the first element of the structure
 */
struct refcnt {
	/*! Reference counter lock */
	pthread_mutex_t lock;
	/* Reference counter */
	uint32_t cnt;
	/*! Delete function, called if cnt == 0 */
	void (* del_fp)(void * arg);
	/*! Argument to the del_fp function */
	void * del_arg;
	/*! Name of stuff: used for logging */
	char name[20];
};

/*!
 * \brief Initialize reference counting
 *
 * It sets the reference counter to 1
 * \param refcnt reference counter
 * \param del_fp Delete function, called if cnt will be zero
 * \param del_arg Argument to del_fp
 * \param name Name, used for debugging
 */
static inline void refcnt_init(struct refcnt * refcnt, void (* del_fp)(void * arg), void * del_arg, char * name)
{
	if (refcnt == NULL)
		return;

	pthread_mutex_init(&refcnt->lock, NULL);
	refcnt->del_fp = del_fp;
	refcnt->del_arg = del_arg;
	refcnt->cnt = 1;
	if (name != NULL)
		strncpy(refcnt->name, name, sizeof(refcnt->name));
	else
		refcnt->name[0] = '\0';
}

/*!
 * \brief Get reference counter (increase)
 * \ingroup internal-corelib
 * \param refcnt Reference counter
 * \param func Function name, where it has been called (see __FUNCTION__ macro), used for debugging
 */
static inline void _refcnt_get(struct refcnt * refcnt, const char * func)
{
	if (refcnt == NULL)
		return;

	pthread_mutex_lock(&refcnt->lock);
	refcnt->cnt++;
#ifdef REFCNT_DEBUG
	DEBUG("Refcnt GET: '%s' @ %s > %u", refcnt->name, func, refcnt->cnt);
#endif
	pthread_mutex_unlock(&refcnt->lock);
}

/*!
 * \brief Get reference counter (increase)
 * \param refcnt Reference counter
 */
#define refcnt_get(refcnt)	_refcnt_get(refcnt, __FUNCTION__)

/*!
 * \brief Put reference counter (decrease)
 * \ingroup internal-corelib
 * If cnt will be zero, the delete function will be executed
 * \param refcnt Reference counter
 * \param func Function name, where it has been called (see __FUNCTION__ macro), used for debugging
 */
static inline void _refcnt_put(struct refcnt * refcnt, const char * func)
{
	if (refcnt == NULL)
		return;

	pthread_mutex_lock(&refcnt->lock);
	if (refcnt->cnt > 0)
		refcnt->cnt--;
	else {
		ERROR("Refcnt put with zero refcnt value! ('%s' @ %s)", refcnt->name, func);
		return;
	}

#ifdef REFCNT_DEBUG
	DEBUG("Refcnt PUT: '%s' @ %s > %u", refcnt->name, func, refcnt->cnt);
#endif

	if (refcnt->cnt == 0) {
		pthread_mutex_unlock(&refcnt->lock);
		if (refcnt->del_fp != NULL)
			refcnt->del_fp(refcnt->del_arg);
	} else
		pthread_mutex_unlock(&refcnt->lock);
}

/*!
 * \brief Put reference counter (decrease)
 *
 * If cnt will be zero, the delete function will be executed
 * \param refcnt Reference counter
 */
#define refcnt_put(refcnt)	_refcnt_put(refcnt, __FUNCTION__)

/*!
 * \brief Memcpy implementation with reference counter increase
 *
 * Wrapper for memcpy. It casts the argument to struct refcnt, and 
 * calls refcnt_get for it. If dest is zero, it gets a reference pointer
 * but do not perform memcpy.
 * \param dest Output pointer
 * \param src Input pointer, to copy
 * \param n Size of src
 * \return returning with dest pointer or NULL if error
 */
static inline void * refcnt_copy(void * dest, const void * src, size_t n)
{
	struct refcnt * refcnt;

	if (src == NULL || n == 0)
		return NULL;

	if (dest != NULL) {
		dest = memcpy(dest, src, n); 
		if (dest == NULL)
			return NULL;
	}
			
	if (n >= sizeof(struct refcnt)) {
		refcnt = (struct refcnt *)src;
		refcnt_get(refcnt);
	}

	return dest;
}

#endif /* MIP6D_NG_REFCNT_H */

/*! \} */

