

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_MODULE_H
#define MIP6D_NG_MODULE_H

#ifndef MIP6D_NG_MODULE
#include <config.h>
#else
#include <mip6d-ng/config.h>
#endif
#include <confuse.h>

/*!
 * \brief get_name symbol for symbol lookup
 *
 * The name of the function in the module must be equal with that
 */
#define MIP6_MODULE_GET_NAME_SYM		"mip6_module_get_name"

/*!
 * \brief get_seq symbol for symbol lookup
 *
 * The name of the function in the module must be equal with that
 */
#define MIP6_MODULE_GET_SEQ_SYM			"mip6_module_get_seq"

/*!
 * \brief get_config_desc symbol for symbol lookup
 *
 * The name of the function in the module must be equal with that
 */
#define MIP6_MODULE_GET_CONFIG_DESC_SYM	"mip6_module_get_config_desc"

/*!
 * \brief initialize symbol for symbol lookup
 *
 * The name of the function in the module must be equal with that
 */
#define MIP6_MODULE_INIT_SYM			"mip6_module_init"

/*!
 * \brief get_name symbol function type
 *
 * \return It should return with the name of the module (max 99 characters)
 */
typedef char * mip6_module_get_name_t(void);

/*!
 * \brief get_seq symbol function type
 *
 * \return It should return with the requested sequence number of the module
 */
typedef unsigned char mip6_module_get_seq_t(void);

/*!
 * \brief get_config_desc function type
 *
 * The descriptors should be organized as follows:
 *   First the main (root) descriptor
 *   Next, the descriptors which are referenced (CFG_SEC) from the main descriptor
 *   Next, the descriptors, which are referenced from the first nested descriptor
 *   and so on...
 *
 * \param idx Configure descriptor index
 * \return It should return with the name of the idx. configure descriptor (cfg_opt_t)
 */
#ifdef HAVE_LTDL_H
typedef char  * mip6_module_get_config_desc_t(unsigned int idx);
#else
typedef cfg_opt_t * mip6_module_get_config_desc_t(unsigned int idx);
#endif


/*!
 * \brief initialize symbol function type
 *
 * \param argc Number of command line arguments
 * \param argv Command line arguments
 * \param r_argc Number of remaining (unused) command line arguments
 * \param r_argv Remaining (unused) command line arguments
 * \param cfg Confuse configure handler
 * \return Status code (if everything is fine, it should be zero)
 */
typedef int mip6_module_init_t(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg);

#ifdef HAVE_LTDL_H
#define MIP6_MODULE_INIT__(modulename, name, seq, init_fn, ...) \
	char * mip6_module_get_name(void) { \
		return name; \
	} \
	\
	unsigned int mip6_module_get_seq(void) { \
		return seq; \
	} \
	char * mip6_module_get_config_desc(unsigned int idx) { \
		static char * optstr = "" #__VA_ARGS__; \
		int len = strlen(optstr); \
		int i, last_start; \
		int cnt = idx; \
		int olen; \
		char * opt = NULL; \
		for (i = last_start = 0; i < len; i++) { \
			if (optstr[i] == ',' || i == len - 1) { \
				cnt--; \
				if (cnt < 0) { \
					olen = (i == len - 1) ? (i - last_start + 2) : (i - last_start + 1); \
					opt = malloc(olen); \
					if (opt != NULL) { \
						memset(opt, 0, olen); \
						strncpy(opt, &optstr[last_start], olen); \
						opt[olen - 1] = '\0'; \
					} \
					return opt; \
				} \
				last_start = i + 1; \
			} \
		} \
		return NULL; \
	} \
	int mip6_module_init(int argc, char * const argv[], int * const r_argc, char *** r_argv, cfg_t * cfg) { \
		return init_fn(argc, argv, r_argc, r_argv, cfg); \
	}
#else
#define MIP6_MODULE_INIT__(modulename, name, seq, init_fn, ...) \
	cfg_opt_t * modulename ## _get_config(unsigned int idx) { \
		static cfg_opt_t * opts[] = { __VA_ARGS__ }; \
		static unsigned char opt_num = sizeof(opts) / sizeof(cfg_opt_t *); \
		if (idx >= opt_num) \
			return NULL; \
		return opts[idx]; \
	} \
	void modulename ## _register_module() { \
		module_register(name, seq, modulename ## _get_config, init_fn); \
	}

void module_register(char * name, unsigned int seq, mip6_module_get_config_desc_t * module_get_config_desc_fp, mip6_module_init_t * module_init_fp);
#endif
	
#define MIP6_MODULE_INIT_(modulename, name, seq, init_fn, ...) \
	MIP6_MODULE_INIT__(modulename, name, seq, init_fn, ##__VA_ARGS__)

#define MIP6_MODULE_INIT(name, seq, init_fn, ...) \
	MIP6_MODULE_INIT_(MODULE_NAME, name, seq, init_fn, ##__VA_ARGS__)

#endif  /* MIP6D_NG_MODULE_H */

/*! \} */

