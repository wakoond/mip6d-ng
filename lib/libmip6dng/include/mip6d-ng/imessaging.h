

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_IMESSAGING_H
#define MIP6D_NG_IMESSAGING_H

#include <limits.h>
#include <stddef.h>

/*!
 * \brief Event handler function type
 *
 * Called to delivering events to the destination
 * \param sender ID of sender
 * \param event ID of event
 * \param arg Event arg (could be NULL)
 * \param need_to_free If not null, arg should be free'd
 */
typedef void event_handler_t(unsigned int sender, unsigned int event, void * arg, unsigned char need_to_free);

/*!
 * \brief Message handler function type
 *
 * Called to deliver events to the destination
 * \param sender ID of sender
 * \param message ID of message
 * \param arg Message arg (could be NULL)
 * \param reply Pointer for unsigned int ID of reply message
 * \param reply_arg Repy arg (could be NULL)
 * \return Could return to the message sender
 */
typedef int  message_handler_t(unsigned int sender, unsigned int message, void * arg, void * reply_arg);

/*!
 * \brief
 * Options for imsg registration
 */
struct imsg_opts {
	/*! Event handler function */		event_handler_t * event_fp;
	/*! Maximum id of event IDs */		unsigned int event_max;
	/*! Message handler function */		message_handler_t * message_fp;
};

/* \ingroup internal-corelib */
int imsg_init(void);
int imsg_register(const char * const name, const struct imsg_opts * opts);
int imsg_unregister(unsigned int id);
int imsg_event_subscribe(unsigned int id, unsigned int event, unsigned int dest);
int imsg_event_unsubscribe(unsigned int id, unsigned int event, unsigned int dest);
int imsg_get_id(const char * const name);
int imsg_get_name(unsigned int id, char * const name, size_t nlen);

int imsg_send_event(unsigned int sender, unsigned int event, void * arg, size_t alen, unsigned char duplicate,
		void * (* copy)(void * dest, const void * src, size_t n));
int imsg_message(unsigned int sender, unsigned int dest, unsigned int message, void * arg, size_t alen, 
		void * reply_arg, size_t ralen, unsigned char duplicate, void * (* copy)(void * dest, const void * src, size_t n),
		unsigned long long timeout_usec);

/*!
 * \brief Default timeout for messages
 */
#define IMSG_DEFAULT_TIMEOUT								900000

/*!
 * \brief Helper structure 
 */
struct imsg_arg_reply_len {
	/*! Length of reply arg */ size_t ralen;
};

/*!
 * \brief Simple event (without argument)
 *
 * Wrapper for imsg_send_event
 */
#define IMSG_EVT(sender, event)								imsg_send_event(sender, event, NULL, 0, 0, NULL)

/*!
 * \brief Simple event with argument
 *
 * Wrapper for imsg_send_event
 */
#define IMSG_EVT_ARG(sender, event, arg, alen)				imsg_send_event(sender, event, arg, alen, 0, NULL)

/*!
 * \brief Simple message (without arguments)
 *
 * Wrapper for imsg_message
 */
#define IMSG_MSG(sender, dest, msg)	\
			imsg_message(sender, dest, msg, NULL, 0, NULL, 0, 0, IMSG_DEFAULT_TIMEOUT)

/*!
 * \brief Simple message with argument
 *
 * Wrapper for imsg_message
 */
#define IMSG_MSG_ARGS(sender, dest, msg, arg, alen, rarg, ralen) \
			imsg_message(sender, dest, msg, arg, alen, rarg, ralen, 0, NULL, IMSG_DEFAULT_TIMEOUT)

/*!
 * \brief Simple message to request something
 *
 * Argument: the length of the reply arg
 * Wrapper for imsg_message
 */
#define IMSG_MSG_REPLY(__sender, __dest, __msg, __rarg, __ralen) ({ \
			struct imsg_arg_reply_len __iarl; \
			int __r; \
			__iarl.ralen = (__ralen); \
			__r = imsg_message(__sender, __dest, __msg, &__iarl, sizeof(__iarl), __rarg, __ralen, 0, NULL, IMSG_DEFAULT_TIMEOUT); \
			__r; \
		})

#endif /* MIP6D_NG_IMESSAGING_H */

/*! \} */

