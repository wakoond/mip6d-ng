

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_LOGGER_H
#define MIP6D_NG_LOGGER_H

/*!
 * Log levels
 */
enum logger_level {
	/*! Error */			LERR,
	/*! Informational */	LINF,
	/*! Debug */			LDBG
};

#include <stdarg.h>

/*! \ingroup internal-corelib */
void logger_init(const char * const file, enum logger_level level, unsigned int size);
/*! \ingroup internal-corelib */
void logger_reinit(const char * const logfile, enum logger_level level, unsigned int size);

void logger(enum logger_level level, const char * const module, const char * const msg, ...);

#ifndef MIP6_MODULE_LOG_NAME
/*!
 * Define this to set the displayed module name in the log lines
 */
#define MIP6_MODULE_LOG_NAME "unknown"
#endif

/*!
 * \brief Error logging
 *
 * Wrapper for logger, with LERR loglevel, and MIP6_MODULE_LOG_NAME module name
 */
#define ERROR(msg, ...)		logger(LERR, MIP6_MODULE_LOG_NAME, msg, ##__VA_ARGS__)
/*!
 * \brief Info logging
 *
 * Wrapper for logger, with LINF loglevel, and MIP6_MODULE_LOG_NAME module name
 */
#define INFO(msg, ...)		logger(LINF, MIP6_MODULE_LOG_NAME, msg, ##__VA_ARGS__)
/*!
 * \brief Debugging
 *
 * Wrapper for logger, with LDBG loglevel, and MIP6_MODULE_LOG_NAME module name
 */
#define DEBUG(msg, ...)		logger(LDBG, MIP6_MODULE_LOG_NAME, msg, ##__VA_ARGS__)

#endif /* MIP6D_NG_LOGGER_H */

/*! \} */
