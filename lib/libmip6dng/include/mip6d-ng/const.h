

/*
 * This file is part of mip6d-ng distribution,
 *    the Next Generation Mobile IPv6 Daemon for Linux.
 *
 * Copyright (C) 2012-2013 András Takács <wakoond@wakoond.hu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \addtogroup iapi-corelib
 * \{
 */

#ifndef MIP6D_NG_CONST_H_
#define MIP6D_NG_CONST_H_

/*!
 * \brief All node multi-cast IPv6 address 
 */
const struct in6_addr in6addr_all_nodes_mc =   { { { 0xff,0x02,0,0,0,0,0,0,0,0,0,0,0,0,0,0x1 } } };

/*!
 * \brief All routers multi-cast IPv6 address
 */
const struct in6_addr in6addr_all_routers_mc = { { { 0xff,0x02,0,0,0,0,0,0,0,0,0,0,0,0,0,0x2 } } };

#endif /* MIP6D_NG_CONST_H_ */

/*! \} */
