#!/bin/bash

CPPCHECK_ENABLE=1

CPPCHECK_OPTS="--enable=all --inline-suppr"
if [ "`cppcheck --version | sed -ne 's/[a-zA-Z ]*\([0-9\.]\)/\1/p'`" != "1.44" ]; then
       CPPCHECK_OPTS="${CPPCHECK_OPTS} --suppress=variableScope"
fi

CFLAGS_D_AND_I=
for FLAG in ${CFLAGS}; do
	if [ "${FLAG:0:2}" = "-D" -o ${FLAG:0:2} = "-I" ]; then
		CFLAGS_D_AND_I="${CFLAGS_D_AND_I} ${FLAG}"
	fi
done

if [ ${CPPCHECK_ENABLE} -ne 0 ]; then
	echo -e "--- CPPCHECK check ---\ncppcheck ${CPPCHECK_OPTS} ${CFLAGS_D_AND_I} ." && \
	cppcheck ${CPPCHECK_OPTS} ${CFLAGS_D_AND_I} .
	if [ $? -ne 0 ]; then
		exit 1
	fi
fi

#
#     UNUSED BELOW
# Splint: not supporting some of the Linux headers
#     UNUSED BELOW
#

SPLINT_ENABLE=0

SPLINT_OPTS="-badflag -warnposixheaders -formattype -predboolint -compdef +matchanyintegral -nullret -unrecog -mustfreefresh \
			 -uniondef -boolops -shiftimplementation -mayaliasunique -nullpass -nullassign -initallelements -usedef -compdestroy \
			 -ifempty -preproc -retvalint +charint"
SPLINT_WEAK_OPTS="-weak -badflag -warnposixheaders -formattype +matchanyintegral -unrecog -initallelements -ifempty -preproc"

SPLINT_EXPERIMENTAL_FLAGS="-Dasm=__asm__ -D__signed__=signed -D__attribute=__attribute__"

if [ ${SPLINT_ENABLE} -ne 0 ]; then
	echo "--- Splint weak check ---\n    splint ${SPLINT_WEAK_OPTS} ${SPLINT_EXPERIMENTAL_FLAGS} ${CFLAGS} ${SOURCES}" && \
	splint ${SPLINT_WEAK_OPTS} ${SPLINT_EXPERIMENTAL_FLAGS} ${CFLAGS} ${SOURCES} && \
	echo "--- Splint strict check ---\n    splint ${SPLINT_OPTS} ${SPLINT_EXPERIMENTAL_FLAGS} ${CFLAGS} ${SOURCES}" && \
	splint ${SPLINT_OPTS} ${SPLINT_EXPERIMENTAL_FLAGS} ${CFLAGS} ${SOURCES}
fi

