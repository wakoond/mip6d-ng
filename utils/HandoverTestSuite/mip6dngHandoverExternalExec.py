
import threading
import os
import subprocess
import re
import datetime
import shlex

class ExternalExec(threading.Thread):

	def __init__(self, cmd):
		self.cmd = cmd
		self.stop = False

		threading.Thread.__init__(self)

	def doStop(self):
		self.stop = True
		self.ee.terminate()

	def run(self):
		self.ee = subprocess.Popen(shlex.split(self.cmd), bufsize=-1, stdout=subprocess.PIPE)
		while True:
			line = self.ee.stdout.readline()
			if not line:
				break
			print line

