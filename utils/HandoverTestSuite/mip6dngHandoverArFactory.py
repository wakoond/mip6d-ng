
from mip6dngHandoverAr import Ar

class ArFactory:

	def __init__(self):
		self.ars = []

	def addAr(self, number, address, port):
		self.ars.append(Ar(number, address, port))

	def enable(self, eArs, plot=None):
		for ar in self.ars:
			if ar.getNumber() in eArs:
				ar.enable(plot=plot)
			else:
				ar.disable(plot=plot)



