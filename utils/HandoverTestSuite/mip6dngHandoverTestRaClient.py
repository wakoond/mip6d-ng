#!/usr/bin/env python

import os
import sys
import getopt
import SocketServer

from mip6dngHandoverRaHandler import RaHandler
from mip6dngHandoverRaServer import RaServer

def usage(txt = None):
	print 'Usage: ' + sys.argv[0]+ ' [hap]'
	print
	print '-h --help            Display this text'
	print '-a --address         Listening address'
	print '-p --port			Listening port'
	print '-c --client          Accepted client adress'
	if txt != None:
		print
		print txt

if __name__ == '__main__':
	try:
		opts, args = getopt.getopt(sys.argv[1:], "ha:p:c:", ["help", "address=", "port=", "client="])
	except getopt.GetoptError, err:
		usage()
		exit(1)

	address = None
	port = 0
	client = None

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			exit(0)
		elif o in ("-a", "--address"):
			address = a
		elif o in ("-p", "--port"):
			try:
				port = int(a)
			except ValueError:
				usage('Invaid port number');
				exit(1)
		elif o in ("-c", "--client"):
			client = a

	if address == None or port == 0:
		usage('Missing required address and port')
		exit(1)

	if not os.geteuid()==0:
		usage('Only root can run this')
		exit(1)

	p = os.popen('ip6tables -L OUTPUT | grep "^DROP[ \t]\+all[ \t]\+anywhere[ \t]\+anywhere[ \t]*$"','r')
	while 1:
		line = p.readline()
		if not line: break
		os.system('ip6tables -D OUTPUT -j DROP')
		print 'ip6tables -D OUTPUT -j DROP'
	p = os.popen('ip6tables -L FORWARD | grep "^DROP[ \t]\+all[ \t]\+anywhere[ \t]\+anywhere[ \t]*$"','r')
	while 1:
		line = p.readline()
		if not line: break
		os.system('ip6tables -D FORWARD -j DROP')
		print 'ip6tables -D FORWARD -j DROP'

	server = RaServer((address, port), RaHandler)
	if client != None:
		server.setAcceptedClient(client)
	server.serve_forever()

