#!/usr/bin/env python

import sys
import getopt
import time
import subprocess
import shlex

from mip6dngHandoverArFactory import ArFactory
from mip6dngHandoverTcpdumpThread import TcpdumpThread
from mip6dngHandoverPingThread import PingThread
from mip6dngHandoverExternalExec import ExternalExec
from mip6dngHandoverData import Data
from mip6dngHandoverPlot import Plot

def usage(txt = None):
	print 'Usage: ' + sys.argv[0]+ ' [hs]'
	print
	print '-h --help            Display this text'
	print '-s --sample			Set handover sample config file'
	print '-n --no-plot			Do not make plot'
	if txt != None:
		print
		print txt

if __name__ == '__main__':
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hs:n", ["help", "sample=", "no-plot"])
	except getopt.GetoptError, err:
		usage()
		exit(1)

	sample = None
	do_plot = True

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			exit(0)
		elif o in ("-s", "--sample"):
			sample = a
		elif o in ("-n", "--no-plot"):
			do_plot = False

	if sample == None:
		usage('Handover sample config file is required')
		exit(1)

	try:
		sampleF = open(sample, 'r')
	except IOError:
		usage('Cannot open handover sample config file: %s' % sample)
		exit(1)

	data = Data()
	plot = Plot(data)
	arFactory = ArFactory()
	tcpdumpThreads = []
	ha = ''
	bids = []
	pingThread = None

	while True:
		sampleL = sampleF.readline()
		if not sampleL or sampleL == '':
			break

		sampleL = sampleL.strip()

		if len(sampleL) == 0:
			print 'Disable all ARs'
			arFactory.enable([], plot=plot)
		elif sampleL[0] == '#':
			print 'Comment: %s' % sampleL[1:]
		elif sampleL[0] == 'C':
			print 'Access Router Configuration: %s' % sampleL[1:]
			c = sampleL[1:].split(':')
			if len(c) < 3:
				print 'Invalid configuration line'
			else:
				try:
					arNum = int(c[0])
					arAddr = c[1]
					arPort = int(c[2])
				except ValueError:
					print 'Invalid configuration line. AR number or port is not number'
				else:
					print 'AR number %d @ %s port %d' % (arNum, arAddr, arPort)
					arFactory.addAr(arNum, arAddr, arPort)
		elif sampleL[0] == 'H':
			print 'Home Agent address: %s' % sampleL[1:]
			ha = sampleL[1:].strip()
		elif sampleL[0] == 'I':
			print 'New interface: %s' % sampleL[1:]
			iface = sampleL[1:].strip()
			tcpdumpThread = TcpdumpThread(data, ha, iface)
			tcpdumpThread.start()
			tcpdumpThreads.append(tcpdumpThread)
		elif sampleL[0] == 'b':
			try:
				bid = int(sampleL[1:])
			except ValueError:
				print 'Invalid BID: %s' % sampleL[1:]
			else:
				print 'New BID: %d' % bid
				bids.append(bid)
		elif sampleL[0] == 'e':
			eargs = sampleL[1:]
			print 'Start external executable: ' + str(eargs)
			eexec = ExternalExec(eargs)
			eexec.start()
		elif sampleL[0] == 'h':
			print 'ping host: %s' % sampleL[1:]
			pingThread = PingThread(plot, bids, sampleL[1:])
			pingThread.start()
		elif sampleL[0] == 't':
			try:
				delay = int(sampleL[1:])
			except ValueError:
				print 'Invalid delay: %s' % sampleL[1:]
			else:
				print 'Delay: %d sec' % delay
				time.sleep(delay)
		elif sampleL[0] == 's':
			print 'Switch to the next BID'
			if pingThread != None:
				pingThread.nextBid()
		elif sampleL[0] == 'N':
			ars = sampleL[1:].split()
			arNums = []
			for ar in ars:
				try:
					arNum = int(ar)
				except ValueError:
					print 'Invalid AR number: %s' % ar
				else:
					arNums.append(arNum)
			arFactory.enable(arNums, plot=plot)	
		else:
			ars = sampleL.split()
			arNums = []
			for ar in ars:
				try:
					arNum = int(ar)
				except ValueError:
					print 'Invalid AR number: %s' % ar
				else:
					arNums.append(arNum)
			arFactory.enable(arNums, plot=plot)
			pingThread.calculateHH()
			

	for tcpdumpThread in tcpdumpThreads:
		tcpdumpThread.doStop()

	if pingThread != None:
		pingThread.doStop()

	if do_plot == True:
		plot.finish()

	print '--- END ---'

