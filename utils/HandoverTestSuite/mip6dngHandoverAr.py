
import socket
import os

class Ar:
	
	def __init__(self, number, address, port):
		self.number = number
		self.address = address
		self.port = port

		self.sock = None
		self.connected = False
		self.running = False
		self.runningInited = False

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.sock.connect((self.address, self.port))
		except socket.error:
			print 'Unable to connect to AR at %s:%d' % (self.address, self.port)
			self.sock.close()
			self.sock = None
		else:
			self.connected = True

	def getNumber(self):
		return self.number

	def enable(self, plot=None):
		if self.running != True or self.runningInited == False:
			print 'Enabling AR: %d' % self.number
			if self.connected == True:
				self.sock.sendall('enable\n')
				data = self.sock.recv(1024).strip()
				print '  Reply: %s' % data
				if data == 'enable':
					self.running = True
					if self.runningInited != True:
						self.runningInited = True
					if plot != None:
						plot.startConnection(self.number)
			else:
				print 'Not connected!'
		else:
			print 'AR %d enabled' % self.number
		os.system('/usr/bin/mip6d-ng-console log "TEST +++ %s enable"' % self.number)
	
	def disable(self, plot=None):
		if self.running != False or self.runningInited == False:
			print 'Disabling AR: %d' % self.number
			if self.connected == True:
				self.sock.sendall('disable\n')
				data = self.sock.recv(1024).strip()
				print '  Reply: %s' % data
				if data == 'disable':
					self.running = False
					if self.runningInited != True:
						self.runningInited = True
					if plot != None:
						plot.stopConnection(self.number)
			else:
				print 'Not connected!'
		else:
			print 'AR %d disabled' % self.number
		os.system('/usr/bin/mip6d-ng-console log "TEST --- %s disable"' % self.number)


