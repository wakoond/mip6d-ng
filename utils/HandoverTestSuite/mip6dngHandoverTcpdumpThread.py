
import threading
import os
import subprocess
import re
import datetime
import shlex
from mip6dngHandoverData import Data

class TcpdumpThread(threading.Thread):

	def __init__(self, data, ha, interface):
		self.data = data
		self.ha = ha
		self.iface = interface
		try:
			self.iface_id = int(self.iface[3:])
		except ValueError:
			self.iface_id = 0

		self.stop = False

		threading.Thread.__init__(self)

	def doStop(self):
		self.stop = True
		self.td.terminate()

	def run(self):
		tcmd = 'tcpdump -s 1000 -lni %s src or dst %s and ip6[6]=41' % (self.iface, self.ha)
		self.td = subprocess.Popen(shlex.split(tcmd), bufsize=-1, stdout=subprocess.PIPE)
		rex = re.compile(r'^([0-9]{2}):([0-9]{2}):([0-9]{2})\.([0-9]+) IP6 ([0-9a-f:]+) > [0-9a-f:]+: IP6 [0-9a-f:]+ > [0-9a-f:]+: ICMP6, echo (request|reply), seq ([0-9]+), length [0-9]+$')
		while True:
			line = self.td.stdout.readline()
			if not line:
				break
			rgroups = rex.findall(line.strip())
			if len(rgroups) < 1: continue
			if len(rgroups[0]) < 6: continue
			try:
				thour = int(rgroups[0][0])
				tmin = int(rgroups[0][1])
				tsec = int(rgroups[0][2])
				tmsec = int(rgroups[0][3])
				srca = rgroups[0][4]
				echo = rgroups[0][5]
				seq = int(rgroups[0][6])
				dtNow = datetime.datetime.today()
				dtTs = datetime.datetime(dtNow.year, dtNow.month, dtNow.day, thour, tmin, tsec)
				ts = int(dtTs.strftime('%s'))
			except ValueError: continue
			print '[%d.%d] %s echo %s seq %d src %s' % (ts, tmsec, self.iface, echo, seq, srca)
			if echo == 'request':
				self.data.add(ts, tmsec, self.iface_id, Data.OUTPUT)
			elif echo == 'reply':
				self.data.add(ts, tmsec, self.iface_id, Data.INPUT)

