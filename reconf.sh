#!/bin/sh

#--enable-environment-debug \
#--enable-icmp6-core-debug \
#--enable-icmp6-ra-debug \
#--enable-ic-debug \
#--enable-mod-environment-test \
#--enable-mh-core-debug \
#--enable-refcount-debug \
#--enable-lock-unlock-debug \
#--without-ltdl \
CONFIGURE_OPTIONS="\
--prefix=/usr --sysconfdir=/etc \
--enable-mod-flusher \
--enable-mod-logger-api \
--with-doxygen-doc-dir=${PWD}/../mip6d-ng-doc/doc \
--enable-refcount-debug \
--enable-icmp6-ra-debug \
"

if [ "x${1}" != "xnoauto" ]; then
	echo "--- autoreconf ---"
	autoreconf
fi
echo "--- configure ---"
echo "/// $CONFIGURE_OPTIONS ///"
./configure $CONFIGURE_OPTIONS CFLAGS="-Wstrict-aliasing -Wunused-variable -Wimplicit-function-declaration -Wpointer-sign" 

