#!/bin/bash

find modules -name "*.c" -o -name "*.h" > cscope.files
cscope -q -R -b -i cscope.files

function decho() {
	echo -n " * "
	echo "$1"
}

echo "/*!"
decho "\page moduledeps Dependencies of mip6d-ng modules"
decho ""

DEPS_LIST=$(cscope -dL -3 imsg_get_id | sed -ne 's/^modules\/\([a-zA-Z0-9_\.\-]\+\)\/[a-zA-Z0-9_\.\-]\+ \([0-9a-zA-Z_]\+\) .*(\([A-Z0-9_]\+\));[ \t]*$/\1#\2#\3/p' | sort)

MODULE=
for DEP in $DEPS_LIST; do
	M=$(echo "$DEP" | cut -d '#' -f 1)
	if [ "$MODULE" != "$M" ]; then
		MODULE=$M
		decho ""
		decho "The module '\b $MODULE' depends on the following modules:"
	fi
	D=$(echo "$DEP" | cut -d '#' -f 3)
	DEP_MODULE=$(cscope -dL -4 $D | sed -ne "s/^[a-zA-Z0-9_\/\.\-]\+\/\([a-zA-Z0-9_\-]\+\)\.h <unknown> [0-9]\+ #define[ \t]\+$D[ \t]\+\"[a-zA-Z0-9 _]\+\"[ \t]*$/\1/p" | head -n 1)
	decho " - $DEP_MODULE"
done

echo " */"
echo ""

echo "/*!"
decho "\page moduledepsgraph Dependency graph of mip6d-ng modules"
decho ""
decho "Legend:"
decho " - solid line: in module initialization phase"
decho " - dashed line: in the core_all_inited hook"
decho " - dotted line: in the core_all_started hook"
decho ""
decho ""
decho "\dot"
decho "digraph moduledepsgraph {"
decho "    size=\"200,200\";"
decho "    ranksep=1;"
decho "    node [shape=box, fontname=Helvetica, fontsize=10];"

MODULE=
MODULES=
for DEP in $DEPS_LIST; do
	M=$(echo "$DEP" | cut -d '#' -f 1)
	if [ "$MODULE" != "$M" ]; then
		MODULE=$M
		MODULES="${MODULES}${MODULE} "
		SEQ_NAME=$(cscope -dL -4 MIP6_MODULE_INIT | grep "^modules/$MODULE/" | sed -ne 's/^.\+MIP6_MODULE_INIT[ \t]*([ \t]*[a-zA-Z0-9"_ \-]\+[ \t]*,[ \t]*\([A-Za-z0-9_\-]\+\)[ \t]*,.\+$/\1/p')
		SEQ_VALUE=$(cscope -dL -1 $SEQ_NAME | sed -ne "s/^.\+#define[ \t]\+$SEQ_NAME[ \t]\+\([0-9]\+\)[ \t]*$/\1/p")
		if [ "x$SEQ_VALUE" = "x" ]; then
			SEQ_VALUE=$SEQ_NAME
		fi
		REF_EXISTS=$(grep -IrnH "\defgroup iapi-$MODULE" modules)
		if [ "x$REF_EXISTS" = "x" ]; then
			decho "    $( echo $MODULE | tr '-' '_') [label=\"$MODULE\nSequence numer: $SEQ_VALUE\"];"
		else
			decho "    $( echo $MODULE | tr '-' '_') [label=\"$MODULE\nSequence numer: $SEQ_VALUE\" URL=\"\ref iapi-$MODULE\"];"
		fi
	fi
done
for DEP in $DEPS_LIST; do
	D=$(echo "$DEP" | cut -d '#' -f 3)
	DEP_MODULE=$(cscope -dL -4 $D | sed -ne "s/^[a-zA-Z0-9_\/\.\-]\+\/\([a-zA-Z0-9_\-]\+\)\.h <unknown> [0-9]\+ #define[ \t]\+$D[ \t]\+\"[a-zA-Z0-9 _]\+\"[ \t]*$/\1/p" | head -n 1)
	FOUND=0
	for M in $MODULES; do
		if [ "$M" = "$DEP_MODULE" ]; then
			FOUND=1
		fi
	done
	if [ $FOUND -eq 0 ]; then
		MODULES="${MODULES}${DEP_MODULE} "
		SEQ_NAME=$(cscope -dL -4 MIP6_MODULE_INIT | grep "^modules/$DEP_MODULE/" | sed -ne 's/^.\+MIP6_MODULE_INIT[ \t]*([ \t]*[a-zA-Z0-9"_ \-]\+[ \t]*,[ \t]*\([A-Za-z0-9_\-]\+\)[ \t]*,.\+$/\1/p')
		SEQ_VALUE=$(cscope -dL -1 $SEQ_NAME | sed -ne "s/^.\+#define[ \t]\+$SEQ_NAME[ \t]\+\([0-9]\+\)[ \t]*$/\1/p")
		if [ "x$SEQ_VALUE" = "x" ]; then
			SEQ_VALUE=$SEQ_NAME
		fi
		REF_EXISTS=$(grep -IrnH "\defgroup iapi-$DEP_MODULE" modules)
		if [ "x$REF_EXISTS" = "x" ]; then
			decho "    $( echo $DEP_MODULE | tr '-' '_') [label=\"$DEP_MODULE\nSequence numer: $SEQ_VALUE\"];"
		else
			decho "    $( echo $DEP_MODULE | tr '-' '_') [label=\"$DEP_MODULE\nSequence numer: $SEQ_VALUE\" URL=\"\ref iapi-$DEP_MODULE\"];"
		fi
	fi
done

decho "    edge [style=solid];"
for DEP in $DEPS_LIST; do
	M=$(echo "$DEP" | cut -d '#' -f 1)
	F=$(echo "$DEP" | cut -d '#' -f 2)
	D=$(echo "$DEP" | cut -d '#' -f 3)
	if [ "x$(echo $F | grep '_module_init')" != "x" ]; then
		DEP_MODULE=$(cscope -dL -4 $D | sed -ne "s/^[a-zA-Z0-9_\/\.\-]\+\/\([a-zA-Z0-9_\-]\+\)\.h <unknown> [0-9]\+ #define[ \t]\+$D[ \t]\+\"[a-zA-Z0-9 _]\+\"[ \t]*$/\1/p" | head -n 1)
		decho "    $( echo $M | tr '-' '_') -> $( echo $DEP_MODULE | tr '-' '_');"
	fi
done

decho "    edge [style=dashed];"
for DEP in $DEPS_LIST; do
	M=$(echo "$DEP" | cut -d '#' -f 1)
	F=$(echo "$DEP" | cut -d '#' -f 2)
	D=$(echo "$DEP" | cut -d '#' -f 3)
	if [ "x$(echo $F | grep '_init2')" != "x" ]; then
		DEP_MODULE=$(cscope -dL -4 $D | sed -ne "s/^[a-zA-Z0-9_\/\.\-]\+\/\([a-zA-Z0-9_\-]\+\)\.h <unknown> [0-9]\+ #define[ \t]\+$D[ \t]\+\"[a-zA-Z0-9 _]\+\"[ \t]*$/\1/p" | head -n 1)
		decho "    $( echo $M | tr '-' '_') -> $( echo $DEP_MODULE | tr '-' '_');"
	fi
done

decho "    edge [style=dotted];"
for DEP in $DEPS_LIST; do
	M=$(echo "$DEP" | cut -d '#' -f 1)
	F=$(echo "$DEP" | cut -d '#' -f 2)
	D=$(echo "$DEP" | cut -d '#' -f 3)
	if [ "x$(echo $F | grep '_start')" != "x" ]; then
		DEP_MODULE=$(cscope -dL -4 $D | sed -ne "s/^[a-zA-Z0-9_\/\.\-]\+\/\([a-zA-Z0-9_\-]\+\)\.h <unknown> [0-9]\+ #define[ \t]\+$D[ \t]\+\"[a-zA-Z0-9 _]\+\"[ \t]*$/\1/p" | head -n 1)
		decho "    $( echo $M | tr '-' '_') -> $( echo $DEP_MODULE | tr '-' '_');"
	fi
done



decho "}"
decho "\enddot"

echo " */"

